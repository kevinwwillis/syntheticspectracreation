function tmp_nBdySim_call_v4(pipeline_dir, plateName, beamNum)

% This script uses solved orbital parameters from REAL data to simulate
% the system.

% clearvars


%% Constants

AU = 149597870700;
G0 = 6.67408e-11;
M_sun = 1.9885e30;
M_jup = 1.89813e27;
Yr = 365*24*3600;
s_day = 24 * 60 * 60;

%Dimensionless number which controls the dynamics, and results from the
%scaling of mass, distance and time parameters to make them dimensionless.
G = G0;%*Yr^2*M_sun/(AU^3);


%% Load solved orbital params

% load(['binary_orbitParams.mat'], 'orbitParams')
load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'solved_two2bdyMeth', 'MISC', 'NA', 3, 1, 1), 'orbitParams')



%% Solve for planet - COM separation at time = t_p

ap_m = ( (orbitParams.p1.P.final * s_day)^2 * G0 * (orbitParams.s1.m.final + orbitParams.s2.m.final + orbitParams.p1.m.final) / (4 * pi^2) )^(1 / 3);


%% Total simulation time needed [Earth years]

% masses(1).totYrs = years(days( orbitParams.p1.t.known(end) - orbitParams.p1.t.known(1) ) + days(seconds(orbitParams.P.t.known)));
masses(1).totSec = seconds(days( orbitParams.p1.t.known(end) - orbitParams.p1.t.known(1) ));



%% Solve for initial orbit angle (@ t = 0)

% Mean anomaly
Mp = 2 * pi * (orbitParams.p1.t.known - orbitParams.p1.tp.final) / orbitParams.p1.P.final;

for tn = 1:length(orbitParams.p1.tp.final)
    
    % Eccentric anomaly
    Ep(tn) = mod(newton( mod(Mp(tn), 2 * pi), orbitParams.p1.ecc.final), 2 * pi);
end

initAngle_p = 2 * atan( sqrt( (1 + orbitParams.p1.ecc.final) / (1 - orbitParams.p1.ecc.final) ) * tan( Ep(1) / 2 ) );

while initAngle_p < 0
    
    initAngle_p = initAngle_p + 2*pi;
end

% Mean anomaly
Ms = 2 * pi * (orbitParams.s1.t.known - orbitParams.s1.tp.final) / orbitParams.s1.P.final;

for tn = 1:length(orbitParams.s1.t.known)
    
    % Eccentric anomaly
    Es(tn) = mod(newton( mod(Ms(tn), 2 * pi), orbitParams.s1.ecc.final), 2 * pi);
end

initAngle_s = 2 * atan( sqrt( (1 + orbitParams.s1.ecc.final) / (1 - orbitParams.s1.ecc.final) ) * tan( Es(1) / 2 ) );

while initAngle_s < 0
    
    initAngle_s = initAngle_s + 2*pi;
end



%% Physical System Params

% Object names
name{1} = 'Star 1';
name{2} = 'Star 2';
name{3} = 'Planet 1';
% name{4} = 'Planet 2';

% Object type
objType{1} = 's';
objType{2} = 's';
objType{3} = 'p';
% objType{4} = 'p';

% Object masses [solar masses] (m)
m(1) = orbitParams.s1.m.final;
m(2) = orbitParams.s2.m.final;
m(3) = orbitParams.p1.m.final;
% m(4) = 0.000009 * 1000;

% 238.3 * M_jup / M_sun

% Object orbit configuration (which objects are gravitationally bound to each other?)
gBoundPairIndex{1} = [1, 2];
gBoundPairIndex{2} = [3];
% gBoundPairIndex{3} = [4];

% Initial star separation [AU] (a)
gBoundPair_sep_AU(1) = ( orbitParams.s1.a1.final + orbitParams.s2.a2.final );
% gBoundPair_sep_AU(2) = orbitParams.p1.ap1.final;
gBoundPair_sep_AU(2) = ap_m;
% gBoundPair_sep_AU(3) = 20;

% Initial phase (polar angle) of two-mass elliptical orbit
theta0(1) = initAngle_s;
theta0(2) = initAngle_p;
% theta0(3) = 0;

% Orbit eccentricity [] (e)
ecc(1) = orbitParams.s1.ecc.final;
ecc(2) = orbitParams.p1.ecc.final;
% ecc(3) = 0.0;

% Orientation of semi-major axis of ellipse  (w {omega}) [rad]
smo(1, :) = -[cos(orbitParams.s1.w.final + 0.25 * 2 * pi), sin(orbitParams.s1.w.final + 0.25 * 2 * pi), 0]; 
smo(2, :) = -[cos(orbitParams.p1.w.final + 0.25 * 2 * pi), sin(orbitParams.p1.w.final + 0.25 * 2 * pi), 0];
% smo(3, :) = [1,1,0];

% Rotation of ellipse clockwise about d [radians]
alpha(1) = 0;%orbitParams.s1.w.final;
alpha(2) = 0;%orbitParams.p1.w.final;
% alpha(3) = 0;

% Center of mass initial coordinates [AU]
h0 = [0,0,0];

% Center of mass velocity [AU/s]
hdot0 = [0,0,0];


% sqrt( 4 * pi^2 * gBoundPair_sep_AU(1)^3 / (G *(m(1) + m(2))) ) * 365

%% Plot Params

plot_q = 0;

showPathTrails_q = 1;

showObjNames_q = 1;

obj_RGB{1} = [1, 0, 0];
obj_RGB{2} = [0, 1, 0];
obj_RGB{3} = [0, 0, 1];
% obj_RGB{4} = [0.5, 0, 0.5];

% (NOT WORKING) Add cool looking (massless) particle rings around a certain object
addMasslessParticleRings_q(1) = 0;
addMasslessParticleRings_q(2) = 0;
addMasslessParticleRings_q(3) = 0;
% addMasslessParticleRings_q(4) = 0;

% Show velocity on plot 2, as seen from Earth
masses(1).earthVplot = 1;

masses(1).dt = 0.5 * 60 * 60;

%%

% % EXAMINE PERIOD DRIFT. (results show K is probably wrong)
% % days(years(Ps))
% orbitParams.s1.P.final
% days(years(2 * pi * sqrt( (gBoundPair_sep_AU(1))^3 / ( G * sum(m(1:2)) ) )))
% days(seconds((orbitParams.s2.m.final^3 * 2 * pi * G0) / ( ( orbitParams.s1.m.final + orbitParams.s2.m.final )^2 * (1 - orbitParams.s1.ecc.final^2)^(3/2) * orbitParams.s1.K.final^3 )))
% 
% 
% % EXAMINE PERIOD DRIFT. 
% % days(years(Pp))
% orbitParams.p1.P.final
% days(years(2 * pi * sqrt( gBoundPair_sep_AU(2)^3 / ( G * sum(m(:)) ) )))




%% Solve for the system's initial state

% Planet and sCOM

sysNum = 2;

objNum = 3;

hdot = hdot0;

h = h0;

m_tot = m(1) + m(2);

% Determine initial conditions of this system
[ ...
    ux, vx_sCOM, vx(objNum),...
    uy, vy_sCOM, vy(objNum),...
    uz, vz_sCOM, vz(objNum),...
    xc, x_sCOM, x(objNum),...
    yc, y_sCOM, y(objNum),...
    zc, z_sCOM, z(objNum),...
    theta, theta_dot, t, j, J, E, Pp ...
    ] = two_body_elliptical_orbit(  G, m_tot, m(objNum), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );


masses(objNum).name = name{objNum};
masses(objNum).mass = m(objNum);
masses(objNum).radii = 1e-10;
masses(objNum).x0 = x(objNum);
masses(objNum).y0 = y(objNum);
masses(objNum).z0 = z(objNum);
masses(objNum).vx0 = vx(objNum);
masses(objNum).vy0 = vy(objNum);
masses(objNum).vz0 = vz(objNum);
masses(objNum).marker_RGB = obj_RGB{objNum};
masses(objNum).marker_size = 5;
masses(objNum).plot_trail = showPathTrails_q;
masses(objNum).init_orbit_M = m(objNum);
masses(objNum).init_orbit_P = Pp;
masses(objNum).init_orbit_G = G;
masses(objNum).init_orbit_ecc = ecc(sysNum);
masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
masses(objNum).init_orbit_h0 = h0;
masses(objNum).init_orbit_hdot = hdot;
masses(objNum).init_orbit_d = smo(sysNum, :);
masses(objNum).init_orbit_alpha = alpha(sysNum);
masses(objNum).init_orbit_theta0 = theta0(sysNum);
masses(objNum).init_orbit_thetadot0 = theta_dot;
masses(objNum).init_orbit_J = J;
masses(objNum).init_orbit_j = j;
masses(objNum).init_orbit_E = E;




% s1 and s2

sysNum = 1;

% Set the COM velocity to the one calculated above
hdot = [vx_sCOM, vy_sCOM, vz_sCOM];

% Set the COM position to the one calculated above
h = [x_sCOM, y_sCOM, z_sCOM];

obj_ind_save = [1, 2];
obj_ind = obj_ind_save;

% The calculation script
[ ...
    ux, vx(obj_ind(1)), vx(obj_ind(2)),...
    uy, vy(obj_ind(1)), vy(obj_ind(2)),...
    uz, vz(obj_ind(1)), vz(obj_ind(2)),...
    xc, x(obj_ind(1)), x(obj_ind(2)),...
    yc, y(obj_ind(1)), y(obj_ind(2)),...
    zc, z(obj_ind(1)), z(obj_ind(2)),...
    theta, theta_dot, t, j, J, E, Ps ...
    ] = two_body_elliptical_orbit(  G, m(obj_ind_save(1)), m(obj_ind_save(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );




% Place object params into final structure
for objNum = obj_ind_save
    
    masses(objNum).name = name{objNum};
    masses(objNum).mass = m(objNum);
    masses(objNum).radii = 1e-10;
    masses(objNum).x0 = x(objNum);
    masses(objNum).y0 = y(objNum);
    masses(objNum).z0 = z(objNum);
    masses(objNum).vx0 = vx(objNum);
    masses(objNum).vy0 = vy(objNum);
    masses(objNum).vz0 = vz(objNum);
    masses(objNum).marker_RGB = obj_RGB{objNum};
    masses(objNum).marker_size = 5;
    masses(objNum).plot_trail = showPathTrails_q;
    masses(objNum).init_orbit_M = m(objNum);
    masses(objNum).init_orbit_P = Ps;
    masses(objNum).init_orbit_G = G;
    masses(objNum).init_orbit_ecc = ecc(sysNum);
    masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
    masses(objNum).init_orbit_h0 = h0;
    masses(objNum).init_orbit_hdot = hdot;
    masses(objNum).init_orbit_d = smo(sysNum, :);
    masses(objNum).init_orbit_alpha = alpha(sysNum);
    masses(objNum).init_orbit_theta0 = theta0(sysNum);
    masses(objNum).init_orbit_thetadot0 = theta_dot;
    masses(objNum).init_orbit_J = J;
    masses(objNum).init_orbit_j = j;
    masses(objNum).init_orbit_E = E;
end


% If massless rings requested, create them.
rings = [];

for objNum = 1:length(objType)
    
    if addMasslessParticleRings_q(objNum)
        
        %         if strcmp(objType{objNum}, 'p')
        rings(objNum).xc = x(objNum);
        rings(objNum).yc = y(objNum);
        rings(objNum).zc = z(objNum);
        %         else
        %             rings(objNum).xc = x(objNum);
        %             rings(objNum).yc = y(objNum);
        %             rings(objNum).zc = z(objNum);
        %         end
        rings(objNum).vxc = vx(objNum);
        rings(objNum).vyc = vy(objNum);
        rings(objNum).vzc = vz(objNum);
        rings(objNum).num_rings = 40;
        rings(objNum).arc_separation_AU = 1*pi/30;
        rings(objNum).first_ring_radius_AU = 0.10;
        rings(objNum).ring_radius_diff_AU = 0.5;
        rings(objNum).d = masses(objNum).init_orbit_d;
        rings(objNum).alpha = pi/2;
        rings(objNum).mass_at_centre = m(objNum);
        rings(objNum).marker_RGB = obj_RGB{objNum} * 0.8;
        
    end
end


% Cluster objects not supported. Left empty.
clusters = [];




% [masses(:).init_orbit_P] * 365


% plot_q = 1;


%% Run the simulation

[hist, d] = nBodySim(masses, rings, clusters, plot_q, showObjNames_q);

1;



%%

tr = 1:length(hist.t);

t = orbitParams.s1.t.known;

t = t - t(1);

th = orbitParams.s1.t.hi.known;

th = th - th(1);


qf(88,1);

subplot(10,1,1:8); hold on
plot(days(seconds(hist.t(tr))), -hist.m(1).vx(tr) / 1e3, '-k', 'linewidth', 3)
plot(th, (orbitParams.s1.RV.hi.fit + orbitParams.s1.RV_off.final) / 1e3, '--r', 'linewidth', 2)
plot(t, (orbitParams.s1.RV.known + orbitParams.s1.RV_off.final) / 1e3, 'or', 'markerfacecolor', 'r')
title('Primary Star Velocity Seen on Earth')    
grid on
ylabel('v [km/s]')
% xlabel('t [days]')
xlim([0 inf])
sslegend({'Simulation Orbit', 'Real Orbit Fit', 'Real Observed RV'})
set(gca, 'XTickLabel', []);

subplot(10,1,9:10); hold on
err = spline(days(seconds(hist.t(tr))), -hist.m(1).vx(tr), th) - orbitParams.s1.RV.hi.fit + orbitParams.s1.RV_off.final;
err = err - nanmean(err);
plot(th, err, '-k', 'linewidth', 2)
title(cat(2, 'Residual [(sim orbit [3 body]) - (real orbit fit [2 body])]  |  RMS = ', n2s(rms(err), 4), ' m/s'))    
grid on
ylabel('\Delta v [m/s]')
xlabel('t [days]')
xlim([0 inf])
% sslegend({'Simulation Orbit', 'Real Orbit Fit', 'Real Observed RV'})



qf(99,1);

subplot(10,1,1:8); hold on
rvNorm = hist.m(3).vx(tr) / max(hist.m(3).vx(tr)) * max(orbitParams.p1.RV.hi.fit + orbitParams.p1.RV_off.final);
plot(days(seconds(hist.t(tr))), rvNorm, '-k', 'linewidth', 3)
plot(th, orbitParams.p1.RV.hi.fit + orbitParams.p1.RV_off.final, '--b', 'linewidth', 2)
plot(t, orbitParams.p1.RV.known + orbitParams.p1.RV_off.final, 'ob', 'markerfacecolor', 'b')
title('Primary Star (Planetary Induced) Velocity Seen on Earth')    
grid on
ylabel('v [m/s]')
set(gca, 'XTickLabel', []);
xlim([0 inf])
sslegend({'Simulation Planet Orbit (NORMALIZED)', 'Real Orbit Planet Fit', 'Real Observed Planet RV Residual'})

subplot(10,1,9:10); hold on
err = spline(days(seconds(hist.t(tr))), rvNorm, th) - orbitParams.p1.RV.hi.fit + orbitParams.p1.RV_off.final;
err = err - nanmean(err);
plot(th, err, '-k', 'linewidth', 2)
title(cat(2, 'Residual [(sim orbit [3 body]) - (real orbit fit [2 body])]  |  RMS = ', n2s(rms(err), 4), ' m/s'))       
grid on
ylabel('\Delta v [m/s]')
xlabel('t [days]')
xlim([0 inf])
sslegend({'Simulation Planet Orbit (NORMALIZED)', 'Real Orbit Planet Fit', 'Real Observed Planet RV Residual'})

1;

% EXAMINE PERIOD DRIFT. (results show K is probably wrong)
% days(years(Ps))
% orbitParams.s1.P.final
% % days(years(2 * pi * sqrt( (gBoundPair_sep_AU(1))^3 / ( G * sum(m(1:2)) ) )))
% days(seconds((orbitParams.s2.m.final^3 * 2 * pi * G0) / ( ( orbitParams.s1.m.final + orbitParams.s2.m.final )^2 * (1 - orbitParams.s1.ecc.final^2)^(3/2) * orbitParams.s1.K.final^3 )))


% EXAMINE PERIOD DRIFT. 
% days(years(Pp))
% orbitParams.p1.P.final
% days(years(2 * pi * sqrt( gBoundPair_sep_AU(2)^3 / ( G * sum(m(:)) ) )))
1;


%% Solve again with no planet

% 
% % s1 and s2
% 
% sysNum = 1;
% 
% hdot = [0, 0, 0];
% 
% h = [0, 0, 0];
% 
% obj_ind_save = [1, 2];
% obj_ind = obj_ind_save;
% 
% masses(3) = [];
% 
% % Determine initial conditions of this system
% [ ...
%     ux, vx(obj_ind(1)), vx(obj_ind(2)),...
%     uy, vy(obj_ind(1)), vy(obj_ind(2)),...
%     uz, vz(obj_ind(1)), vz(obj_ind(2)),...
%     xc, x(obj_ind(1)), x(obj_ind(2)),...
%     yc, y(obj_ind(1)), y(obj_ind(2)),...
%     zc, z(obj_ind(1)), z(obj_ind(2)),...
%     theta, theta_dot, t, j, J, E, P ...
%     ] = two_body_elliptical_orbit(  G, m(obj_ind_save(1)), m(obj_ind_save(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
% 
% 
% 
% theta_dot = 0;
% 
% 
% % Place params into final structure
% for objNum = obj_ind_save
%     
%     masses(objNum).name = name{objNum};
%     masses(objNum).mass = m(objNum);
%     masses(objNum).radii = 1e-10;
%     masses(objNum).x0 = x(objNum);
%     masses(objNum).y0 = y(objNum);
%     masses(objNum).z0 = z(objNum);
%     masses(objNum).vx0 = vx(objNum);
%     masses(objNum).vy0 = vy(objNum);
%     masses(objNum).vz0 = vz(objNum);
%     masses(objNum).marker_RGB = obj_RGB{objNum};
%     masses(objNum).marker_size = 5;
%     masses(objNum).plot_trail = showPathTrails_q;
%     masses(objNum).init_orbit_M = m(objNum);
%     masses(objNum).init_orbit_P = P;
%     masses(objNum).init_orbit_G = G;
%     masses(objNum).init_orbit_ecc = ecc(sysNum);
%     masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
%     masses(objNum).init_orbit_h0 = h0;
%     masses(objNum).init_orbit_hdot = hdot;
%     masses(objNum).init_orbit_d = smo(sysNum, :);
%     masses(objNum).init_orbit_alpha = alpha(sysNum);
%     masses(objNum).init_orbit_theta0 = theta0(sysNum);
%     masses(objNum).init_orbit_thetadot0 = theta_dot;
%     masses(objNum).init_orbit_J = J;
%     masses(objNum).init_orbit_j = j;
%     masses(objNum).init_orbit_E = E;
% end
% 
% 
% [hist2, d2] = nBodySim(masses, rings, clusters, plot_q, showObjNames_q);






%%


p2_markerStyle = '-';

tr = 1:length(hist.t);

objNums = 1:3;

qf(2,1)

d.spt = 2;

t = days(seconds(hist.t(tr)));

subplot(d.spt, 1, 1); hold on
title('Distance from System''s Center of Mass')
grid on
ylabel('r_{CM} [AU]')
xlim([0 inf])
subplot(d.spt, 1, 1); hold on
for objNum = objNums
    pr_h(objNum) = plot(t, hist.m(objNum).r(tr) / AU, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
end
legend({d.masses(:).name}, 'location', 'northwest', 'AutoUpdate','off')

subplot(d.spt, 1, 2); hold on
if d.earthVplot
title('Velocity Seen on Earth')    
else
title('Velocity')
end
grid on
ylabel('v [km/s]')
xlabel('time [Earth days]')
xlim([0 inf])
for objNum = objNums
    
    if d.earthVplot
        pv_h(objNum) = plot(t, -hist.m(objNum).vx(tr) / 1e3, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    else
        pv_h(objNum) = plot(t, -hist.m(objNum).v(tr) / 1e3, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    end
end

% subplot(d.spt, 1, 3); hold on
% title('Acceleration')
% grid on
% ylabel('a [km/s^2]')
% xlim([0 inf])
% for objNum = objNums
%     %pa_h(objNum) = plot(t, hist.m(objNum).a(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
%     pa_h(objNum) = plot(t, hist.m(objNum).a(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
% end
% 
% subplot(d.spt, 1, 4); hold on
% title('Total Energy of System')
% grid on
% ylabel('E_{tot} [J]')
% xlabel('time [Earth days]')
% xlim([0 inf])
% pU_h = plot(t, hist.U(tr), [p2_markerStyle, 'b']);
% pK_h = plot(t, hist.K(tr), [p2_markerStyle, 'r']);
% pE_h = plot(t, hist.K(tr) + hist.U(tr), [p2_markerStyle, 'k']);





% objNum = 1;
% qf(11,1); 
% plot(hist.t(tr), hist.m(objNum).vx(tr) - hist2.m(objNum).vx(tr), '-r');
% plot(years(days(orbitParams.p1.t.hi.known - orbitParams.p1.t.hi.known(1))), orbitParams.p1.RV.hi.fit, '-k')



% qf(12,1); subplot(2,1,1); hold on
% plot(hist.t(tr), hist.m(objNum).vx(tr), '-k', hist.t(tr), hist2.m(objNum).vx(tr), '--r');
% title('Primary Star Orbital Line-of-Sight Velocity (x) With & Without Planet')
% grid on
% ylabel('RV_x [m/s]')
% xlabel('time [Earth yr]')
% sslegend({'Simulated With Planet', 'Simulated Without Planet'});
% xlim([0 inf])
% 
% subplot(2,1,2); hold on
% plot(hist.t(tr), hist.m(objNum).vx(tr) - hist2.m(objNum).vx(tr), '-k');
% title('Residual')
% grid on
% ylabel('\Delta RV_x [m/s]')
% xlabel('time [Earth yr]')
% xlim([0 inf])



% qf(13,1); 
% 
% [p1, ~] = splineMaximaMinima(spline(hist.t(tr), hist.m(objNum).vx(tr)));
% [p2, ~] = splineMaximaMinima(spline(hist.t(tr), hist2.m(objNum).vx(tr)));
% 
% 
% % pp=spline(hist.t(tr), hist.m(objNum).vx(tr));
% % p_der=fnder(pp,1);
% % y_prime=ppval(p_der,x);
% % pvs = fnzeros(p_der);
% 
% plot(p1(1:end-1), diff(p1)*365, '.k')
% plot(p1(1:end-1), diff(p2)*365, '.r')



% plotSavedSimData_orbits(d, hist)

% plotSavedSimData_orbits(d2, hist2)

% save(['E:\Research\syntheticSpectraCreation\binary_orbitParams.mat'], 'orbitParams', 'hist', 'hist2', 'd', 'd2')

save(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'simulation', 'MISC', 'NA', 3, 1, 1), 'hist', 'd')


