function plotSavedSimData_orbits(pipeline_dir, plateName, beamNum, nameAppend, recovered_q)


if recovered_q
    load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'simulation', 'recovery_MISC', 'NA', 3, 1, 1), 'hist', 'd')
else
    load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'simulation', 'MISC', 'NA', 3, 1, 1), 'hist', 'd')
end

showObjNames_q = 1;



itTime = 1;



%
AU = 149597870700;
G0 = 6.67408e-11;
M_sun = 1.9885e30;
Yr = 31556952; % s / yr

% conversions
m_au = 149597870700;
km_au = m_au * 1e-13;
s_yr = Yr;



objNums = 1:length(d.masses);

tFinal = ceil( d.totYrs / d.dt );
% tFinal = ceil( 20 / d.dt );


%% Plot 2 init

p2_markerStyle = '-';

tr = 1;

qf(2,1)

subplot(4, 1, 1); hold on
title('Distance from System''s Center of Mass')
grid on
ylabel('r_{CM} [AU]')
xlim([0 inf])
subplot(4, 1, 1); hold on
for objNum = objNums
    pr_h(objNum) = plot(hist.t(tr), hist.m(objNum).r(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
end
legend({d.masses(:).name}, 'location', 'northwest', 'AutoUpdate','off')

subplot(4, 1, 2); hold on
if d.earthVplot
    title('Velocity Seen on Earth')
else
    title('Velocity')
end
grid on
ylabel('v [m/s]')
xlim([0 inf])
for objNum = objNums
    
    if d.earthVplot
        pv_h(objNum) = plot(hist.t(tr), hist.m(objNum).vx(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    else
        pv_h(objNum) = plot(hist.t(tr), hist.m(objNum).v(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    end
end

subplot(4, 1, 3); hold on
title('Acceleration')
grid on
ylabel('a [km/s^2]')
xlim([0 inf])
for objNum = objNums
    %pa_h(objNum) = plot(hist.t(tr), hist.m(objNum).a(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    pa_h(objNum) = plot(hist.t(tr), 0, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
end

subplot(4, 1, 4); hold on
title('Total Energy of System')
grid on
ylabel('E_{tot} [J]')
xlabel('time [Earth yr]')
xlim([0 inf])
pU_h = plot(hist.t(tr), hist.U(tr), [p2_markerStyle, 'b']);
pK_h = plot(hist.t(tr), hist.K(tr), [p2_markerStyle, 'r']);
pE_h = plot(hist.t(tr), hist.K(tr) + hist.U(tr), [p2_markerStyle, 'k']);












%% Set up sphere


d.colorWheel = [{'r', 'b', 'g', 'c', 'k'}];

if d.plotSpheres_q
    
    N = 10;
    thetavec = linspace(0,pi,N);
    phivec = linspace(0,2*pi,2*N);
    [th, ph] = meshgrid(thetavec,phivec);
    
    for objNum = objNums
        
        d.masses(objNum).R_buffedRadius = d.masses(objNum).mass^(0.8) * 0.00465 * 10; % Main-sequence star mass-radius relation [AU]   ( M_sol ^ 0.8 = R_sol )
        
        R = d.masses(objNum).R_buffedRadius;
        
        d.masses(objNum).sphereX = R .* sin(th) .* cos(ph);
        d.masses(objNum).sphereY = R .* sin(th) .* sin(ph);
        d.masses(objNum).sphereZ = R .* cos(th);
        
    end
end


%%




qf(1,1);
hold on;
xlabel('x [AU]')
ylabel('y [AU]')
zlabel('z [AU]')
view(30,30)
grid on;




%% Plot object names
if showObjNames_q
    
    figure(1); hold on
    
    for objNum = objNums
        
        if d.plotSpheres_q
            R = d.masses(objNum).R_buffedRadius;
        else
            d.masses(objNum).R_buffedRadius = d.masses(objNum).mass^(0.8) * 0.00465 * 10; % Main-sequence star mass-radius relation [AU]   ( M_sol ^ 0.8 = R_sol )
            
            R = d.masses(objNum).R_buffedRadius;
        end
        
        objName_handle{objNum} = text(d.x(objNum) + R, d.y(objNum) + R, d.z(objNum) + R, d.masses(objNum).name);
    end
end


%Plot initial positions of masses

%Masses
if ~isempty(d.masses)
    
    for n=1:length(d.masses)
        
        r = d.masses(n).marker_RGB(1);
        g = d.masses(n).marker_RGB(2);
        b = d.masses(n).marker_RGB(3);
        
        if d.plotSpheres_q
            d.p(n) = surf(d.masses(n).sphereX + d.x(n), d.masses(n).sphereY + d.y(n), d.masses(n).sphereZ + d.z(n), 'EdgeColor', [0.2, 0.2, 0.2], 'FaceAlpha', 0.5, 'facecolor', [r,g,b]);
            
            d.tail(n) = plot3(d.x(n), d.y(n), d.z(n), '.', 'markerfacecolor', [r,g,b], 'markeredgecolor', [r,g,b], 'markersize', d.tailMarkerSize);
        else
            d.p(n) = plot3(d.x(n),d.y(n),d.z(n),'ro','markerfacecolor',[r,g,b],'markeredgecolor',...
                [r,g,b],'markersize',d.masses(n).marker_size);
            
            d.tail(n) = plot3(d.x(n), d.y(n), d.z(n), '.', 'markerfacecolor', [r,g,b], 'markeredgecolor', [r,g,b], 'markersize', d.tailMarkerSize);
        end
    end
end

%Rings
if ~isempty(d.rings)
    n0 = length(d.masses);
    end_index = n0;
    for n=1:length(d.rings)
        start_index = end_index+1;
        end_index = start_index + d.rings(n).num_masses - 1;
        if isempty( d.rings(n).marker_RGB )
            [r,g,b] = x_to_color( mean( sqrt(d.vx(start_index:end_index).^2 +...
                d.vy(start_index:end_index).^2 + d.vz(start_index:end_index).^2 )),speed_colormap,0,d.max_speed);
        else
            r = d.rings(n).marker_RGB(1);
            g = d.rings(n).marker_RGB(2);
            b = d.rings(n).marker_RGB(3);
        end
        d.p(n0+n) = plot3(d.x(start_index:end_index),d.y(start_index:end_index),d.z(start_index:end_index),'r.','markersize',d.ringMarkerSize,...
            'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
    end
end

%Clusters
if ~isempty(d.clusters)
    n0 = n0 + length(d.rings);
    for n=1:length(d.clusters)
        start_index = end_index+1;
        end_index = start_index + d.clusters(n).num_masses - 1;
        if isempty( d.clusters(n).marker_RGB )
            [r,g,b] = x_to_color( mean( sqrt(d.vx(start_index:end_index).^2 +...
                d.vy(start_index:end_index).^2 + d.vz(start_index:end_index).^2 )),speed_colormap,0,d.max_speed);
        else
            r = d.clusters(n).marker_RGB(1);
            g = d.clusters(n).marker_RGB(2);
            b = d.clusters(n).marker_RGB(3);
        end
        d.p(n0+n) = plot3(d.x(start_index:end_index),d.y(start_index:end_index),d.z(start_index:end_index),'r.','markersize',d.msize,...
            'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
    end
end

%Set title
d.title_str = 'TITLE';
d.title = title(d.title_str);



lim0 = 20;
% axis([-lim0 lim0 -lim0 lim0 -lim0 lim0])
xlim([-lim0 lim0])
ylim([-lim0 lim0])
zlim([-lim0 lim0])
axis square
axis vis3d

if isempty(d.rings)
    set(gca, 'XLimMode', 'auto')
    set(gca, 'YLimMode', 'auto')
    set(gca, 'ZLimMode', 'auto')
else
    
end















for itTime = 2:500:tFinal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    tr = 1:itTime;
    
    
    figure(1); hold on
    
    % Plot object names
    if showObjNames_q
        
        for objNum = objNums
            
            R = d.masses(objNum).R_buffedRadius;
            
            set(objName_handle{objNum}, 'Position', [d.x(objNum) + R, d.y(objNum) + R, d.z(objNum) + R])
        end
    end
    
    %Update positions of masses
    
    %Masses
    if ~isempty(d.masses)
        
        for objNum = objNums
            
            r = d.masses(objNum).marker_RGB(1);
            g = d.masses(objNum).marker_RGB(2);
            b = d.masses(objNum).marker_RGB(3);
            
            %Update position
            if d.plotSpheres_q
                
                set( d.p(objNum), 'Xdata', d.masses(objNum).sphereX + hist.m(objNum).x(itTime) / m_au, 'Ydata', d.masses(objNum).sphereY + hist.m(objNum).y(itTime) / m_au, 'Zdata', d.masses(objNum).sphereZ + hist.m(objNum).z(itTime) / m_au);
            else
                set( d.p(objNum), 'Xdata', hist.m(objNum).x(itTime) / m_au, 'Ydata', hist.m(objNum).y(itTime) / m_au, 'Zdata', hist.m(objNum).z(itTime) / m_au,'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
            end
            
            %Add to trail
            if d.masses(objNum).plot_trail == 1
                
                set( d.tail(objNum), 'Xdata', hist.m(objNum).x(tr) / m_au, 'Ydata', hist.m(objNum).y(tr) / m_au, 'Zdata', hist.m(objNum).z(tr) / m_au, 'markerfacecolor', [r,g,b],'markeredgecolor', [r,g,b] );
            end
        end
    end
    
    
%     %Rings
%     if ~isempty(d.rings)
%         
%         n0 = length(d.masses);
%         end_index = n0;
%         
%         for objNum=1:length(d.rings)
%             
%             start_index = end_index+1;
%             end_index = start_index + d.rings(objNum).num_masses - 1;
%             
%             r = d.rings(objNum).marker_RGB(1);
%             g = d.rings(objNum).marker_RGB(2);
%             b = d.rings(objNum).marker_RGB(3);
%             
%             set( d.p(n0+objNum), 'Xdata', d.x(start_index:end_index), 'Ydata', d.y(start_index:end_index), 'Zdata',...
%                 d.z(start_index:end_index),...
%                 'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
%         end
%     end
    
%     %Clusters
%     if ~isempty(d.clusters)
%         
%         n0 = n0 + length(d.rings);
%         
%         for objNum=1:length(d.clusters)
%             
%             start_index = end_index+1;
%             end_index = start_index + d.clusters(objNum).num_masses - 1;
%             
%             r = d.clusters(objNum).marker_RGB(1);
%             g = d.clusters(objNum).marker_RGB(2);
%             b = d.clusters(objNum).marker_RGB(3);
%             
%             set( d.p(n0+objNum), 'Xdata', d.x(start_index:end_index), 'Ydata', d.y(start_index:end_index), 'Zdata',...
%                 d.z(start_index:end_index),...
%                 'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
%         end
%     end
    
    %Flush pending graphics requests
    drawnow
    
    
    
    % R, V, A, E_tot
    for objNum = objNums
        
        set( pr_h(objNum), 'Xdata', hist.t(tr), 'Ydata', hist.m(objNum).r(tr) )
        
        if d.earthVplot
            set( pv_h(objNum), 'Xdata', hist.t(tr), 'Ydata', hist.m(objNum).vx(tr) )
        else
            set( pv_h(objNum), 'Xdata', hist.t(tr), 'Ydata', hist.m(objNum).v(tr) )
        end
        
        set( pa_h(objNum), 'Xdata', hist.t(tr), 'Ydata', hist.m(objNum).a(tr) )
    end
    
    set( pU_h, 'Xdata', hist.t(tr), 'Ydata', hist.U(tr) )
    set( pK_h, 'Xdata', hist.t(tr), 'Ydata', hist.K(tr) )
    set( pE_h, 'Xdata', hist.t(tr), 'Ydata', hist.E(tr) )
    
    
    
    pause(0.1)
    
end
