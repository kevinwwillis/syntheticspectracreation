clearvars

% Notes:
% 1) Energy var (E) needs to be fixed. It is not used in the simulation,
% but may be used later. Can use to measure/correct energy drift
% 
% 

%% Constants

AU = 149597870700;
G0 = 6.67408e-11;
M_sun = 1.9885e30;
M_jup = 1.89813e27;
Yr = 365*24*3600;


%Dimensionless number which controls the dynamics, and results from the
%scaling of mass, distance and time parameters to make them dimensionless.
G = G0;%*Yr^2*M_sun/(AU^3);


%% Physical System Params

% Total simulation time
totSimTime_yrs = 2;

% Incrimental time steps in simulation (lower = better approximations)
simTimeStep_s = seconds( hours( 12 ) );

% Object names
name{1} = 'Star 1';
name{2} = 'Star 2';
name{3} = 'Planet 1';
% name{4} = 'Planet 2';

% Object type
objType{1} = 's';
objType{2} = 's';
objType{3} = 'p';
% objType{4} = 'p';

% Object masses [solar masses] (m)
m(1) = 1 * M_sun;
m(2) = 1 * M_sun;
m(3) = 1e-8 * M_sun;
% m(4) = 0.000009 * 1000 * M_sun;

% Object orbit configuration (which objects are gravitationally bound to each other?)
gBoundPairIndex{1} = [1, 2];
gBoundPairIndex{2} = [3];
% gBoundPairIndex{3} = [4];

% Initial star separation [AU] (a)
gBoundPair_sep_AU(1) = 1 * AU;
gBoundPair_sep_AU(2) = 3 * AU;
% gBoundPair_sep_AU(3) = 20 * AU;

% Initial phase (polar angle) of two-mass elliptical orbit
theta0(1) = 0;
theta0(2) = 0;
% theta0(3) = 0;

% Orbit eccentricity [] (e)
ecc(1) = 0.0015;
ecc(2) = 0.02;
% ecc(3) = 0.0;

% Orientation of semi-major axis of ellipse
smo(1, :) = [1,0,0]; % <-------------- Needs to be converted to an angle input
smo(2, :) = [1,0,0];
% smo(3, :) = [1,0,0];

% Rotation of ellipse clockwise about d [radians] (w {omega})
alpha(1) = 0;
alpha(2) = 0;
% alpha(3) = 0;

% Center of mass initial coordinates [AU]
h0 = [0,0,0];

% Center of mass velocity [AU/s]
hdot0 = [0,0,0];


%% Plot Params

plot_q = 1;

showPathTrails_q = 1;

showObjNames_q = 1;

obj_RGB{1} = [1, 0, 0];
obj_RGB{2} = [0, 1, 0];
obj_RGB{3} = [0, 0, 1];
% obj_RGB{4} = [0.5, 0, 0.5];

% (NOT WORKING) Add cool looking (massless) particle rings around a certain object
addMasslessParticleRings_q(1) = 0;
addMasslessParticleRings_q(2) = 0;
addMasslessParticleRings_q(3) = 0;
% addMasslessParticleRings_q(4) = 0;


% Show velocity on plot 2, as seen from Earth
masses(1).earthVplot = 1;




%%


%% Solve for the system's initial state

% Planet and sCOM

sysNum = 2;

objNum = 3;

hdot = hdot0;

h = h0;

m_tot = m(1) + m(2);

% Determine initial conditions of this system
[ ...
    ux, vx_sCOM, vx(objNum),...
    uy, vy_sCOM, vy(objNum),...
    uz, vz_sCOM, vz(objNum),...
    xc, x_sCOM, x(objNum),...
    yc, y_sCOM, y(objNum),...
    zc, z_sCOM, z(objNum),...
    theta, theta_dot, t, j, J, E, Pp ...
    ] = two_body_elliptical_orbit(  G, m_tot, m(objNum), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );


masses(objNum).name = name{objNum};
masses(objNum).mass = m(objNum);
masses(objNum).radii = 1e-10;
masses(objNum).x0 = x(objNum);
masses(objNum).y0 = y(objNum);
masses(objNum).z0 = z(objNum);
masses(objNum).vx0 = vx(objNum);
masses(objNum).vy0 = vy(objNum);
masses(objNum).vz0 = vz(objNum);
masses(objNum).marker_RGB = obj_RGB{objNum};
masses(objNum).marker_size = 5;
masses(objNum).plot_trail = showPathTrails_q;
masses(objNum).init_orbit_M = m(objNum);
masses(objNum).init_orbit_P = Pp;
masses(objNum).init_orbit_G = G;
masses(objNum).init_orbit_ecc = ecc(sysNum);
masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
masses(objNum).init_orbit_h0 = h0;
masses(objNum).init_orbit_hdot = hdot;
masses(objNum).init_orbit_d = smo(sysNum, :);
masses(objNum).init_orbit_alpha = alpha(sysNum);
masses(objNum).init_orbit_theta0 = theta0(sysNum);
masses(objNum).init_orbit_thetadot0 = theta_dot;
masses(objNum).init_orbit_J = J;
masses(objNum).init_orbit_j = j;
masses(objNum).init_orbit_E = E;




% s1 and s2

sysNum = 1;

% Set the COM velocity to the one calculated above
hdot = [vx_sCOM, vy_sCOM, vz_sCOM];

% Set the COM position to the one calculated above
h = [x_sCOM, y_sCOM, z_sCOM];

obj_ind_save = [1, 2];
obj_ind = obj_ind_save;

% The calculation script
[ ...
    ux, vx(obj_ind(1)), vx(obj_ind(2)),...
    uy, vy(obj_ind(1)), vy(obj_ind(2)),...
    uz, vz(obj_ind(1)), vz(obj_ind(2)),...
    xc, x(obj_ind(1)), x(obj_ind(2)),...
    yc, y(obj_ind(1)), y(obj_ind(2)),...
    zc, z(obj_ind(1)), z(obj_ind(2)),...
    theta, theta_dot, t, j, J, E, Ps ...
    ] = two_body_elliptical_orbit(  G, m(obj_ind_save(1)), m(obj_ind_save(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );




% Place object params into final structure
for objNum = obj_ind_save
    
    masses(objNum).name = name{objNum};
    masses(objNum).mass = m(objNum);
    masses(objNum).radii = 1e-10;
    masses(objNum).x0 = x(objNum);
    masses(objNum).y0 = y(objNum);
    masses(objNum).z0 = z(objNum);
    masses(objNum).vx0 = vx(objNum);
    masses(objNum).vy0 = vy(objNum);
    masses(objNum).vz0 = vz(objNum);
    masses(objNum).marker_RGB = obj_RGB{objNum};
    masses(objNum).marker_size = 5;
    masses(objNum).plot_trail = showPathTrails_q;
    masses(objNum).init_orbit_M = m(objNum);
    masses(objNum).init_orbit_P = Ps;
    masses(objNum).init_orbit_G = G;
    masses(objNum).init_orbit_ecc = ecc(sysNum);
    masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
    masses(objNum).init_orbit_h0 = h0;
    masses(objNum).init_orbit_hdot = hdot;
    masses(objNum).init_orbit_d = smo(sysNum, :);
    masses(objNum).init_orbit_alpha = alpha(sysNum);
    masses(objNum).init_orbit_theta0 = theta0(sysNum);
    masses(objNum).init_orbit_thetadot0 = theta_dot;
    masses(objNum).init_orbit_J = J;
    masses(objNum).init_orbit_j = j;
    masses(objNum).init_orbit_E = E;
end


% If massless rings requested, create them.
rings = [];

for objNum = 1:length(objType)
    
    if addMasslessParticleRings_q(objNum)
        
        %         if strcmp(objType{objNum}, 'p')
        rings(objNum).xc = x(objNum);
        rings(objNum).yc = y(objNum);
        rings(objNum).zc = z(objNum);
        %         else
        %             rings(objNum).xc = x(objNum);
        %             rings(objNum).yc = y(objNum);
        %             rings(objNum).zc = z(objNum);
        %         end
        rings(objNum).vxc = vx(objNum);
        rings(objNum).vyc = vy(objNum);
        rings(objNum).vzc = vz(objNum);
        rings(objNum).num_rings = 40;
        rings(objNum).arc_separation_AU = 1*pi/30;
        rings(objNum).first_ring_radius_AU = 0.10;
        rings(objNum).ring_radius_diff_AU = 0.5;
        rings(objNum).d = masses(objNum).init_orbit_d;
        rings(objNum).alpha = pi/2;
        rings(objNum).mass_at_centre = m(objNum);
        rings(objNum).marker_RGB = obj_RGB{objNum} * 0.8;
        
    end
end


% Cluster objects not supported. Left empty.
clusters = [];



masses(1).totSec = seconds( years( totSimTime_yrs ) );

masses(1).dt = simTimeStep_s;



nBodySim(masses, rings, clusters, plot_q, showObjNames_q)

