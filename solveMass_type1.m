function m2_kg = solveMass_type1(m1_kg, P1_s, K1_mps, ecc1)

G0 = 6.67408e-11;
M_sun = 1.9885e30;
s_day = 24 * 60 * 60;



% mlo = 0;
% mhi = 10;
%
% bv = inf;
%
% while bv > 1e12
%
%
%     mtrys = linspace(mlo, mhi, 10);
%
%     mtrys2 = mtrys * M_sun;
%
%     %mtrys = mlo :(mhi-mlo)/10: mhi;
%
%     vals0 = nan(1, 10);
%
%    for mtryi = 1:length(mtrys)
%
%        m2t_kg = mtrys2(mtryi);
%
%         vals0(1, mtryi) = m2t_kg^3 / (m1_kg + m2t_kg)^2 - P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2);
%
%    end
%
%    [bv, mi] = min(abs(vals0));
%
%    if mi == 1
%        mlo = mtrys(mi);
%    else
%        mlo = mtrys(mi - 1);
%    end
%
%    if mi == 10
%        mhi = mtrys(mi);
%    else
%        mhi = mtrys(mi + 1);
%    end
%
% %    qf(1,1);
% %    plot(mtrys, abs(vals0), 'o-k')
% end
%
% m2_kg = mtrys2(mi);




% m2_kg = 0 : 1e25: M_sun;
%
% [~, mi] = min(abs(m2_kg.^3 ./ (m1_kg + m2_kg).^2 - P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2)));
%
% x0 = m2_kg(mi)
%
% options = optimset;
% % options = optimset(options,'Display', 'iter');
%
% options = optimset(options,'MaxFunEvals', 1000);
% options = optimset(options,'MaxIter',     1000);
% % options = optimset(options,'TolFun', 1e-10);
% % options = optimset(options,'TolX',   1e-10);
%
%
%
% m2_kg = fmincon(@(m2t_kg) abs(m2t_kg^3 / (m1_kg + m2t_kg)^2 - P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2)), x0, [], [], [], [], 0, 10 * M_sun, [], options);
%
% abs(m2_kg^3 / (m1_kg + m2_kg)^2 - P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2))


% syms m2t_kg
%
% m2_kg = solve(abs(m2t_kg.^3 ./ (m1_kg + m2t_kg).^2 == P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2)));


for itt = 1:3%[1, 5, 10, 15, 20, 25, 30]
    
    if itt == 1
        m2_kg_testVals = 0 : 1e25: M_sun;
    else
        m2_kg_testVals = m2_kg_testVals(mi(1) - 1) : 1e25/(10^itt): m2_kg_testVals(mi(1) + 1);
    end
    
    %     size(m2_kg)
    
    lsqvs = abs(m2_kg_testVals.^3 ./ (m1_kg + m2_kg_testVals).^2 - P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2));
    
    [~, mi] = min(lsqvs);
    
    %     qf(90,1); plot(m2_kg - m2_kg(1), lsqvs, '.-k')
    
    %     qf(9999,1); plot(m2_kg_testVals, lsqvs, 'o-k', m2_kg_testVals(mi), lsqvs(mi), '+m')
    
    if any(diff(m2_kg_testVals) == 0)
        
        m2_kg = m2_kg_testVals(mi);
        break
    end
end

% m2_kg = m2_kg_testVals(mi);

1;

xx = mi + (-2:2);

yfitp = polyfit(xx, lsqvs(xx), 2);

m2_kg = spline(xx, m2_kg_testVals(xx), -yfitp(2) / (2 * yfitp(1)));

% m2_kg - m2

% yfit = polyval(yfitp, xx);

% qf(9999,1); plot(m2_kg_testVals, lsqvs, 'o-k', m2_kg_testVals(mi), lsqvs(mi), '+m', m2_kg_testVals(xx), yfit, '-r')



% options = optimset;
% % options = optimset(options,'Display', 'iter');
% 
% options = optimset(options,'MaxFunEvals', 1000);
% options = optimset(options,'MaxIter',     1000);
% % options = optimset(options,'TolFun', 1e-10);
% % options = optimset(options,'TolX',   1e-10);
% 
% 
% 
% m2_kg_add = fmincon(@(mAdd) sf(mAdd), 0, [], [], [], [], -M_sun, M_sun, [], options);
% 
% m2_kg = m2_kg_add + m2_kg;
% 
% xxx = linspace(-1e18, 1e18, 100);
% qf(21321,1); plot(xxx, sf(xxx), '-k')
% 
% mAdd = 0;
% 
% a_m = ( (P1_s).^2 * G0 .* (m1_kg + m2_kg + mAdd) ./ (4 * pi^2) ).^(1 / 3);
% 
% P = 2 * pi .* sqrt( a_m.^3 ./ ( G0 * (m1_kg + m2_kg + mAdd) ) );
% 
% lsqv = abs(P - P1_s);
% 
% days(seconds(lsqv)) * 365 * 1.5
% 
% 
%     function lsqv = sf(mAdd)
%         
%         a_m = ( (P1_s).^2 * G0 .* (m1_kg + m2_kg + mAdd) ./ (4 * pi^2) ).^(1 / 3);
%         
%         P = 2 * pi .* sqrt( a_m.^3 ./ ( G0 * (m1_kg + m2_kg + mAdd) ) );
%         
%         lsqv = (P - P1_s).^2;
%         
%     end
% 
% 
% end






