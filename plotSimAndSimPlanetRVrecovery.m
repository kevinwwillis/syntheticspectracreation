function plotSimAndSimPlanetRVrecovery(pipeline_dir, plateName, beamNum, nameAppend)


load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'simulation', 'recovery_MISC', 'NA', 3, 1, 1), 'hist', 'd')

hist_r = hist;
d_r = d;

load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'simulation', 'MISC', 'NA', 3, 1, 1), 'hist', 'd')



obsNums = 1:length(orbitParams.s1.t.known);

t = orbitParams.s1.t.known;
t1 = t(1);
t = t - t(1);
th = orbitParams_recovery.s1.t.hi.known;
th = th - th(1);

r1o = (max(orbitParams.s1.RV.hi.fit) + min(orbitParams.s1.RV.hi.fit)) / 2;
r2o = (max(orbitParams_recovery.s1.RV.hi.fit) + min(orbitParams_recovery.s1.RV.hi.fit)) / 2;




t_norm = years(days(orbitParams.p1.t.known - orbitParams.p1.t.known(1)));

t_ind = nan(1, length(t_norm));

for ti = 1:length(t_norm)

    t_ind(1, ti) = indClosestVal(hist.t, t_norm(ti));
    
end






qf(44,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Measured RV''s')
xlabel('Observation #')
ylabel('RV [m/s]')
objNum = 1;
plot(hist.t(tr), hist_r.m(objNum).vx(tr), '-k');
plot(hist.t(tr), hist.m(objNum).vx(tr), '-r');
grid on
legend({'2D RVs of Real Spectra', '2D RVs of Simulated Spectra'},'location', 'best')



qf(444,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Planet Orbits')
xlabel('Observation #')
ylabel('RV [m/s]')
objNum = 3;
plot(hist.t(tr), hist_r.m(objNum).vx(tr), '-k');
plot(hist.t(tr), hist.m(objNum).vx(tr), '-r');

plot(hist.t(t_ind), hist_r.m(objNum).vx(t_ind), 'ok');
plot(hist.t(t_ind), hist.m(objNum).vx(t_ind), '^r');
grid on
legend({'Simulated Planet Orbit', 'Recovered Planet Orbit'}, 'location', 'best')




% qf(444,1); 
% % subplot(4,1,1); hold on
% title('Simulation Recovery: Planet Orbits')
% xlabel('Observation #')
% ylabel('RV [m/s]')
% objNum = 3;
% plot(hist.t(tr), hist_r.m(objNum).vx(tr), '-k');
% plot(hist.t(tr), hist.m(objNum).vx(tr), '-r');
% 
% plot(hist.t(t_ind), hist_r.m(objNum).vx(t_ind), 'ok');
% plot(hist.t(t_ind), hist.m(objNum).vx(t_ind), '^r');
% grid on
% legend({'Simulated Planet Orbit', 'Recovered Planet Orbit'}, 'location', 'best')







qf(55,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Measured RV''s')
xlabel(cat(2, 'JD - ', n2s(round(t1))))
ylabel('RV [m/s]')
plot(th, orbitParams.s1.RV.hi.fit - r1o, '-k', t, orbitParams.s1.RV.known - r1o, 'ok')
plot(th, orbitParams_recovery.s1.RV.hi.fit - r2o, '-r', t, orbitParams_recovery.s1.RV.known - r2o, '^r')
grid on
legend({'2D RVs of Real Spectra', 'Orbit Fit of Real Spectra', '2D RVs of Simulated Spectra', 'Orbit Fit of Simulated Spectra'},'location', 'best')


qf(66,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Planet RV''s and Orbit Fit')
xlabel(cat(2, 'JD - ', n2s(round(t1))))
ylabel('RV [m/s]')
plot(th, orbitParams.(objectStructName).RV.hi.fit, '-k', t, orbitParams.p1.RV.known, 'ok')
plot(th, orbitParams_recovery.(objectStructName).RV.hi.fit, '-r', t, orbitParams_recovery.p1.RV.known, '^r')
grid on
legend({'2D RVs of Real Spectra', 'Orbit Fit of Real Spectra', '2D RVs of Simulated Spectra', 'Orbit Fit of Simulated Spectra'},'location', 'best')






% p2_markerStyle = '-';
% 
% tr = 1:length(hist_r.t);
% 
% objNums = 1:3;
% 
% qf(2,1)
% 
% d_r.spt = 4;
% 
% subplot(d_r.spt, 1, 1); hold on
% title('Distance from System''s Center of Mass')
% grid on
% ylabel('r_{CM} [AU]')
% xlim([0 inf])
% subplot(d_r.spt, 1, 1); hold on
% for objNum = objNums
%     pr_h(objNum) = plot(hist_r.t(tr), hist_r.m(objNum).r(tr), p2_markerStyle, 'markeredgecolor', d_r.masses(objNum).marker_RGB, 'color', d_r.masses(objNum).marker_RGB);
% end
% legend({d_r.masses(:).name}, 'location', 'northwest', 'AutoUpdate','off')
% 
% subplot(d_r.spt, 1, 2); hold on
% if d_r.earthVplot
% title('Velocity Seen on Earth')    
% else
% title('Velocity')
% end
% grid on
% ylabel('v [m/s]')
% xlim([0 inf])
% for objNum = objNums
%     
%     if d_r.earthVplot
%         pv_h(objNum) = plot(hist_r.t(tr), hist_r.m(objNum).vx(tr), p2_markerStyle, 'markeredgecolor', d_r.masses(objNum).marker_RGB, 'color', d_r.masses(objNum).marker_RGB);
%     else
%         pv_h(objNum) = plot(hist_r.t(tr), hist_r.m(objNum).v(tr), p2_markerStyle, 'markeredgecolor', d_r.masses(objNum).marker_RGB, 'color', d_r.masses(objNum).marker_RGB);
%     end
% end
% 
% subplot(d_r.spt, 1, 3); hold on
% title('Acceleration')
% grid on
% ylabel('a [km/s^2]')
% xlim([0 inf])
% for objNum = objNums
%     %pa_h(objNum) = plot(hist_r.t(tr), hist_r.m(objNum).a(tr), p2_markerStyle, 'markeredgecolor', d_r.masses(objNum).marker_RGB, 'color', d_r.masses(objNum).marker_RGB);
%     pa_h(objNum) = plot(hist_r.t(tr), hist_r.m(objNum).a(tr), p2_markerStyle, 'markeredgecolor', d_r.masses(objNum).marker_RGB, 'color', d_r.masses(objNum).marker_RGB);
% end
% 
% subplot(d_r.spt, 1, 4); hold on
% title('Total Energy of System')
% grid on
% ylabel('E_{tot} [J]')
% xlabel('time [Earth yr]')
% xlim([0 inf])
% pU_h = plot(hist_r.t(tr), hist_r.U(tr), [p2_markerStyle, 'b']);
% pK_h = plot(hist_r.t(tr), hist_r.K(tr), [p2_markerStyle, 'r']);
% pE_h = plot(hist_r.t(tr), hist_r.K(tr) + hist_r.U(tr), [p2_markerStyle, 'k']);






















% 
% qf(44,1); 
% % subplot(4,1,1); hold on
% title('Simulation Recovery: Measured RV''s')
% xlabel('Observation #')
% ylabel('RV [m/s]')
% plot(obsNums, orbitParams.s1.RV.known - r1o, '-ok')
% plot(obsNums, orbitParams_recovery.s1.RV.known - r2o, '-^r')
% grid on
% legend({'2D RVs of Real Spectra', '2D RVs of Simulated Spectra'},'location', 'best')
% 
% 
% qf(55,1); 
% % subplot(4,1,1); hold on
% title('Simulation Recovery: Measured RV''s')
% xlabel(cat(2, 'JD - ', n2s(round(t1))))
% ylabel('RV [m/s]')
% plot(th, orbitParams.s1.RV.hi.fit - r1o, '-k', t, orbitParams.s1.RV.known - r1o, 'ok')
% plot(th, orbitParams_recovery.s1.RV.hi.fit - r2o, '-r', t, orbitParams_recovery.s1.RV.known - r2o, '^r')
% grid on
% legend({'2D RVs of Real Spectra', 'Orbit Fit of Real Spectra', '2D RVs of Simulated Spectra', 'Orbit Fit of Simulated Spectra'},'location', 'best')
% 
% 
% qf(66,1); 
% % subplot(4,1,1); hold on
% title('Simulation Recovery: Planet RV''s and Orbit Fit')
% xlabel(cat(2, 'JD - ', n2s(round(t1))))
% ylabel('RV [m/s]')
% plot(th, orbitParams.(objectStructName).RV.hi.fit, '-k', t, orbitParams.p1.RV.known, 'ok')
% plot(th, orbitParams_recovery.(objectStructName).RV.hi.fit, '-r', t, orbitParams_recovery.p1.RV.known, '^r')
% grid on
% legend({'2D RVs of Real Spectra', 'Orbit Fit of Real Spectra', '2D RVs of Simulated Spectra', 'Orbit Fit of Simulated Spectra'},'location', 'best')
% 



% 
% qf(44,1); 
% subplot(4,1,1); hold on
% title('Simulation Recovery: Binary Orbit Fit')
% xlabel('JD')
% ylabel('RV [m/s]')
% plot(orbitParams.s1.t.hi.known, orbitParams.s1.RV.hi.fit, '-k', orbitParams.s1.t.known, orbitParams.s1.RV.fit, 'ok', orbitParams.s1.t.known, orbitParams.s1.RV.known, '+k')
% plot(orbitParams_recovery.s1.t.hi.known, orbitParams_recovery.s1.RV.hi.fit, '-r', orbitParams_recovery.s1.t.known, orbitParams_recovery.s1.RV.fit, 'or', orbitParams_recovery.s1.t.known, orbitParams_recovery.s1.RV.known, '+r')
% grid on
% legend({'Binary Orbit', 'Binary RV Fit', 'Binary Actual RV'},'location', 'best')
% 
% subplot(4,1,2); hold on
% title('Binary Orbit Fit Error')
% xlabel('JD')
% ylabel('RV Error [m/s]')
% plot(orbitParams.(objectStructName).t.known, orbitParams.s1.RV.known - orbitParams.s1.RV.fit, 'ok')
% grid on
% 
% subplot(4,1,3); hold on
% title('Simulation Recovery: Planet Orbit Fit')
% xlabel('JD')
% ylabel('RV [m/s]')
% plot(orbitParams.(objectStructName).t.hi.known, orbitParams.(objectStructName).RV.hi.fit, '-k', orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.fit, 'og', orbitParams.p1.t.known, orbitParams.p1.RV.known, '+k')
% grid on
% legend({'Planet Orbit', 'Planet RV Fit', 'Planet RV'},'location', 'best')
% 
% subplot(4,1,4); hold on
% title('Planet Orbit Fit Error')
% xlabel('JD')
% ylabel('RV Error [m/s]')
% plot(orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit, 'ok')
% grid on