function plotRealAndSimPlanetRVrecovery(pipeline_dir, plateName, beamNum, nameAppend)


load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'solved_two2bdyMeth_recovery', 'MISC', 'NA', 3, 1, 1), 'orbitParams')

orbitParams_recovery = orbitParams;

load(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'solved_two2bdyMeth', 'MISC', 'NA', 3, 1, 1), 'orbitParams')



obsNums = 1:length(orbitParams.s1.t.known);

t = orbitParams.s1.t.known;
t1 = t(1);
t = t - t(1);
th = orbitParams_recovery.s1.t.hi.known;
th = th - th(1);

r1o = (max(orbitParams.s1.RV.hi.fit) + min(orbitParams.s1.RV.hi.fit)) / 2;
r2o = (max(orbitParams_recovery.s1.RV.hi.fit) + min(orbitParams_recovery.s1.RV.hi.fit)) / 2;


qf(44,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Measured RV''s')
xlabel('Observation #')
ylabel('RV [m/s]')
plot(obsNums, orbitParams.s1.RV.known - r1o, '-ok')
plot(obsNums, orbitParams_recovery.s1.RV.known - r2o, '-^r')
grid on
legend({'2D RVs of Real Spectra', '2D RVs of Simulated Spectra'},'location', 'best')


qf(55,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Measured RV''s')
xlabel(cat(2, 'JD - ', n2s(round(t1))))
ylabel('RV [m/s]')
plot(th, orbitParams.s1.RV.hi.fit - r1o, '-k', t, orbitParams.s1.RV.known - r1o, 'ok')
plot(th, orbitParams_recovery.s1.RV.hi.fit - r2o, '-r', t, orbitParams_recovery.s1.RV.known - r2o, '^r')
grid on
legend({'2D RVs of Real Spectra', 'Orbit Fit of Real Spectra', '2D RVs of Simulated Spectra', 'Orbit Fit of Simulated Spectra'},'location', 'best')


qf(66,1); 
% subplot(4,1,1); hold on
title('Simulation Recovery: Planet RV''s and Orbit Fit')
xlabel(cat(2, 'JD - ', n2s(round(t1))))
ylabel('RV [m/s]')
plot(th, orbitParams.(objectStructName).RV.hi.fit, '-k', t, orbitParams.p1.RV.known, 'ok')
plot(th, orbitParams_recovery.(objectStructName).RV.hi.fit, '-r', t, orbitParams_recovery.p1.RV.known, '^r')
grid on
legend({'2D RVs of Real Spectra', 'Orbit Fit of Real Spectra', '2D RVs of Simulated Spectra', 'Orbit Fit of Simulated Spectra'},'location', 'best')




% 
% qf(44,1); 
% subplot(4,1,1); hold on
% title('Simulation Recovery: Binary Orbit Fit')
% xlabel('JD')
% ylabel('RV [m/s]')
% plot(orbitParams.s1.t.hi.known, orbitParams.s1.RV.hi.fit, '-k', orbitParams.s1.t.known, orbitParams.s1.RV.fit, 'ok', orbitParams.s1.t.known, orbitParams.s1.RV.known, '+k')
% plot(orbitParams_recovery.s1.t.hi.known, orbitParams_recovery.s1.RV.hi.fit, '-r', orbitParams_recovery.s1.t.known, orbitParams_recovery.s1.RV.fit, 'or', orbitParams_recovery.s1.t.known, orbitParams_recovery.s1.RV.known, '+r')
% grid on
% legend({'Binary Orbit', 'Binary RV Fit', 'Binary Actual RV'},'location', 'best')
% 
% subplot(4,1,2); hold on
% title('Binary Orbit Fit Error')
% xlabel('JD')
% ylabel('RV Error [m/s]')
% plot(orbitParams.(objectStructName).t.known, orbitParams.s1.RV.known - orbitParams.s1.RV.fit, 'ok')
% grid on
% 
% subplot(4,1,3); hold on
% title('Simulation Recovery: Planet Orbit Fit')
% xlabel('JD')
% ylabel('RV [m/s]')
% plot(orbitParams.(objectStructName).t.hi.known, orbitParams.(objectStructName).RV.hi.fit, '-k', orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.fit, 'og', orbitParams.p1.t.known, orbitParams.p1.RV.known, '+k')
% grid on
% legend({'Planet Orbit', 'Planet RV Fit', 'Planet RV'},'location', 'best')
% 
% subplot(4,1,4); hold on
% title('Planet Orbit Fit Error')
% xlabel('JD')
% ylabel('RV Error [m/s]')
% plot(orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit, 'ok')
% grid on