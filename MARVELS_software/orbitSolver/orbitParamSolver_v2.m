% function orbitParamSolver_v2(plateName)

clearvars


%%

s = dslash;

orbitParams.plateName = plateName;

orbitParams.beamNum = 87;

expectDataTohaveFullPeriod_q = 0;

objectStructName = 's1';

orbitParams.(objectStructName).P.known = nan;
orbitParams.(objectStructName).P.known = 42.02;
orbitParams.(objectStructName).K.known = nan;
orbitParams.(objectStructName).ecc.known = nan;
% orbitParams.(objectStructName).ecc.known = 0.68;
orbitParams.(objectStructName).w.known = nan;

orbitParams.(objectStructName).solverOptions.parP_q = 0;
orbitParams.(objectStructName).solverOptions.MaxFevals = 5000;
orbitParams.(objectStructName).solverOptions.MaxIter = 5000;
orbitParams.(objectStructName).solverOptions.TolFun = 1e-20;
orbitParams.(objectStructName).solverOptions.TolX = 1e-20;
orbitParams.(objectStructName).solverOptions.numRandPoints = 50;
orbitParams.(objectStructName).solverOptions.globalSolver_q = 1;

graph_q = 0;
optimizationMatlabGraph_q = 0;


orbitParams.p1.P.known = nan;
orbitParams.p1.P.known = 394.66;
orbitParams.p1.K.known = nan;
orbitParams.p1.ecc.known = nan;
orbitParams.p1.ecc.known = 0.15;
orbitParams.p1.w.known = nan;

orbitParams.p1.solverOptions.parP_q = 0;
orbitParams.p1.solverOptions.MaxFevals = 5000;
orbitParams.p1.solverOptions.MaxIter = 5000;
orbitParams.p1.solverOptions.TolFun = 1e-20;
orbitParams.p1.solverOptions.TolX = 1e-20;
orbitParams.p1.solverOptions.numRandPoints = 20;
orbitParams.p1.solverOptions.globalSolver_q = 1;


%%

if strcmp(s, '/')
    neils_dir = '/astro/data/marvels/nthomas819Working/pipeline/May15';
else
    neils_dir = 'R:\astro\data\marvels\nthomas819Working\pipeline\May15';
end

pipeline_dir = 'E:\Research\Ge_neils_2D';

load('E:\Research\Ge_neils_2D\MARVELS_data\RV\oldResults2D\FIELD1572_V2','V2')


if 0 == exist(dirFinder(pipeline_dir, orbitParams.plateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0), 'file')
    load(strcat(neils_dir, s, orbitParams.plateName, s, 'results', s, 'MJDobs.mat'), 'MJDobs')

    save(dirFinder(pipeline_dir, orbitParams.plateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 1),'MJDobs')
else
    load(dirFinder(pipeline_dir, orbitParams.plateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0),'MJDobs')
end

%%

orbitParams.(objectStructName).t.known = MJDobs(:, 2)';


orbitParams.(objectStructName).t.hi.known = orbitParams.(objectStructName).t.known(1) :0.01: orbitParams.(objectStructName).t.known(end);


%%

% orbitParams.(objectStructName).RV.known = double(V2(orbitParams.beamNum, :));

orbitParams.(objectStructName).RV.known = mean(double(V2(orbitParams.beamNum + (0:1), :)), 1);

%%

    
    
% Time of pasterion    

orbitParams.(objectStructName).tp.sv = 1;
orbitParams.(objectStructName).tp.lb = 0;
orbitParams.(objectStructName).tp.ub = inf;

% RV max

orbitParams.(objectStructName).K.sv = max(abs(orbitParams.(objectStructName).RV.known));
orbitParams.(objectStructName).K.lb = 0.01;
orbitParams.(objectStructName).K.ub = 100000;

% Argument of pasterion

orbitParams.(objectStructName).w.sv = 0.001;
orbitParams.(objectStructName).w.lb = 0;
orbitParams.(objectStructName).w.ub = 10000*2*pi;

% Eccentricity

orbitParams.(objectStructName).ecc.sv = 0.001;
orbitParams.(objectStructName).ecc.lb = 0;
orbitParams.(objectStructName).ecc.ub = 1;
% orbitParams.(objectStructName).ecc.lb = 0;
% orbitParams.(objectStructName).ecc.ub = 0.2;


% RV Offset

orbitParams.(objectStructName).RV_off.sv = 0;
orbitParams.(objectStructName).RV_off.lb = min(orbitParams.(objectStructName).RV.known);
orbitParams.(objectStructName).RV_off.ub = max(orbitParams.(objectStructName).RV.known);



% Period

orbitParams.(objectStructName).P.sv = 42;

% orbitParams.(objectStructName).P.lb = 0.01;
% orbitParams.(objectStructName).P.ub = 2000;

orbitParams.(objectStructName).P.lb = 41;

if expectDataTohaveFullPeriod_q
    orbitParams.(objectStructName).P.ub = ( orbitParams.(objectStructName).t.known(end) - orbitParams.(objectStructName).t.known(1) ) / 2;
else
    orbitParams.(objectStructName).P.ub = 43;
end


for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams.(objectStructName).(varName{1}), 'known')
        
        if isfinite(orbitParams.(objectStructName).(varName{1}).known)
            
            orbitParams.(objectStructName).(varName{1}).sv = orbitParams.(objectStructName).(varName{1}).known;
        end
    end
end



orbitParams.(objectStructName) = solveOrbitParams(orbitParams.(objectStructName), graph_q, optimizationMatlabGraph_q);



for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams.(objectStructName).(varName{1}), 'known')
        
        if ~isfinite(orbitParams.(objectStructName).(varName{1}).known)
            
            orbitParams.(objectStructName).(varName{1}).final = orbitParams.(objectStructName).(varName{1}).solved;
            
        else
            
            orbitParams.(objectStructName).(varName{1}).final = orbitParams.(objectStructName).(varName{1}).known;
        end
    
    else
        
        orbitParams.(objectStructName).(varName{1}).final = orbitParams.(objectStructName).(varName{1}).solved;
        
    end
end



%     tp, K, w,  e,  off

qf(3,1); 
subplot(2,1,1); 
plot(orbitParams.(objectStructName).t.hi.known, orbitParams.(objectStructName).RV.hi.fit, '-k', orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.fit, 'og', orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.known, '+k')
grid on

subplot(2,1,2);
plot(orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit, 'ok')
grid on


gi = isfinite(orbitParams.(objectStructName).RV.known);

orbitParams.(objectStructName).fitStats.Rsquared = Rsqr_imgResid(orbitParams.(objectStructName).RV.known(gi), orbitParams.(objectStructName).RV.fit(gi));

% X2 = chiSqr(orbitParams.(objectStructName).RV.known(gi), orbitParams.(objectStructName).RV.fit(gi), 5);

% sum( abs( abs(orbitParams.(objectStructName).RV.fit(gi)) - abs(orbitParams.(objectStructName).RV.known(gi)) ) / sum(abs(orbitParams.(objectStructName).RV.fit(gi))) )

[~, ~, c] = chi2gof( abs(orbitParams.(objectStructName).RV.fit(gi) - orbitParams.(objectStructName).RV.known(gi)) );

orbitParams.(objectStructName).fitStats.reducedChiSquared = c.chi2stat / 5;

orbitParams.(objectStructName).fitStats.errorRMS = nanrms(orbitParams.(objectStructName).RV.fit(gi) - orbitParams.(objectStructName).RV.known(gi));

disp(' ')
disp(cat(2, 'tp = ', n2s(orbitParams.(objectStructName).tp.final)))
disp(' ')
disp(cat(2, 'K = ', n2s(orbitParams.(objectStructName).K.final)))
disp(' ')
disp(cat(2, 'w = ', n2s(orbitParams.(objectStructName).w.final)))
disp(' ')
disp(cat(2, 'ecc = ', n2s(orbitParams.(objectStructName).ecc.final)))
disp(' ')
disp(cat(2, 'off = ', n2s(orbitParams.(objectStructName).RV_off.final)))
disp(' ')
disp(cat(2, 'P = ', n2s(orbitParams.(objectStructName).P.final)))
disp(' ')
disp(cat(2, 'RMS = ', n2s(orbitParams.(objectStructName).fitStats.errorRMS)))
disp(' ')
disp(cat(2, 'Rsquared = ', n2s(orbitParams.(objectStructName).fitStats.Rsquared)))
disp(' ')
disp(cat(2, 'Reduced Chi Squared = ', n2s(orbitParams.(objectStructName).fitStats.reducedChiSquared)))
disp(' ')








%% Planet 1

objectStructName = 'p1';

orbitParams.(objectStructName).RV.known = orbitParams.s1.RV.known - orbitParams.s1.RV.fit;

orbitParams.(objectStructName).t = orbitParams.s1.t;



    
% Time of pasterion    

orbitParams.(objectStructName).tp.sv = 1;
orbitParams.(objectStructName).tp.lb = 0;
orbitParams.(objectStructName).tp.ub = inf;

% RV max

orbitParams.(objectStructName).K.sv = max(abs(orbitParams.(objectStructName).RV.known));
orbitParams.(objectStructName).K.lb = 0.01;
orbitParams.(objectStructName).K.ub = max(abs(orbitParams.(objectStructName).RV.known)) + 3 * nanstd(abs(orbitParams.(objectStructName).RV.known));

% Argument of pasterion

orbitParams.(objectStructName).w.sv = 0.001;
orbitParams.(objectStructName).w.lb = 0;
orbitParams.(objectStructName).w.ub = 2*pi * 1000;

% Eccentricity

orbitParams.(objectStructName).ecc.sv = 0.0;
orbitParams.(objectStructName).ecc.lb = 0;
orbitParams.(objectStructName).ecc.ub = 1;
% orbitParams.(objectStructName).ecc.lb = 0;
% orbitParams.(objectStructName).ecc.ub = 0.2;


% RV Offset

orbitParams.(objectStructName).RV_off.sv = 0;
orbitParams.(objectStructName).RV_off.lb = min(orbitParams.(objectStructName).RV.known);
orbitParams.(objectStructName).RV_off.ub = max(orbitParams.(objectStructName).RV.known);



% Period

orbitParams.(objectStructName).P.sv = 394.66;

% orbitParams.(objectStructName).P.lb = 0.01;
% orbitParams.(objectStructName).P.ub = 2000;

orbitParams.(objectStructName).P.lb = 0.01;

if expectDataTohaveFullPeriod_q
    orbitParams.(objectStructName).P.ub = ( orbitParams.(objectStructName).t.known(end) - orbitParams.(objectStructName).t.known(1) ) / 2;
else
    orbitParams.(objectStructName).P.ub = 10000;
end





for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams.(objectStructName).(varName{1}), 'known')
        
        if isfinite(orbitParams.(objectStructName).(varName{1}).known)
            
            orbitParams.(objectStructName).(varName{1}).sv = orbitParams.(objectStructName).(varName{1}).known;
        end
    end
end


orbitParams.(objectStructName) = solveOrbitParams(orbitParams.(objectStructName), graph_q, optimizationMatlabGraph_q);



for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams.(objectStructName).(varName{1}), 'known')
        
        if ~isfinite(orbitParams.(objectStructName).(varName{1}).known)
            
            orbitParams.(objectStructName).(varName{1}).final = orbitParams.(objectStructName).(varName{1}).solved;
            
        else
            
            orbitParams.(objectStructName).(varName{1}).final = orbitParams.(objectStructName).(varName{1}).known;
        end
    
    else
        
        orbitParams.(objectStructName).(varName{1}).final = orbitParams.(objectStructName).(varName{1}).solved;
        
    end
end



%     tp, K, w,  e,  off

qf(3,1); 
subplot(4,1,1); hold on
title('Binary Orbit Fit')
xlabel('JD')
ylabel('RV [m/s]')
plot(orbitParams.s1.t.hi.known, orbitParams.s1.RV.hi.fit, '-k', orbitParams.s1.t.known, orbitParams.s1.RV.fit, 'og', orbitParams.s1.t.known, orbitParams.s1.RV.known, '+k')
grid on
legend({'Binary Orbit', 'Binary RV Fit', 'Binary Actual RV'},'location', 'best')

subplot(4,1,2); hold on
title('Binary Orbit Fit Error')
xlabel('JD')
ylabel('RV Error [m/s]')
plot(orbitParams.(objectStructName).t.known, orbitParams.s1.RV.known - orbitParams.s1.RV.fit, 'ok')
grid on

subplot(4,1,3); hold on
title('Planet Orbit Fit')
xlabel('JD')
ylabel('RV [m/s]')
plot(orbitParams.(objectStructName).t.hi.known, orbitParams.(objectStructName).RV.hi.fit, '-k', orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.fit, 'og', orbitParams.p1.t.known, orbitParams.p1.RV.known, '+k')
grid on
legend({'Planet Orbit', 'Planet RV Fit', 'Planet RV'},'location', 'best')

subplot(4,1,4); hold on
title('Planet Orbit Fit Error')
xlabel('JD')
ylabel('RV Error [m/s]')
plot(orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit, 'ok')
grid on




gi = isfinite(orbitParams.s1.RV.known) & isfinite(orbitParams.p1.RV.known);

aa = orbitParams.s1.RV.known(gi) - orbitParams.s1.RV.fit(gi);
aa = aa - nanmean(aa);

bb = orbitParams.p1.RV.known(gi) - orbitParams.p1.RV.fit(gi);
bb = bb - nanmean(bb);

R2 = Rsqr_imgResid(aa, bb);

qf(4,1)
% subplot(2,1,1); hold on
title(cat(2, 'Planet Fit Significance | Binary vs Planet Fit Error | Significance = ', n2s(R2), ' (0 ~= very significant, 1 ~= no significance)'))
xlabel('JD')
ylabel('RV Error [m/s]')
plot(orbitParams.(objectStructName).t.known, orbitParams.s1.RV.known - orbitParams.s1.RV.fit, 'ob')
plot(orbitParams.(objectStructName).t.known, orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit, '^r')
legend({'Binary RV Fit Error', 'Planet RV Fit Error'},'location', 'best')
grid on

% subplot(2,1,2); hold on
% title('Binary / Planet Fit Error Residual')
% xlabel('JD')
% ylabel('RV [m/s]')
% plot(orbitParams.s1.t.known, (orbitParams.s1.RV.known - orbitParams.s1.RV.fit) - (orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit), '+k')
% plot(orbitParams.(objectStructName).t.hi.known, orbitParams.(objectStructName).RV.hi.fit, '-k')
% grid on





gi = isfinite(orbitParams.(objectStructName).RV.known);

orbitParams.(objectStructName).fitStats.Rsquared = Rsqr_imgResid(orbitParams.(objectStructName).RV.known(gi), orbitParams.(objectStructName).RV.fit(gi));

% X2 = chiSqr(orbitParams.(objectStructName).RV.known(gi), orbitParams.(objectStructName).RV.fit(gi), 5);

% sum( abs( abs(orbitParams.(objectStructName).RV.fit(gi)) - abs(orbitParams.(objectStructName).RV.known(gi)) ) / sum(abs(orbitParams.(objectStructName).RV.fit(gi))) )

[~, ~, c] = chi2gof( abs(orbitParams.(objectStructName).RV.fit(gi) - orbitParams.(objectStructName).RV.known(gi)) );

orbitParams.(objectStructName).fitStats.reducedChiSquared = c.chi2stat / 5;

orbitParams.(objectStructName).fitStats.errorRMS = nanrms(orbitParams.(objectStructName).RV.fit(gi) - orbitParams.(objectStructName).RV.known(gi));

disp(' ')
disp(cat(2, 'tp = ', n2s(orbitParams.(objectStructName).tp.final)))
disp(' ')
disp(cat(2, 'K = ', n2s(orbitParams.(objectStructName).K.final)))
disp(' ')
disp(cat(2, 'w = ', n2s(orbitParams.(objectStructName).w.final)))
disp(' ')
disp(cat(2, 'ecc = ', n2s(orbitParams.(objectStructName).ecc.final)))
disp(' ')
disp(cat(2, 'off = ', n2s(orbitParams.(objectStructName).RV_off.final)))
disp(' ')
disp(cat(2, 'P = ', n2s(orbitParams.(objectStructName).P.final)))
disp(' ')
disp(cat(2, 'RMS = ', n2s(orbitParams.(objectStructName).fitStats.errorRMS)))
disp(' ')
disp(cat(2, 'Rsquared = ', n2s(orbitParams.(objectStructName).fitStats.Rsquared)))
disp(' ')
disp(cat(2, 'Reduced Chi Squared = ', n2s(orbitParams.(objectStructName).fitStats.reducedChiSquared)))
disp(' ')






% rv_p_mps = orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit;
% 
% save(['E:\Research', s, 'rv_p_mps.mat'], 'rv_p_mps')


% save(['E:\Research', s, 'binary_orbitParams.mat'], 'orbitParams')







% Notes:
% 1) Energy var (E) needs to be fixed. It is not used in the simulation,
% but may be used later. Can use to measure/correct energy drift
% 
% 

%% Constants

AU = 149597870700;
G0 = 6.67408e-11;
M_sun = 1.9885e30;
M_jup = 1.89813e27;
Yr = 365*24*3600;
s_day = 24 * 60 * 60;

%Dimensionless number which controls the dynamics, and results from the
%scaling of mass, distance and time parameters to make them dimensionless.
G = G0*Yr^2*M_sun/(AU^3);

%% Solve other params

% Stars
P1_s = orbitParams.s1.P.final * s_day;    
K1_mps = orbitParams.s1.K.final;
ecc1 = orbitParams.s1.ecc.final;
[m1_kg, orbitParams.s1.m.final] = deal(1 * M_sun);

m2_kg = solveMass_type1(m1_kg, P1_s, K1_mps, ecc1);

% m2_kg / M_sun

orbitParams.s2.m.final = m2_kg;

a_m = ( P1_s^2 * G0 * (m1_kg + m2_kg) / (4 * pi^2) )^(1 / 3);

a_AU = a_m / AU;

a1_m = m2_kg / (m1_kg + m2_kg) * a_m;

a1_AU = a1_m / AU;

orbitParams.s1.a1.final = a1_m;

a2_m = m1_kg / (m1_kg + m2_kg) * a_m;

a2_AU = a2_m / AU;

orbitParams.s2.a2.final = a2_m;

% Planet
orbitParams.p1.m.final = solveMass_type1(m1_kg + m2_kg, orbitParams.p1.P.final * s_day, orbitParams.p1.K.final, orbitParams.p1.ecc.final);

% orbitParams.p1.m.final / M_sun

ap_m = ( (orbitParams.p1.P.final * s_day)^2 * G0 * (m1_kg + m2_kg + orbitParams.p1.m.final) / (4 * pi^2) )^(1 / 3);

ap_AU = ap_m / AU;

ap1_m = (m1_kg + m2_kg) / (m1_kg + m2_kg + orbitParams.p1.m.final) * ap_m;

ap1_AU = ap1_m / AU;

orbitParams.p1.ap1.final = ap1_m;


save(['E:\Research\syntheticSpectraCreation', s, 'binary_orbitParams.mat'], 'orbitParams')


%% Physical System Params

% Object names
name{1} = 'Star 1';
name{2} = 'Star 2';
name{3} = 'Planet 1';
% name{4} = 'Planet 2';

% Object type
objType{1} = 's';
objType{2} = 's';
objType{3} = 'p';
% objType{4} = 'p';

% Object masses [solar masses] (m)
m(1) = orbitParams.s1.m.final / M_sun;
m(2) = orbitParams.s2.m.final / M_sun;
m(3) = orbitParams.p1.m.final / M_sun;
% m(4) = 0.000009 * 1000;

% 238.3 * M_jup / M_sun

% Object orbit configuration (which objects are gravitationally bound to each other?)
gBoundPairIndex{1} = [1, 2];
gBoundPairIndex{2} = [3];
% gBoundPairIndex{3} = [4];

% Initial star separation [AU] (a)
gBoundPair_sep_AU(1) = ( orbitParams.s1.a1.final + orbitParams.s2.a2.final ) / AU;
gBoundPair_sep_AU(2) = orbitParams.p1.ap1.final / AU;
% gBoundPair_sep_AU(3) = 20;

% Initial phase (polar angle) of two-mass elliptical orbit
theta0(1) = 0;
theta0(2) = 2*pi * 0.5;
% theta0(3) = 0;

% Orbit eccentricity [] (e)
ecc(1) = orbitParams.s1.ecc.final;
ecc(2) = orbitParams.p1.ecc.final;
% ecc(3) = 0.0;

% Orientation of semi-major axis of ellipse
smo(1, :) = [1,0,0]; % <-------------- Needs to be converted to an angle input
smo(2, :) = [1,0,0];
% smo(3, :) = [1,1,0];

% Rotation of ellipse clockwise about d [radians] (w {omega})
alpha(1) = 0;%orbitParams.s1.w.final;
alpha(2) = 0;%orbitParams.p1.w.final;
% alpha(3) = 0;

% Center of mass initial coordinates [AU]
h0 = [0,0,0];

% Center of mass velocity [AU/s]
hdot0 = [0,0,0];


% sqrt( 4 * pi^2 * gBoundPair_sep_AU(1)^3 / (G *(m(1) + m(2))) ) * 365

%% Plot Params

plot_q = 1;

showPathTrails_q = 1;

showObjNames_q = 1;

obj_RGB{1} = [1, 0, 0];
obj_RGB{2} = [0, 1, 0];
obj_RGB{3} = [0, 0, 1];
% obj_RGB{4} = [0.5, 0, 0.5];

% (NOT WORKING) Add cool looking (massless) particle rings around a certain object
addMasslessParticleRings_q(1) = 0;
addMasslessParticleRings_q(2) = 0;
addMasslessParticleRings_q(3) = 0;
% addMasslessParticleRings_q(4) = 0;



%%

sys_ind = 1:length(gBoundPair_sep_AU);

for sysNum = sys_ind
    
    obj_ind = gBoundPairIndex{sysNum};
    
    %     if sysNum ~= 1
    %
    %         foundMatch_ind = false((sysNum - 1), 2);
    %
    %         obj_ind2 = 0;
    %
    %         % Is either of these objects already accounted for in this loop?
    %         for objNum = obj_ind
    %
    %             obj_ind2 = obj_ind2 + 1;
    %
    %             for sysNum2 = sys_ind(1:(sysNum - 1))
    %
    %                 foundMatch_ind(sysNum2, obj_ind2) = any( gBoundPairIndex{sysNum2} == objNum );
    %             end
    %         end
    %     else
    %
    %     end
    
    
    if length(obj_ind) > 2 | length(obj_ind) <= 0
        
        error(cat(2, 'ERROR: The number of objects in "gBoundPairIndex" for system #', n2s(sysNum), ' is ', n2s(length(gBoundPairIndex{sysNum})), ', but only 1 or 2 objects allowed per bound system.'))
        
        
    elseif length(obj_ind) == 2 % Binary star systems and star-planet systems
        
        
        if strcmp(objType{obj_ind(1)}, 's') & strcmp(objType{obj_ind(2)}, 's')
            
            hdot = hdot0;
            
            h = h0;
            
            obj_ind_save = obj_ind;
            
            % Determine initial conditions of this system
            [ ...
                ux, vx(obj_ind(1)), vx(obj_ind(2)),...
                uy, vy(obj_ind(1)), vy(obj_ind(2)),...
                uz, vz(obj_ind(1)), vz(obj_ind(2)),...
                xc, x(obj_ind(1)), x(obj_ind(2)),...
                yc, y(obj_ind(1)), y(obj_ind(2)),...
                zc, z(obj_ind(1)), z(obj_ind(2)),...
                theta, theta_dot, t, j, J, E, P ...
                ] = two_body_elliptical_orbit(  G, m(obj_ind(1)), m(obj_ind(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
            
        else
            
            hdot = [vx(obj_ind(1)), vy(obj_ind(1)), vz(obj_ind(1))];
            
            h = [x(obj_ind(1)), y(obj_ind(1)), z(obj_ind(1))];
            
            obj_ind_save = obj_ind(2);
            
            % Determine initial conditions of this system
            [ ...
                ux, ~, vx(obj_ind(2)),...
                uy, ~, vy(obj_ind(2)),...
                uz, ~, vz(obj_ind(2)),...
                xc, ~, x(obj_ind(2)),...
                yc, ~, y(obj_ind(2)),...
                zc, ~, z(obj_ind(2)),...
                theta, theta_dot, t, j, J, E, P ...
                ] = two_body_elliptical_orbit(  G, m(obj_ind(1)), m(obj_ind(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
            
            
        end
        
        

        
        
        % Place params into final structure
        for objNum = obj_ind_save
            
            masses(objNum).name = name{objNum};
            masses(objNum).mass = m(objNum);
            masses(objNum).radii = 1e-10;
            masses(objNum).x0 = x(objNum);
            masses(objNum).y0 = y(objNum);
            masses(objNum).z0 = z(objNum);
            masses(objNum).vx0 = vx(objNum);
            masses(objNum).vy0 = vy(objNum);
            masses(objNum).vz0 = vz(objNum);
            masses(objNum).marker_RGB = obj_RGB{objNum};
            masses(objNum).marker_size = 5;
            masses(objNum).plot_trail = showPathTrails_q;
            masses(objNum).init_orbit_M = m(objNum);
            masses(objNum).init_orbit_P = P;
            masses(objNum).init_orbit_G = G;
            masses(objNum).init_orbit_ecc = ecc(sysNum);
            masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
            masses(objNum).init_orbit_h0 = h0;
            masses(objNum).init_orbit_hdot = hdot;
            masses(objNum).init_orbit_d = smo(sysNum, :);
            masses(objNum).init_orbit_alpha = alpha(sysNum);
            masses(objNum).init_orbit_theta0 = theta0(sysNum);
            masses(objNum).init_orbit_thetadot0 = theta_dot;
            masses(objNum).init_orbit_J = J;
            masses(objNum).init_orbit_j = j;
            masses(objNum).init_orbit_E = E;
        end
        
        
        
        
    elseif length(obj_ind) == 1 & strcmp(objType(obj_ind), 'p') % Circumbinary Planet
          
        
        objNum = obj_ind;
            
        hdot = hdot0;
        
        h = h0;
        
        m_tot = m(1) + m(2);
        
        % Determine initial conditions of this system
        [ ...
            ux, ~, vx(objNum),...
            uy, ~, vy(objNum),...
            uz, ~, vz(objNum),...
            xc, ~, x(objNum),...
            yc, ~, y(objNum),...
            zc, ~, z(objNum),...
            theta, theta_dot, t, j, J, E, P ...
            ] = two_body_elliptical_orbit(  G, m_tot, m(objNum), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
        
        
        masses(objNum).name = name{objNum};
        masses(objNum).mass = m(objNum);
        masses(objNum).radii = 1e-10;
        masses(objNum).x0 = x(objNum);
        masses(objNum).y0 = y(objNum);
        masses(objNum).z0 = z(objNum);
        masses(objNum).vx0 = vx(objNum);
        masses(objNum).vy0 = vy(objNum);
        masses(objNum).vz0 = vz(objNum);
        masses(objNum).marker_RGB = obj_RGB{objNum};
        masses(objNum).marker_size = 5;
        masses(objNum).plot_trail = showPathTrails_q;
        masses(objNum).init_orbit_M = m(objNum);
        masses(objNum).init_orbit_P = P;
        masses(objNum).init_orbit_G = G;
        masses(objNum).init_orbit_ecc = ecc(sysNum);
        masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
        masses(objNum).init_orbit_h0 = h0;
        masses(objNum).init_orbit_hdot = hdot;
        masses(objNum).init_orbit_d = smo(sysNum, :);
        masses(objNum).init_orbit_alpha = alpha(sysNum);
        masses(objNum).init_orbit_theta0 = theta0(sysNum);
        masses(objNum).init_orbit_thetadot0 = theta_dot;
        masses(objNum).init_orbit_J = J;
        masses(objNum).init_orbit_j = j;
        masses(objNum).init_orbit_E = E;
        
        
        
    elseif length(obj_ind) == 1 & strcmp(objType(obj_ind), 's') % Circumbinary Star
        
        error('ERROR: Simulating a circumbinary star is not currently available. Please remove that star.')
    end
    
    
    
end


rings = [];

for objNum = 1:length(objType)
    
    if addMasslessParticleRings_q(objNum)
        
%         if strcmp(objType{objNum}, 'p')
            rings(objNum).xc = x(objNum);
            rings(objNum).yc = y(objNum);
            rings(objNum).zc = z(objNum);
%         else
%             rings(objNum).xc = x(objNum);
%             rings(objNum).yc = y(objNum);
%             rings(objNum).zc = z(objNum);
%         end
        rings(objNum).vxc = vx(objNum);
        rings(objNum).vyc = vy(objNum);
        rings(objNum).vzc = vz(objNum);
        rings(objNum).num_rings = 40;
        rings(objNum).arc_separation_AU = 1*pi/30;
        rings(objNum).first_ring_radius_AU = 0.10;
        rings(objNum).ring_radius_diff_AU = 0.5;
        rings(objNum).d = masses(objNum).init_orbit_d;
        rings(objNum).alpha = pi/2;
        rings(objNum).mass_at_centre = m(objNum);
        rings(objNum).marker_RGB = obj_RGB{objNum} * 0.8;
        
    end
end


clusters = [];




% [masses(:).init_orbit_P] * 365




nBodySim(masses, rings, clusters, plot_q, showObjNames_q)




