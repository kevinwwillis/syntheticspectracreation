function orbitParams = solveOrbitParams(orbitParams, graph_q, optimizationMatlabGraph_q)


% orbitParams.tp.solved = y0(1);
% orbitParams.K.solved = y0();
% orbitParams.w.solved = y0();
% orbitParams.ecc.solved = y0();
% orbitParams.RV_off.solved = y0();
% orbitParams.P.solved = y0();
% orbitParams.RV.fit = orbit_mps;
% orbitParams.RV.hi.fit = orbit_hiR_mps;

if ~isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 1111
    
    %    tp, off
    x0 = [0, 0];
    
    lb = [orbitParams.tp.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.RV_off.ub];
    %                                                                       tp, K, w, e, t, off, P
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.known, orbitParams.ecc.known, orbitParams.t.known, y0(2), orbitParams.P.known));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv, orbitParams.t.hi.known, y0(2), orbitParams.P.sv));
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv, orbitParams.t.known, y0(2), orbitParams.P.sv));
    
    
    orbitParams.tp.solved = y0(1);
    %     orbitParams.K.solved = y0();
    %     orbitParams.w.solved = y0();
    %     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(2);
    %     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif ~isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 1110
    
    %     tp, off,  P
    x0 = [0, 0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(2), y0(3)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(2), y0(3)));
    
    t2 = orbitParams.t.hi.known.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(2), y0(3)));
    
    
    orbitParams.tp.solved = y0(1);
%     orbitParams.K.solved = y0();
%     orbitParams.w.solved = y0();
%     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(2);
    orbitParams.P.solved = y0(3);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif ~isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 1100
    
    %     tp, e,  off,  P
    x0 = [0, orbitParams.ecc.sv,  0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, y0(2), t2, y0(3), y0(4)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, y0(2), t2, y0(3), y0(4)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, y0(2), t2, y0(3), y0(4)));
    
    
    orbitParams.tp.solved = y0(1);
%     orbitParams.K.solved = y0();
%     orbitParams.w.solved = y0();
    orbitParams.ecc.solved = y0(2);
    orbitParams.RV_off.solved = y0(3);
    orbitParams.P.solved = y0(4);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif ~isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 1000
    
    %     tp, w,  e,  off,  P
    x0 = [0, orbitParams.w.sv, orbitParams.ecc.sv,  0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.w.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.w.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), y0(3), t2, y0(4), y0(5)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), y0(3), t2, y0(4), y0(5)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), y0(3), t2, y0(4), y0(5)));
    
    
    orbitParams.tp.solved = y0(1);
%     orbitParams.K.solved = y0();
    orbitParams.w.solved = y0(2);
    orbitParams.ecc.solved = y0(3);
    orbitParams.RV_off.solved = y0(4);
    orbitParams.P.solved = y0(5);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 0100
    
    %     tp, K,  e,  off,  P
    x0 = [0, orbitParams.K.sv, orbitParams.ecc.sv,  0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, y0(3), t2, y0(4), y0(5)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, y0(3), t2, y0(4), y0(5)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, y0(3), t2, y0(4), y0(5)));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
%     orbitParams.w.solved = y0();
    orbitParams.ecc.solved = y0(3);
    orbitParams.RV_off.solved = y0(4);
    orbitParams.P.solved = y0(5);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
    
elseif isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 0110
    
    %     tp, K, off,  P
    x0 = [0, orbitParams.K.sv, 0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(3), y0(4)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(3), y0(4)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(3), y0(4)));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
%     orbitParams.w.solved = y0();
%     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(3);
    orbitParams.P.solved = y0(4);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 0111
    
    %     tp, K, off
    x0 = [0, orbitParams.K.sv, 0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(3), orbitParams.P.sv));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(3), orbitParams.P.sv));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, orbitParams.ecc.sv, t2, y0(3), orbitParams.P.sv));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
%     orbitParams.w.solved = y0();
%     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(3);
%     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 0010
    
    %     tp, K, w, off,  P
    x0 = [0, orbitParams.K.sv, orbitParams.w.sv, 0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.w.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.w.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), orbitParams.ecc.sv, t2, y0(4), y0(5)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), orbitParams.ecc.sv, t2, y0(4), y0(5)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), orbitParams.ecc.sv, t2, y0(4), y0(5)));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
    orbitParams.w.solved = y0(3);
%     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(4);
    orbitParams.P.solved = y0(5);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 0011
    
    %     tp, K, w, off
    x0 = [0, orbitParams.K.sv, orbitParams.w.sv, 0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.w.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.w.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), orbitParams.ecc.sv, t2, y0(4), orbitParams.P.sv));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), orbitParams.ecc.sv, t2, y0(4), orbitParams.P.sv));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), orbitParams.ecc.sv, t2, y0(4), orbitParams.P.sv));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
    orbitParams.w.solved = y0(3);
%     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(4);
%     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
    
elseif isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 0001
    
    %     tp, K, w,  e,  off
    x0 = [0, orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv,  0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.w.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.w.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), y0(4), t2, y0(5), orbitParams.P.known));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), y0(4), t2, y0(5), orbitParams.P.sv));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), y0(4), t2, y0(5), orbitParams.P.sv));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
    orbitParams.w.solved = y0(3);
    orbitParams.ecc.solved = y0(4);
    orbitParams.RV_off.solved = y0(5);
%     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
    
elseif ~isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 1001
    
    %     tp, w,  e,  off
    x0 = [0, orbitParams.w.sv, orbitParams.ecc.sv,  0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.w.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.w.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.known, y0(2), y0(3), t2, y0(4), orbitParams.P.known));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.known, y0(2), y0(3), t2, y0(4), orbitParams.P.known));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.known, y0(2), y0(3), t2, y0(4), orbitParams.P.known));
    
    
    orbitParams.tp.solved = y0(1);
%     orbitParams.K.solved = y0();
    orbitParams.w.solved = y0(2);
    orbitParams.ecc.solved = y0(3);
    orbitParams.RV_off.solved = y0(4);
%     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
    
elseif ~isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 1101
    
    %     tp, e,  off
    x0 = [0, orbitParams.ecc.sv,  0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, y0(2), t2, y0(3), orbitParams.P.sv));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, y0(2), t2, y0(3), orbitParams.P.sv));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, orbitParams.w.sv, y0(2), t2, y0(3), orbitParams.P.sv));
    
    
    orbitParams.tp.solved = y0(1);
%     orbitParams.K.solved = y0();
%     orbitParams.w.solved = y0();
    orbitParams.ecc.solved = y0(2);
    orbitParams.RV_off.solved = y0(3);
%     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
    
elseif isnan(orbitParams.K.known) & ~isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 0101
    
    %     tp, K, e,  off
    x0 = [0, orbitParams.K.sv, orbitParams.ecc.sv,  0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, y0(3), orbitParams.t.known, y0(4), orbitParams.P.sv));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, y0(3), orbitParams.t.known, y0(4), orbitParams.P.sv));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), orbitParams.w.sv, y0(3), orbitParams.t.hi.known, y0(4), orbitParams.P.sv));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
%     orbitParams.w.solved = y0();
    orbitParams.ecc.solved = y0(3);
    orbitParams.RV_off.solved = y0(4);
%     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif ~isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 1010
    
    %     tp,w,  off,  P
    x0 = [0, orbitParams.w.sv,  0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.w.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.w.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), orbitParams.ecc.sv, t2, y0(3), y0(4)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), orbitParams.ecc.sv, t2, y0(3), y0(4)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), orbitParams.ecc.sv, t2, y0(3), y0(4)));
    
    
    orbitParams.tp.solved = y0(1);
%     orbitParams.K.solved = y0();
    orbitParams.w.solved = y0(2);
%     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(3);
    orbitParams.P.solved = y0(4);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
elseif isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & isnan(orbitParams.ecc.known) & isnan(orbitParams.P.known) % 0000
    
    %     tp, K, w,  e,  off,  P
    x0 = [0, orbitParams.K.sv, orbitParams.w.sv, orbitParams.ecc.sv,  0,  orbitParams.P.sv];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.K.lb, orbitParams.w.lb, orbitParams.ecc.lb, orbitParams.RV_off.lb, orbitParams.P.lb];
    ub = [orbitParams.tp.ub, orbitParams.K.ub, orbitParams.w.ub, orbitParams.ecc.ub, orbitParams.RV_off.ub, orbitParams.P.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), y0(4), t2, y0(5), y0(6)));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), y0(4), t2, y0(5), y0(6)));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), y0(2), y0(3), y0(4), t2, y0(5), y0(6)));
    
    
    orbitParams.tp.solved = y0(1);
    orbitParams.K.solved = y0(2);
    orbitParams.w.solved = y0(3);
    orbitParams.ecc.solved = y0(4);
    orbitParams.RV_off.solved = y0(5);
    orbitParams.P.solved = y0(6);
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
    
    
elseif ~isnan(orbitParams.K.known) & isnan(orbitParams.w.known) & ~isnan(orbitParams.ecc.known) & ~isnan(orbitParams.P.known) % 1011
    
    %    tp, w, off
    x0 = [0, orbitParams.w.sv, 0];
    
    t2 = orbitParams.t.known;
    %                                                                       tp, K, w, e, t, off, P
    
    lb = [orbitParams.tp.lb, orbitParams.w.lb, orbitParams.RV_off.lb];
    ub = [orbitParams.tp.ub, orbitParams.w.ub, orbitParams.RV_off.ub];
    func = @(y0) nanrms(orbitParams.RV.known - orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), orbitParams.ecc.sv, t2, y0(3), orbitParams.P.sv));
    
    [y0, ~] = solve3DMinimizationProblem_V2(...
        func, orbitParams.solverOptions.parP_q, orbitParams.solverOptions.MaxFevals, orbitParams.solverOptions.MaxIter, orbitParams.solverOptions.TolFun, orbitParams.solverOptions.TolX, graph_q, optimizationMatlabGraph_q, orbitParams.solverOptions.numRandPoints, x0, lb, ub, orbitParams.solverOptions.globalSolver_q);
    
    orbit_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), orbitParams.ecc.sv, t2, y0(3), orbitParams.P.sv));
    
    t2 = orbitParams.t.hi.known;
    orbit_hiR_mps = real(orbitCreator_Nbody_V1(y0(1), orbitParams.K.sv, y0(2), orbitParams.ecc.sv, t2, y0(3), orbitParams.P.sv));
    
    orbitParams.tp.solved = y0(1);
    %     orbitParams.K.solved = y0();
    orbitParams.w.solved = y0(2);
    %     orbitParams.ecc.solved = y0();
    orbitParams.RV_off.solved = y0(3);
    %     orbitParams.P.solved = y0();
    orbitParams.RV.fit = orbit_mps;
    orbitParams.RV.hi.fit = orbit_hiR_mps;
    
end
