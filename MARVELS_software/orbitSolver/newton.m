% Solves for eccentric anomaly, E from a given mean anomaly, M
% and eccentricty, e.  Performs a simple Newton-Raphson iteration
%
% M must be in radians and E is returned in radians.
%
% Default tolerances is 10^-8 rad.
%
% E = CalcEA(M,e) uses default tolerances
%
% E = CalcEA(M,e,tol) will use a user specified tolerance, tol
% 
% Richard Rieber
% 1/23/2005
% E-mail problems/suggestions rrieber@gmail.com

function E = newton(M,e)
tol=1e-9;
%Checking for user inputed tolerance
if nargin == 2
    %using default value
    tol = 10^-8;
elseif nargin > 3
    error('Too many inputs.  See help CalcE')
elseif nargin < 2
    error('Too few inputs.  See help CalcE')
end

Etemp = M;
ratio = 1;
it = 0;
done = 0;
while done == 0
    f_E = Etemp - e*sin(Etemp) - M;
    f_Eprime = 1 - e*cos(Etemp);
    ratio = f_E/f_Eprime;
    if abs(ratio) > tol
        Etemp = Etemp - ratio;
    else
        E = Etemp;
        done = 1;
    end
    it = it + 1;
    if it == 1000; done = 1; E = Etemp; end
end
% 
% function [eccentric] = newton(mean,ecc)
% 
% en=0;
% if mean<pi 
%     enplus1=mean+ecc;
% end
% if mean>pi 
%     enplus1=mean-ecc;
% end
% mean
% enplus1
% if mod(mean,pi)~=0
%     while abs(enplus1-en)>1e-9
%         enplus2=enplus1-( (mean-enplus1+ecc*sin(enplus1))/(-1+ecc*cos(enplus1)) );
%         en=enplus1;
%         enplus1=enplus2;
%     end
%     eccentric=enplus1;
% else
%     eccentric=mean;
% end
