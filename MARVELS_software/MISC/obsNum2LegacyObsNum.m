function obsNumStr = obsNum2LegacyObsNum(obsNum)


if obsNum<10; obsNumStr = strcat('0', num2str(obsNum));else obsNumStr = num2str(obsNum);end