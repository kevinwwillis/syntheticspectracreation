function beamNumStr = beamNum2legacyBeamNumStr(beamNum)


beamNumStr = num2str(beamNum); 

if beamNum < 100
    beamNumStr = strcat('0', num2str(beamNum));
end

if beamNum < 10
    beamNumStr = strcat('00', num2str(beamNum));
end


