function output0 = normScale(input0, min0, max0)

% Normalize input data to a scale of the given range


if isempty(setdiff(input0, input0(1)))
    
    output0 = ones(size(input0)) * min0;
    
else
    
    if size(input0,1) == 1 | size(input0,2) == 1
        
        output0 = ( input0 - min(input0) ) .* ( max0 - min0 ) ./ ( max(input0) - min(input0) ) + min0;
        
    else
        minUni = min(input0(:));
        maxUni = max(input0(:));
        output0 = ( input0 - minUni ) .* ( max0 - min0 ) ./ ( maxUni - minUni ) + min0;
    end
end

