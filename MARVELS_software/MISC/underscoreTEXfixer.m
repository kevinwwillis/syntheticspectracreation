function fixedString = underscoreTEXfixer(string_0)

% Fixes issue where underscores are interpreted as a subscript action

fixedString = strrep(string_0, '_', '\_');

