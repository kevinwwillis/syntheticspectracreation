function [out]=moveavg(in,radius,minmax)
%Determines the moving average of a vector
%Neil Thomas
%25Mar14
%in - input vector
%out - filtered output vector
%radius - +- to define window
%minmax 1 to reject min and max in window

out=nan*ones(size(in));

for i=1+radius:length(in)-radius
    block= in(i-radius:i+radius);
    if minmax
        ind=find(block==max(block));
        block(ind)=[];
        ind=find(block==min(block));
        block(ind)=[];
    end
    out(i)=nanmean(block);
end

out(1:radius)=out(1+radius)*ones(1,radius);
out(length(in)-radius+1:length(in))=out(length(in)-radius)*ones(1,radius);

