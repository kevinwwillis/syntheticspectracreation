function str = n2s(varargin)


%str = num2str(num);

if length(varargin) == 1
    
    str = num2str(varargin{1});
    
elseif length(varargin) == 2
    
    str = num2str(roundsd(varargin{1}, varargin{2}));
    
elseif length(varargin) == 3
    
    str = num2str(roundsd(varargin{1}, varargin{2}, varargin{3}));
    
elseif length(varargin) > 3
    
    disp('ERROR: Too many inputs into function "n2s()"')
    
end





