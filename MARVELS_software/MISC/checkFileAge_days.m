function dayDiff = checkFileAge_days(obsNums, search_dir)


now_date = datetime;



b = dir(search_dir);


if length(obsNums) == size(b, 1)
    
    dayDiff = cell2mat(arrayfun(@(x) days(now_date - datetime(b(x).date)), obsNums, 'uniformoutput', 0));
    
else
    
    dayDiff = nan(1, length(obsNums));
end