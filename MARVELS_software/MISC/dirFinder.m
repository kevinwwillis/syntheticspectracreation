function [out_dir, fileName] = dirFinder(pipeline_dir, plateName, beamNum, obsNum, dataClass, fileType, obsType, procStage, dataDimension, dataUnitType, dirVersion, includeFileName_q, createDir_q)

% Returns the directory of the input filetype


warning('off','all')

s = dslash;

data_dir = [pipeline_dir, s, 'MARVELS_data', s, dataClass];


obsType = lower(obsType);
% procStage = lower(procStage);
% dataDimension = lower(dataDimension);

errPush = 0;

if isempty(pipeline_dir) | isempty(fileType) | isempty(dataDimension) | isempty(dirVersion)
    
    errPush = 1;
end

% if isempty(plateName)
%     
%     if isempty(beamNum) | isempty(obsNum)
%         
%         errPush = 1;
%     end
% end


if errPush
    
    error(cat(2, '****** ERROR: An important input to the script "dirFinder" was empty. Program halted in debug mode.'))
    
    dbstop in dirFinder if error
end


%% Filename

fileName = [];

if includeFileName_q
    
%     if isempty(beamNum) & isempty(obsNum) & isempty(obsType)
%         
%         fileName = [plateName, '_', procStage, '_', dataDimension, '_', dataUnitType];
%         
%     elseif isempty(obsNum) & isempty(obsType)
%         
%         fileName = [plateName, '_beam', n2s(beamNum), '_', procStage, '_', dataDimension];
%         
%     elseif isempty(obsType)
%         
%         fileName = [plateName, '_beam', n2s(beamNum), '_obs', n2s(obsNum), '_', procStage, '_', dataDimension];
%         
%     elseif isempty(beamNum) & isempty(obsNum)
%         
%         fileName = [obsType, '_', plateName, '_', procStage, '_', dataDimension];
%         
%     elseif isempty(obsNum)
%         
%         fileName = [obsType, '_', plateName, '_beam', n2s(beamNum), '_', procStage, '_', dataDimension];
%         
%     else
%         
%         fileName = [obsType, '_', plateName, '_beam', n2s(beamNum), '_obs', n2s(obsNum), '_', procStage, '_', dataDimension];
%     end
    
    
    if ~isempty(dataClass)
        fileName = [fileName, dataClass];
    end
    
    if ~isempty(obsType)
        fileName = [fileName, '_', obsType];
    end
    
    if ~isempty(plateName)
        fileName = [fileName, '_', plateName];
    end
    
    if ~isempty(beamNum)
        fileName = [fileName, '_beam', n2s(beamNum)];
    end
    
    if ~isempty(obsNum)
        fileName = [fileName, '_obs', n2s(obsNum)];
    end
    
    if ~isempty(procStage)
        fileName = [fileName, '_', procStage];
    end
    
    if ~isempty(dataDimension)
        fileName = [fileName, '_', dataDimension];
    end
    
    if ~isempty(dataUnitType)
        fileName = [fileName, '_', dataUnitType];
    end
    
    fileName = [fileName , '.', fileType];
    
    if strcmp(fileName(1), '_')
        fileName(1) = [];
    end
end



%% Folder

if dirVersion == 3 %% New (v3) Dir Structure
    
%     if includeFileName_q
        
        out_dir = data_dir;
        
        if ~isempty(procStage)
            out_dir = [out_dir, s, procStage];
        end
        
        if ~isempty(plateName)
            out_dir = [out_dir, s, plateName];
        end
        
        if ~isempty(beamNum)
            out_dir = [out_dir, s, 'beam', n2s(beamNum)];
        end

        
        %out_dir = [data_dir, s, procStage, s, plateName, s, 'beam', n2s(beamNum), s, fileName];
        
%     else
%         
%         out_dir = [data_dir, s, procStage, s, plateName, s, 'beam', n2s(beamNum)];
%     end
    
    
elseif dirVersion == 2 %% Old (v2) Dir Structure
    
    
    
    
elseif dirVersion == 1 %% Legacy (v1) Dir Structure
    
end


%%

% Create the directory
if createDir_q
    
   mkdir(out_dir) 
end

% Add filename to the output directory string
if ~isempty(fileName)
    out_dir = [out_dir, s, fileName];
end



















