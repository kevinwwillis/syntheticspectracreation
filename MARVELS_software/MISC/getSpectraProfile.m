function [smoothedProfile, smoothedProfileNorm] = getSpectraProfile(spectrum0, plot_q)


for rowNum = 1:size(spectrum0, 1)
    
    validCols = find(spectrum0(rowNum, :) ~=0 & ~isnan(spectrum0(rowNum, :)));
    
    
    spectraTMP = ([repmat(spectrum0(rowNum, validCols(1)), 1, length(1:validCols(1)-1)), ...
        spectrum0(rowNum, validCols), ...
        repmat(spectrum0(rowNum, validCols(end)), 1, length(validCols(end)+1:size(spectrum0,2)))]);
    
    
    %spectraTMP = medfilt1(spectraTMP(1, :), 40);
    spectraTMP = medfilt2(spectraTMP(1, :), [1, 40]);
    
%     spectraTMP(1, validCols) = spectraTMP_tmp(1, validCols);
    
%     spectraTMP = ([repmat(spectraTMP(rowNum, validCols(1)), 1, length(1:validCols(1)-1)), ...
%         spectraTMP(rowNum, validCols), ...
%         repmat(spectraTMP(rowNum, validCols(end)), 1, length(validCols(end)+1:size(spectraTMP,2)))]);
    
    smoothedProfile(rowNum, :) = polyval(polyfit(1:4001, spectraTMP, 4), 1:4001);
%     smoothedProfile(rowNum, :) = feval(fit((1:4001)', spectraTMP', 'smoothingspline', 'SmoothingParam', 0.000001), 1:4001)';
    
if plot_q    
    figure(1); clf(figure(1)); hold on; plot(1:4001, spectraTMP, '-k', 1:4001, smoothedProfile(rowNum, :), ':m')
end
end

smoothedProfileNorm = normScale(smoothedProfile, 1, 10);

%save('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfile', 'smoothedProfileNorm')


if plot_q
    figure(21); clf(figure(21));
    
    subplot(211); hold on
    titlel('Spectrum')
    mesh(spectrum0)
%     contour3(spectrum0)
    view(-124, 38)
    axis([1 size(spectrum0, 2) 1 size(spectrum0, 1), -inf, inf])
    grid on
    hold off
    
    subplot(212); hold on
    titlel('Spectrum: Smoothed')
    mesh(smoothedProfile)
%     contour3(smoothedProfile)
    view(-124, 38)
    axis([1 size(spectrum0, 2) 1 size(spectrum0, 1), -inf, inf])
    grid on
    hold off
    
%     subplot(313); hold on
%     titlel('Spectrum: Smoothed and Normalized')
% %     mesh(smoothedProfileNorm)
%     contour3(smoothedProfileNorm)
%     view(-124, 38)
%     axis([1 size(spectrum0, 2) 1 size(spectrum0, 1), -inf, inf])
%     grid on
%     hold off
end




