function [ out ] = nanrms( in )
%provides rms based on real numbers

out=rms(in(~isnan(in)));


end

