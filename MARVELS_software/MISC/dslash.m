function s = dslash()


dir0 = pwd;

if ~isempty(strfind(dir0, '\'))
    
   s = '\';
   
elseif ~isempty(strfind(dir0, '/'))
    
   s = '/'; 
   
else
    
    OS = computer;
    
    if strcmp(OS, 'PCWIN64') || strcmp(OS, 'PCWIN')
        
        s = '\';
        
    else
        
        s = '/';
    end
end







