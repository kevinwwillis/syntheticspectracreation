function X2 = chiSqr(real, fit, degFreedom)

X2 = (real - fit).^2 ./ std([fit ; real], 1);

X2 = sum( X2(:) ) / degFreedom;