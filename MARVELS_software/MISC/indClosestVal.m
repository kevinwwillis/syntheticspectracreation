function ind = indClosestVal(range0, val0)

ind = find(min(abs(range0 - val0)) == abs(range0 - val0));

if ~isempty(ind)
    ind = ind(1);
end