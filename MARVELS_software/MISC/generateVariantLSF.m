function [PSF_chunks, R, wl_center_ang] = generateVariantLSF(lowRes, highRes, wl_hiR_ang, chunkWidth)

% Generate a 2D array of PSFs, one for each column (typically 4001 in MARVELS)



% Testing params
% lowRes = 10000;
% highRes = 30000;
% wl_loR_ang = wl_example_m * 1e10;
% wl_hiR_ang = wl_m * 1e10;
% colCount = size(combImg, 2);

xM_hiR = size(wl_hiR_ang, 2);

chunkCount = floor((xM_hiR - mod(xM_hiR, 2)) / chunkWidth);
chunkLeftEdges = 1:chunkWidth:(xM_hiR - mod(xM_hiR, 2));
chunkLeftEdges(end) = [];
chunkRightEdges = chunkLeftEdges + chunkWidth - 1;
chunkCenters = round((chunkRightEdges(1) - chunkLeftEdges(1)) / 2 + chunkLeftEdges);

R = linspace(lowRes, highRes, chunkCount);

dWL_hiR_dPX = nanmean(diff(wl_hiR_ang));

wl_center_ang = wl_hiR_ang(chunkCenters);

PSF_chunks = nan(chunkCount, 901);


% parfor colNum = 1:colCount;
for chunkNum = 1:chunkCount
    
    PSF_chunks(chunkNum, :) = createLSF_R(dWL_hiR_dPX, wl_center_ang(chunkNum), R(chunkNum));
end



% for rowNum = 1:size(PSF, 2)
%     
% %     figure(88); clf(figure(88)); hold on
% %     plot(colNums, PSF(colNums, rowNum), '.k'); 
%     
%    PSF(:, rowNum) = interp1(colNums, PSF(colNums, rowNum), 1:colCount, 'spline');
%    
% %    plot(1:colCount, PSF(:, rowNum), ':m');
% %    pause()
% end
% 
% % [X, Y] = meshgrid(1:size(PSF, 1), 1:size(PSF, 2));
% % PSF_interpolant = griddedInterpolant(X', Y', PSF);
% 
% figure(9); mesh(PSF(round(linspace(10, colCount, 920)), :))








