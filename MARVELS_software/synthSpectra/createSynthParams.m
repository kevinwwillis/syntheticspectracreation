function [wl_ang, plateName, BCV_beam_mps, RV_BCV_mps, dX_id_px] = createSynthParams(...
    s, pipeline_dir, templatePlateName, beamNum, orbitPeriod_day, maxRV_mps, RV_mps, obsNums, modelObsNum, JDs, ...
    copyReal_RVs_q, copyReal_BCVs_q, forced_BCVs_q, BCVs_mps, ...
    forcedInstrumentDrifts_q, forcedInstrumentDrifts_px, ...
    BCVmax_mps, spectraQuantity, surveyDaySpan_day, plateName, applyInstDrift_q, makeBCV_q, linearWLsol_q, makeBinarySpectra_q)




% %% Create directory structure and save directory paths as variables
% MARVELS_directories(pipeline_dir, plateName, '');
% 
% 
% %% Load needed directory names
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, plateName, '_directories.mat'))


%% Load and set the wavelengths for these spectra
load(dirFinder(pipeline_dir, [], [], [], 'WL', 'mat', [], [], 'beam_WL_estimates_legacy', 'ang', 3, 1, 0), 'wlsoln')
wl_ang = squeeze(wlsoln(:, 1, beamNum))';

if linearWLsol_q
    wl_ang = linspace(wl_ang(1), wl_ang(end), 4001);
end


%% Create model definition array and save
% model_dir = dirFinder(pipeline_dir, plateName, [], [], 'spectra', 'mat', [], 'reference', 'observation', 'num', 3, 1, 1);
% if 0 == exist(model_dir, 'file')
    model = nan(1, 120);
% else
%     load(model_dir, 'model')
% end
model(beamNum) = modelObsNum;
save(dirFinder(pipeline_dir, plateName, [], [], 'spectra', 'mat', [], 'reference', 'observation', 'num', 3, 1, 1), 'model')


%% Copy over a required file from real results
% magFile = strcat(pipeline_orig_dir, s, '47UMA', s, 'results', s, 'mag.mat');
% destMagFile2 = strcat(general_dir, s, 'mag.mat');
% copyfile(magFile, destMagFile2);
order2mag = nan(120, obsNums(end));
save(dirFinder(pipeline_dir, plateName, [], [], 'stellarParams', 'mat', [], [], 'beam2_Vmag_legacy', 'mag', 3, 1, 1), 'order2mag')
% save(destMagFile2, mag)



%% Create and save Barycentric velocity data

BCV_mps = nan(120, max(obsNums));

if copyReal_BCVs_q
    
    load(dirFinder(pipeline_dir, templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 0), 'bc')
    
    BCV_mps(beamNum, :) = bc(beamNum, :);
    
elseif forced_BCVs_q
    
    BCV_mps(beamNum, :) = BCVs_mps;
    
elseif makeBCV_q
    
    BCV_mps(beamNum, :) = -BCVmax_mps + 2 * BCVmax_mps * rand(1, length(obsNums));
    
    BCV_mps(beamNum, 1) = 0;
    
else
    
    BCV_mps(beamNum, :) = 0;
end

bc = BCV_mps;

BCV_beam_mps = BCV_mps(beamNum, :);

save(dirFinder(pipeline_dir, plateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 1),'bc')


%% Create and save observation date data

% obsDate_MJD = sort(randperm(surveyDaySpan_day, spectraQuantity), 'ascend');
% obsDate_MJD = (orbitPeriod_day - obsDate_MJD(1)) + obsDate_MJD;
% MJDobs = obsDate_MJD;

if copyReal_BCVs_q & copyReal_RVs_q
    
    load(dirFinder(pipeline_dir, templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 1), 'MJDobs')
    
    obsDate_MJD = MJDobs(:, 2)';
else
    
    if isempty(JDs)
        MJDobs = linspace(1, surveyDaySpan_day, max(obsNums))';
        obsDate_MJD = MJDobs;
    else
        MJDobs = JDs(:);
        obsDate_MJD = JDs(:);
    end
end
% if 0 == exist(strcat(general_dir, s, 'MJDobs.mat'), 'file')
%     MJDobs = nan(1, max(obsNums));
% else
%     load(strcat(general_dir, s, 'MJDobs.mat'), 'MJDobs')
% end
% BCV_mps(beamNum, 1:length(BCV_beam_mps)) = BCV_beam_mps;
% save(strcat(general_dir, s, 'MJDobs.mat'), 'MJDobs')
save(dirFinder(pipeline_dir, plateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 1),'MJDobs')


%% Load precalculated spectral line position and intensity data
% % load(strcat(oneDpipelineDir, s, basisPlate, s, 'wlsoln', s, 'intermediate', s, 'STAR_linelist.mat'), 'CENTERS')
% load(strcat(synthPipelineDir, s, 'lineCenters_HIP14810_STAR.mat'), 'spectraLinePos_px')
% spectralLineCenters_px_star = spectraLinePos_px;
% load(cat(2, synthPipelineDir, s, 'lineIntensitiesAndModels_HIP14810_STAR.mat'), 'model', 'lineIntensities')
% lineIntensities_star = lineIntensities;

% spectralLineCenters_px_star_orig = sort(randperm(4001, 400), 'ascend');
% spectralLineCenters_px_star_orig = 10 :20: 3991;
% lineIntensities_star_orig = ones(1, length(spectralLineCenters_m_star_orig));

% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_WL_ang.mat', 'WL_ang')
% wl_sun_m = WL_ang' * 1e-10;
% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')
% flux_sun_pxv = flux_pxv';
% 
% % Hi-res sun data WL granularity
% dWL_dPX = 0.002;


% Create instrument drift data
if applyInstDrift_q
    
    if forcedInstrumentDrifts_q
        
        dX_id_px = forcedInstrumentDrifts_px;
        
    else
        
        dX_id_px = normrnd(-1, 1, 1, spectraQuantity);
        dX_id_px(:,1) = 0;
    end
    
else
    
    dX_id_px = zeros(1, spectraQuantity);
end

% % Load precalculated spectral line position and intensity data
% % load(strcat(oneDpipelineDir, s, basisPlate, s, 'wlsoln', s, 'intermediate', s, 'TIO_linelist.mat'), 'CENTERS')
% load(strcat(synthPipelineDir, s, 'lineCenters_HIP14810_TIO.mat'), 'spectraLinePos_px')
% spectralLineCenters_px_tio = spectraLinePos_px; clear spectraLinePos_px
% load(cat(2, synthPipelineDir, s, 'lineIntensitiesAndModels_HIP14810_TIO.mat'), 'model', 'lineIntensities')
% lineIntensities_tio = lineIntensities;


%% Create and save radial velocity data


if makeBinarySpectra_q & isempty(RV_mps)
    
    % RV_mps = [randi([-1000 1000], 1, length(dX_id_px)) ; randi([-1000 1000], 1, length(dX_id_px))];
    RV_mps = [100000 * ones(1, length(dX_id_px)) ; 0 * ones(1, length(dX_id_px))];
    
    RV_BCV_mps = RV_mps + repmat(BCV_mps(beamNum, :), 2, 1);
    
    
elseif makeBinarySpectra_q & ~isempty(RV_mps)
    
    RV_BCV_mps = RV_mps + repmat(BCV_mps(beamNum, :), 2, 1);
    
elseif copyReal_RVs_q
    
    if (beamNum == 53 || beamNum == 54) && strcmp(templatePlateName, 'HD118203')
        
        load(strcat(pipeline_dir, s, 'MARVELS_data', s, 'RV', s, 'actual', s, 'HIRES_HD.mat'))
        RV_mps = HIRES - nanmean(HIRES);
        
    elseif (beamNum == 101 || beamNum == 102) && strcmp(templatePlateName, 'HIP14810')
        
        load(strcat(pipeline_dir, s, 'MARVELS_data', s, 'RV', s, 'actual', s, 'HIRES.mat'))
        RV_mps = HIRES - nanmean(HIRES);
    end
    
    RV_BCV_mps = RV_mps + BCV_mps(beamNum, :);
    
else
    
    if isempty(RV_mps)
        
        RV_mps = maxRV_mps * sin(2 * pi / orbitPeriod_day * obsDate_MJD);
    end
    
    RV_BCV_mps = RV_mps + BCV_mps(beamNum, :);
end








