function synthPlateCreationFunction_v2_0_1(A)

% create a typical plate of spectra synthetically with their own unique parameters


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s = dslash;

load(strcat('synthParamsFileTemplate.mat'), 'synthParams')



%% Create plate name

if A.options.forcedPlateName
    
    A.plateName = A.plateName_forced;
    
else
    
    A.plateName = synthPlateNameCreation(A);
end



%% Set up lo-res spatial dimensions

x_lo_px = 1:A.spectrumWidth_px;

y_lo_px = 1:A.spectrumHeight_px;



%% Determine Spectra Quantity

if A.options.spectraQuantity ~= 0
    
    obsNums = 1:A.options.spectraQuantity;
    
else
    % Load BCV array to determine how many observations were used
    if 0 == exist(dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 0), 'file')
        
        if 0 == exist(A.dir.neil_dir, 'dir')
            error(cat(2, 'A local copy of the BCV values for ', A.options.templatePlateName, ' does not exist. The remote directory cannot be reached to download the proper file ("bc.mat"). Check remote connection.'))
        else
            getNeilFile(s, dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 1), strcat(A.dir.neil_dir, s, A.options.templatePlateName, s, 'results', s, 'bc.mat'))
        end
    end
    
    load(dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 0), 'bc');
    
    A.options.spectraQuantity = size(bc, 2);
    
    obsNums = 1:A.options.spectraQuantity;
end



%% Determine model observation #

% % Load model observation number
% if 0 == exist(strcat(A.dir.pipeline_dir, s, 'plates', s, A.options.templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'file')
%     getNeilFile(s, strcat(A.dir.pipeline_dir, s, 'plates', s, A.options.templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), strcat(A.dir.neil_dir, s, A.options.templatePlateName, s, 'results', s, 'model.mat'))
% end
% load(strcat(A.dir.pipeline_dir, s, 'plates', s, A.options.templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'model');
%
% modelObsNum_orig = model(A.options.templateBeamNum); clear model

% Is new model obs number being forced?
if A.options.forceNewModelObs
    
    A.modelObs = A.modelObs;
else
    
    A.modelObs = 1;
end



%% WL Dispersion Load/Creation

if A.options.WL.dispersion.addDispersion
    
    %     [dWL_dispersion_star_ang, dWL_dispersion_tio_ang, badWLsolutions_star, badWLsolutions_tio, A.options.spectraQuantity] = createDispersionData(...
    %         s, A.dir.pipeline_dir, A.dir.nolan_dir, A.dir.neil_dir, A.plateName, A.options.templatePlateName, A.options.templateBeamNum, A.options.spectraQuantity, A.options.WL.dispersion.dispersionLimit_ang, ...
    %         A.options.WL.dispersion.applyTIO_dispersion, A.options.WL.dispersion.applyStar_dispersion, A.options.WL.useNolanWLsolutions, A.options.WL.useKevinWLsolutions, A.options.WL.useSynthWLsolutions, A.options.WL.dispersion.setDispProfZeroAt2000px, A.options.WL.dispersion.correctStarDispByTIOdispOffsets, ...
    %         A.options.WL.dispersion.meanCenterDispersionTIO, A.options.WL.dispersion.meanCenterDispersionStar, A.modelObs);
    
    
    dWL_dispersion_star_ang(isnan(dWL_dispersion_star_ang)) = 0;
    dWL_dispersion_tio_ang(isnan(dWL_dispersion_tio_ang)) = 0;
    
else
    
    badWLsolutions_tio = [];
    badWLsolutions_star = [];
    
    dWL_dispersion_star_ang = [];
    dWL_dispersion_tio_ang = [];
end

% Recalibrate input RVs array
if isempty(A.options.orbitParams.RV_mps)
    
    A.options.orbitParams.RV_mps = zeros(1, size(A.options.spectraQuantity, 1));
end



%% Load and set the wavelengths for these spectra

load(dirFinder(A.dir.pipeline_dir, [], [], [], 'WL', 'mat', [], [], 'beam_WL_estimates_legacy', 'ang', 3, 1, 0), 'wlsoln')

wl_lo_ang = squeeze(wlsoln(:, 1, A.options.templateBeamNum))';

if A.options.WL.linearWLsol
    
    wl_lo_ang = linspace(wl_lo_ang(1), wl_lo_ang(end), 4001);
end

wl_lo_orig_ang = wl_lo_ang;



%% Create model definition array and save

% model_dir = dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'spectra', 'mat', [], 'reference', 'observation', 'num', 3, 1, 1);
% if 0 == exist(model_dir, 'file')
    model = nan(1, 120);
% else
%     load(model_dir, 'model')
% end
model(A.options.templateBeamNum) = A.modelObs;
save(dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'spectra', 'mat', [], 'reference', 'observation', 'num', 3, 1, 1), 'model')



%% Copy over a required file from real results

% magFile = strcat(pipeline_orig_dir, s, '47UMA', s, 'results', s, 'mag.mat');
% destMagFile2 = strcat(general_dir, s, 'mag.mat');
% copyfile(magFile, destMagFile2);
order2mag = nan(120, obsNums(end));

save(dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'stellarParams', 'mat', [], [], 'beam2_Vmag_legacy', 'mag', 3, 1, 1), 'order2mag')
% save(destMagFile2, mag)



%% Create and save Barycentric velocity data

if A.options.orbitParams.forced_BCVs
    
    BCV_orig = A.options.orbitParams.A.options.orbitParams.BCV_mps;
    
    BCV_mps = nan(120, max(obsNums));
    
    BCV_mps(A.options.templateBeamNum, :) = BCV_orig;
    
else
    
    BCV_mps = nan(120, max(obsNums));
    
    if A.options.orbitParams.copyRealBCVs
        
        load(dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 0), 'bc')
        
        BCV_mps(A.options.templateBeamNum, :) = bc(A.options.templateBeamNum, :);
        
    elseif A.options.orbitParams.randomBCV
        
        BCV_mps(A.options.templateBeamNum, :) = -A.options.orbitParams.BCVmax_mps + 2 * A.options.orbitParams.BCVmax_mps * rand(1, length(obsNums));
        
        BCV_mps(A.options.templateBeamNum, 1) = 0;
        
    else
        
        BCV_mps(A.options.templateBeamNum, :) = 0;
    end
end

bc = BCV_mps;

BCV_mps = BCV_mps(A.options.templateBeamNum, :);

save(dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 1),'bc')



%% Create and save observation date data

% obsDate_MJD = sort(randperm(A.options.orbitParams.surveyTimeSpan_day, A.options.spectraQuantity), 'ascend');
% obsDate_MJD = (A.options.orbitParams.orbitPeriod_day - obsDate_MJD(1)) + obsDate_MJD;
% MJDobs = obsDate_MJD;

if A.options.orbitParams.copyRealBCVs & A.options.orbitParams.copyRealRVs
    
    load(dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 1), 'MJDobs')
    
    obsDate_MJD = MJDobs(:, 2)';
else
    
    if isempty(A.JDs)
        MJDobs = linspace(1, A.options.orbitParams.surveyTimeSpan_day, max(obsNums))';
        obsDate_MJD = MJDobs;
    else
        MJDobs = A.JDs(:);
        obsDate_MJD = A.JDs(:);
    end
end
% if 0 == exist(strcat(general_dir, s, 'MJDobs.mat'), 'file')
%     MJDobs = nan(1, max(obsNums));
% else
%     load(strcat(general_dir, s, 'MJDobs.mat'), 'MJDobs')
% end
% BCV_mps(A.options.templateBeamNum, 1:length(BCV_beam_mps)) = BCV_beam_mps;
% save(strcat(general_dir, s, 'MJDobs.mat'), 'MJDobs')
save(dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 1),'MJDobs')



%% Load precalculated spectral line position and intensity data

% % load(strcat(oneDpipelineDir, s, basisPlate, s, 'wlsoln', s, 'intermediate', s, 'STAR_linelist.mat'), 'CENTERS')
% load(strcat(synthPipelineDir, s, 'lineCenters_HIP14810_STAR.mat'), 'spectraLinePos_px')
% spectralLineCenters_px_star = spectraLinePos_px;
% load(cat(2, synthPipelineDir, s, 'lineIntensitiesAndModels_HIP14810_STAR.mat'), 'model', 'lineIntensities')
% lineIntensities_star = lineIntensities;

% spectralLineCenters_px_star_orig = sort(randperm(4001, 400), 'ascend');
% spectralLineCenters_px_star_orig = 10 :20: 3991;
% lineIntensities_star_orig = ones(1, length(spectralLineCenters_m_star_orig));

% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_WL_ang.mat', 'WL_ang')
% wl_sun_m = WL_ang' * 1e-10;
% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\sun_flux_pxv.mat', 'flux_pxv')
% flux_sun_pxv = flux_pxv';
% 
% % Hi-res sun data WL granularity
% dWL_dPX = 0.002;


% Create instrument drift data
if A.options.drift.applyInstrumentDrift
    
    if A.options.drift.forcedInstrumentDrifts
        
        dX_id_px = A.options.drift.drifts_px;
        
    else
        
        dX_id_px = normrnd(-1, 1, 1, A.options.spectraQuantity);
        dX_id_px(:,1) = 0;
    end
    
else
    
    dX_id_px = zeros(1, A.options.spectraQuantity);
end

% % Load precalculated spectral line position and intensity data
% % load(strcat(oneDpipelineDir, s, basisPlate, s, 'wlsoln', s, 'intermediate', s, 'TIO_linelist.mat'), 'CENTERS')
% load(strcat(synthPipelineDir, s, 'lineCenters_HIP14810_TIO.mat'), 'spectraLinePos_px')
% spectralLineCenters_px_tio = spectraLinePos_px; clear spectraLinePos_px
% load(cat(2, synthPipelineDir, s, 'lineIntensitiesAndModels_HIP14810_TIO.mat'), 'model', 'lineIntensities')
% lineIntensities_tio = lineIntensities;



%% Create and save radial velocity data

if A.options.spectrum.type.makeBinarySpectra & isempty(A.options.orbitParams.RV_mps)
    
    % RV_mps = [randi([-1000 1000], 1, length(dX_id_px)) ; randi([-1000 1000], 1, length(dX_id_px))];
    A.options.orbitParams.RV_mps = [100000 * ones(1, length(dX_id_px)) ; 0 * ones(1, length(dX_id_px))];
    
    RV_BCV_mps = A.options.orbitParams.RV_mps + repmat(BCV_mps(A.options.templateBeamNum, :), 2, 1);
    
    
elseif A.options.spectrum.type.makeBinarySpectra & ~isempty(A.options.orbitParams.RV_mps)
    
    RV_BCV_mps = A.options.orbitParams.RV_mps + repmat(BCV_mps(A.options.templateBeamNum, :), 2, 1);
    
elseif A.options.orbitParams.copyRealRVs
    
    if (A.options.templateBeamNum == 53 || A.options.templateBeamNum == 54) && strcmp(A.options.templatePlateName, 'HD118203')
        
        load(strcat(A.dir.pipeline_dir, s, 'MARVELS_data', s, 'RV', s, 'actual', s, 'HIRES_HD.mat'))
        A.options.orbitParams.RV_mps = HIRES - nanmean(HIRES);
        
    elseif (A.options.templateBeamNum == 101 || A.options.templateBeamNum == 102) && strcmp(A.options.templatePlateName, 'HIP14810')
        
        load(strcat(A.dir.pipeline_dir, s, 'MARVELS_data', s, 'RV', s, 'actual', s, 'HIRES.mat'))
        A.options.orbitParams.RV_mps = HIRES - nanmean(HIRES);
    end
    
    RV_BCV_mps = A.options.orbitParams.RV_mps + BCV_mps(A.options.templateBeamNum, :);
    
else
    
    if isempty(A.options.orbitParams.RV_mps)
        
        A.options.orbitParams.RV_mps = A.options.orbitParams.maxRV_mps * sin(2 * pi / A.options.orbitParams.orbitPeriod_day * obsDate_MJD);
    end
    
    RV_BCV_mps = A.options.orbitParams.RV_mps + BCV_mps(A.options.templateBeamNum, :);
end


if A.options.spectrum.type.makeBinarySpectra
    
    RV_star1_BCV_mps = RV_BCV_mps(1, :);
    RV_star2_BCV_mps = RV_BCV_mps(2, :);
end



%% Load hi-res Sun and TIO spectra sampled at the wl range of the comb img

% Star
if A.options.spectrum.type.singleLineSpectrum
    
    load([A.dir.pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'templates', s, 'star_master_synth_template.mat'], 'wl_ang')
    
    lineLoc_ind = indClosestVal(wl_lo_ang, A.options.spectrum.type.singleLine.lineLoc_ang);
    
    if A.spectrumWidth_px ~= 4001
        
        new_ind = (1:A.spectrumWidth_px) + lineLoc_ind;
        
        new_ind = new_ind - round(A.spectrumWidth_px / 2);
        
        wl_lo_ang = wl_lo_ang(new_ind);
    end
    
    gi = wl_ang > (wl_lo_ang(1) - 10) & wl_ang < (wl_lo_ang(end) + 10);
    
    wl_ang = wl_ang(gi);
    
    lineLoc_ind = indClosestVal(wl_ang, A.options.spectrum.type.singleLine.lineLoc_ang);
    
    wl_hi_orig_ang = wl_lo_orig_ang(1) :0.01: wl_lo_orig_ang(end);
    
    % Create a single spectral line
    singleLine_adu = ones(size(wl_ang));
    
    singleLine_adu(lineLoc_ind) = A.options.spectrum.type.singleLine.lineIntensity_adu;

    %  qf(10, 1); plot(wl_ang, singleLine_adu, '-k'); xlim([wl_ang(1) wl_ang(end)]); grid on
    
    flux_adu = singleLine_adu;
    
    1;
else
    
    load([A.dir.pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'templates', s, 'star_master_synth_template.mat'], 'flux_adu', 'wl_ang')
    
    gi = wl_ang > (wl_lo_ang(1) - 10) & wl_ang < (wl_lo_ang(end) + 10);
    
    wl_ang = wl_ang(gi);
    flux_adu = flux_adu(gi);
    
    wl_ang = wl_ang(1:10:end);
    flux_adu = flux_adu(1:10:end);
    
    wl_hi_orig_ang = [];
end


wl_hi_ang = wl_ang(:)';
flux_hiR_star_adu = flux_adu(:)';

clear wl_ang
clear flux_adu


% TIO
load([A.dir.pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'templates', s, 'tio_template_R300000_marvelsRange.mat'], 'flux_adu', 'wl_ang')

flux_hiR_tio_adu = interp1(wl_ang(:)', flux_adu(:)', wl_hi_ang, 'spline', nan);

clear wl_ang
clear flux_adu



%% Load master comb and its params

% load([A.dir.pipeline_dir, s, 'MARVELS_data', s, 'comb', s, 'templates', s, 'combParamEstimates_b101.mat'], 'wl_2_epsilon_F', 'wl_2_d0_pp')

y_hi_px = 1 :0.1: A.spectrumHeight_px;

[WL_hi_ang, Y_hi_px] = meshgrid(wl_hi_ang, y_hi_px);


% ep = wl_2_epsilon_F({y_hi_px, wl_hi_ang * 1e-10});
ep = 6.3e-8 * ones(length(y_hi_px), length(wl_hi_ang));

% d0 = ppval(wl_2_d0_pp, wl_hi_ang * 1e-10);
% d0 = linspace(7.03, 7.01, length(wl_hi_ang)) * 1e-3;
d0 = 0.007 * ones(1, length(wl_hi_ang));
% bi = wl_2_d0_pp.breaks(1) > wl_hi_ang * 1e-10;
% d0(bi) = nan;

d0 = repmat(d0, length(y_hi_px), 1);

flux_hi_comb_adu = (1 + cos(2 * pi * (d0 + ep .* Y_hi_px) ./ (WL_hi_ang * 1e-10)) ) / 2;

clear ep d0

% qf(1,1); imagesc(flux_hi_comb_adu)

% Rename comb flux
if A.options.GPU_calulations
    flux_hi_comb_adu = gpuArray(flux_hi_comb_adu);
end

load(dirFinder(A.dir.pipeline_dir, [], [], [], 'WL', 'mat', [], [], 'beam_WL_estimates_legacy', 'ang', 3, 1, 0), 'wlsoln')

wl_ang = squeeze(wlsoln(:, 1, A.options.templateBeamNum))';

period_px = ( wl_ang * 1e-10 / ( 6.3e-8 ));



%% Restructure beam WL array and convert it to logspace

wl_lo_tio_ang = wl_lo_ang;

wl_lo_star_ang = wl_lo_ang;



%% Create directory structure and retrieve dir/file names

% % Load needed directory names
% load(strcat(A.dir.pipeline_dir, s, 'plates', s, A.plateName, s, A.plateName, '_directories.mat'), ...
%     'results_1D_dir', 'star_spectra_deslanted_orig_dir', 'tio_spectra_deslanted_orig_dir')
%
% % Create file names then load the needed names
% MARVELS_fileNames(A.dir.pipeline_dir)
% load(strcat(A.dir.pipeline_dir, s, 'MARVELS_fileNames.mat'), 'spectrum_deslanted_fileName');


%% Create plate directories

% % Star
% if A.options.resetSpectraDir; rmdir(star_spectra_deslanted_orig_dir,'s'); end
% mkdir(star_spectra_deslanted_orig_dir);
%
%
% % TIO
% % Define img load and save locations
% if A.options.resetSpectraDir; rmdir(tio_spectra_deslanted_orig_dir,'s'); end
% mkdir(tio_spectra_deslanted_orig_dir);



%% Illumination profile matching

if A.options.illumination.matchMARVELSprofile
    
%     if A.options.illumination.profileNormalization
%         
%         %load('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfileNorm')
%         [~, smoothedProfileNorm] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
%         profileTemplate = smoothedProfileNorm; clear smoothedProfileNorm
%     else
%         
%         %load('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfile')
%         [smoothedProfile, ~] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
%         profileTemplate = smoothedProfile; clear smoothedProfile
%     end


    load(dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0), 'MJDobs')

    profileTemplate = cell(length(obsNums), 1);
    
    if length(obsNums) > size(MJDobs, 1)
       
        obs2Use = randi([1, size(MJDobs, 1)], 1, length(obsNums));
    else
        obs2Use = obsNums;
    end
    
    for obsNum = obsNums
        
        load(dirFinder(A.dir.pipeline_dir, A.options.templatePlateName, A.options.templateBeamNum, obs2Use(obsNum), 'spectra', 'mat', 'star', 'deslanted', 'flux', 'adu', 3, 1, 0), 'I_deslanted_adu')
        
        if A.options.illumination.profileNormalization
            [~, profileTemplate_tmp] = getSpectraProfile(I_deslanted_adu, 0);
        else
            [profileTemplate_tmp, ~] = getSpectraProfile(I_deslanted_adu, 0);
        end
        
        
        if length(x_lo_px) ~= size(profileTemplate_tmp, 2) & length(y_lo_px) ~= size(profileTemplate_tmp, 1)
            
            [XX, YY] = meshgrid(1:size(profileTemplate_tmp, 2), 1:size(profileTemplate_tmp, 1));
            [XX2, YY2] = meshgrid(x_lo_px, y_lo_px);
            profileTemplate_tmp = interp2(XX, YY, profileTemplate_tmp, XX2, YY2);
            clear XX YY
        end
        
        [XX, YY] = meshgrid(1:size(profileTemplate_tmp, 2), 1:size(profileTemplate_tmp, 1));
        
        pp9 = polyFit2D(profileTemplate_tmp, XX, YY, 3, 2);
        
        profileTemplate_tmp = polyVal2D(pp9, XX, YY, 3, 2);
        
        profileTemplate{obsNum} = profileTemplate_tmp;
    end
else
    profileTemplate = [];
end



%% Create Atm Turbulance LSF data

if A.options.LSF.applyAtmTurbBlur
    [PSF_AtmTurb_chunks, ~] = generateVariantLSF(A.options.LSF.atmoBlur.R_left_atmTurb, A.options.LSF.atmoBlur.R_right_atmTurb, wl_hi_ang, A.options.LSF.variant.chunkWidth);
else
    PSF_AtmTurb_chunks = [];
end



%% Create deadObs_ind.mat File

% if 0 == exist(strcat(A.dir.pipeline_dir, s, 'plates', s, A.plateName, s, 'results_1D', s, 'deadObs_ind.mat'), 'file')

deadObs_ind = ones(120, A.options.spectraQuantity);
%     mkdir(strcat(A.dir.pipeline_dir, s, 'plates', s, A.plateName, s, 'results_1D'))
%     save(strcat(A.dir.pipeline_dir, s, 'plates', s, A.plateName, s, 'results_1D', s, 'deadObs_ind.mat'), 'deadObs_ind')

save(dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'filter', 'mat', [], 'badObs', 'obs', 'num', 3, 1, 1), 'deadObs_ind')
% end



%% Preallocation of varaibles

%[combLineSep_px, fringePeriod_px, combLineSlope_px] = deal(nan(A.spectrumWidth_px, 1));

totalShift_px_star = nan(1, A.options.spectraQuantity(end), A.spectrumWidth_px);
totalShift_chunks_px_star = nan(1, A.options.spectraQuantity(end), 32);


A.options.templateBeamNumStr = num2str(A.options.templateBeamNum);



%% Create gridded data for final spectrum downsample interpolation

[WL_lo_tio_ang, ~] = meshgrid(wl_lo_tio_ang, y_lo_px);
[WL_lo_star_ang, Y_lo_px] = meshgrid(wl_lo_star_ang, y_lo_px);



%% Create LSF

if A.options.LSF.applyVariantLSF
    
    %     [LSF_inst_chunks, R_variant_chunks, wl_center_ang] = generateVariantLSF(A.options.LSF.variant.R_left, A.options.LSF.variant.R_right, wl_hi_ang, A.options.LSF.variant.chunkWidth);
    %
    %     % Spatially variant LSF plot
    %     xM_hiR = size(wl_hi_ang, 2);
    %     chunkCount = floor((xM_hiR - mod(xM_hiR, 2)) / A.options.LSF.variant.chunkWidth);
    %     chunkLeftEdges = 1:A.options.LSF.variant.chunkWidth:(xM_hiR - mod(xM_hiR, 2));
    %     chunkLeftEdges(end) = [];
    %     chunkRightEdges = chunkLeftEdges + A.options.LSF.variant.chunkWidth - 1;
    %     chunkCenters = round((chunkRightEdges(1) - chunkLeftEdges(1)) / 2 + chunkLeftEdges);
    %
    %
    %     figure(93); clf(figure(93))
    %     subplot(10, 1, 1:7); hold on
    %     titlel('Instument LSF (Spatially variant)')
    %     for chunkNum = 1:chunkCount
    %         %       [X, Y, Z] = meshgrid(chunkLeftEdges(chunkNum):100:chunkRightEdges(chunkNum), 1:length(LSF_inst_chunks(chunkNum, :)), LSF_inst_chunks(chunkNum, :));
    %         %       surf(X, Y, Z)
    %
    %         [X, Y] = meshgrid(wl_hi_ang([chunkLeftEdges(chunkNum), chunkRightEdges(chunkNum) - 1]), 1:length(LSF_inst_chunks(chunkNum, :)));
    %         mesh(X, Y, repmat(LSF_inst_chunks(chunkNum, :), length([chunkLeftEdges(chunkNum), chunkRightEdges(chunkNum) - 1]), 1)', 'edgecolor', 'interp', 'edgelighting', 'gouraud')
    %         colormap jet
    %     end
    %     xlabel('$\lambda [\AA]$')
    %     ylabel('LSF Channel [px]')
    %     zlabel('$\Vert$LSF Value$\Vert$')
    %     axis([min(wl_hi_ang), max(wl_hi_ang), 1, size(LSF_inst_chunks, 2), min(LSF_inst_chunks(:)), max(LSF_inst_chunks(:))])
    %     grid on
    %     view(-65, 36)
    %     hold off
    %
    %     subplot(10, 1, 9:10); hold on
    %     titlel('Resolution')
    %     stairs(wl_hi_ang([chunkLeftEdges, chunkRightEdges(end)]), [R_variant_chunks, R_variant_chunks(end)], '-k')
    %     grid on
    %     axis([min(wl_hi_ang), max(wl_hi_ang), min(R_variant_chunks), max(R_variant_chunks)])
    %     xlabel('$\lambda [\AA]$')
    %     ylabel('Optical Resolution')
    %     hold off
    %
    %     plot_LSF_dir = strcat(A.dir.pipeline_dir, s, 'plates', s, A.plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'LSF');
    %     try mkdir(plot_LSF_dir); catch; end
    %     print(gcf, '-r0', '-dpng', strcat(plot_LSF_dir, s, 'star_beam', A.options.templateBeamNumStr, '_spatiallyVariantLSF_plot.png'))
    
else
    
    LSF_inst_chunks = [];
    
    LSF_inst_invariable_tio = createLSF(wl_lo_tio_ang, wl_hi_ang, A.options.LSF.invariant.R);
    LSF_inst_invariable_star = createLSF(wl_lo_star_ang, wl_hi_ang, A.options.LSF.invariant.R);
end



%% Create TIO instrument drift conversion arrays (LINEAR PIXEL SHIFTING)

WL2PX_tio_pp = spline(wl_lo_tio_ang(1, :), (1:A.spectrumWidth_px));
WL2PX_star_pp = spline(wl_lo_star_ang(1, :), (1:A.spectrumWidth_px));

X_hi_tio_px = ppval(WL2PX_tio_pp, wl_hi_ang);
X_hi_star_px = ppval(WL2PX_star_pp, wl_hi_ang);

%X_hiR_tio_px = ppval(WL2PX_tio_pp, wl_hiR_comb_ang);
%X_hiR_star_px = ppval(WL2PX_star_pp, wl_hiR_comb_ang);



%% Create Spectra

try pp = parpool(2); catch; end

if A.options.makeTIOspectra
    
    parfor obsNum = obsNums
        
        disp(cat(2, 'Working on obs ', num2str(obsNum)))
        
        %% TIO
        createSingleObsSynthSpectrum_v2(s, A.dir.pipeline_dir, A.plateName, A.options.templateBeamNum, obsNum, 'TIO', A.options.templatePlateName, A.options.templateBeamNum, 0, ...
            wl_hi_ang, WL_hi_ang, wl_lo_tio_ang, WL_lo_tio_ang, dWL_dispersion_tio_ang, wl_lo_orig_ang, wl_hi_orig_ang, ...
            flux_hiR_tio_adu, flux_hi_comb_adu, ...
            RV_BCV_mps, dX_id_px, profileTemplate, A.options.noise.SNR, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_tio, A.options.LSF.invariant.R, ...
            y_lo_px, Y_lo_px, Y_hi_px, y_hi_px, ...
            A.options.spectrum.type.singleLineSpectrum, A.options.spectrum.type.singleLine.lineLoc_ang, A.options.spectrum.type.singleLine.lineIntensity_adu, A.options.spectrum.type.singleLine.lineWidthSigma_ang, ...
            A.options.WL.dispersion.addDispersion, A.options.WL.dispersion.useFullBeamDispersion, A.options.WL.dispersion.applyTIO_dispersion, A.options.LSF.applyAtmTurbBlur, A.options.resample.method.binningDownsample, A.options.illumination.matchMARVELSprofile, A.options.noise.addNoise, A.options.LSF.applyVariantLSF, A.options.GPU_calulations, ...
            A.options.plots.tio_realComparisonPlot, A.options.plots.tio_obsDispersionPlot, A.options.plots.tio_stageContinuumPlot, A.options.plots.tio_finalSpectraPlot, A.options.plots.tio_stageImagePlot)
    end
    
end


if A.options.makeSTARspectra
    
    if A.options.spectrum.type.makeBinarySpectra
        
        parfor obsNum = obsNums
            
            disp(cat(2, 'Working on obs ', num2str(obsNum)))
            
            createSingleObsBinarySynthSpectrum(s, A.dir.pipeline_dir, A.plateName, A.options.templateBeamNum, obsNum, 'STAR', A.options.templatePlateName, A.options.templateBeamNum, ...
                wl_hi_ang, WL_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, ...
                flux_hiR_star_adu, flux_hi_comb_adu, ...
                RV_star1_BCV_mps, RV_star2_BCV_mps, dX_id_px, profileTemplate, A.options.noise.SNR, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
                y_lo_px, Y_lo_px, Y_hi_px, y_hi_px, ...
                A.options.spectrum.type.binary.luminosityRatio, ...
                A.options.WL.dispersion.addDispersion, A.options.WL.dispersion.useFullBeamDispersion, A.options.WL.dispersion.applyStar_dispersion, A.options.LSF.applyAtmTurbBlur, A.options.resample.method.binningDownsample, A.options.illumination.matchMARVELSprofile, A.options.noise.addNoise, A.options.LSF.applyVariantLSF, A.options.GPU_calulations, ...
                A.options.plots.star_realComparisonPlot, A.options.plots.star_obsDispersionPlot, A.options.plots.star_stageContinuumPlot, A.options.plots.star_finalSpectraPlot, A.options.plots.star_stageImagePlot)
            
        end
        
    else
        
        parfor obsNum = obsNums
            
            disp(cat(2, 'Working on obs ', num2str(obsNum)))
            
            %% STAR
            createSingleObsSynthSpectrum_v2(s, A.dir.pipeline_dir, A.plateName, A.options.templateBeamNum, obsNum, 'STAR', A.options.templatePlateName, A.options.templateBeamNum, 0, ...
                wl_hi_ang, WL_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, wl_lo_orig_ang, wl_hi_orig_ang, ...
                flux_hiR_star_adu, flux_hi_comb_adu, ...
                RV_BCV_mps, dX_id_px, profileTemplate, A.options.noise.SNR, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, A.options.LSF.invariant.R, ...
                y_lo_px, Y_lo_px, Y_hi_px, y_hi_px, ...
                A.options.spectrum.type.singleLineSpectrum, A.options.spectrum.type.singleLine.lineLoc_ang, A.options.spectrum.type.singleLine.lineIntensity_adu, A.options.spectrum.type.singleLine.lineWidthSigma_ang, ...
                A.options.WL.dispersion.addDispersion, A.options.WL.dispersion.useFullBeamDispersion, A.options.WL.dispersion.applyStar_dispersion, A.options.LSF.applyAtmTurbBlur, A.options.resample.method.binningDownsample, A.options.illumination.matchMARVELSprofile, A.options.noise.addNoise, A.options.LSF.applyVariantLSF, A.options.GPU_calulations, ...
                A.options.plots.star_realComparisonPlot, A.options.plots.star_obsDispersionPlot, A.options.plots.star_stageContinuumPlot, A.options.plots.star_finalSpectraPlot, A.options.plots.star_stageImagePlot)
            
        end
        
        if A.options.saveHighRes
            
            parfor obsNum = obsNums
                
                disp(cat(2, 'Working on obs ', num2str(obsNum)))
                
                createSingleObsSynthSpectrum_hiRes(s, A.dir.pipeline_dir, A.plateName, A.options.templateBeamNum, obsNum, 'STAR', A.options.templatePlateName, A.options.templateBeamNum, ...
                    wl_hi_ang, WL_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, ...
                    flux_hiR_star_adu, flux_hi_comb_adu, ...
                    RV_BCV_mps, dX_id_px, profileTemplate, A.options.noise.SNR, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
                    y_lo_px, Y_lo_px, Y_hi_px, y_hi_px, ...
                    A.options.WL.dispersion.addDispersion, A.options.WL.dispersion.useFullBeamDispersion, A.options.WL.dispersion.applyStar_dispersion, A.options.LSF.applyAtmTurbBlur, A.options.resample.method.binningDownsample, A.options.illumination.matchMARVELSprofile, A.options.noise.addNoise, A.options.LSF.applyVariantLSF, A.options.GPU_calulations, ...
                    A.options.plots.star_realComparisonPlot, A.options.plots.star_obsDispersionPlot, A.options.plots.star_stageContinuumPlot, A.options.plots.star_finalSpectraPlot, A.options.plots.star_stageImagePlot)
            end
            
        end
    end
end







% Calcs for saved variables
%     totalShift_px_star(1, obsNum, :) = dX_DopplerAndInstShift_px_star;

%     [phaseShift_rad_star(:, 1, obsNum), phaseShift_px_star(:, 1, obsNum)] = deriveFringePhaseShift(...
%       wl_up_rest_ang, X_0_px, combLineSep_px(:, 1), ...
%       fringePeriod_px(:, 1), RV_BCV_mps(obsNum), dX_DopplerAndInstShift_px_star);

%     figure(34); clf(figure(34)); hold on
%     plot(X_px, squeeze(phaseShift_px_star(:, 1, obsNum)), '-k')
%     plot(X_px, squeeze(phaseShift_rad_star(:, 1, obsNum)), '-m')

%     for chunkNum = 1:34
%       chunkRange = 301 + 100 * (chunkNum - 1) : 301 + 100 * chunkNum;
%       totalShift_chunks_px_star(1, obsNum, chunkNum) = mean(dX_DopplerAndInstShift_px_star(chunkRange));
%     end

%     toc
% 
% end

% save(strcat(synthPipelineDir, s, A.plateName, s, 'synthParams.mat'), ...
%   'A.options.orbitParams.RV_mps', 'BCV_mps', 'dX_id_px', 'wl_instDrift_ang_star', 'wl_instDrift_ang_tio', 'totalShift_px_star', ...
%   'combLineSep_px_star', 'fringePeriod_px_star', 'combLineSlope_px_star', 'phaseShift_rad_star', 'phaseShift_px_star', ...
%   'combLineSep_px_tio', 'fringePeriod_px_tio', 'combLineSlope_px_tio', 'phaseShift_rad_tio', 'phaseShift_px_tio');


if 1%~A.options.spectrum.type.makeBinarySpectra
    
    if A.options.spectrum.type.singleLineSpectrum
        
        synthParams{end + 1, 1} = 'A.options.spectrum.type.singleLineSpectrum';
        synthParams{end + 1, 1} = 'A.options.spectrum.type.singleLine.lineLoc_ang';
        synthParams{end + 1, 1} = 'A.options.spectrum.type.singleLine.lineIntensity_adu';
        synthParams{end + 1, 1} = 'A.options.spectrum.type.singleLine.lineWidthSigma_ang';
        
        synthParams(:, 2) = {...
            A.plateName...
            [A.options.templateBeamNum]...
            A.options.templateBeamNum...
            A.options.spectraQuantity...
            A.modelObs...
            A.options.templatePlateName...
            A.spectrumWidth_px...
            A.spectrumHeight_px...
            A.options.orbitParams.RV_mps(1,:)...
            BCV_mps(1,:)...
            RV_BCV_mps(1,:)...
            dX_id_px...
            A.options.GPU_calulations...
            A.options.WL.linearWLsol ...
            A.options.drift.applyInstrumentDrift...
            A.options.orbitParams.randomBCV...
            A.options.orbitParams.BCVmax_mps...
            A.options.resample.method.binningDownsample...
            A.options.noise.addNoise...
            A.options.noise.SNR...
            A.options.LSF.applyVariantLSF...
            A.options.LSF.variant.R_left...
            A.options.LSF.variant.R_right...
            A.options.LSF.invariant.R...
            A.options.LSF.applyAtmTurbBlur...
            A.options.LSF.atmoBlur.R_left_atmTurb...
            A.options.LSF.atmoBlur.R_right_atmTurb...
            A.options.LSF.variant.chunkWidth...
            A.options.illumination.matchMARVELSprofile...
            A.options.illumination.profileNormalization...
            A.options.WL.dispersion.addDispersion...
            A.options.WL.dispersion.useFullBeamDispersion...
            A.options.WL.dispersion.applyTIO_dispersion...
            A.options.WL.dispersion.applyStar_dispersion...
            A.options.templatePlateName...
            badWLsolutions_tio...
            badWLsolutions_star...
            A.options.spectrum.type.singleLineSpectrum...
            A.options.spectrum.type.singleLine.lineLoc_ang...
            A.options.spectrum.type.singleLine.lineIntensity_adu...
            A.options.spectrum.type.singleLine.lineWidthSigma_ang...
            };
        
    else
        
        synthParams(:, 2) = {...
            A.plateName...
            [A.options.templateBeamNum]...
            A.options.templateBeamNum...
            A.options.spectraQuantity...
            A.modelObs...
            A.options.templatePlateName...
            A.spectrumWidth_px...
            A.spectrumHeight_px...
            A.options.orbitParams.RV_mps(1,:)...
            BCV_mps(1,:)...
            RV_BCV_mps(1,:)...
            dX_id_px...
            A.options.GPU_calulations...
            A.options.WL.linearWLsol ...
            A.options.drift.applyInstrumentDrift...
            A.options.orbitParams.randomBCV...
            A.options.orbitParams.BCVmax_mps...
            A.options.resample.method.binningDownsample...
            A.options.noise.addNoise...
            A.options.noise.SNR...
            A.options.LSF.applyVariantLSF...
            A.options.LSF.variant.R_left...
            A.options.LSF.variant.R_right...
            A.options.LSF.invariant.R...
            A.options.LSF.applyAtmTurbBlur...
            A.options.LSF.atmoBlur.R_left_atmTurb...
            A.options.LSF.atmoBlur.R_right_atmTurb...
            A.options.LSF.variant.chunkWidth...
            A.options.illumination.matchMARVELSprofile...
            A.options.illumination.profileNormalization...
            A.options.WL.dispersion.addDispersion...
            A.options.WL.dispersion.useFullBeamDispersion...
            A.options.WL.dispersion.applyTIO_dispersion...
            A.options.WL.dispersion.applyStar_dispersion...
            A.options.templatePlateName...
            badWLsolutions_tio...
            badWLsolutions_star...
            };
    end
    
    
    mkdir(strcat(A.dir.pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, A.plateName))
    save(strcat(A.dir.pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, A.plateName, s, A.plateName, '_synthParams.mat'), ...
        'synthParams', 'RV_mps', 'BCV_mps', 'RV_BCV_mps', 'dX_id_px');
    
    % save(dirFinder(A.dir.pipeline_dir, A.plateName, [], [], 'synthPlateInfo', 'mat', [], [], 'synth_creation', 'params', 3, 1, 1),'imgdeslant')
    
    save(dirFinder(A.dir.pipeline_dir, A.plateName, A.options.templateBeamNum, [], 'spectra', 'mat', 'star', 'illuminationProfile', 'flux', 'adu', 3, 1, 1), 'profileTemplate')
    
    save(dirFinder(A.dir.pipeline_dir, A.plateName, A.options.templateBeamNum, [], 'comb', 'mat', [], 'combPeriod', 'NA', 'px', 3, 1, 1), 'period_px')
    
    writetable(cell2table(synthParams, 'VariableNames', {'ParamName', 'Value'}), strcat(A.dir.pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, A.plateName, s, A.plateName, '_synthParamsReport.txt'), 'Delimiter', '\t')
end

