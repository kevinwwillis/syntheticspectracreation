function [] = createSingleObsSynthSpectrum_hiRes(s, pipeline_dir, plateName, beamNum, obsNum, type0, plateEmulationName, templateBeam, ...
    wl_hi_ang, WL_hi_ang, wl_lo_ang, WL_lo_ang, dWL_dispersion_ang, ...
    flux_hi_adu, flux_hi_comb_adu, ...
    RV_BCV_mps, dX_instDrift_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable, ...
    y_lo_px, Y_lo_px, Y_hi_px, y_hi_px, ...
    addDispersion_q, useFullBeamDispersion_q, applyDispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
    realComparisonPlot_q, obsDispersionPlot_q, stageContinuumPlot_q, finalSpectraPlot_q, stageImagePlot_q)


% load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'spectrum_deslanted_fileName');

beamNumStr = n2s(templateBeam);
obsNumStr = n2s(obsNum);




validWL_ind = indClosestVal(wl_hi_ang, wl_lo_ang(1)) : indClosestVal(wl_hi_ang, wl_lo_ang(end));

wl_hi_ang = wl_hi_ang(1, validWL_ind);

WL_hi_ang = WL_hi_ang(:, validWL_ind);

flux_hi_adu = flux_hi_adu(:, validWL_ind);

flux_hi_comb_adu = flux_hi_comb_adu(:, validWL_ind);


%% Apply TIO instrument drifts to low res WL solution
% if dX_instDrift_px(1, obsNum) ~= 0
    
    xhi = linspace(1, length(wl_hi_ang), length(wl_hi_ang));
    
    wl_lo_id_ang = spline(xhi, wl_hi_ang, xhi - dX_instDrift_px(1, obsNum));
    
    WL_lo_id_ang = repmat(wl_lo_id_ang, length(y_lo_px), 1);
    
% else
%     
%     wl_lo_id_ang = wl_hi_ang;
%     
%     WL_lo_id_ang = WL_hi_ang;
%     
%     
% %     if saveHighRes_q
% %         wl_hi_id_ang = wl_hi_ang;
% %         
% %         WL_hi_id_ang = WL_hi_ang;
% %     end
% end



%% Add WL dispersion
if addDispersion_q == 1 & useFullBeamDispersion_q == 1 & applyDispersion_q == 1
    
%     % Create dispersed WL final sample array
%     WL_lo_id_dispersed_ang = WL_lo_id_ang + repmat(dWL_dispersion_ang(obsNum, :), y_lo_px(end), 1);
%     
%     wl_lo_id_dispersed_ang = wl_lo_id_ang + dWL_dispersion_ang(obsNum, :);
%     
%     % Plot & save disperison profile
%     if obsDispersionPlot_q
%         figure(81); clf(figure(81)); hold on;
%         titlel(cat(2, {cat(2, type0, ' Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', beamNumStr, ' | Obs: ', obsNumStr)}))
%         plot(1:4001, (WL_lo_id_dispersed_ang(1, :) - WL_lo_id_ang(1, :)), '-k');
%         xlabel('x [px]')
%         ylabel('\delta Wavelength [angstrom]')
%         grid on
%         hold off
%         
% %         plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
% %         try mkdir(plot_dispersion_dir); catch; end
% %         print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, upper(type0), '_beam', beamNumStr, '_obs', num2str(obsNum), '_dispersionPlot.png'))
%     end
    
else
    
    WL_lo_id_dispersed_ang = WL_lo_id_ang;
    
    wl_lo_id_dispersed_ang = wl_lo_id_ang;
end


%% Apply atmospheric turbulance blurring
if applyAtmTurbBlur_q
    
    % If obsNum is not = 1 and there was is no instrument drift in all observations, we can resuse the original solution here by skipping.
    flux_hi_atmT_adu = convSV_LSF(flux_hi_adu, PSF_AtmTurb_chunks);
    
else
    
    flux_hi_atmT_adu = flux_hi_adu;
end



%% Determine Doppler shifted WLs
if strcmp(upper(type0), 'STAR')
    
    % Calc WL shift from doppler formula. Only use these to find pixel shift.
    if RV_BCV_mps(obsNum) == 0
        
        wl_hi_ds_ang = wl_hi_ang;
        
        %wl_lo_shifted_ang = wl_lo_ang;
        
    else
        wl_hi_ds_ang = wl_hi_ang * (1 - RV_BCV_mps(obsNum) / c_mps);
        
        %wl_lo_shifted_ang = wl_lo_ang + (1 - RV_BCV_mps(obsNum) / c_mps);
    end
    
else
    
    % TIO has no Doppler shift.
    wl_hi_ds_ang = wl_hi_ang;
end


%% Apply Doppler shifts
if strcmp(upper(type0), 'STAR')
    
    flux_hi_atmT_ds_adu = interp1( ...
        wl_hi_ang, flux_hi_atmT_adu, wl_hi_ds_ang, 'spline', nanmean(flux_hi_atmT_adu(:)));
    
else
    
    flux_hi_atmT_ds_adu = flux_hi_atmT_adu;
end


%% Replicate rows to create 2D spectra
flux_hi_atmT_ds_2D_adu = repmat(flux_hi_atmT_ds_adu, size(flux_hi_comb_adu, 1), 1);



%% Combine the comb and spectra images
flux_hi_atmT_ds_2D_comb_adu = flux_hi_comb_adu .* flux_hi_atmT_ds_2D_adu;



%% Convolve synth spectra with the LSF
if applyVariableLSF_q
    
    flux_hi_atmT_ds_2D_comb_LSF_adu = convSV_LSF(flux_hi_atmT_ds_2D_comb_adu, LSF_inst_chunks);
    
else
    
    goodCols = isfinite(flux_hi_atmT_ds_2D_comb_adu(1,:));
    
    flux_hi_atmT_ds_2D_comb_LSF_adu = flux_hi_atmT_ds_2D_comb_adu;
    
    flux_hi_atmT_ds_2D_comb_LSF_adu(:, goodCols) = conv2(flux_hi_atmT_ds_2D_comb_adu(:, goodCols), LSF_inst_invariable, 'same');
end


if GPU_calulations_q
    flux_hi_atmT_ds_2D_comb_adu = gather(flux_hi_atmT_ds_2D_comb_adu);
    flux_hi_atmT_ds_2D_comb_LSF_adu = gather(flux_hi_atmT_ds_2D_comb_LSF_adu);
end



%% Downsample spectrum
if binningDownsample_q == 1
    
    flux_deslanted_adu = binImgResizeWL_V2(flux_hi_atmT_ds_2D_comb_LSF_adu, Y_hi_px, WL_lo_id_dispersed_ang(1, :), WL_hi_ang(1, :));
    
else
    
    goodCols = isfinite(flux_hi_atmT_ds_2D_comb_LSF_adu(1,:));
    
    flux_deslanted_adu = interp2( ...
        WL_hi_ang(:, goodCols), Y_hi_px(:, goodCols), flux_hi_atmT_ds_2D_comb_LSF_adu(:, goodCols), ...
        WL_lo_id_dispersed_ang, repmat(Y_lo_px(:, 1), 1, size(WL_lo_id_dispersed_ang, 2)), 'spline', nan);
end



%% Normalize
flux_deslanted_hi_adu = flux_deslanted_adu / max(flux_deslanted_adu(:));

% % if saveHighRes_q
%     
% %     goodCols = isfinite(flux_hi_atmT_ds_2D_comb_LSF_adu(1,:));
%     
    wl_deslanted_hi_ang = WL_lo_id_dispersed_ang(1,:);%(:, goodCols);
%     
%     flux_hi_atmT_ds_2D_comb_LSF_filt_adu = flux_hi_atmT_ds_2D_comb_LSF_adu;%(:, goodCols);
% %     
% %     validWL_ind = indClosestVal(wl_deslanted_hi_ang, WL_lo_id_dispersed_ang(1)) : indClosestVal(wl_deslanted_hi_ang, WL_lo_id_dispersed_ang(end));
%     
% %     wl_deslanted_hi_ang = wl_deslanted_hi_ang;%(validWL_ind);
%     
% %     flux_deslanted_hi_adu = flux_hi_atmT_ds_2D_comb_LSF_filt_adu;%(:, validWL_ind);
%     
%     flux_deslanted_hi_adu = flux_hi_atmT_ds_2D_comb_LSF_filt_adu / max(flux_hi_atmT_ds_2D_comb_LSF_filt_adu(:));
%     
% % end


%% Conform to MARVELS illumination profile
if matchMARVELSprofile_q
    flux_deslanted_adu = profileTemplate .* flux_deslanted_adu;
end


% %for noiseLevel = [1, 5, 8, (10 :10: 90), 95, 99]
% for noiseLevel = [0.001 1 10 100]

%% Add noise to the image

flux_deslanted_noise_adu = flux_deslanted_adu;

if addNoise_q
    
        
        if length(noiseLevel) > 1
            I_noise = photNoiseGen(noiseLevel(obsNum), flux_deslanted_hi_adu);
        else
            I_noise = photNoiseGen(noiseLevel, flux_deslanted_hi_adu);
        end
    
        flux_deslanted_hi_adu = (flux_deslanted_hi_adu .* 100 + I_noise) ./ 100;
end


% qf(1,1); subplot(3,1,1); imagesc(flux_deslanted_adu); subplot(3,1,2); imagesc(flux_deslanted_noise_adu); subplot(3,1,3); imagesc(I_noise);


% %fitswrite(flux_deslanted_adu, ['E:\Research\sankalpNoiseSpectra\I_star_SNR', strrep(n2s((100/noiseLevel)), '.', 'p'), '_adu.fits'])
% fitswrite(flux_deslanted_noise_adu, ['E:\Research\sankalpNoiseSpectra\I_', type0, '_SNR', n2s((100/noiseLevel), 3), '_adu.fits'])
% end
% % fitswrite(flux_deslanted_adu, 'E:\Research\sankalpNoiseSpectra\I_star_CLEAN_adu.fits')



%% Create spectra save name
% fitsSavePath = strcat(spectra_deslanted_orig_dir, s, upper(type0), '_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.fits');
% matSavePath = strcat(spectra_deslanted_orig_dir, s, upper(type0), '_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.mat');


%% Save the spectra
% fitswrite(flux_deslanted_adu, fitsSavePath)
% flux_adu = flux_deslanted_adu;
% save(matSavePath, 'flux_adu')

% fitswrite(flux_deslanted_adu, dirFinder(pipeline_dir, plateName, beamNum, obsNum, 'spectra', 'fits', lower(type0), 'deslanted', 'flux', 'adu', 3, 1, 1));

% I_deslanted_adu = flux_deslanted_noise_adu;
% % save(matSavePath, 'flux_adu')
% save(dirFinder(pipeline_dir, plateName, beamNum, obsNum, 'spectra', 'mat', lower(type0), 'deslanted', 'flux', 'adu', 3, 1, 1), 'I_deslanted_adu')

% % Save WL solution
% fileDir0 = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'wlsoln_rest');
% mkdir(fileDir0)
% fileName0 = strcat(fileDir0, s, upper(type0), '_beam', beamNumStr, '_allObs_wlsoln.mat');
% 
% if 0 ~= exist(fileName0, 'file') & obsNum ~= 1
%     load(fileName0, 'wl_allObs_ang')
% else
%     wl_allObs_ang = nan(obsNum, 4001);
% end
% wl_allObs_ang(obsNum, :) = wl_lo_id_dispersed_ang;
% 
% save(fileName0, 'wl_allObs_ang')

% fileName0 = dirFinder(pipeline_dir, plateName, beamNum, [], 'WL', 'mat', lower(type0), 'actual', 'WL', 'ang', 3, 1, 1);
% 
% if 0 ~= exist(fileName0, 'file')
%     load(fileName0, 'wl_allObs_ang')
% else
%     wl_allObs_ang = nan(obsNum, 4001);
% end
% wl_allObs_ang(obsNum, :) = wl_lo_id_dispersed_ang;
% 
% save(fileName0, 'wl_allObs_ang')


% if saveHighRes_q
    
    I_deslanted_hi_adu = flux_deslanted_hi_adu;
    
    save(dirFinder(pipeline_dir, plateName, beamNum, obsNum, 'spectra', 'mat', lower(type0), 'deslanted', 'hi_flux_wl', 'adu', 3, 1, 1), 'I_deslanted_hi_adu', 'wl_deslanted_hi_ang')
% end


%% Star Plots

% % Continuum plots
% if stageContinuumPlot_q
%     synthContinuumPlots_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%         wl_lo_ang, wl_hi_ang, ...
%         flux_hi_adu, flux_hi_atmT_ds_adu, flux_hi_atmT_ds_2D_comb_adu, flux_hi_atmT_ds_2D_comb_LSF_adu, flux_deslanted_adu, type0)
% end
% 
% % Image plots
% if stageImagePlot_q
%     synthImagePlots_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%         wl_lo_ang, wl_hi_ang, ...
%         flux_hi_adu, flux_hi_atmT_ds_adu, flux_hi_atmT_ds_2D_comb_adu, flux_hi_atmT_ds_2D_comb_LSF_adu, flux_deslanted_adu, type0)
% end
% 
% % Final spectrum continuum plot
% if finalSpectraPlot_q
%     synthFinalContinuumPlot_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
%         wl_lo_ang, flux_deslanted_adu, type0)
% end
% 
% 
% if realComparisonPlot_q
% %     realAndSynthComparison(s, pipeline_dir, plateName, beamNumStr, obsNum, flux_deslanted_adu, 1, type0, 1, plateEmulationName, templateBeam)
% end

1;






