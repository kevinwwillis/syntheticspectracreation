function synthPlateCreationFunction(...
    GPU_calulations_q, randomInstDrift_q, randomBCV_q, binningDownsample_q, addDispersion_q, linearWLsol_q, resetSpectraDir_q, ...
    useFullBeamDispersion_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, ...
    meanZeroDispersionStar_q, meanZeroDispersionTIO_q, correctStarDispByTIOdispOffsets_q, setDispProfZeroAt2000px_q, ...
    applyTIOdispersion_q, applySTARdispersion_q, templatePlateName, beamNum, addNoise_q, noiseLevel, ...
    applyVariableLSF_q, R_left_inst, R_right_inst, R_invar_LSF, applyAtmTurbBlur_q, R_left_atmTurb, R_right_atmTurb, ...
    LSF_chunkWidth, matchMARVELSprofile_q, profileNormalization_q, BCVmax_mps, spectrumPixelWidth, ...
    spectrumPixelHeight, maxRV_mps, orbitPeriod_day, surveyDaySpan_day, plots_q, ...
    tio_realComparisonPlot_q, tio_obsDispersionPlot_q, tio_stageContinuumPlot_q, tio_finalSpectraPlot_q, tio_stageImagePlot_q, ...
    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q, ...
    spectraQuantity, WLdispersionLimit_ang, RV_mps, forceNewModel_q, modelObsNum_forced, forcedObsNums_q, obsNums_forced, forcedPlateName_q, plateName_forced, makeTIOspectra_q, makeSTARspectra_q, ...
    pipeline_dir, neil_dir, nolan_dir)


% create a typical plate of spectra synthetically with their own unique parameters




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s = dslash;

load(strcat(pwd, s, 'synthParamsFileTemplate.mat'), 'synthParams')


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create plate save name

if forcedPlateName_q

    plateName = plateName_forced;
    
else
    plateName = synthPlateNameCreation(s, pipeline_dir, templatePlateName, beamNum, ...
        addDispersion_q, linearWLsol_q, applyTIOdispersion_q, applySTARdispersion_q, meanZeroDispersionStar_q, ...
        meanZeroDispersionTIO_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, setDispProfZeroAt2000px_q, addNoise_q);
end


%%

x_beam_px = 1:spectrumPixelWidth;

y_beam_px = 1:spectrumPixelHeight;


%%
if forcedObsNums_q
    obsNums = obsNums_forced;
    
elseif spectraQuantity ~= 0
    obsNums = 1:spectraQuantity;
    
else
    % Load BCV array to determine how many observations were used
    if 0 == exist(strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'bc.mat'), 'file')
        getNeilFile(s, strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'bc.mat'), strcat(neil_dir, s, templatePlateName, s, 'results', s, 'bc.mat'))
    end
    load(strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'bc.mat'), 'bc');
    
    spectraQuantity = size(bc, 2);
    
    obsNums = 1:spectraQuantity;
end


%% Determine model observation #

% % Load model observation number
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'file')
%     getNeilFile(s, strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), strcat(neil_dir, s, templatePlateName, s, 'results', s, 'model.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'model');
% 
% modelObsNum_orig = model(beamNum); clear model

% Is new model obs number being forced?
if forceNewModel_q
    
    % Sanity check
    if forcedObsNums_q & 0 == sum(modelObsNum_forced == obsNums_forced)
        
        cprintf([1,0,0],  '\n FATAL ERROR: ')
        cprintf(-[1,0,0],  'Forced obs model # is not included in the forced obs numbers array. Change the plate creation settings.')
        cprintf(-[1,0,0], '\n');
        
        return
    end
    
    modelObsNum = modelObsNum_forced;
else
    modelObsNum = 1;
end


%% WL Dispersion Load/Creation

if addDispersion_q
    
    [dWL_dispersion_star_ang, dWL_dispersion_tio_ang, badWLsolutions_star, badWLsolutions_tio, spectraQuantity] = createDispersionData(...
        s, pipeline_dir, nolan_dir, neil_dir, plateName, templatePlateName, beamNum, spectraQuantity, WLdispersionLimit_ang, ...
        applyTIOdispersion_q, applySTARdispersion_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, setDispProfZeroAt2000px_q, correctStarDispByTIOdispOffsets_q, ...
        meanZeroDispersionTIO_q, meanZeroDispersionStar_q, modelObsNum);
    
    
    dWL_dispersion_star_ang(isnan(dWL_dispersion_star_ang)) = 0;
    dWL_dispersion_tio_ang(isnan(dWL_dispersion_tio_ang)) = 0;
    
else
    badWLsolutions_tio = [];
    badWLsolutions_star = [];
    
    dWL_dispersion_star_ang = [];
    dWL_dispersion_tio_ang = [];
end

% Recalibrate input RVs array
if isempty(RV_mps)
    RV_mps = zeros(1, size(spectraQuantity, 1));
end



%% Load master comb and its params

% masterCombFolderName = 'masterComb_R247500_test';
% masterComb_dir = strcat('E:\Research\MARVELS_software\synthSpectraMaker_2D\masterCombImages\', masterCombFolderName);

% load(strcat(masterComb_dir, s, 'Y_px.mat'), 'Y_px')
% load(strcat(masterComb_dir, s, 'wl_m.mat'), 'wl_m')


% masterCombFolderName = 'masterComb_R247500_o7mm_dd6p4en8';
% masterComb_dir = ['E:\Research\MARVELS_software\comb\data\masterSynthCombImages\', masterCombFolderName];

% load(strcat(masterComb_dir, s, 'masterCombImg.mat'), 'combImg', 'Y_px', 'wl_m')

y_px = 1 :0.1: 26;

wl_m = (4900 :0.01: 5900) * 1e-10;

[WL_m, Y_px] = meshgrid(wl_m, y_px);

ep = 6.8e-8 * ones(length(y_px), length(wl_m));

d0 = linspace(7.03, 7.01, length(wl_m)) * 1e-3;

d0 = repmat(d0, length(y_px), 1);

combImg = (1 + cos(2 * pi * (d0 + ep .* Y_px) ./ WL_m) ) / 2;

% qf(1,1); imagesc(combImg)

% Rename comb flux
if GPU_calulations_q
    flux_hiR_comb_adu = gpuArray(combImg); clear combImg
else
    flux_hiR_comb_adu = combImg; clear combImg
end

% Ensure comb flux is normalized
% flux_hiR_comb_adu = mat2gray(flux_hiR_comb_adu);

% Rename comb WLs
wl_hiR_comb_ang = wl_m * 1e10; clear wl_m

% Create logspace comb WLs
wl_hiR_log_comb_ang = log(wl_hiR_comb_ang);

% Create gridded data for final spectrum downsample interpolation
[wl_hiR_log_gridded_comb_ang, Y_hiR_gridded_comb_px] = meshgrid(wl_hiR_log_comb_ang, y_px);


%% Create synth plate params

[wl_beams_ang, plateName, BCV_mps, RV_BCV_mps, dX_id_px] = createSynthParams(...
    s, pipeline_dir, beamNum, orbitPeriod_day, maxRV_mps, RV_mps, obsNums, modelObsNum, ...
    BCVmax_mps, spectraQuantity, surveyDaySpan_day, plateName, randomInstDrift_q, randomBCV_q, linearWLsol_q);



%% Load hi-res sun and TIO spectra sampled at the wl range of the comb img

load([pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'star', s, 'templates', s, 'star_master_synth_template.mat'], 'flux_adu', 'wl_ang')
% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\data\templates\masterSuntemplate.mat', 'wl_hiR_wide_sun_m', 'flux_hiR_wide_sun_adu')

wl_hiR_wide_star_ang = wl_ang(:)'; clear wl_ang
wl_hiR_wide_log_star_ang = log(wl_hiR_wide_star_ang);
flux_hiR_wide_star_adu = flux_adu(:)'; clear flux_adu



load([pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'tio', s, 'templates', s, 'tio_template_R300000_marvelsRange.mat'], 'flux_adu', 'wl_ang')
% load('E:\Research\MARVELS_software\synthSpectraMaker_2D\data\templates\masterTiOtemplate.mat', 'wl_hiR_wide_tio_m', 'wl_hiR_wide_log_tio_m', 'flux_hiR_wide_tio_adu')

wl_hiR_wide_tio_ang = wl_ang(:)'; clear wl_ang
wl_hiR_wide_log_tio_ang = log(wl_hiR_wide_tio_ang);
flux_hiR_wide_tio_adu = flux_adu(:)'; clear flux_adu



%% Restructure beam WL array and convert it to logspace

if linearWLsol_q
    wl_beams_tio_ang(1, :) = wl_beams_ang;
    wl_beams_log_tio_ang(1, :) = log(wl_beams_ang);
    
    wl_beams_star_ang(1, :) = wl_beams_ang;
    wl_beams_log_star_ang(1, :) = log(wl_beams_ang);
    
else
    
    wl_beams_tio_ang(1, :) = wl_beams_ang(1, :);
    wl_beams_log_tio_ang(1, :) = log(wl_beams_ang(1, :));
    
    wl_beams_star_ang(1, :) = wl_beams_ang(1, :);
    wl_beams_log_star_ang(1, :) = log(wl_beams_ang(1, :));
end



%% Create directory structure and retrieve dir/file names

% Load needed directory names
load(strcat(pipeline_dir, s, 'plates', s, plateName, s, plateName, '_directories.mat'), ...
    'results_1D_dir', 'star_spectra_deslanted_orig_dir', 'tio_spectra_deslanted_orig_dir')

% Create file names then load the needed names
MARVELS_fileNames(pipeline_dir)
load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'spectrum_deslanted_fileName');


%% Create plate directories

% Star
if resetSpectraDir_q; rmdir(star_spectra_deslanted_orig_dir,'s'); end
mkdir(star_spectra_deslanted_orig_dir);


% TIO
% Define img load and save locations
if resetSpectraDir_q; rmdir(tio_spectra_deslanted_orig_dir,'s'); end
mkdir(tio_spectra_deslanted_orig_dir);



%% Illumination profile matching
if matchMARVELSprofile_q
    
    if profileNormalization_q
        
        %load('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfileNorm')
        [~, smoothedProfileNorm] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
        profileTemplate = smoothedProfileNorm; clear smoothedProfileNorm
    else
        
        %load('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfile')
        [smoothedProfile, ~] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
        profileTemplate = smoothedProfile; clear smoothedProfile
    end
    
    if x_beam_px(end) ~= size(profileTemplate, 2) & y_beam_px(end) ~= size(profileTemplate, 1)
        
        [XX, YY] = meshgrid(1:size(profileTemplate, 2), 1:size(profileTemplate, 1));
        [XX2, YY2] = meshgrid(x_beam_px, y_beam_px);
        profileTemplate = interp2(XX, YY, profileTemplate, XX2, YY2);
        clear XX YY
    end
    
else
    profileTemplate = [];
end


%% Create Atm Turbulance LSF data
if applyAtmTurbBlur_q
    [PSF_AtmTurb_chunks, ~] = generateVariantLSF(R_left_atmTurb, R_right_atmTurb, wl_hiR_comb_ang, LSF_chunkWidth);
else
    PSF_AtmTurb_chunks = [];
end



%% Create deadObs_ind.mat File

if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'results_1D', s, 'deadObs_ind.mat'), 'file')
    deadObs_ind = ones(120, spectraQuantity);
    mkdir(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'results_1D'))
    save(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'results_1D', s, 'deadObs_ind.mat'), 'deadObs_ind')
end




%% Preallocation of varaibles

%[combLineSep_px, fringePeriod_px, combLineSlope_px] = deal(nan(4001, 1));

totalShift_px_star = nan(1, spectraQuantity(end), 4001);
totalShift_chunks_px_star = nan(1, spectraQuantity(end), 32);




beamNumStr = num2str(beamNum);


% Rest WLs of this beam
wl_beam_tio_ang = wl_beams_tio_ang(1, :);
wl_beam_star_ang = wl_beams_star_ang(1, :);

wl_beam_log_tio_ang = wl_beams_log_tio_ang(1, :);
wl_beam_log_star_ang = wl_beams_log_star_ang(1, :);


%% Create gridded data for final spectrum downsample interpolation
[wl_beam_log_gridded_tio_ang, ~] = meshgrid(wl_beam_log_tio_ang, y_beam_px);
[wl_beam_log_gridded_star_ang, Y_beam_log_gridded_0_px] = meshgrid(wl_beam_log_star_ang, y_beam_px);


%% Create LSF
if applyVariableLSF_q
    
    [LSF_inst_chunks, R_variant_chunks, wl_center_ang] = generateVariantLSF(R_left_inst, R_right_inst, wl_hiR_comb_ang, LSF_chunkWidth);
    
    % Spatially variant LSF plot
    xM_hiR = size(wl_hiR_comb_ang, 2);
    chunkCount = floor((xM_hiR - mod(xM_hiR, 2)) / LSF_chunkWidth);
    chunkLeftEdges = 1:LSF_chunkWidth:(xM_hiR - mod(xM_hiR, 2));
    chunkLeftEdges(end) = [];
    chunkRightEdges = chunkLeftEdges + LSF_chunkWidth - 1;
    chunkCenters = round((chunkRightEdges(1) - chunkLeftEdges(1)) / 2 + chunkLeftEdges);
    
    
    figure(93); clf(figure(93))
    subplot(10, 1, 1:7); hold on
    titlel('Instument LSF (Spatially variant)')
    for chunkNum = 1:chunkCount
        %       [X, Y, Z] = meshgrid(chunkLeftEdges(chunkNum):100:chunkRightEdges(chunkNum), 1:length(LSF_inst_chunks(chunkNum, :)), LSF_inst_chunks(chunkNum, :));
        %       surf(X, Y, Z)
        
        [X, Y] = meshgrid(wl_hiR_comb_ang([chunkLeftEdges(chunkNum), chunkRightEdges(chunkNum) - 1]), 1:length(LSF_inst_chunks(chunkNum, :)));
        mesh(X, Y, repmat(LSF_inst_chunks(chunkNum, :), length([chunkLeftEdges(chunkNum), chunkRightEdges(chunkNum) - 1]), 1)', 'edgecolor', 'interp', 'edgelighting', 'gouraud')
        colormap jet
    end
    xlabel('$\lambda [\AA]$')
    ylabel('LSF Channel [px]')
    zlabel('$\Vert$LSF Value$\Vert$')
    axis([min(wl_hiR_comb_ang), max(wl_hiR_comb_ang), 1, size(LSF_inst_chunks, 2), min(LSF_inst_chunks(:)), max(LSF_inst_chunks(:))])
    grid on
    view(-65, 36)
    hold off
    
    subplot(10, 1, 9:10); hold on
    titlel('Resolution')
    stairs(wl_hiR_comb_ang([chunkLeftEdges, chunkRightEdges(end)]), [R_variant_chunks, R_variant_chunks(end)], '-k')
    grid on
    axis([min(wl_hiR_comb_ang), max(wl_hiR_comb_ang), min(R_variant_chunks), max(R_variant_chunks)])
    xlabel('$\lambda [\AA]$')
    ylabel('Optical Resolution')
    hold off
    
    plot_LSF_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'LSF');
    try mkdir(plot_LSF_dir); catch; end
    print(gcf, '-r0', '-dpng', strcat(plot_LSF_dir, s, 'star_beam', beamNumStr, '_spatiallyVariantLSF_plot.png'))
    
else
    
    LSF_inst_chunks = [];
    
    LSF_inst_invariable_tio = createLSF(wl_beam_tio_ang, wl_hiR_comb_ang, R_invar_LSF);
    LSF_inst_invariable_star = createLSF(wl_beam_star_ang, wl_hiR_comb_ang, R_invar_LSF);
end


% TIO instrument drift conversion arrays (LINEAR PIXEL SHIFTING)
WL2PX_tio_pp = spline(wl_beams_tio_ang(1, :), (1:4001));
WL2PX_star_pp = spline(wl_beams_star_ang(1, :), (1:4001));

X_hiR_wide_tio_px = ppval(WL2PX_tio_pp, wl_hiR_wide_star_ang);
X_hiR_wide_star_px = ppval(WL2PX_star_pp, wl_hiR_wide_star_ang);

%X_hiR_tio_px = ppval(WL2PX_tio_pp, wl_hiR_comb_ang);
%X_hiR_star_px = ppval(WL2PX_star_pp, wl_hiR_comb_ang);




for obsNum = obsNums
    
    disp(cat(2, 'Working on Beam ', num2str(beamNum), ', Obs ', num2str(obsNum)))
    
    
    if makeTIOspectra_q
        %% TIO
        createSingleObsSynthSpectrum(s, pipeline_dir, plateName, beamNum, obsNum, 'TIO', templatePlateName, beamNum, ...
            wl_hiR_log_comb_ang, wl_beams_tio_ang, wl_beam_log_tio_ang, wl_hiR_wide_log_tio_ang, wl_beam_log_gridded_tio_ang, dWL_dispersion_tio_ang, wl_hiR_log_gridded_comb_ang, ...
            flux_hiR_wide_tio_adu, flux_hiR_comb_adu, ...
            RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_tio, ...
            X_hiR_wide_tio_px, ...
            y_beam_px, Y_beam_log_gridded_0_px, Y_hiR_gridded_comb_px, y_px, ...
            tio_spectra_deslanted_orig_dir, ...
            addDispersion_q, useFullBeamDispersion_q, applyTIOdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
            tio_realComparisonPlot_q, tio_obsDispersionPlot_q, tio_stageContinuumPlot_q, tio_finalSpectraPlot_q, tio_stageImagePlot_q)
    end
    
    
    
    if makeSTARspectra_q
        %% STAR
        createSingleObsSynthSpectrum(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, ...
            wl_hiR_log_comb_ang, wl_beams_star_ang, wl_beam_log_star_ang, wl_hiR_wide_log_star_ang, wl_beam_log_gridded_star_ang, dWL_dispersion_star_ang, wl_hiR_log_gridded_comb_ang, ...
            flux_hiR_wide_star_adu, flux_hiR_comb_adu, ...
            RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
            X_hiR_wide_star_px, ...
            y_beam_px, Y_beam_log_gridded_0_px, Y_hiR_gridded_comb_px, y_px, ...
            star_spectra_deslanted_orig_dir, ...
            addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
            star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
    end
    
    
    
    
    
    
    
    % Calcs for saved variables
    %     totalShift_px_star(1, obsNum, :) = dX_DopplerAndInstShift_px_star;
    
    %     [phaseShift_rad_star(:, 1, obsNum), phaseShift_px_star(:, 1, obsNum)] = deriveFringePhaseShift(...
    %       wl_up_rest_ang, X_0_px, combLineSep_px(:, 1), ...
    %       fringePeriod_px(:, 1), RV_BCV_mps(obsNum), dX_DopplerAndInstShift_px_star);
    
    %     figure(34); clf(figure(34)); hold on
    %     plot(X_px, squeeze(phaseShift_px_star(:, 1, obsNum)), '-k')
    %     plot(X_px, squeeze(phaseShift_rad_star(:, 1, obsNum)), '-m')
    
    %     for chunkNum = 1:34
    %       chunkRange = 301 + 100 * (chunkNum - 1) : 301 + 100 * chunkNum;
    %       totalShift_chunks_px_star(1, obsNum, chunkNum) = mean(dX_DopplerAndInstShift_px_star(chunkRange));
    %     end
    
    %     toc
    
end

% save(strcat(synthPipelineDir, s, plateName, s, 'synthParams.mat'), ...
%   'RV_mps', 'BCV_mps', 'dX_id_px', 'wl_instDrift_ang_star', 'wl_instDrift_ang_tio', 'totalShift_px_star', ...
%   'combLineSep_px_star', 'fringePeriod_px_star', 'combLineSlope_px_star', 'phaseShift_rad_star', 'phaseShift_px_star', ...
%   'combLineSep_px_tio', 'fringePeriod_px_tio', 'combLineSlope_px_tio', 'phaseShift_rad_tio', 'phaseShift_px_tio');


synthParams(:, 2) = {...
    plateName...
    [beamNum]...
    beamNum...
    spectraQuantity...
    modelObsNum...
    templatePlateName...
    spectrumPixelWidth...
    spectrumPixelHeight...
    RV_mps...
    BCV_mps...
    RV_BCV_mps...
    dX_id_px...
    GPU_calulations_q...
    linearWLsol_q ...
    randomInstDrift_q...
    randomBCV_q...
    BCVmax_mps...
    binningDownsample_q...
    addNoise_q...
    noiseLevel...
    applyVariableLSF_q...
    R_left_inst...
    R_right_inst...
    R_invar_LSF...
    applyAtmTurbBlur_q...
    R_left_atmTurb...
    R_right_atmTurb...
    LSF_chunkWidth...
    matchMARVELSprofile_q...
    profileNormalization_q...
    addDispersion_q...
    useFullBeamDispersion_q...
    applyTIOdispersion_q...
    applySTARdispersion_q...
    templatePlateName...
    badWLsolutions_tio...
    badWLsolutions_star...
    };


save(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'synthParams.mat'), ...
    'synthParams', 'RV_mps', 'BCV_mps', 'RV_BCV_mps', 'dX_id_px');


writetable(cell2table(synthParams, 'VariableNames', {'ParamName', 'Value'}), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'synthParamsReport.txt'), 'Delimiter', '\t')


