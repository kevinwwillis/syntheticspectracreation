function [rv_mps, lumRat, JDs] = binarySim2SpectraModule(orbitParams, hist, d, forcedLinearObsDates_q, forcedObsCount)


M_sun = 1.9885e30;

if forcedLinearObsDates_q
    
    t_span = seconds(days(orbitParams.p1.t.known(end) - orbitParams.p1.t.known(1)));
    
    t_norm = linspace(0, t_span, forcedObsCount);
    
else
    t_norm = seconds(days(orbitParams.p1.t.known - orbitParams.p1.t.known(1)));
end

t_ind = nan(1, length(t_norm));

% hist.t = days(seconds(hist.t));

for ti = 1:length(t_norm)

    t_ind(1, ti) = indClosestVal(hist.t, t_norm(ti));
    
end

t_diff_hr = minutes(seconds(hist.t(t_ind) -  t_norm));

JDs = days(seconds(hist.t(t_ind))) + orbitParams.p1.t.known(1);

t_diff_RMS_hr = rms(t_diff_hr);
t_diff_STD_hr = std(t_diff_hr);
t_diff_MEAN_hr = mean(t_diff_hr);
t_diff_MAX_hr = max(t_diff_hr);

disp(cat(2, 'Observations sim time vs actual time stats: RMS = ', n2s(t_diff_RMS_hr, 3), ...
    ' min, STD = ', n2s(t_diff_STD_hr, 3), ' min, AVG = ', n2s(t_diff_MEAN_hr, 3), ' min, MAX = ', n2s(t_diff_MAX_hr, 3), ' min'))


lumRat = detLum(orbitParams.s2.m.final, M_sun) / detLum(orbitParams.s1.m.final, M_sun);

rv_mps(1, :) = -hist.m(1).vx(t_ind);
rv_mps(2, :) = -hist.m(2).vx(t_ind);
















function L = detLum(m, M_sun)


if m < 0.42 * M_sun
    
    L = 0.23 * ( m / M_sun )^2.3;
    
elseif 0.42 * M_sun <= m | m < 2 * M_sun
    
    L = ( m / M_sun )^4;
    
elseif 2 * M_sun <= m | m < 20 * M_sun
    
    L = 1.4 * ( m / M_sun )^3.5;
    
elseif 55 * M_sun <= m
    
    L = 32000 * ( m / M_sun );
    
end




