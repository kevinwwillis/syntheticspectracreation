function [] = createSingleObsSynthSpectrum_v2(s, pipeline_dir, plateName, beamNum, obsNum, type0, plateEmulationName, templateBeam, saveHighRes_q, ...
    wl_hi_ang, WL_hi_ang, wl_lo_ang, WL_lo_ang, dWL_dispersion_ang, wl_lo_orig_ang, wl_hi_orig_ang, wl_crop_ind, ...
    flux_hi_adu, flux_hi_comb_adu, ...
    RV_BCV_mps, dX_instDrift_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable, R_invar_LSF, ...
    y_lo_px, Y_lo_px, Y_hi_px, y_hi_px, ...
    applyFringeDistortion_q, FDD_px, ...
    singleLineSpectrum_q, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
    addDispersion_q, useFullBeamDispersion_q, applyDispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
    realComparisonPlot_q, obsDispersionPlot_q, stageContinuumPlot_q, finalSpectraPlot_q, stageImagePlot_q)


% load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'spectrum_deslanted_fileName');

beamNumStr = n2s(templateBeam);
obsNumStr = n2s(obsNum);

Y_lo_px = repmat(y_lo_px', 1, length(wl_lo_ang));


%% Apply TIO instrument drifts to low res WL solution
if dX_instDrift_px(1, obsNum) ~= 0
    
    wl_lo_id_ang = spline(1:length(wl_lo_ang), wl_lo_ang, (1:length(wl_lo_ang)) - dX_instDrift_px(1, obsNum));
    
    WL_lo_id_ang = repmat(wl_lo_id_ang, length(y_lo_px), 1);
    
    if singleLineSpectrum_q
        wl_lo_orig_id_ang = spline(1:length(wl_lo_orig_ang), wl_lo_orig_ang, (1:length(wl_lo_orig_ang)) - dX_instDrift_px(1, obsNum));
    end
    
else
    
    wl_lo_id_ang = wl_lo_ang;
    
    WL_lo_id_ang = WL_lo_ang;
    
    if singleLineSpectrum_q
        wl_lo_orig_id_ang = wl_lo_orig_ang;
    end
    
    
%     if saveHighRes_q
%         wl_hi_id_ang = wl_hi_ang;
%         
%         WL_hi_id_ang = WL_hi_ang;
%     end
end



%% Add WL dispersion
if addDispersion_q == 1% & useFullBeamDispersion_q == 1 & applyDispersion_q == 1
    
    % Create dispersed WL final sample array
    WL_lo_id_dispersed_ang = WL_lo_id_ang + repmat(dWL_dispersion_ang(obsNum, wl_crop_ind), y_lo_px(end), 1);
    
    wl_lo_id_dispersed_ang = wl_lo_id_ang + dWL_dispersion_ang(obsNum, wl_crop_ind);
    
    if singleLineSpectrum_q
        wl_lo_id_dispersed_orig_ang = wl_lo_orig_id_ang + dWL_dispersion_ang(obsNum, :);
        
        WL_lo_id_dispersed_orig_ang = repmat(wl_lo_id_dispersed_orig_ang + dWL_dispersion_ang(obsNum, :), length(y_lo_px), 1);
    end
    
    % Plot & save disperison profile
    if obsDispersionPlot_q
        figure(81); clf(figure(81)); hold on;
        titlel(cat(2, {cat(2, type0, ' Dispersion')}, {cat(2, 'Plate: ', underscoreTEXfixer(plateName), ' | Beam: ', beamNumStr, ' | Obs: ', obsNumStr)}))
        plot(1:4001, (WL_lo_id_dispersed_ang(1, :) - WL_lo_id_ang(1, :)), '-k');
        xlabel('x [px]')
        ylabel('\delta Wavelength [angstrom]')
        grid on
        hold off
        
%         plot_dispersion_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'dispersion');
%         try mkdir(plot_dispersion_dir); catch; end
%         print(gcf, '-r0', '-dpng', strcat(plot_dispersion_dir, s, upper(type0), '_beam', beamNumStr, '_obs', num2str(obsNum), '_dispersionPlot.png'))
    end
    
else
    
    WL_lo_id_dispersed_ang = WL_lo_id_ang;
    
    wl_lo_id_dispersed_ang = wl_lo_id_ang;
    
    if singleLineSpectrum_q
        wl_lo_id_dispersed_orig_ang = wl_lo_orig_id_ang;
        
        WL_lo_id_dispersed_orig_ang = repmat(wl_lo_id_dispersed_orig_ang, length(y_lo_px), 1);
    end
end


%% Apply atmospheric turbulance blurring
if applyAtmTurbBlur_q
    
    % If obsNum is not = 1 and there was is no instrument drift in all observations, we can resuse the original solution here by skipping.
    flux_hi_atmT_adu = convSV_LSF(flux_hi_adu, PSF_AtmTurb_chunks);
    
else
    
    flux_hi_atmT_adu = flux_hi_adu;
end



%% Determine Doppler shifted WLs
if strcmp(upper(type0), 'STAR')
    
    % Calc WL shift from doppler formula. Only use these to find pixel shift.
    if RV_BCV_mps(obsNum) == 0
        
        wl_hi_ds_ang = wl_hi_ang;
        
        %wl_lo_shifted_ang = wl_lo_ang;
        
        if singleLineSpectrum_q
            wl_hi_orig_ds_ang = wl_hi_orig_ang;
        end
        
    else
        wl_hi_ds_ang = wl_hi_ang * (1 - RV_BCV_mps(obsNum) / c_mps);
        
        %wl_lo_shifted_ang = wl_lo_ang + (1 - RV_BCV_mps(obsNum) / c_mps);
        
        if singleLineSpectrum_q
            wl_hi_orig_ds_ang = wl_hi_orig_ang * (1 - RV_BCV_mps(obsNum) / c_mps);
        end
    end
    
else
    
    % TIO has no Doppler shift.
    wl_hi_ds_ang = wl_hi_ang;
end


%% Apply Doppler shifts
if strcmp(upper(type0), 'STAR')
    
    if singleLineSpectrum_q
    
        % New WL range needs to include this value and all WL in the wl_lo_ang vector
        wl_line_hi_ds_ang = lineLoc_ang * (1 + RV_BCV_mps(obsNum) / c_mps);
        
        %ind = indClosestVal(WL_lo_id_dispersed_ang(1, :), wl_line_hi_ds_ang);
        
        %dwl = abs(WL_lo_id_dispersed_ang(1, ind) - wl_line_hi_ds_ang) / 10;
        
        dwl = 0.27;
        wl_hi_ang2 = 0;
        
        while length(wl_hi_ang2) < 1000
            
            dwl = dwl / 2;
            
            wl_hi_ang2 = (wl_line_hi_ds_ang :-dwl: wl_hi_ang(1));
            
            wl_hi_ang2 = sort(wl_hi_ang2);
            
            wl_hi_ang2 = [wl_hi_ang2, (wl_hi_ang2(end)+dwl :dwl: wl_hi_ang(end))];
        end
        
        wl_hi_ang = wl_hi_ang2;
        
%         qf(11,1);
%         plot(WL_lo_id_dispersed_ang(1, :), arrayfun(@(wn) wl_hi_ang2(indClosestVal(wl_hi_ang2, WL_lo_id_dispersed_ang(1, wn))) - WL_lo_id_dispersed_ang(1, wn), 1:length(WL_lo_id_dispersed_ang(1, :))), '.-k')
%         plot(wl_line_hi_ds_ang, wl_hi_ang2(indClosestVal(wl_hi_ang2, wl_line_hi_ds_ang)) - wl_line_hi_ds_ang, '*m')
        
%         qf(12,1); plot(wl_hi_ang2(1:end-1), diff(wl_hi_ang2), '.k')
        

        flux_hi_atmT_ds_adu = normpdf(wl_hi_ang2, wl_hi_ang2(indClosestVal(wl_hi_ang2, wl_line_hi_ds_ang)), lineWidthSigma_ang);
        
        flux_hi_atmT_ds_adu = 1000 - normScale(flux_hi_atmT_ds_adu, 0, 1000 * (1 - lineIntensity_adu));
        
        flux_hi_atmT_ds_orig_adu = 1000 * ones(1, size(wl_hi_orig_ang, 2));
        
        
        % Recreate original image (for plotting)
        flux_hi_adu = normpdf(wl_hi_ang2, wl_hi_ang2(indClosestVal(wl_hi_ang2, lineLoc_ang)), lineWidthSigma_ang);
        
        flux_hi_adu = 1000 - normScale(flux_hi_adu, 0, 1000 * (1 - lineIntensity_adu));
        
        flux_hi_orig_adu = 1000 * ones(size(Y_hi_px, 1), size(wl_hi_orig_ang, 2));
        
        
        % Recreate comb
        flux_hi_comb_adu = createComb(wl_hi_ang, y_hi_px);
        
        flux_hi_comb_orig_adu = createComb(wl_hi_orig_ang, y_hi_px);
        
        
        % Recreate LSF
        LSF_inst_invariable = createLSF(WL_lo_id_dispersed_ang(1, :), wl_hi_ang, R_invar_LSF);
        
        LSF_inst_invariable_orig = createLSF(wl_lo_orig_id_ang(1, :), wl_hi_orig_ang, R_invar_LSF);
        
        
        % Remake Y array
        Y_hi_px = repmat(Y_hi_px(:, 1), 1, length(wl_hi_ang));
        
        Y_hi_orig_px = repmat(Y_hi_px(:, 1), 1, length(wl_hi_orig_ang));
        
        
        % Remake WL_hi array
        WL_hi_ang = repmat(wl_hi_ang, length(y_hi_px), 1);
        
        WL_hi_orig_ang = repmat(wl_hi_orig_ang, length(y_hi_px), 1);
        
    else
        
        flux_hi_atmT_ds_adu = interp1( ...
            wl_hi_ang, flux_hi_atmT_adu, wl_hi_ds_ang, 'spline', nanmean(flux_hi_atmT_adu(:)));
    end
    
else
    
    flux_hi_atmT_ds_adu = flux_hi_atmT_adu;
end



% Rescale intensity to a higher value
if addNoise_q
    if length(noiseLevel) > 1
        flux_hi_atmT_ds_adu = (flux_hi_atmT_ds_adu / max(flux_hi_atmT_ds_adu(:)) * noiseLevel(obsNum)^2);
        
        if singleLineSpectrum_q
            flux_hi_atmT_ds_orig_adu = (flux_hi_atmT_ds_orig_adu / max(flux_hi_atmT_ds_orig_adu(:)) * noiseLevel(obsNum)^2);
        end
    else
        flux_hi_atmT_ds_adu = (flux_hi_atmT_ds_adu / max(flux_hi_atmT_ds_adu(:)) * noiseLevel^2);
        
        if singleLineSpectrum_q
            flux_hi_atmT_ds_orig_adu = (flux_hi_atmT_ds_orig_adu / max(flux_hi_atmT_ds_orig_adu(:)) * noiseLevel^2);
        end
    end
else
    flux_hi_atmT_ds_adu = (flux_hi_atmT_ds_adu / max(flux_hi_atmT_ds_adu(:)) * 10000);
    
    if singleLineSpectrum_q
        flux_hi_atmT_ds_orig_adu = (flux_hi_atmT_ds_orig_adu / max(flux_hi_atmT_ds_orig_adu(:)) * 10000);
    end
end


%% Replicate rows to create 2D spectra
flux_hi_atmT_ds_2D_adu = repmat(flux_hi_atmT_ds_adu, length(y_hi_px), 1);

if singleLineSpectrum_q
    flux_hi_atmT_ds_2D_orig_adu = repmat(flux_hi_atmT_ds_orig_adu, length(y_hi_px), 1);
end


%% Combine the comb and spectra images

flux_hi_atmT_ds_2D_comb_adu = flux_hi_comb_adu .* flux_hi_atmT_ds_2D_adu;

if singleLineSpectrum_q
    flux_hi_atmT_ds_2D_comb_orig_adu = flux_hi_comb_orig_adu .* flux_hi_atmT_ds_2D_orig_adu;
end


%% Convolve synth spectra with the LSF

if applyVariableLSF_q
    
    flux_hi_atmT_ds_2D_comb_LSF_adu = convSV_LSF(flux_hi_atmT_ds_2D_comb_adu, LSF_inst_chunks);
    
else
    
    goodCols = isfinite(flux_hi_atmT_ds_2D_comb_adu(1,:));
    
    flux_hi_atmT_ds_2D_comb_LSF_adu = flux_hi_atmT_ds_2D_comb_adu;
    
    flux_hi_atmT_ds_2D_comb_LSF_adu(:, goodCols) = conv2(flux_hi_atmT_ds_2D_comb_adu(:, goodCols), LSF_inst_invariable, 'same');
    
    cutoff = 1:sum(LSF_inst_invariable > max(LSF_inst_invariable) * 0.25);
    
    flux_hi_atmT_ds_2D_comb_LSF_adu(:, [cutoff, (end - cutoff)]) = nan;
    
%     qf(322,1); imagesc(flux_hi_atmT_ds_2D_comb_adu)
    
    if singleLineSpectrum_q
        
        goodCols = isfinite(flux_hi_atmT_ds_2D_comb_orig_adu(1,:));
        
        flux_hi_atmT_ds_2D_comb_LSF_orig_adu = flux_hi_atmT_ds_2D_comb_orig_adu;
        
        flux_hi_atmT_ds_2D_comb_LSF_orig_adu(:, goodCols) = conv2(flux_hi_atmT_ds_2D_comb_orig_adu(:, goodCols), LSF_inst_invariable_orig, 'same');
        
        cutoff = 1:sum(LSF_inst_invariable_orig > max(LSF_inst_invariable_orig) * 0.10);
        
        flux_hi_atmT_ds_2D_comb_LSF_orig_adu(:, [cutoff, (end - cutoff)]) = nan;
        
        
        cutoff = 1:round(0.27 / abs(wl_hi_orig_ang(2) - wl_hi_orig_ang(1)) * 10);
        
        flux_hi_atmT_ds_2D_comb_LSF_orig_adu(:, [cutoff, (end - cutoff)]) = nan;
        
        %qf(322,1); plot(wl_hi_orig_ang, mean(flux_hi_atmT_ds_2D_comb_LSF_orig_adu, 1), '-k')
        
        
    end
end


if GPU_calulations_q
    flux_hi_atmT_ds_2D_comb_adu = gather(flux_hi_atmT_ds_2D_comb_adu);
    flux_hi_atmT_ds_2D_comb_LSF_adu = gather(flux_hi_atmT_ds_2D_comb_LSF_adu);
end



%% Downsample spectrum

if binningDownsample_q == 1
    
    flux_deslanted_adu = binImgResizeWL_V2(flux_hi_atmT_ds_2D_comb_LSF_adu, Y_hi_px(:, 1)', WL_lo_id_dispersed_ang(1, :), WL_hi_ang(1, :));
    
else
    
    goodCols = isfinite(flux_hi_atmT_ds_2D_comb_LSF_adu(1,:));
    
    if applyFringeDistortion_q
        
        [~, ii] = intersect(wl_lo_id_dispersed_orig_ang, wl_lo_id_dispersed_ang);
        
        flux_deslanted_adu = interp2( ...
            WL_hi_ang(:, goodCols), Y_hi_px(:, goodCols), flux_hi_atmT_ds_2D_comb_LSF_adu(:, goodCols), ...
            WL_lo_id_dispersed_ang, Y_lo_px + FDD_px{obsNum}(:, ii), 'spline');    
    else
        flux_deslanted_adu = interp2( ...
            WL_hi_ang(:, goodCols), Y_hi_px(:, goodCols), flux_hi_atmT_ds_2D_comb_LSF_adu(:, goodCols), ...
            WL_lo_id_dispersed_ang, Y_lo_px, 'spline');
    end
    
    if singleLineSpectrum_q
        
        goodCols = isfinite(flux_hi_atmT_ds_2D_comb_LSF_orig_adu(1,:));
        
        if applyFringeDistortion_q
            flux_deslanted_orig_adu = interp2( ...
                WL_hi_orig_ang(:, goodCols), Y_hi_orig_px(:, goodCols), flux_hi_atmT_ds_2D_comb_LSF_orig_adu(:, goodCols), ...
                WL_lo_id_dispersed_orig_ang, repmat(Y_lo_px(:, 1), 1, size(WL_lo_id_dispersed_orig_ang, 2)) + FDD_px{obsNum}, 'spline');
        else
            flux_deslanted_orig_adu = interp2( ...
                WL_hi_orig_ang(:, goodCols), Y_hi_orig_px(:, goodCols), flux_hi_atmT_ds_2D_comb_LSF_orig_adu(:, goodCols), ...
                WL_lo_id_dispersed_orig_ang, repmat(Y_lo_px(:, 1), 1, size(WL_lo_id_dispersed_orig_ang, 2)), 'spline');
        end
        
        
        bi = ~isfinite(sum(flux_deslanted_adu, 1));
        
        flux_deslanted_adu(:, bi) = [];
        
        WL_lo_id_dispersed_ang(:, bi) = [];
        
        wl_lo_id_dispersed_ang(:, bi) = [];
        
        %qf(2132,1); plot(WL_lo_id_dispersed_orig_ang(1,:), mean(flux_deslanted_orig_adu, 1), 'o-k', WL_lo_id_dispersed_ang(1,:), mean(flux_deslanted_adu, 1), '+-m')
        
        rep_ind = cell2mat(arrayfun(@(cn) indClosestVal(WL_lo_id_dispersed_orig_ang(1,:), WL_lo_id_dispersed_ang(1, cn)), 1:size(WL_lo_id_dispersed_ang, 2), 'uniformoutput', 0));
        
        rep_wlDiff = cell2mat(arrayfun(@(cn) (WL_lo_id_dispersed_orig_ang(1, rep_ind(cn)) - WL_lo_id_dispersed_ang(1, cn)), 1:length(rep_ind), 'uniformoutput', 0));
        
        rep_fluxDiff = mean(flux_deslanted_orig_adu(:, [rep_ind(2), rep_ind(end)-1]), 1) - mean(flux_deslanted_adu(:, [2, size(flux_deslanted_adu,2)-1]), 1);
        
        
        
%         if any(abs(rep_wlDiff) > 0.01) | any(abs(rep_fluxDiff) > 0.1)
%            
%             error('Single Line replacement failed. Program halted')
%         end
        
        flux_deslanted_orig_adu(:, rep_ind) = flux_deslanted_adu;
        
        flux_deslanted_adu = flux_deslanted_orig_adu;
        
        wl_lo_id_dispersed_ang = wl_lo_id_dispersed_orig_ang;
        
        flux_deslanted_adu(:, [1:20, end - (1:20)]) = nan;
    end
end

if addNoise_q
    if length(noiseLevel) > 1
        flux_deslanted_adu = floor(flux_deslanted_adu / max(flux_deslanted_adu(:)) * noiseLevel(obsNum)^2);
    else
        flux_deslanted_adu = floor(flux_deslanted_adu / max(flux_deslanted_adu(:)) * noiseLevel^2);
    end
else
    flux_deslanted_adu = floor(flux_deslanted_adu / max(flux_deslanted_adu(:)) * 10000);
end

%% Normalize

% flux_deslanted_adu = flux_deslanted_adu / max(flux_deslanted_adu(:));

if saveHighRes_q
    
%     goodCols = isfinite(flux_hi_atmT_ds_2D_comb_LSF_adu(1,:));
    
    wl_deslanted_hi_ang = wl_hi_ang;%(:, goodCols);
    
    flux_hi_atmT_ds_2D_comb_LSF_filt_adu = flux_hi_atmT_ds_2D_comb_LSF_adu;%(:, goodCols);
%     
%     validWL_ind = indClosestVal(wl_deslanted_hi_ang, WL_lo_id_dispersed_ang(1)) : indClosestVal(wl_deslanted_hi_ang, WL_lo_id_dispersed_ang(end));
    
%     wl_deslanted_hi_ang = wl_deslanted_hi_ang;%(validWL_ind);
    
%     flux_deslanted_hi_adu = flux_hi_atmT_ds_2D_comb_LSF_filt_adu;%(:, validWL_ind);
    
    flux_deslanted_hi_adu = flux_hi_atmT_ds_2D_comb_LSF_filt_adu / max(flux_hi_atmT_ds_2D_comb_LSF_filt_adu(:));
    
end


%% Conform to MARVELS illumination profile
if matchMARVELSprofile_q
    flux_deslanted_adu = profileTemplate{obsNum} .* flux_deslanted_adu;
end


% %for noiseLevel = [1, 5, 8, (10 :10: 90), 95, 99]
% for noiseLevel = [0.001 1 10 100]

%% Add noise to the image

% flux_deslanted_adu = flux_deslanted_adu;

flux_deslanted_noise_adu = flux_deslanted_adu;

if addNoise_q
    
%     %goodCols = isfinite(flux_deslanted_adu(1,:));
%     
%     %if length(noiseLevel) > 1
% %         I_noise = photNoiseGen(noiseLevel(obsNum), flux_deslanted_adu);
% %     else
% %         I_noise = photNoiseGen(noiseLevel, flux_deslanted_adu);
% %     end
%     
%     I_noise = photNoiseGen(1, flux_deslanted_adu);
%     
% %     flux_deslanted_noise_adu(:, goodCols) = imnoise(flux_deslanted_adu(:, goodCols) .* noiseLevel / 1e12, 'poisson') .* 1e12 / noiseLevel;
%     
%     if length(noiseLevel) > 1
%         flux_deslanted_noise_adu = (flux_deslanted_noise_adu .* ( noiseLevel(obsNum) + I_noise ) );
%     else
%         flux_deslanted_noise_adu = (flux_deslanted_noise_adu .* ( noiseLevel + I_noise ) );
%     end
    

       flux_deslanted_noise_adu = poissrnd(flux_deslanted_adu);

    
%     for rn = 1:size(flux_deslanted_noise_adu, 1)
%         flux_deslanted_noise_adu(rn, :) = arrayfun(@(cn) poissrnd(flux_deslanted_adu(rn, cn), 1, 1), 1:size(flux_deslanted_noise_adu, 2));
%         
%         %flux_deslanted_noise_adu(rn, :) = arrayfun(@(cn)  normrnd(flux_deslanted_adu(rn, cn), sqrt(flux_deslanted_adu(rn, cn)), 1, 1), 1:size(flux_deslanted_noise_adu, 2));
%     end




%     I_noise = flux_deslanted_noise_adu - flux_deslanted_adu;
%     
%     xx = 1:size(WL_lo_id_dispersed_ang, 2);
%     xx = setdiff(xx, indClosestVal(WL_lo_id_dispersed_ang(1,:), lineLoc_ang * (1 + RV_BCV_mps(obsNum) / c_mps)) + (-3:3));
%     ii = flux_deslanted_adu(:, xx);
%     nn = I_noise(:, xx);
%     %SNR_comb = std(ii(:)) / std(nn(:));
%     SNR_comb = mean(ii(:)) / std(nn(:));
%     
%     noiseLevel(obsNum)
%     SNR_comb
    
    flux_deslanted_adu = flux_deslanted_noise_adu;
end


% qf(1,1); subplot(3,1,1); imagesc(flux_deslanted_adu); subplot(3,1,2); imagesc(flux_deslanted_noise_adu); subplot(3,1,3); imagesc(I_noise);


% %fitswrite(flux_deslanted_adu, ['E:\Research\sankalpNoiseSpectra\I_star_SNR', strrep(n2s((100/noiseLevel)), '.', 'p'), '_adu.fits'])
% fitswrite(flux_deslanted_noise_adu, ['E:\Research\sankalpNoiseSpectra\I_', type0, '_SNR', n2s((100/noiseLevel), 3), '_adu.fits'])
% end
% % fitswrite(flux_deslanted_adu, 'E:\Research\sankalpNoiseSpectra\I_star_CLEAN_adu.fits')

1;

%% Create spectra save name
% fitsSavePath = strcat(spectra_deslanted_orig_dir, s, upper(type0), '_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.fits');
% matSavePath = strcat(spectra_deslanted_orig_dir, s, upper(type0), '_beam', beamNumStr, '_obs', obsNumStr, spectrum_deslanted_fileName, '.mat');


%% Save the spectra
% fitswrite(flux_deslanted_adu, fitsSavePath)
% flux_adu = flux_deslanted_adu;
% save(matSavePath, 'flux_adu')

% fitswrite(flux_deslanted_adu, dirFinder(pipeline_dir, plateName, beamNum, obsNum, 'spectra', 'fits', lower(type0), 'deslanted', 'flux', 'adu', 3, 1, 1));

I_deslanted_adu = flux_deslanted_noise_adu;
% save(matSavePath, 'flux_adu')
save(dirFinder(pipeline_dir, plateName, beamNum, obsNum, 'spectra', 'mat', lower(type0), 'deslanted', 'flux', 'adu', 3, 1, 1), 'I_deslanted_adu')

% % Save WL solution
% fileDir0 = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'wlsoln_rest');
% mkdir(fileDir0)
% fileName0 = strcat(fileDir0, s, upper(type0), '_beam', beamNumStr, '_allObs_wlsoln.mat');
% 
% if 0 ~= exist(fileName0, 'file') & obsNum ~= 1
%     load(fileName0, 'wl_allObs_ang')
% else
%     wl_allObs_ang = nan(obsNum, 4001);
% end
% wl_allObs_ang(obsNum, :) = wl_lo_id_dispersed_ang;
% 
% save(fileName0, 'wl_allObs_ang')

fileName0 = dirFinder(pipeline_dir, plateName, beamNum, [], 'WL', 'mat', lower(type0), 'actual', 'WL', 'ang', 3, 1, 1);

if 0 ~= exist(fileName0, 'file')
    load(fileName0, 'wl_allObs_ang')
    if size(wl_allObs_ang, 2) ~= length(wl_lo_id_dispersed_ang)
        wl_allObs_ang = nan(obsNum, length(wl_lo_id_dispersed_ang));
    end
else
    wl_allObs_ang = nan(obsNum, length(wl_lo_id_dispersed_ang));
end
wl_allObs_ang(obsNum, :) = wl_lo_id_dispersed_ang;

save(fileName0, 'wl_allObs_ang')
% load(dirFinder(pipeline_dir, plateName, beamNum, [], 'WL', 'mat', lower(type0), 'actual', 'WL', 'ang', 3, 1, 1), 'wl_allObs_ang')

if saveHighRes_q
    
    I_deslanted_hi_adu = flux_deslanted_hi_adu;
    
    save(dirFinder(pipeline_dir, plateName, beamNum, obsNum, 'spectra', 'mat', lower(type0), 'deslanted', 'hi_flux_wl', 'adu', 3, 1, 1), 'I_deslanted_hi_adu', 'wl_deslanted_hi_ang')
end


%% Star Plots

if singleLineSpectrum_q
    zoomWL_ang = lineLoc_ang * (1 + RV_BCV_mps(obsNum) / c_mps) + [-2 2] * 0.27;
    %indClosestVal(wl_lo_ang , lineLoc_ang) + [-10, 10];
else
    zoomWL_ang = [5320 5335];
end

% Continuum plots
if stageContinuumPlot_q
    synthContinuumPlots_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
        wl_lo_id_dispersed_ang, wl_hi_ang, ...
        flux_hi_adu, flux_hi_atmT_ds_adu, flux_hi_atmT_ds_2D_comb_adu, flux_hi_atmT_ds_2D_comb_LSF_adu, flux_deslanted_adu, type0, zoomWL_ang)
end

% Image plots
if stageImagePlot_q
    synthImagePlots_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
        wl_lo_id_dispersed_ang, wl_hi_ang, ...
        flux_hi_adu, flux_hi_atmT_ds_adu, flux_hi_atmT_ds_2D_comb_adu, flux_hi_atmT_ds_2D_comb_LSF_adu, flux_deslanted_adu, type0, zoomWL_ang)
end

% Final spectrum continuum plot
if finalSpectraPlot_q
    synthFinalContinuumPlot_logspace(s, pipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
        wl_lo_id_dispersed_ang, flux_deslanted_adu, type0)
end


if realComparisonPlot_q
%     realAndSynthComparison(s, pipeline_dir, plateName, beamNumStr, obsNum, flux_deslanted_adu, 1, type0, 1, plateEmulationName, templateBeam)
end


% qf(10,1); plot(1:length(wl_lo_id_dispersed_ang), nanmean(I_deslanted_adu,1), '-k'); grid on

1;









