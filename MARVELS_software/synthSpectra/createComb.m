function I = createComb(wl_hi_ang, y_hi_px)



[WL_hi_ang, Y_hi_px] = meshgrid(wl_hi_ang * 1e-10, y_hi_px);


% ep = wl_2_epsilon_F({y_hi_px, wl_hi_ang * 1e-10});
ep = 6.3e-8 * ones(length(y_hi_px), length(wl_hi_ang));

% d0 = ppval(wl_2_d0_pp, wl_hi_ang * 1e-10);
% d0 = linspace(7.03, 7.01, length(wl_hi_ang)) * 1e-3;
d0 = 0.007 * ones(1, length(wl_hi_ang));
% bi = wl_2_d0_pp.breaks(1) > wl_hi_ang * 1e-10;
% d0(bi) = nan;

d0 = repmat(d0, length(y_hi_px), 1);

I = (1 + cos(2 * pi * (d0 + ep .* Y_hi_px) ./ (WL_hi_ang)) ) / 2;


% qf(1,1); imagesc(flux_hi_comb_adu)

% Rename comb flux
% if GPU_calulations_q
%     flux_hi_comb_adu = gpuArray(flux_hi_comb_adu);
% end

% load(dirFinder(pipeline_dir, [], [], [], 'WL', 'mat', [], [], 'beam_WL_estimates_legacy', 'ang', 3, 1, 0), 'wlsoln')
% 
% wl_ang = squeeze(wlsoln(:, 1, beamNum))';
% 
% period_px = ( wl_ang*1e-10 / ( 6.3e-8 ));



