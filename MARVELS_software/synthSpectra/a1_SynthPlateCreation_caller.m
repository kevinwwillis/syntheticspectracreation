% function a1_SynthPlateCreation_caller
 
% This script is where all parameters can be changed before calling for synthetic spectra creation.
 
 
%% User Options
 
%% General
 
pipeline_dir = pwd; %SYNTHETICSPECTRACREATION
 
 templatePlateName = 'HIP14810';     % Star based on which plate? Tries to simulate all relevant params (WL, flux profile, etc.), unless noted otherwise in the options below.
%  templatePlateName = 'FIELD1572';     % Star based on which plate? Tries to simulate all relevant params (WL, flux profile, etc.), unless noted otherwise in the options below.
 
templateBeamNum = 101;%87;              % Star based on which beam?
%  templateBeamNum = 87;
 
%beamNums = 1;                       % Beams to create (only use 1; Software not ready for more than 1 right now)
 
forceNewModel_q = 0;                % If 0, program uses pipeline derived obs model #. If 1, uses new forced model number
 
modelObsNum = 1;                    % If forceNewModel_q is set to 1, this is the new model obs #
 
spectrumWidth_px = 4001;            % Choose pixel width of each spectra
 
spectrumHeight_px = 26;             % Choose pixel height of each spectra
 
GPU_calulations_q = 0;              % Perform calulations on a GPU.

parallelProcessing_q = 0;           % Multi-CPU processing.
 
spectraQuantity = 0;                % If 0, follows other rules, if not, only this quantity is created.
 
forcedObsNums_q = 0;                % Create only the obs numbers supplied
 
obsNums_forced = [];                 % Create only the obs numbers supplied
 
forcedPlateName_q = 1;              % 0 = auto name creation, 1 = forced plate name
 
% plateName_forced = 'synth_hiResShftTest_v1';%'synth_noiseTest_noID_noBCV_v2';        % The forced plate name
% plateName_forced = 'synth_noiseTest_noID_noBCV_v5_geTest';        % The forced plate name
% plateName_forced = 'synth_IP_v2';        % The forced plate name
% plateName_forced = 'synth_dVdP_CalibrationSet_v2';
% plateName_forced = 'synth_singleLine_obs1_tSet_RV30kms_v10';
plateName_forced = 'synth_DoNotUse';
% plateName_forced = 'synth_HIP14810_b101_ideal_1SL';

% plateName_forced = 'synth_singleLine_obs_RV30kms_DISPER_v11';
% plateName_forced = 'synth_singleLine_obs_RV30kms_Tset_v11';
 
makeTIOspectra_q = 1;
 
makeSTARspectra_q = 1;
 
resetSpectraDir_q = 0;
 
saveHighRes_q = 0;

%% Fringe distortion

applyFringeDistortion_q = 0;
 
%% Instrument Drift
 
applyInstrumentDrift_q = 0;              
 
forcedInstrumentDrifts_q = 0;       % 1 = used supplied inst drifts, 0 = Add random instrument drift
 
forcedInstrumentDrifts_px = [-5 :0.1: 5];
 
 
%% Orbit params
 
randomBCV_q = 0;                    % Add randomized BCVs
 
copyReal_RVs_q = 0;
 
forcedRVs_q = 1;                    % If 1, the program will simulate the RVs specified below in the variable "RV_mps"
 
% RV_mps = randi([-1e3 1e3], 1, length(repmat([(0 :0.1: 10)], 1, 10)));                        % [m/s] Leave empty array, "[]", to automatically fill with zeros of appropriate length
% RV_mps = randi([-3e4 3e4], 1, length(repmat([(10 :10: 100)], 1, 50)));                        % [m/s] Leave empty array, "[]", to automatically fill with zeros of appropriate length
% RV_mps = randi([-3e4 3e4], 1, 50);                        % [m/s] Leave empty array, "[]", to automatically fill with zeros of appropriate length
% RV_mps = [-30012.123, -20079.465, -10095.321, 10077.444, 20062.689, 29968.777];                        % [m/s] Leave empty array, "[]", to automatically fill with zeros of appropriate length
% RV_mps = RV_mps(1:length(noiseLevel));
% RV_mps = -30056.6454;

% RV_mps = -30000 + (-500 :20: 500);
RV_mps = repmat(randi([-30000000 + [-50000 50000]], 1, 11) * 1e-3, 1, 50);

 
maxRV_mps = 50000;                  % [m/s] Maximum RV to create. Values above, if stated, override this.
 
BCVmax_mps = 0;                     % [m/s] Max barycentric velocity to use
 
copyReal_BCVs_q = 0;
 
forced_BCVs_q = 0;
 
BCVs_mps = [];
 
orbitPeriod_day = 300;              % [days] Obit period of planet
 
surveyDaySpan_day = 600;            % [days] Survey span
 
%% WL
 
linearWLsol_q = 0;                  % Linear WL spacing from pixel to pixel (dispersion)
 
% WL solution choice
 
useNolanWLsolutions_q = 0;            % Retrieve Nolan's corrected WL solutions for the dispersion calculations
 
useKevinWLsolutions_q = 1;            % Retrieve Kevin's corrected WL solutions for the dispersion calculations
 
useSynthWLsolutions_q = 0;            % Generate randomly dispersed WL solutions
 
% Dispersion
 
addDispersion_q = 0;                 % Toggle adding dispersion into the spectrum
 
useFullBeamDispersion_q = 1;         % SCRIPT: "findRealDispersion(fileDir)"
 
applyTiO_dispersion_q = 0;
 
applyStar_dispersion_q = 1;
 
WLdispersionLimit_ang = 0.0001;          % Limits (max and min) on randomized dispersion profile dWL range
 
meanCenterDispersionStar_q = 0;
 
meanCenterDispersionTIO_q = 0;
 
correctStarDispByTIOdispOffsets_q = 0;
 
setDispProfZeroAt2000px_q = 0;
 
%% Stellar Spectrum Type
 
% Single Line Spectrum
singleLineSpectrum_q = 0;
 
lineLoc_ang = 5300;
 
lineIntensity_adu = 0.5;
 
lineWidthSigma_ang = 0.039;
 
%
 
%% Binary Spectra
 
makeBinarySpectra_q = 0; 
 
useSimData_q = 1;

% simFile_dir = '';
% simFile_dir = 'E:\Research\syntheticSpectraCreation\binary_orbitParams.mat';
simFile_dir = dirFinder(pipeline_dir, 'FIELD1572_testSet1', 87, [], 'orbitParams', 'mat', [], 'simulation', 'MISC', 'NA', 3, 1, 1);

forcedLinearObsDates_q = 0;

forcedObsCount = 5;            % How many linearly spaced observation dates?
 
 
%% Noise
 
addNoise_q = 1;                   % Add Possion noise to the spectra
 
% noiseLevel = sort(repmat([(10 :10: 100)], 1, 50));                   % Controls the amount of Possion noise applied
% noiseLevel = 50;                   % Controls the amount of Possion noise applied
noiseLevel = sort(repmat([(20 :10: 100), 150, 200], 1, 50));                   % Controls the amount of Possion noise applied


%% Gaussian Noise (for RVs)
 
addGausianNoise_q = 0; % Adds Gaussian Noise to the RVs
 
gaussianNoise_Variance = 100; % Variance; adjust accordingly 
 
gaussianNoise_Mean = 0; % Mean value; zero by default


%% LSF
 
R_invar_LSF = 11000;                % Resolution for the spatially invariant LSF
 
applyVariableLSF_q = 0;             % Apply a spatially variant LSF to the spectra
 
LSF_chunkWidth = 1000;              % Spatially variant LSF granularity is defined here by pixel width of each LSF applicaiton area
 
R_left_inst = 9000;                % Instrument resolution at low WLs
 
R_right_inst = 16000;               % Instrument resolution at high WLs
 
%% Downsampling techniques
 
binningDownsample_q = 0;              % Downsample spectra by binning instead of interpolation
 

 
%% Atmospheric effects (DO NOT USE. IS DISABLED)
 
applyAtmTurbBlur_q = 0;             % Applies atmospheric turbulance blur to the spectrum
 
R_left_atmTurb = 100000;            % Resolution at low wavelengths
 
R_right_atmTurb = 1000000;            % Resolution at high wavelengths
 
 

%% Illumination profile
 
matchMARVELSprofile_q = 0;          % Applies a typical MARVELS illumination profile to the spectra

synthIlluminationProfiles_q = 1;
 
profileNormalization_q = 0;         % Normalizes the illumination profile so that ADU is not scaled to normal MARVELS spectum ADU values
 

%% Plots
 
plots_q                     = 1;                        % Global toggle for plot viewing
 
tio_realComparisonPlot_q    = 1;
tio_obsDispersionPlot_q     = 1;
tio_stageContinuumPlot_q    = 1;
tio_finalSpectraPlot_q      = 1;
tio_stageImagePlot_q        = 1;
 
star_realComparisonPlot_q   = 1;
star_obsDispersionPlot_q    = 1;
star_stageContinuumPlot_q   = 1;
star_finalSpectraPlot_q     = 1;
star_stageImagePlot_q       = 1;
 
% tio_realComparisonPlot_q    = 0;
% tio_obsDispersionPlot_q     = 0;
% tio_stageContinuumPlot_q    = 0;
% tio_finalSpectraPlot_q      = 0;
% tio_stageImagePlot_q        = 0;
%  
% star_realComparisonPlot_q   = 0;
% star_obsDispersionPlot_q    = 0;
% star_stageContinuumPlot_q   = 0;
% star_finalSpectraPlot_q     = 0;
% star_stageImagePlot_q       = 0;

 
%%
 
 
 
s = '\';%dslash;
 
if strcmp(s, '\')
    
    neil_dir = 'R:\astro\data\marvels\nthomas819Working\pipeline\May15';
    
    nolan_dir = 'R:\astro\data\et2\ngrieves\marvels_pipeline\May15_wln';
    
else
    neil_dir = '/astro/data/marvels/nthomas819Working/pipeline/May15';
    
    nolan_dir = '/astro/data/et2/ngrieves/marvels_pipeline/May15_wln';
end
 
 
%% Get binary data
 
if makeBinarySpectra_q & useSimData_q
    
    singleLineSpectrum_q = 0;
    
    forcedRVs_q = 1;
    
    if isempty(simFile_dir)
        simFile_dir = dirFinder(pipeline_dir, templatePlateName, templateBeamNum, [], 'orbitParams', 'mat', [], 'simulation', 'MISC', 'NA', 3, 1, 0);
    end
    
    load(simFile_dir, 'hist', 'd')
    
    simFile2_dir = dirFinder(pipeline_dir, templatePlateName, templateBeamNum, [], 'orbitParams', 'mat', [], 'solved_two2bdyMeth', 'MISC', 'NA', 3, 1, 0);
    
    if 0 == exist(simFile2_dir, 'file')
        
        orbitParams.p1.t.known = days(seconds(hist.t(sort(randi([1, length(hist.t)], 1, forcedObsCount)))));
        
        orbitParams.s1.m.final = d.masses(1).mass;
        
        orbitParams.s2.m.final = d.masses(2).mass;
        
    else
        
        load(simFile2_dir, 'orbitParams')
    end
    
    [RV_mps, lumRat, JDs] = binarySim2SpectraModule(orbitParams, hist, d, forcedLinearObsDates_q, forcedObsCount);
else
    JDs = [];
end
 
 
%% Variable corrections
 
if ~makeBinarySpectra_q
   lumRat = []; 
end
 
 
if makeTIOspectra_q == 0
    
end
 
if makeSTARspectra_q == 0
    
    forcedRVs_q = 0;
    RV_mps = [];
    randomBCV_q = 0;
end
 
 
if forcedRVs_q == 0
    RV_mps = [];
end
 
if forcedInstrumentDrifts_q == 0
   forcedInstrumentDrifts_px = [];
end
 
if ~addNoise_q
    noiseLevel = [];
end
 
if singleLineSpectrum_q
    spectrumWidth_px = 30;
end
 
 
%% Error checking and warnings
 
if forcedRVs_q & isempty(RV_mps)
    
    error('Option "forcedRVs_q" was true, but the array "RV_mps" was empty. Set "forcedRVs_q" to false or add RVs to simulate')
end
 
if forcedInstrumentDrifts_q & isempty(forcedInstrumentDrifts_px)
    
    error('Option "forcedInstrumentDrifts_q" was true, but the array "forcedInstrumentDrifts_px" was empty. Set "forcedInstrumentDrifts_q" to false or add RVs to simulate')
end
 
if forcedInstrumentDrifts_q & forcedRVs_q
   
    if length(RV_mps(1,:)) ~= length(forcedInstrumentDrifts_px)
       
        error('Forced RV and Instrument Drift arrays are of different sizes. They must match in size.')
    end
end
 
if makeTIOspectra_q == 0 & forcedInstrumentDrifts_q
    
    warning('WARNING: You are creating stellar spectra with instrument drifts, without TIO calibration spectra')
end
 
 
if copyReal_RVs_q
    
    if (templateBeamNum == 101 || templateBeamNum == 102) && strcmp(templatePlateName, 'HIP14810')
        
    elseif (templateBeamNum == 53 || templateBeamNum == 54) && strcmp(templatePlateName, 'HD118203')
        
    else
        error('Option "copyReal_RVs_q" was true, but the current template plate & beam do not have actual RVs known.')
    end
end
 
if copyReal_RVs_q & forcedRVs_q
    error('Options "copyReal_RVs_q" and "forcedRVs_q" are both true. Only one can be true.')
end
 
if copyReal_BCVs_q & randomBCV_q
    error('Options "copyReal_BCVs_q" and "randomBCV_q" are both true. Only one can be true.')
end
 
if copyReal_BCVs_q & forced_BCVs_q
    error('Options "copyReal_BCVs_q" and "forced_BCVs_q" are both true. Only one can be true.')
end
 
%%
 
 
if forcedRVs_q
    
    spectraQuantity = length(RV_mps(1,:));  % How many observations
    
elseif forcedInstrumentDrifts_q
    
    spectraQuantity = length(forcedInstrumentDrifts_px);    % How many observations
end
 
 
 
 
 
 
synthPlateCreationFunction_v2_0_0(...
    GPU_calulations_q, applyInstrumentDrift_q, randomBCV_q, binningDownsample_q, addDispersion_q, linearWLsol_q, resetSpectraDir_q, saveHighRes_q, ...
    useFullBeamDispersion_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, ...
    meanCenterDispersionStar_q, meanCenterDispersionTIO_q, correctStarDispByTIOdispOffsets_q, setDispProfZeroAt2000px_q, ...
    applyTiO_dispersion_q, applyStar_dispersion_q, templatePlateName, templateBeamNum, addNoise_q, noiseLevel, ...
    applyVariableLSF_q, R_left_inst, R_right_inst, R_invar_LSF, applyAtmTurbBlur_q, R_left_atmTurb, R_right_atmTurb, ...
    LSF_chunkWidth, matchMARVELSprofile_q, profileNormalization_q, BCVmax_mps, spectrumWidth_px, ...
    spectrumHeight_px, maxRV_mps, orbitPeriod_day, surveyDaySpan_day, plots_q, ...
    copyReal_RVs_q, copyReal_BCVs_q, forced_BCVs_q, BCVs_mps, ...
    tio_realComparisonPlot_q, tio_obsDispersionPlot_q, tio_stageContinuumPlot_q, tio_finalSpectraPlot_q, tio_stageImagePlot_q, ...
    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q, ...
    forcedInstrumentDrifts_q, forcedInstrumentDrifts_px, ...
    singleLineSpectrum_q, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
    makeBinarySpectra_q, lumRat, JDs, synthIlluminationProfiles_q, ...
    addGausianNoise_q, gaussianNoise_Variance, gaussianNoise_Mean, ...
    applyFringeDistortion_q, ...
    spectraQuantity, WLdispersionLimit_ang, RV_mps, forceNewModel_q, modelObsNum, forcedObsNums_q, obsNums_forced, forcedPlateName_q, plateName_forced, makeTIOspectra_q, makeSTARspectra_q, ...
    pipeline_dir, neil_dir, nolan_dir, parallelProcessing_q)

