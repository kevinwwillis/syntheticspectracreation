function synthPlateCreationFunction_v2_0_0(...
    GPU_calulations_q, applyInstDrift_q, randomBCV_q, binningDownsample_q, addDispersion_q, linearWLsol_q, resetSpectraDir_q, saveHighRes_q, ...
    useFullBeamDispersion_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, ...
    meanZeroDispersionStar_q, meanZeroDispersionTIO_q, correctStarDispByTIOdispOffsets_q, setDispProfZeroAt2000px_q, ...
    applyTIOdispersion_q, applySTARdispersion_q, templatePlateName, beamNum, addNoise_q, noiseLevel, ...
    applyVariableLSF_q, R_left_inst, R_right_inst, R_invar_LSF, applyAtmTurbBlur_q, R_left_atmTurb, R_right_atmTurb, ...
    LSF_chunkWidth, matchMARVELSprofile_q, profileNormalization_q, BCVmax_mps, spectrumPixelWidth, ...
    spectrumPixelHeight, maxRV_mps, orbitPeriod_day, surveyDaySpan_day, plots_q, ...
    copyReal_RVs_q, copyReal_BCVs_q, forced_BCVs_q, BCVs_mps, ...
    tio_realComparisonPlot_q, tio_obsDispersionPlot_q, tio_stageContinuumPlot_q, tio_finalSpectraPlot_q, tio_stageImagePlot_q, ...
    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q, ...
    forcedInstrumentDrifts_q, forcedInstrumentDrifts_px, ...
    singleLineSpectrum_q, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
    makeBinarySpectra_q, lumRat, JDs, synthIlluminationProfiles_q, ...
    addGausianNoise_q, gaussianNoise_Variance, gaussianNoise_Mean, ...
    applyFringeDistortion_q, ...
    spectraQuantity, WLdispersionLimit_ang, RV_mps, forceNewModel_q, modelObsNum_forced, forcedObsNums_q, obsNums_forced, forcedPlateName_q, plateName_forced, makeTIOspectra_q, makeSTARspectra_q, ...
    pipeline_dir, neil_dir, nolan_dir, parallelProcessing_q)


% create a typical plate of spectra synthetically with their own unique parameters




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

s = dslash;




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create plate save name

if forcedPlateName_q
    
    plateName = plateName_forced;
    
else
    plateName = synthPlateNameCreation(s, pipeline_dir, templatePlateName, beamNum, ...
        addDispersion_q, linearWLsol_q, applyTIOdispersion_q, applySTARdispersion_q, meanZeroDispersionStar_q, ...
        meanZeroDispersionTIO_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, setDispProfZeroAt2000px_q, addNoise_q);
end


%%

x_lo_px = 1:spectrumPixelWidth;

y_lo_px = 1:spectrumPixelHeight;


%%
if forcedObsNums_q
    obsNums = obsNums_forced;
    
elseif spectraQuantity ~= 0
    obsNums = 1:spectraQuantity;
    
else
    % Load BCV array to determine how many observations were used
    if 0 == exist(dirFinder(pipeline_dir, templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 0), 'file')
        
        if 0 == exist(neil_dir, 'dir')
            error(cat(2, 'A local copy of the BCV values for ', templatePlateName, ' does not exist. The remote directory cannot be reached to download the proper file ("bc.mat"). Check remote connection.'))
        else
            getNeilFile(s, dirFinder(pipeline_dir, templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 1), strcat(neil_dir, s, templatePlateName, s, 'results', s, 'bc.mat'))
        end
    end
    load(dirFinder(pipeline_dir, templatePlateName, [], [], 'RV', 'mat', [], [], 'BCV', 'mps', 3, 1, 0), 'bc');
    
    spectraQuantity = size(bc, 2);
    
    obsNums = 1:spectraQuantity;
end


%% Determine model observation #

% % Load model observation number
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'file')
%     getNeilFile(s, strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), strcat(neil_dir, s, templatePlateName, s, 'results', s, 'model.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, templatePlateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'model');
%
% modelObsNum_orig = model(beamNum); clear model

% Is new model obs number being forced?
if forceNewModel_q
    
    % Sanity check
    if forcedObsNums_q & 0 == sum(modelObsNum_forced == obsNums_forced)
        
        cprintf([1,0,0],  '\n FATAL ERROR: ')
        cprintf(-[1,0,0],  'Forced obs model # is not included in the forced obs numbers array. Change the plate creation settings.')
        cprintf(-[1,0,0], '\n');
        
        return
    end
    
    modelObsNum = modelObsNum_forced;
else
    modelObsNum = 1;
end


%% WL Dispersion Load/Creation

if addDispersion_q
    
%     [dWL_dispersion_star_ang, dWL_dispersion_tio_ang, badWLsolutions_star, badWLsolutions_tio, spectraQuantity] = createDispersionData(...
%         s, pipeline_dir, nolan_dir, neil_dir, plateName, templatePlateName, beamNum, spectraQuantity, WLdispersionLimit_ang, ...
%         applyTIOdispersion_q, applySTARdispersion_q, useNolanWLsolutions_q, useKevinWLsolutions_q, useSynthWLsolutions_q, setDispProfZeroAt2000px_q, correctStarDispByTIOdispOffsets_q, ...
%         meanZeroDispersionTIO_q, meanZeroDispersionStar_q, modelObsNum);
    
    
    for on = obsNums


        dWL_dispersion_star_ang(on, :) = normScale( polyval( polyfit( linspace(1, 4001, 5), randi([-10000, 10000], 1, 5), 4), 1:4001), -0.001, 0.001);
        
        %qf(82,1); plot(dWL_dispersion_star_ang)
        
        dWL_dispersion_tio_ang(on, :) = dWL_dispersion_star_ang(on, :);
        
        %dWL_dispersion_tio_ang(on, :) = normScale( polyval( polyfit( linspace(1, 4001, 5), randi([-10000, 10000], 1, 5), 4), 1:4001), -0.001, 0.001);
        
    end

    dWL_dispersion_star_ang(isnan(dWL_dispersion_star_ang)) = 0;
    dWL_dispersion_tio_ang(isnan(dWL_dispersion_tio_ang)) = 0;
    
else
    badWLsolutions_tio = [];
    badWLsolutions_star = [];
    
    dWL_dispersion_star_ang = [];
    dWL_dispersion_tio_ang = [];
end

% Recalibrate input RVs array
if isempty(RV_mps)
    RV_mps = zeros(1, size(spectraQuantity, 1));
end


%% Create synth plate params


[wl_lo_ang, plateName, BCV_mps, RV_BCV_mps, dX_id_px] = createSynthParams(...
    s, pipeline_dir, templatePlateName, beamNum, orbitPeriod_day, maxRV_mps, RV_mps, obsNums, modelObsNum, JDs, ...
    copyReal_RVs_q, copyReal_BCVs_q, forced_BCVs_q, BCVs_mps, ...
    forcedInstrumentDrifts_q, forcedInstrumentDrifts_px, ...
    BCVmax_mps, spectraQuantity, surveyDaySpan_day, plateName, applyInstDrift_q, randomBCV_q, linearWLsol_q, makeBinarySpectra_q);

wl_lo_orig_ang = wl_lo_ang;
wl_hi_orig_ang = [];


if addGausianNoise_q
    
    if makeBinarySpectra_q
        
        RV_noise_mps(1,:) = sqrt(gaussianNoise_Variance) * randn(1, size(RV_BCV_mps(1, :), 2)) + gaussianNoise_Mean;
        RV_noise_mps(2,:) = sqrt(gaussianNoise_Variance) * randn(1, size(RV_BCV_mps(2, :), 2)) + gaussianNoise_Mean;
        
        RV_BCV_mps = RV_BCV_mps + RV_noise_mps;
        
    else
        
        RV_noise_mps = sqrt(gaussianNoise_Variance) * randn(1, size(RV_BCV_mps, 2)) + gaussianNoise_Mean;
        
        RV_BCV_mps = RV_BCV_mps + RV_noise_mps;
    end
else
    
    RV_noise_mps = zeros(size(RV_BCV_mps));
end

if makeBinarySpectra_q
    
    RV_star1_BCV_mps = RV_BCV_mps(1, :);
    RV_star2_BCV_mps = RV_BCV_mps(2, :);
end


%% Load hi-res Sun and TIO spectra sampled at the wl range of the comb img

% Star
if singleLineSpectrum_q
    
    load([pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'templates', s, 'star_master_synth_template.mat'], 'wl_ang')
    
    lineLoc_ind = indClosestVal(wl_lo_ang, lineLoc_ang);
    
    if spectrumPixelWidth ~= 4001
        
        wl_crop_ind = (1:spectrumPixelWidth) + lineLoc_ind;
        
        wl_crop_ind = wl_crop_ind - round(spectrumPixelWidth / 2);
        
        wl_lo_ang = wl_lo_ang(wl_crop_ind);
    end
    
    gi = wl_ang > (wl_lo_ang(1) - 10) & wl_ang < (wl_lo_ang(end) + 10);
    
    wl_ang = wl_ang(gi);
    
    lineLoc_ind = indClosestVal(wl_ang, lineLoc_ang);
    
    wl_hi_orig_ang = wl_lo_orig_ang(1) :0.01: wl_lo_orig_ang(end);
    
    %wl_ang = wl_ang(1:10:end);
    
    % Create a single spectral line
    %     sigma = ( ( wl_ang(1) / R / diff(wl_ang(1:2)) ) / 2.3548);
    %
    %     singleLine_adu = normpdf(wl_ang, lineLoc_ang, 0.01);
    %
    %     singleLine_adu = max(singleLine_adu(:)) * 1.01 - singleLine_adu;
    %
    %     singleLine_adu = singleLine_adu / max(singleLine_adu(:));
    
    singleLine_adu = ones(size(wl_ang));
    singleLine_adu(lineLoc_ind) = lineIntensity_adu;
    
    %     qf(10, 1); plot(wl_ang, singleLine_adu, '-k'); xlim([wl_ang(1) wl_ang(end)]); grid on
    
    flux_adu = singleLine_adu;
    
    1;
else
    
    load([pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'templates', s, 'star_master_synth_template.mat'], 'flux_adu', 'wl_ang')
    
    gi = wl_ang > (wl_lo_ang(1) - 10) & wl_ang < (wl_lo_ang(end) + 10);
    
    wl_ang = wl_ang(gi);
    flux_adu = flux_adu(gi);
    
    wl_ang = wl_ang(1:10:end);
    flux_adu = flux_adu(1:10:end);
    
    wl_crop_ind = 1:4001;
end


wl_star_hi_ang = wl_ang(:)';
flux_hiR_star_adu = flux_adu(:)';

clear wl_ang
clear flux_adu


% TIO
load([pipeline_dir, s, 'MARVELS_data', s, 'spectra', s, 'templates', s, 'tio_template_R300000_marvelsRange.mat'], 'flux_adu', 'wl_ang')

% flux_hiR_tio_adu = interp1(wl_ang(:)', flux_adu(:)', wl_star_hi_ang, 'spline', nan);

gi = wl_ang > (wl_lo_orig_ang(1) - 10) & wl_ang < (wl_lo_orig_ang(end) + 10);

wl_ang = wl_ang(gi);
flux_adu = flux_adu(gi);

wl_tio_hi_ang = wl_ang(1:10:end);
flux_hiR_tio_adu = flux_adu(1:10:end);

clear wl_ang
clear flux_adu



%% Load master comb and its params

% load([pipeline_dir, s, 'MARVELS_data', s, 'comb', s, 'templates', s, 'combParamEstimates_b101.mat'], 'wl_2_epsilon_F', 'wl_2_d0_pp')

y_hi_px = 1 :0.1: spectrumPixelHeight;


% Star

[WL_star_hi_ang, Y_star_hi_px] = meshgrid(wl_star_hi_ang, y_hi_px);

% ep = wl_2_epsilon_F({y_hi_px, wl_hi_ang * 1e-10});
ep = 6.3e-8 * ones(length(y_hi_px), length(wl_star_hi_ang));

% d0 = ppval(wl_2_d0_pp, wl_hi_ang * 1e-10);
% d0 = linspace(7.03, 7.01, length(wl_hi_ang)) * 1e-3;
d0 = 0.007 * ones(1, length(wl_star_hi_ang));
% bi = wl_2_d0_pp.breaks(1) > wl_hi_ang * 1e-10;
% d0(bi) = nan;

d0 = repmat(d0, length(y_hi_px), 1);

flux_star_hi_comb_adu = (1 + cos(2 * pi * (d0 + ep .* Y_star_hi_px) ./ (WL_star_hi_ang * 1e-10)) ) / 2;

% TIO

[WL_tio_hi_ang, Y_tio_hi_px] = meshgrid(wl_tio_hi_ang, y_hi_px);

ep = 6.3e-8 * ones(length(y_hi_px), length(wl_tio_hi_ang));

d0 = 0.007 * ones(1, length(wl_tio_hi_ang));

d0 = repmat(d0, length(y_hi_px), 1);

flux_tio_hi_comb_adu = (1 + cos(2 * pi * (d0 + ep .* Y_tio_hi_px) ./ (WL_tio_hi_ang * 1e-10)) ) / 2;


clear ep d0

% qf(1,1); imagesc(flux_hi_comb_adu)

% Rename comb flux
if GPU_calulations_q
    flux_star_hi_comb_adu = gpuArray(flux_star_hi_comb_adu);
end

load(dirFinder(pipeline_dir, [], [], [], 'WL', 'mat', [], [], 'beam_WL_estimates_legacy', 'ang', 3, 1, 0), 'wlsoln')

wl_ang = squeeze(wlsoln(:, 1, beamNum))';

period_px = ( wl_ang*1e-10 / ( 6.3e-8 ));



%% Restructure beam WL array and convert it to logspace

wl_lo_tio_ang = wl_lo_orig_ang;

wl_lo_star_ang = wl_lo_ang;



% %% Create directory structure and retrieve dir/file names
%
% % Load needed directory names
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, plateName, '_directories.mat'), ...
%     'results_1D_dir', 'star_spectra_deslanted_orig_dir', 'tio_spectra_deslanted_orig_dir')
%
% % Create file names then load the needed names
% MARVELS_fileNames(pipeline_dir)
% load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'spectrum_deslanted_fileName');


%% Create plate directories

% % Star
% if resetSpectraDir_q; rmdir(star_spectra_deslanted_orig_dir,'s'); end
% mkdir(star_spectra_deslanted_orig_dir);
%
%
% % TIO
% % Define img load and save locations
% if resetSpectraDir_q; rmdir(tio_spectra_deslanted_orig_dir,'s'); end
% mkdir(tio_spectra_deslanted_orig_dir);




%% Fringe Distiortion

if applyFringeDistortion_q
    
    [XX, YY] = meshgrid(1:4001, y_lo_px);
    
    yp_lb = -2 * ones(1, 4);
    yp_ub = 2 * ones(1, 4);
    
    xis = linspace(1, 4001, length(yp_lb));
    yis = linspace(1, y_lo_px(end), length(yp_lb));
    
    [XX0, YY0] = meshgrid(xis, yis);
    
    FDD_px = cell(length(obsNums), 1);
    
    for obsNum = obsNums
        
        for xi = 1:length(xis)
            
            for yi = 1:length(xis)
                
                Zy(yi, xi) = randi([yp_lb(yi), yp_ub(yi)] * 1000, 1, 1) / 1000;
            end
        end
        
        
        Z0 = Zy;
        
        Z = polyFit2D(Z0, XX0, YY0, 2, 3);
        
        FD_tmp_px = polyVal2D(Z, XX, YY, 2, 3);
        
%         qf(1,1);
%         subplot(2,1,1); mesh(XX0, YY0, Z0); grid on
%         subplot(2,1,2); mesh(XX, YY, FD_tmp_px); grid on
        
        FDD_px{obsNum} = FD_tmp_px;
    end
    
else
    FDD_px = [];
end



%% Illumination profile matching

if matchMARVELSprofile_q
    
    %     if profileNormalization_q
    %
    %         %load('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfileNorm')
    %         [~, smoothedProfileNorm] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
    %         profileTemplate = smoothedProfileNorm; clear smoothedProfileNorm
    %     else
    %
    %         %load('E:\Research\1a_syntheticPipelineTestEnv\HIP14810_beam101_avgObsProfile.mat', 'smoothedProfile')
    %         [smoothedProfile, ~] = getSpectraProfile(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'), 1);
    %         profileTemplate = smoothedProfile; clear smoothedProfile
    %     end
    
    
    if synthIlluminationProfiles_q
        
        [XX, YY] = meshgrid(1:4001, y_lo_px);
        
        yp_lb = [10, 20, 30, 40, 30, 20, 10];
        yp_ub = [10, 20, 30, 40, 30, 20, 10] * 5;
        
        
        xp_lb = [10, 20, 30, 40, 30, 20, 10] * 100;
        xp_ub = [10, 20, 30, 40, 30, 20, 10] * 1.2 * 100;
        
        xis = linspace(1, 4001, length(xp_lb));
        yis = linspace(1, y_lo_px(end), length(yp_lb));
        
        [XX0, YY0] = meshgrid(xis, yis);
        
        profileTemplate = cell(length(obsNums), 1);
        
        for obsNum = obsNums
            
            for xi = 1:length(xis)
                Zx(1, xi) = randi([xp_lb(xi), xp_ub(xi)], 1, 1);
                
                for yi = 1:length(xis)
                    Zy(yi, xi) = randi([yp_lb(yi), yp_ub(yi)], 1, 1);
                end
            end

            
            Z0 = repmat(Zx, length(Zy), 1) .* Zy;%repmat(Zy, 1, length(Zx));
            
            Z = polyFit2D(Z0, XX0, YY0, 3, 4);
            
            profileTemplate_tmp = polyVal2D(Z, XX, YY, 3, 4);
            
%             qf(1,1); 
%             subplot(2,1,1); mesh(XX0, YY0, Z0); grid on
%             subplot(2,1,2); mesh(XX, YY, profileTemplate_tmp); grid on
            
            profileTemplate{obsNum} = profileTemplate_tmp / max(profileTemplate_tmp(:));
        end
        
    else
        
        load(dirFinder(pipeline_dir, templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0), 'MJDobs')
        
        profileTemplate = cell(length(obsNums), 1);
        
        if length(obsNums) > size(MJDobs, 1)
            
            obs2Use = randi([1, size(MJDobs, 1)], 1, length(obsNums));
        else
            obs2Use = obsNums;
        end
        
        for obsNum = obsNums
            
            load(dirFinder(pipeline_dir, templatePlateName, beamNum, obs2Use(obsNum), 'spectra', 'mat', 'star', 'deslanted', 'flux', 'adu', 3, 1, 0), 'I_deslanted_adu')
            
            if profileNormalization_q
                [~, profileTemplate_tmp] = getSpectraProfile(I_deslanted_adu, 0);
            else
                [profileTemplate_tmp, ~] = getSpectraProfile(I_deslanted_adu, 0);
            end
            
            
            if length(x_lo_px) ~= size(profileTemplate_tmp, 2) & length(y_lo_px) ~= size(profileTemplate_tmp, 1)
                
                [XX, YY] = meshgrid(1:size(profileTemplate_tmp, 2), 1:size(profileTemplate_tmp, 1));
                [XX2, YY2] = meshgrid(x_lo_px, y_lo_px);
                profileTemplate_tmp = interp2(XX, YY, profileTemplate_tmp, XX2, YY2);
                clear XX YY
            end
            
            [XX, YY] = meshgrid(1:size(profileTemplate_tmp, 2), 1:size(profileTemplate_tmp, 1));
            
            pp9 = polyFit2D(profileTemplate_tmp, XX, YY, 3, 2);
            
            profileTemplate_tmp = polyVal2D(pp9, XX, YY, 3, 2);
            
            profileTemplate{obsNum} = profileTemplate_tmp / max(profileTemplate_tmp(:));
        end
    end
else
    profileTemplate = [];
end



%% Create Atm Turbulance LSF data
if applyAtmTurbBlur_q
    [PSF_AtmTurb_chunks, ~] = generateVariantLSF(R_left_atmTurb, R_right_atmTurb, wl_star_hi_ang, LSF_chunkWidth);
else
    PSF_AtmTurb_chunks = [];
end



%% Create deadObs_ind.mat File

% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'results_1D', s, 'deadObs_ind.mat'), 'file')

deadObs_ind = ones(120, spectraQuantity);
%     mkdir(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'results_1D'))
%     save(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'results_1D', s, 'deadObs_ind.mat'), 'deadObs_ind')

save(dirFinder(pipeline_dir, plateName, [], [], 'filter', 'mat', [], 'badObs', 'obs', 'num', 3, 1, 1), 'deadObs_ind')
% end




%% Preallocation of varaibles

%[combLineSep_px, fringePeriod_px, combLineSlope_px] = deal(nan(spectrumPixelWidth, 1));

totalShift_px_star = nan(1, spectraQuantity(end), spectrumPixelWidth);
totalShift_chunks_px_star = nan(1, spectraQuantity(end), 32);


beamNumStr = num2str(beamNum);


%% Create gridded data for final spectrum downsample interpolation
[WL_lo_tio_ang, ~] = meshgrid(wl_lo_tio_ang, y_lo_px);
[WL_lo_star_ang, Y_lo_px] = meshgrid(wl_lo_star_ang, y_lo_px);


%% Create LSF
if applyVariableLSF_q
    
    %     [LSF_inst_chunks, R_variant_chunks, wl_center_ang] = generateVariantLSF(R_left_inst, R_right_inst, wl_hi_ang, LSF_chunkWidth);
    %
    %     % Spatially variant LSF plot
    %     xM_hiR = size(wl_hi_ang, 2);
    %     chunkCount = floor((xM_hiR - mod(xM_hiR, 2)) / LSF_chunkWidth);
    %     chunkLeftEdges = 1:LSF_chunkWidth:(xM_hiR - mod(xM_hiR, 2));
    %     chunkLeftEdges(end) = [];
    %     chunkRightEdges = chunkLeftEdges + LSF_chunkWidth - 1;
    %     chunkCenters = round((chunkRightEdges(1) - chunkLeftEdges(1)) / 2 + chunkLeftEdges);
    %
    %
    %     figure(93); clf(figure(93))
    %     subplot(10, 1, 1:7); hold on
    %     titlel('Instument LSF (Spatially variant)')
    %     for chunkNum = 1:chunkCount
    %         %       [X, Y, Z] = meshgrid(chunkLeftEdges(chunkNum):100:chunkRightEdges(chunkNum), 1:length(LSF_inst_chunks(chunkNum, :)), LSF_inst_chunks(chunkNum, :));
    %         %       surf(X, Y, Z)
    %
    %         [X, Y] = meshgrid(wl_hi_ang([chunkLeftEdges(chunkNum), chunkRightEdges(chunkNum) - 1]), 1:length(LSF_inst_chunks(chunkNum, :)));
    %         mesh(X, Y, repmat(LSF_inst_chunks(chunkNum, :), length([chunkLeftEdges(chunkNum), chunkRightEdges(chunkNum) - 1]), 1)', 'edgecolor', 'interp', 'edgelighting', 'gouraud')
    %         colormap jet
    %     end
    %     xlabel('$\lambda [\AA]$')
    %     ylabel('LSF Channel [px]')
    %     zlabel('$\Vert$LSF Value$\Vert$')
    %     axis([min(wl_hi_ang), max(wl_hi_ang), 1, size(LSF_inst_chunks, 2), min(LSF_inst_chunks(:)), max(LSF_inst_chunks(:))])
    %     grid on
    %     view(-65, 36)
    %     hold off
    %
    %     subplot(10, 1, 9:10); hold on
    %     titlel('Resolution')
    %     stairs(wl_hi_ang([chunkLeftEdges, chunkRightEdges(end)]), [R_variant_chunks, R_variant_chunks(end)], '-k')
    %     grid on
    %     axis([min(wl_hi_ang), max(wl_hi_ang), min(R_variant_chunks), max(R_variant_chunks)])
    %     xlabel('$\lambda [\AA]$')
    %     ylabel('Optical Resolution')
    %     hold off
    %
    %     plot_LSF_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots', s, 'LSF');
    %     try mkdir(plot_LSF_dir); catch; end
    %     print(gcf, '-r0', '-dpng', strcat(plot_LSF_dir, s, 'star_beam', beamNumStr, '_spatiallyVariantLSF_plot.png'))
    
else
    
    LSF_inst_chunks = [];
    
    LSF_inst_invariable_tio = createLSF(wl_lo_tio_ang, wl_tio_hi_ang, R_invar_LSF);
    LSF_inst_invariable_star = createLSF(wl_lo_star_ang, wl_star_hi_ang, R_invar_LSF);
end


% TIO instrument drift conversion arrays (LINEAR PIXEL SHIFTING)
WL2PX_tio_pp = spline(wl_lo_tio_ang(1, :), (1:length(wl_lo_tio_ang(1, :))));
WL2PX_star_pp = spline(wl_lo_star_ang(1, :), (1:length(wl_lo_star_ang(1, :))));

X_hi_tio_px = ppval(WL2PX_tio_pp, wl_star_hi_ang);
X_hi_star_px = ppval(WL2PX_star_pp, wl_star_hi_ang);

%X_hiR_tio_px = ppval(WL2PX_tio_pp, wl_hiR_comb_ang);
%X_hiR_star_px = ppval(WL2PX_star_pp, wl_hiR_comb_ang);



if parallelProcessing_q
    try pp = parpool(1); catch; end
end

if makeTIOspectra_q
    
    if parallelProcessing_q
        parfor obsNum = obsNums
            
            disp(cat(2, 'Working on obs ', num2str(obsNum)))
            
            %% TIO
            createSingleObsSynthSpectrum_v2(s, pipeline_dir, plateName, beamNum, obsNum, 'TIO', templatePlateName, beamNum, 0, ...
                wl_tio_hi_ang, WL_tio_hi_ang, wl_lo_tio_ang, WL_lo_tio_ang, dWL_dispersion_tio_ang, wl_lo_orig_ang, wl_hi_orig_ang, 1:length(wl_lo_tio_ang), ...
                flux_hiR_tio_adu, flux_tio_hi_comb_adu, ...
                RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_tio, R_invar_LSF, ...
                y_lo_px, Y_lo_px, Y_tio_hi_px, y_hi_px, ...
                applyFringeDistortion_q, FDD_px, ...
                0, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
                addDispersion_q, useFullBeamDispersion_q, applyTIOdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                tio_realComparisonPlot_q, tio_obsDispersionPlot_q, tio_stageContinuumPlot_q, tio_finalSpectraPlot_q, tio_stageImagePlot_q)
        end
    else
        for obsNum = obsNums
            
            disp(cat(2, 'Working on obs ', num2str(obsNum)))
            
            %% TIO
            createSingleObsSynthSpectrum_v2(s, pipeline_dir, plateName, beamNum, obsNum, 'TIO', templatePlateName, beamNum, 0, ...
                wl_tio_hi_ang, WL_tio_hi_ang, wl_lo_tio_ang, WL_lo_tio_ang, dWL_dispersion_tio_ang, wl_lo_orig_ang, wl_hi_orig_ang, 1:length(wl_lo_tio_ang), ...
                flux_hiR_tio_adu, flux_tio_hi_comb_adu, ...
                RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_tio, R_invar_LSF, ...
                y_lo_px, Y_lo_px, Y_tio_hi_px, y_hi_px, ...
                applyFringeDistortion_q, FDD_px, ...
                0, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
                addDispersion_q, useFullBeamDispersion_q, applyTIOdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                tio_realComparisonPlot_q, tio_obsDispersionPlot_q, tio_stageContinuumPlot_q, tio_finalSpectraPlot_q, tio_stageImagePlot_q)
        end
        
    end
    
end


if makeSTARspectra_q
    
    if makeBinarySpectra_q
        
        if parallelProcessing_q
            parfor obsNum = obsNums
                
                disp(cat(2, 'Working on obs ', num2str(obsNum)))
                
                createSingleObsBinarySynthSpectrum(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, ...
                    wl_star_hi_ang, WL_star_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, ...
                    flux_hiR_star_adu, flux_star_hi_comb_adu, ...
                    RV_star1_BCV_mps, RV_star2_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
                    y_lo_px, Y_lo_px, Y_star_hi_px, y_hi_px, ...
                    lumRat, ...
                    addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
                
            end
            
        else
            for obsNum = obsNums
                
                disp(cat(2, 'Working on obs ', num2str(obsNum)))
                
                createSingleObsBinarySynthSpectrum(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, ...
                    wl_star_hi_ang, WL_star_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, ...
                    flux_hiR_star_adu, flux_star_hi_comb_adu, ...
                    RV_star1_BCV_mps, RV_star2_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
                    y_lo_px, Y_lo_px, Y_star_hi_px, y_hi_px, ...
                    lumRat, ...
                    addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
                
            end
            
        end
        
    else
        
        if parallelProcessing_q
            parfor obsNum = obsNums
                
                disp(cat(2, 'Working on obs ', num2str(obsNum)))
                
                %% STAR
                createSingleObsSynthSpectrum_v2(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, 0, ...
                    wl_star_hi_ang, WL_star_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, wl_lo_orig_ang, wl_hi_orig_ang, wl_crop_ind, ...
                    flux_hiR_star_adu, flux_star_hi_comb_adu, ...
                    RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, R_invar_LSF, ...
                    y_lo_px, Y_lo_px, Y_star_hi_px, y_hi_px, ...
                    applyFringeDistortion_q, FDD_px, ...
                    singleLineSpectrum_q, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
                    addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
                
            end
            
        else
            
            for obsNum = obsNums
                
                disp(cat(2, 'Working on obs ', num2str(obsNum)))
                
                %% STAR
                createSingleObsSynthSpectrum_v2(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, 0, ...
                    wl_star_hi_ang, WL_star_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, wl_lo_orig_ang, wl_hi_orig_ang, wl_crop_ind, ...
                    flux_hiR_star_adu, flux_star_hi_comb_adu, ...
                    RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, R_invar_LSF, ...
                    y_lo_px, Y_lo_px, Y_star_hi_px, y_hi_px, ...
                    applyFringeDistortion_q, FDD_px, ...
                    singleLineSpectrum_q, lineLoc_ang, lineIntensity_adu, lineWidthSigma_ang, ...
                    addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                    star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
                
            end
        end
        
        
        if saveHighRes_q
            
            if parallelProcessing_q
                parfor obsNum = obsNums
                    
                    disp(cat(2, 'Working on obs ', num2str(obsNum)))
                    
                    createSingleObsSynthSpectrum_hiRes(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, ...
                        wl_star_hi_ang, WL_star_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, ...
                        flux_hiR_star_adu, flux_star_hi_comb_adu, ...
                        RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
                        y_lo_px, Y_lo_px, Y_star_hi_px, y_hi_px, ...
                        addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                        star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
                end
                
            else
                for obsNum = obsNums
                    
                    disp(cat(2, 'Working on obs ', num2str(obsNum)))
                    
                    createSingleObsSynthSpectrum_hiRes(s, pipeline_dir, plateName, beamNum, obsNum, 'STAR', templatePlateName, beamNum, ...
                        wl_star_hi_ang, WL_star_hi_ang, wl_lo_star_ang, WL_lo_star_ang, dWL_dispersion_star_ang, ...
                        flux_hiR_star_adu, flux_star_hi_comb_adu, ...
                        RV_BCV_mps, dX_id_px, profileTemplate, noiseLevel, PSF_AtmTurb_chunks, LSF_inst_chunks, LSF_inst_invariable_star, ...
                        y_lo_px, Y_lo_px, Y_star_hi_px, y_hi_px, ...
                        addDispersion_q, useFullBeamDispersion_q, applySTARdispersion_q, applyAtmTurbBlur_q, binningDownsample_q, matchMARVELSprofile_q, addNoise_q, applyVariableLSF_q, GPU_calulations_q, ...
                        star_realComparisonPlot_q, star_obsDispersionPlot_q, star_stageContinuumPlot_q, star_finalSpectraPlot_q, star_stageImagePlot_q)
                end
            end
        end
    end
end

% save(strcat(synthPipelineDir, s, plateName, s, 'synthParams.mat'), ...
%   'RV_mps', 'BCV_mps', 'dX_id_px', 'wl_instDrift_ang_star', 'wl_instDrift_ang_tio', 'totalShift_px_star', ...
%   'combLineSep_px_star', 'fringePeriod_px_star', 'combLineSlope_px_star', 'phaseShift_rad_star', 'phaseShift_px_star', ...
%   'combLineSep_px_tio', 'fringePeriod_px_tio', 'combLineSlope_px_tio', 'phaseShift_rad_tio', 'phaseShift_px_tio');


load(strcat('synthParamsFileTemplate.mat'), 'synthParams')

if 1%~makeBinarySpectra_q
    
    if singleLineSpectrum_q
        
        synthParams{end + 1, 1} = 'singleLineSpectrum_q';
        synthParams{end + 1, 1} = 'lineLoc_ang';
        synthParams{end + 1, 1} = 'lineIntensity_adu';
        synthParams{end + 1, 1} = 'lineWidthSigma_ang';
        synthParams{end + 1, 1} = 'RV_whiteNoise_mps';
        
        
        
        synthParams(:, 2) = {...
            plateName...
            [beamNum]...
            beamNum...
            spectraQuantity...
            modelObsNum...
            templatePlateName...
            spectrumPixelWidth...
            spectrumPixelHeight...
            RV_mps(1,:)...
            BCV_mps(1,:)...
            RV_BCV_mps(1,:)...
            dX_id_px...
            GPU_calulations_q...
            linearWLsol_q ...
            applyInstDrift_q...
            randomBCV_q...
            BCVmax_mps...
            binningDownsample_q...
            addNoise_q...
            noiseLevel...
            applyVariableLSF_q...
            R_left_inst...
            R_right_inst...
            R_invar_LSF...
            applyAtmTurbBlur_q...
            R_left_atmTurb...
            R_right_atmTurb...
            LSF_chunkWidth...
            matchMARVELSprofile_q...
            profileNormalization_q...
            addDispersion_q...
            useFullBeamDispersion_q...
            applyTIOdispersion_q...
            applySTARdispersion_q...
            templatePlateName...
            badWLsolutions_tio...
            badWLsolutions_star...
            singleLineSpectrum_q...
            lineLoc_ang...
            lineIntensity_adu...
            lineWidthSigma_ang...
            RV_noise_mps...
            };
        
    else
        
        synthParams{end + 1, 1} = 'RV_whiteNoise_mps';
        
        synthParams(:, 2) = {...
            plateName...
            [beamNum]...
            beamNum...
            spectraQuantity...
            modelObsNum...
            templatePlateName...
            spectrumPixelWidth...
            spectrumPixelHeight...
            RV_mps(1,:)...
            BCV_mps(1,:)...
            RV_BCV_mps(1,:)...
            dX_id_px...
            GPU_calulations_q...
            linearWLsol_q ...
            applyInstDrift_q...
            randomBCV_q...
            BCVmax_mps...
            binningDownsample_q...
            addNoise_q...
            noiseLevel...
            applyVariableLSF_q...
            R_left_inst...
            R_right_inst...
            R_invar_LSF...
            applyAtmTurbBlur_q...
            R_left_atmTurb...
            R_right_atmTurb...
            LSF_chunkWidth...
            matchMARVELSprofile_q...
            profileNormalization_q...
            addDispersion_q...
            useFullBeamDispersion_q...
            applyTIOdispersion_q...
            applySTARdispersion_q...
            templatePlateName...
            badWLsolutions_tio...
            badWLsolutions_star...
            RV_noise_mps...
            };
    end
    
    
    mkdir(strcat(pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, plateName))
    save(strcat(pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, plateName, s, plateName, '_synthParams.mat'), ...
        'synthParams', 'RV_mps', 'BCV_mps', 'RV_BCV_mps', 'dX_id_px');
    
    % save(dirFinder(pipeline_dir, plateName, [], [], 'synthPlateInfo', 'mat', [], [], 'synth_creation', 'params', 3, 1, 1),'imgdeslant')
    
    save(dirFinder(pipeline_dir, plateName, beamNum, [], 'spectra', 'mat', 'star', 'illuminationProfile', 'flux', 'adu', 3, 1, 1), 'profileTemplate')
    
    save(dirFinder(pipeline_dir, plateName, beamNum, [], 'fringe', 'mat', 'star', 'distortion', 'translation_input', 'adu', 3, 1, 1), 'FDD_px')
    
    if applySTARdispersion_q
        save(dirFinder(pipeline_dir, plateName, beamNum, [], 'WL', 'mat', 'star', 'dispersion', 'translation_input', 'ang', 3, 1, 1), 'dWL_dispersion_star_ang')
    end
    
    if applyTIOdispersion_q
        save(dirFinder(pipeline_dir, plateName, beamNum, [], 'WL', 'mat', 'tio', 'dispersion', 'translation_input', 'ang', 3, 1, 1), 'dWL_dispersion_tio_ang')
    end
    
    save(dirFinder(pipeline_dir, plateName, beamNum, [], 'comb', 'mat', [], 'combPeriod', 'NA', 'px', 3, 1, 1), 'period_px')
    
    writetable(cell2table(synthParams, 'VariableNames', {'ParamName', 'Value'}), strcat(pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, plateName, s, plateName, '_synthParamsReport.txt'), 'Delimiter', '\t')
end

