function synthContinuumPlots_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
    wl_lo_ang, wl_hi_ang, ...
    flux_hi_adu, flux_hi_atmT_ds_adu, flux_hi_atmT_ds_2D_comb_adu, flux_hi_atmT_ds_2D_comb_LSF_adu, flux_deslanted_adu, type0, zoomWL_ang)


% wl1 = 5320;
% wl2 = 5335;

wl1 = zoomWL_ang(1);
wl2 = zoomWL_ang(2);

wl_lims = [wl_lo_ang(1) wl_lo_ang(end)];

wl_lims_zoom = [wl1 wl2];

x1 = find(min(abs(wl1-wl_lo_ang)) == abs(wl1-wl_lo_ang));
x2 = find(min(abs(wl2-wl_lo_ang)) == abs(wl2-wl_lo_ang));

x1h = find(min(abs(wl1-wl_hi_ang)) == abs(wl1-wl_hi_ang));
x2h = find(min(abs(wl2-wl_hi_ang)) == abs(wl2-wl_hi_ang));

% midh = round(size(flux_hi_adu, 1) / 2);
% 
% midl = round(size(flux_deslanted_adu, 1) / 2);

[~, midh] = max(arrayfun(@(rn) max(flux_hi_atmT_ds_2D_comb_LSF_adu(rn, x1h:x2h)) - min(flux_hi_atmT_ds_2D_comb_LSF_adu(rn, x1h:x2h)), 1:size(flux_hi_atmT_ds_2D_comb_LSF_adu, 1)));

[~, midl] = max(arrayfun(@(rn) max(flux_deslanted_adu(rn, x1:x2)) - min(flux_deslanted_adu(rn, x1:x2)), 1:size(flux_deslanted_adu, 1)));


figure(1); clf(figure(1));

% 1
subplot(5,6,1:4); hold on
if strcmp(lower(type0), 'tio')
    titlel(cat(2, 'Orig ', type0, ' Synth Spectrum'))
else
    if abs(RV_BCV_mps(obsNum)) < 0.001
        titlel(cat(2, 'Orig ', type0, ' Synth Spectrum. RV = 0 m/s'))
    else
        titlel(cat(2, 'Orig ', type0, ' Synth Spectrum. RV = ', num2str(RV_BCV_mps(obsNum)), ' m/s'))
    end
end
plot(wl_hi_ang, flux_hi_adu(1, :), '-k');
xlim(wl_lims)


set(gca,'YDir','normal')
ylabel('Norm. Flux [ADU]')
set(gca,'XTickLabel',{' '})
grid on

subplot(5,6,5:6); hold on
titlel(cat(2, 'Zoom'))
plot(wl_hi_ang(x1h:x2h), flux_hi_adu(1, x1h:x2h), '-k');
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0 inf])
set(gca,'XTickLabel',{' '})
grid on

% 2
subplot(5,6,7:10); hold on
titlel(cat(2, 'Doppler Shifted'))
plot(wl_hi_ang, flux_hi_atmT_ds_adu(1, :), '-k');
xlim(wl_lims)
ylim([0 inf])
set(gca,'YDir','normal')
set(gca,'XTickLabel',{' '})
grid on

subplot(5,6,11:12); hold on
plot(wl_hi_ang(x1h:x2h), flux_hi_atmT_ds_adu(1, x1h:x2h), '-k');
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0 inf])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
grid on

% 3
subplot(5,6,13:16); hold on
titlel(cat(2, 'Comb Multipled'))
plot(wl_hi_ang, flux_hi_atmT_ds_2D_comb_adu(midh, :), '-k');
xlim(wl_lims)
ylim([0 inf])
set(gca,'YDir','normal')
set(gca,'XTickLabel',{' '})
grid on

subplot(5,6,17:18); hold on
plot(wl_hi_ang(x1h:x2h), flux_hi_atmT_ds_2D_comb_adu(midh, x1h:x2h), '-k');
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0 inf])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
grid on

% 4
subplot(5,6,19:22); hold on
titlel(cat(2, 'LSF Applied'))
plot(wl_hi_ang, flux_hi_atmT_ds_2D_comb_LSF_adu(midh, :), '-k');
xlim(wl_lims)
ylim([0 inf])
set(gca,'YDir','normal')
set(gca,'XTickLabel',{' '})
grid on

subplot(5,6,23:24); hold on
plot(wl_hi_ang(x1h:x2h), flux_hi_atmT_ds_2D_comb_LSF_adu(midh, x1h:x2h), '-k');
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0 inf])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
grid on

% 5
subplot(5,6,25:28); hold on
titlel(cat(2, 'Downsampled (final)'))
plot(wl_lo_ang, flux_deslanted_adu(midl, :), '-k');
xlim(wl_lims)
ylim([0 inf])
xlabel('Wavelength [angstrom]')
set(gca,'YDir','normal')
grid on

subplot(5,6,29:30); hold on
plot(wl_lo_ang(x1:x2), flux_deslanted_adu(midl, x1:x2), '-k');
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0 inf])
set(gca,'YTickLabel',{' '})
xlabel('Wavelength [angstrom]')
grid on



% plot_dir = strcat(synthPipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots');
% try mkdir(strcat(plot_dir, s, 'stage_line_plot')); catch; end
% print(gcf, '-r0', '-dpng', strcat(plot_dir, s, 'stage_line_plot', s, type0, '_beam', beamNumStr, '_obs', num2str(obsNum), '_stage_line_plot.png'))
