function synthImagePlots_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
    wl_lo_ang, wl_hi_ang, ...
    flux_hi_adu, flux_hi_atmT_ds_adu, flux_hi_atmT_ds_2D_comb_adu, flux_hi_atmT_ds_2D_comb_LSF_adu, flux_deslanted_adu, type0, wl_lims_zoom)


% wl1 = 5320;
% wl2 = 5335;

wl1 = wl_lims_zoom(1);
wl2 = wl_lims_zoom(2);

wl_lims = [wl_lo_ang(1) wl_lo_ang(end)];

% wl_lims_zoom = [wl1 wl2];

x1 = find(min(abs(wl1-wl_lo_ang)) == abs(wl1-wl_lo_ang));
x2 = find(min(abs(wl2-wl_lo_ang)) == abs(wl2-wl_lo_ang));

x1h = find(min(abs(wl1-wl_hi_ang)) == abs(wl1-wl_hi_ang));
x2h = find(min(abs(wl2-wl_hi_ang)) == abs(wl2-wl_hi_ang));

x1hi = find(min(abs(wl_lo_ang(1)-wl_hi_ang)) == abs(wl_lo_ang(1)-wl_hi_ang));
x2hi = find(min(abs(wl_lo_ang(end)-wl_hi_ang)) == abs(wl_lo_ang(end)-wl_hi_ang));





figure(2); clf(figure(2));

subplot(5,6,1:4); hold on
if strcmp(lower(type0), 'tio')
    titlel(cat(2, 'Orig ', type0, ' Synth Spectrum'))
else
    if abs(RV_BCV_mps(obsNum)) < 0.001
        titlel(cat(2, 'Orig ', type0, ' Synth Spectrum. RV = 0 m/s'))
    else
        titlel(cat(2, 'Orig ', type0, ' Synth Spectrum. RV = ', num2str(RV_BCV_mps(obsNum)), ' m/s'))
    end
end
imagesc(wl_hi_ang(:, x1hi:x2hi), 1:size(flux_hi_adu, 1), flux_hi_adu(:, x1hi:x2hi)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims)
ylim([0.5 (size(flux_hi_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
hold off

subplot(5,6,5:6); hold on
titlel(cat(2, 'Zoom'))
imagesc(wl_hi_ang(x1h:x2h), 1:size(flux_hi_adu, 1), flux_hi_adu(:, x1h:x2h)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0.5 (size(flux_hi_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
hold off

subplot(5,6,7:10); hold on
titlel(cat(2, 'Doppler Shifted'))
imagesc(wl_hi_ang(:, x1hi:x2hi), 1:size(flux_hi_atmT_ds_adu, 1), flux_hi_atmT_ds_adu(:, x1hi:x2hi)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims)
ylim([0.5 (size(flux_hi_atmT_ds_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
hold off

subplot(5,6,11:12); hold on
imagesc(wl_hi_ang(x1h:x2h), 1:size(flux_hi_atmT_ds_adu, 1), flux_hi_atmT_ds_adu(:, x1h:x2h)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0.5 (size(flux_hi_atmT_ds_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
hold off

subplot(5,6,13:16); hold on
titlel(cat(2, 'Comb Multipled'))
imagesc(wl_hi_ang(:, x1hi:x2hi), 1:size(flux_hi_atmT_ds_2D_comb_adu, 1), flux_hi_atmT_ds_2D_comb_adu(:, x1hi:x2hi)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims)
ylim([0.5 (size(flux_hi_atmT_ds_2D_comb_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
hold off

subplot(5,6,17:18); hold on
imagesc(wl_hi_ang(x1h:x2h), 1:size(flux_hi_atmT_ds_2D_comb_adu, 1), flux_hi_atmT_ds_2D_comb_adu(:, x1h:x2h)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0.5 (size(flux_hi_atmT_ds_2D_comb_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
hold off

subplot(5,6,19:22); hold on
titlel(cat(2, 'LSF Applied'))
imagesc(wl_hi_ang(:, x1hi:x2hi), 1:size(flux_hi_atmT_ds_2D_comb_LSF_adu, 1), flux_hi_atmT_ds_2D_comb_LSF_adu(:, x1hi:x2hi)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims)
ylim([0.5 (size(flux_hi_atmT_ds_2D_comb_LSF_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
set(gca,'XTickLabel',{' '})
hold off

subplot(5,6,23:24); hold on
imagesc(wl_hi_ang(x1h:x2h), 1:size(flux_hi_atmT_ds_2D_comb_LSF_adu, 1), flux_hi_atmT_ds_2D_comb_LSF_adu(:, x1h:x2h)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0.5 (size(flux_hi_atmT_ds_2D_comb_LSF_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
hold off

subplot(5,6,25:28); hold on
titlel(cat(2, 'Downsampled (final)'))
imagesc(wl_lo_ang, 1:size(flux_deslanted_adu, 1), flux_deslanted_adu); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims)
ylim([0.5 (size(flux_deslanted_adu, 1) + 0.5)])
xlabel('Wavelength [angstrom]')
set(gca,'YTickLabel',{' '})
hold off

subplot(5,6,29:30); hold on
imagesc(wl_lo_ang(x1:x2), 1:size(flux_deslanted_adu, 1), flux_deslanted_adu(:, x1:x2)); colormap gray
set(gca,'YDir','normal')
xlim(wl_lims_zoom)
ylim([0.5 (size(flux_deslanted_adu, 1) + 0.5)])
set(gca,'YTickLabel',{' '})
xlabel('Wavelength [angstrom]')
hold off



% plot_dir = strcat(synthPipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots');
% try mkdir(strcat(plot_dir, s, 'stage_img_plot')); catch; end
% print(gcf, '-r0', '-dpng', strcat(plot_dir, s, 'stage_img_plot', s, type0, '_beam', beamNumStr, '_obs', num2str(obsNum), '_stage_img_plot.png'))





