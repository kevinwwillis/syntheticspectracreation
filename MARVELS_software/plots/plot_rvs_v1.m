function plot_rvs_v1(pipeline_dir, plateName, beamNum, rv_1D_mps, rv_new_mBCV_MZ_mps, rv_old_mBCV_MZ_mps, orbit_mps, isSynth_q)






plot_xRV_yJD_q = 0;


s = dslash;

% pipeline_dir = 'E:\Research\1a_syntheticPipelineTestEnv';
% 
% 
% neil_dir = 'R:\astro\data\marvels\nthomas819Working\pipeline\May15';
% 
% 
% 

% %% Load old RV data
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName), 'dir')
%     mkdir(strcat(pipeline_dir, s, 'plates', s, plateName))
% end
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results'), 'dir')
%     mkdir(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results'))
% end
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'file')
%     copyfile(strcat(neil_dir, s, plateName, s, 'results', s, 'model.mat'), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'model.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'model.mat'), 'model');
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'V.mat'), 'file')
%     copyfile(strcat(neil_dir, s, plateName, s, 'results', s, 'V.mat'), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'V.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'V.mat'), 'V', 'Vrawci');
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'bc.mat'), 'file')
%     copyfile(strcat(neil_dir, s, plateName, s, 'results', s, 'bc.mat'), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'bc.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'bc.mat'), 'bc');
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'MJDobs.mat'), 'file')
%     copyfile(strcat(neil_dir, s, plateName, s, 'results', s, 'MJDobs.mat'), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'MJDobs.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'MJDobs.mat'), 'MJDobs');
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), 'file')
%     copyfile(strcat(neil_dir, s, plateName, s, 'results', s, 'BRdd.mat'), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'BRdd.mat'), 'dd');
% 
% if 0 == exist(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'RVraw.mat'), 'file')
%     copyfile(strcat(neil_dir, s, plateName, s, 'results', s, 'RVraw.mat'), strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'RVraw.mat'))
% end
% load(strcat(pipeline_dir, s, 'plates', s, plateName, s, 'legacy_data', s, 'results', s, 'RVraw.mat'), 'RVraw');
% 
% 
% try
%     load(['E:\Research\MARVELS_data\RV\oldResults2D\', plateName, '_V2.mat'], 'V2')
%     
%     rv_old_mBCV_mps = V2(beamNum, :);
% catch
%     
%     rv_old_mBCV_mps = V(beamNum, :);
% end
% 
% %% Setup variables from old values
% 
% d_BCV_mps = bc(beamNum, model(beamNum)) - bc(beamNum, :);
% 
% obsIDs = 1:length(MJDobs(:, 2));
% 
% rv_old_raw_mps = squeeze(Vrawci(beamNum, :, :));
% 
% % rv_old_mBCV_mps = nanmean(rv_old_raw_mps, 2)' + d_BCV_mps;
% 
% 
% % New RV
% 
% rv_old_mBCV_MZ_mps = rv_old_mBCV_mps - nanmean(rv_old_mBCV_mps);
% 
% % rv_new_mBCV_MZ_mps = rv_new_mps - nanmean(rv_new_mps);
% 
% rv_new_mBCV_MZ_mps = rv_new_mps;
% 
% rv_new_mBCV_MZ_mps = rv_new_mBCV_MZ_mps(:)';
% 
% 
% %% Load or create orbit
% 
% if strcmp('HD118203', plateName) & any(beamNum == [53 54])
%     load('E:\Research\1D_pipeline_orig\inputs\HIRES_HD.mat', 'HIRES')
%     orbit_mps = HIRES; clear HIRES
%     
% elseif strcmp('HIP14810', plateName) & any(beamNum == [101 102])
%     load('E:\Research\1D_pipeline_orig\inputs\HIRES.mat', 'HIRES')
%     orbit_mps = HIRES; clear HIRES
% else
%     
%     load('orbitParamsNeilsGhezzi.mat')
%     plateNum = plateName2PlateNum(plateName);
% 
%     
%     if ~isempty(plateNum)
%         
%         pn = [0 ; cell2mat(orbitParamsNeils(2:end, 2))];
%         bn = [0 ; cell2mat(orbitParamsNeils(2:end, 3))];
%         rowNum = find(plateNum == pn & beamNum == bn);
%         
%         if ~isempty(rowNum)
%             
%             outlier_nsig = 2;
%             filterIterations = 3;
%             filterOutliers_q = 0;
%             paramSolverPlot_q = 1;
%             plotOrbit_q = 0;
%             
%             %         basedOnOld_q = 0;
%             %         [orbit_mps, orbit_hiR_mps, rv_filt_mps] = findSolveAndPlotOrbits(plateName, beamNum, rv_1D_mps, Vraw2_MZ, MJDobs(:, 2), outlier_nsig, filterIterations, filterOutliers_q, paramSolverPlot_q, plotOrbit_q, basedOnOld_q);
%             
%             basedOnOld_q = 1;
%             [orbit_mps, orbit_hiR_mpsOLD, rv_filt_mpsOLD] = findSolveAndPlotOrbits(plateName, beamNum, [], rv_old_mBCV_MZ_mps, MJDobs(:, 2), outlier_nsig, filterIterations, filterOutliers_q, paramSolverPlot_q, plotOrbit_q, basedOnOld_q);
%             
%         else
%             orbit_mps = zeros(size(rv_old_mBCV_mps));
%         end
%     else
%         orbit_mps = zeros(size(rv_old_mBCV_mps));
%     end
% end
% % x_hiR = spline()
% 
% % % Show discrepancy between Neil''s 'actual' orbits and mine.
% % figure(3); clf(figure(3)); hold on
% % titlel(cat(2, 'Plate: ', plateName, '  |  Beam: ', num2str(beamNum)))
% % plot(HIRES, '.-k')
% % plot(orbit_mpsOLD, '.-b')
% % plot(Vraw2_MZ, 'ok')
% % xlabel('Observation ID')
% % ylabel('RV [m/s]')
% % grid on
% % slegend(legend(...
% %     cat(2, 'Neil''s "Actual" RVs  |  RMS = ', num2str(roundsd(nanrms(Vraw2_MZ - HIRES), 3)), 'm/s'), ...
% %     cat(2, 'My Derived "Actual" RVs  |  RMS = ', num2str(roundsd(nanrms(Vraw2_MZ - orbit_mpsOLD), 3)), 'm/s'), ...
% %     'Neil''s RVs'))
% 
% 
% 
% 
% 
% %% Do LSQ fit to make best alignment of RVs
% 
gi = isfinite(rv_old_mBCV_MZ_mps);
rv_new_mBCV_MZ_mps(~gi) = nan;
% 
% gi = isfinite(rv_new_mBCV_MZ_mps);
% shift_new = fminbnd(@(off0) rms( orbit_mps(gi) - (rv_new_mBCV_MZ_mps(gi) + off0) ), -10000, 10000);
% rv_new_mBCV_MZ_mps = rv_new_mBCV_MZ_mps + shift_new;
% % rv_new_mBCV_MZ_mps = rv_new_mBCV_MZ_mps;
% 
% rv_old_mBCV_MZ_mps(~gi) = nan;
% gi = isfinite(rv_old_mBCV_MZ_mps);
% shift_neil = fminbnd(@(off0) rms( orbit_mps(gi) - (rv_old_mBCV_MZ_mps(gi) + off0) ), -10000, 10000);
% rv_old_mBCV_MZ_mps = rv_old_mBCV_MZ_mps + shift_neil;
% 
% % Neil Based orbit
% 


orbit_mps = orbit_mps - nanmean(orbit_mps);


if ~isSynth_q
    neilRVerrors_mps = rv_old_mBCV_MZ_mps - orbit_mps;
else
    rv_1D_mps = rv_1D_mps - nanmean(rv_1D_mps);
    
    newRVerrors_mps1d = rv_1D_mps - orbit_mps;
end

rv_new_mBCV_MZ_mps = rv_new_mBCV_MZ_mps - nanmean(rv_new_mBCV_MZ_mps);

newRVerrors_mps = rv_new_mBCV_MZ_mps - orbit_mps;




obsIDs = 1:length(rv_new_mBCV_MZ_mps);

% load([pipeline_dir, s, 'MARVELS_data', s, 'stellarParams', s, 'Vmag.mat', 'Vmag')
load(dirFinder(pipeline_dir, plateName, [], [], 'stellarParams', 'mat', [], [], 'beam2_Vmag_legacy', 'mag', 3, 1, 1), 'order2mag')

% pn = plateName2PlateNum(plateName);
% 
% if isempty(pn)
%    vmag = nan; 
% else
%     gvi = Vmag(:, 1) == pn & Vmag(:, 2) == beamNum;
%     
%     vmag = Vmag(gvi, 3);
% end

vmag = order2mag(beamNum, 1);

if  ~isSynth_q

    figure(521); clf(figure(521));

    subplot(10,1,1:6)
    hold on
    % if strcmp(upper(plateName(1:5)), 'SYNTH')
    %     titlel(cat(2, {cat(2, method0, ' Measured 1D RV: Synth ', type0, ' Dispersion Applied'), ...
    %         cat(2, 'Plate: ', underscoreTEXfixer(plateName), '  |  Beam: ', num2str(beamNum))}))
    % else
        titlel(cat(2, 'Plate: ', underscoreTEXfixer(plateName), '  |  Beam: ', num2str(beamNum), '  |  Vmag = ', n2s(vmag, 4)))
    % end
    plot(obsIDs, rv_old_mBCV_MZ_mps, 'o-k', 'linewidth', 2)
    plot(obsIDs, rv_new_mBCV_MZ_mps, 's-b', 'linewidth', 1.3)
    plot(obsIDs, orbit_mps, '^-g', 'linewidth', 0.5)
    %plot(obsIDs(model(beamNum)), orbit_mps(model(beamNum)), 'om')
    ylabel('RV [m/s]')
    %     xlabel('Observation ID')
    set(gca,'xticklabel',[])
    legend({'Neil''s', 'Kevin''s', 'Actual'})
    legend('boxoff')
    xlim([min(obsIDs) max(obsIDs)])
    grid on
    hold off

    subplot(10, 1, 7:8)
    hold on
    titlel(cat(2, 'Neil''s RV Error'))
    plot(obsIDs, neilRVerrors_mps, 'ok')
    plot([-10, 50], [0, 0], '-k')
    grid minor
    ylim([floor(nanmin([neilRVerrors_mps, newRVerrors_mps])), ceil(nanmax([neilRVerrors_mps, newRVerrors_mps]))])
    xlim([min(obsIDs) max(obsIDs)])
    set(gca,'xticklabel',[])
    ylabel('RV Error [m/s]')
    slegend(legend(cat(2, 'Error | RMS = ', sfnum2str(nanrms(neilRVerrors_mps), 3), ' m/s'), 'location', 'southwest'))
    %     xlabel('Observation ID')

    subplot(10, 1, 9:10)
    hold on

    titlel(cat(2, 'New RV Error'))
    plot(obsIDs, newRVerrors_mps, 'ob')
    plot([-10, 50], [0, 0], '-k')
    grid minor
    ylabel('RV Error [m/s]')
    ylim([floor(nanmin([neilRVerrors_mps, newRVerrors_mps])), ceil(nanmax([neilRVerrors_mps, newRVerrors_mps]))])
    xlim([min(obsIDs) max(obsIDs)])
    xlabel('Observation ID')
    slegend(legend(cat(2, 'Error | RMS = ', sfnum2str(nanrms(newRVerrors_mps), 3), ' m/s'), 'location', 'southwest'))


    % plot_DC_dir = strcat(pipeline_dir, s, 'plates', s, underscoreTEXfixer(plateName), s, 'plots', s, 'dispersionCorrection');
    % try mkdir(plot_DC_dir); catch; end
    % pause(0.01)
    % set(gcf, 'PaperUnits', 'inches');
    % x_width= 30 ;
    % y_width = 30;
    % set(gcf, 'PaperPosition', [0 0 x_width y_width]); %
    % print(gcf, '-r0', '-dpng', strcat(plot_DC_dir, s, method0p, '_RVandOrbit_plot.png'))

elseif sum(orbit_mps) == 444440
    
    
    qf(521, 1);
    
    titlel(cat(2, 'Final RVs | Orbit unknown | Plate: ', underscoreTEXfixer(plateName), '  |  Beam: ', num2str(beamNum), '  |  Mag = ', n2s(vmag, 4)))
    
    plot(obsIDs, rv_old_mBCV_MZ_mps, 'o-k')
    plot(obsIDs, rv_new_mBCV_MZ_mps, 's-b')
    ylabel('RV [m/s]')
    xlabel('Observation')
    legend({cat(2, 'Neil''s (RMS = ', n2s(round(nanrms(rv_old_mBCV_MZ_mps))), ' m/s)'), cat(2, 'Kevin''s (RMS = ', n2s(round(nanrms(rv_new_mBCV_MZ_mps))), ' m/s)')})
    legend('boxoff')
    xlim([min(obsIDs) max(obsIDs)])
    grid on
    
elseif isSynth_q
    
    
    figure(521); clf(figure(521));

    subplot(10,1,1:6)
    hold on
    titlel(cat(2, 'SYNTHETIC TEST: Plate: ', underscoreTEXfixer(plateName), '  |  Beam: ', num2str(beamNum), '  |  Vmag = ', n2s(vmag, 4)))
    
    plot(obsIDs, rv_1D_mps, 'o-k', 'linewidth', 2)
    plot(obsIDs, rv_new_mBCV_MZ_mps, 's-b', 'linewidth', 1.5)
    plot(obsIDs, orbit_mps, '^-g', 'linewidth', 0.5)
    %plot(obsIDs(model(beamNum)), orbit_mps(model(beamNum)), 'om')
    
    ylabel('RV [m/s]')
    set(gca,'xticklabel',[])
    legend({'1D', '2D', 'Actual'})
    legend('boxoff')
    xlim([min(obsIDs) max(obsIDs)])
    grid on
    hold off
    
    
    
        subplot(10, 1, 7:8)
    hold on
    titlel(cat(2, '1D RV Error'))
    plot(obsIDs, newRVerrors_mps1d, 'ok')
    plot([-10, 50], [0, 0], '-k')
    grid minor
    ylim([floor(nanmin([newRVerrors_mps1d, newRVerrors_mps])), ceil(nanmax([newRVerrors_mps1d, newRVerrors_mps]))])
    xlim([min(obsIDs) max(obsIDs)])
    set(gca,'xticklabel',[])
    ylabel('RV Error [m/s]')
    slegend(legend(cat(2, 'Error | RMS = ', sfnum2str(nanrms(newRVerrors_mps1d), 3), ' m/s'), 'location', 'southwest'))
    
    

    subplot(10, 1, 9:10)
    hold on

    titlel(cat(2, '2D RV Error'))
    plot(obsIDs, newRVerrors_mps, 'ob')
    plot([-10, 50], [0, 0], '-k')
    grid minor
    ylabel('RV Error [m/s]')
    ylim([floor(nanmin([newRVerrors_mps1d newRVerrors_mps])), ceil(nanmax([newRVerrors_mps1d newRVerrors_mps]))])
    xlim([min(obsIDs) max(obsIDs)])
    xlabel('Observation ID')
    slegend(legend(cat(2, 'Error | RMS = ', sfnum2str(nanrms(newRVerrors_mps), 3), ' m/s'), 'location', 'southwest'))

    
    
end




% 
% %% Plot with real JD
% if plot_xRV_yJD_q
%     
%     date_JD = MJDobs(:, 2)';
%     figure(5221); clf(figure(5221));
%     subplot(10,1,1:6)
%     hold on
%     titlel(cat(2, {cat(2, method0, ' Measured 1D RV: Synth ', type0, ' Dispersion Applied'), ...
%         cat(2, 'Plate: ', underscoreTEXfixer(plateName), '  |  Beam: ', num2str(beamNum))}))
%     plot(date_JD, -rv_new_mBCV_mps, '.r')
%     plot(date_JD, rv_old_mBCV_MZ_mps, '.k')
%     plot(date_JD, rv_new_mBCV_MZ_mps, '.b')
%     %     plot(x, orbit_mps, '-g')
%     plot(date_JD, orbit_mps, '.g')
%     ylabel('RV [m/s]')
%     %     xlabel('Observation ID')
%     slegend(legend(cat(2, 'Synth. 1D: ', type0, ' Dispersion Applied'), 'Neil''s 1D', cat(2, 'Neil''s RV - Synth RV (', type0, ' Dispersion Applied)'), 'Actual', 'location', 'southwest'))
%     set(gca,'xticklabel',[])
%     xlim([min(date_JD) max(date_JD)])
%     grid on
%     hold off
%     
%     subplot(10, 1, 7:8)
%     hold on
%     titlel(cat(2, 'Neil''s Error: (Meas. RV - Orbit)'))
%     %     plot(x, neilsError, 'ok')
%     plot(date_JD, neilRVerrors_mps, 'ok')
%     plot([-10, 50], [0, 0], '-k')
%     grid minor
%     ylim([floor(nanmin([neilRVerrors_mps, newRVerrors_mps])), ceil(nanmax([neilRVerrors_mps, newRVerrors_mps]))])
%     xlim([min(date_JD) max(date_JD)])
%     set(gca,'xticklabel',[])
%     ylabel('RV Error [m/s]')
%     slegend(legend(cat(2, 'Error | RMS = ', sfnum2str(nanrms(neilRVerrors_mps), 3), ' m/s  |  Avg. = ', sfnum2str(nanmean(abs(neilRVerrors_mps)), 3), ' m/s'), 'location', 'southwest'))
%     %     xlabel('Observation ID')
%     
%     subplot(10, 1, 9:10)
%     hold on
%     
%     titlel(cat(2, 'Dispersion Corrected Error'))
%     %     plot(x, dispCorrError, 'ok')
%     plot(date_JD, newRVerrors_mps, 'ob')
%     plot([-10, 50], [0, 0], '-k')
%     grid minor
%     ylabel('RV Error [m/s]')
%     ylim([floor(nanmin([neilRVerrors_mps, newRVerrors_mps])), ceil(nanmax([neilRVerrors_mps, newRVerrors_mps]))])
%     xlim([min(date_JD) max(date_JD)])
%     xlabel('Observation ID')
%     slegend(legend(cat(2, 'Error | RMS = ', sfnum2str(nanrms(newRVerrors_mps), 3), ' m/s  |  Avg. = ', sfnum2str(nanmean(abs(newRVerrors_mps)), 3), ' m/s'), 'location', 'southwest'))
% end
% 



