function realAndSynthComparison(s, pipeline_dir, plateName, beamNumStr, obsNum, synthSpectrum, savePlot_q, type0, norm_q, plateEmulationName, modelBeamNum)


% Compare real Sun spectum to a synthetic one

% synthSpectrum = mat2gray(synthSpectrum);
% realSpectrum = mat2gray(fitsread('E:\Research\1a_syntheticPipelineTestEnv\39_deslant_sun.fits'));

type0 = upper(type0);

modelBeamNumStr = n2s(modelBeamNum);

plate_dir = [pipeline_dir, s, 'plates', s, plateEmulationName];

% if strcmp(type0, 'STAR')
%     load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'star_spectra_deslanted_dir')
% else
%     load(strcat(pipeline_dir, s, 'MARVELS_fileNames.mat'), 'tio_spectra_deslanted_dir')
% end


%% Load real spectrum
    
if strcmp(type0, 'STAR')
    
    
    %if 0 == exist([plate_dir, s, type0, s, type0, '_flux_deslanted', s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'], 'file')
    if 0 ~= exist(strcat(neil_dir, s, plateEmulationName, s, 'deslanted', s, beamNum2legacyBeamNumStr(modelBeamNum), '_deslanted_fit', s, obsNum2LegacyObsNum(obsNum), '_deslant.fits'), 'file')
        
        realSpectrum = fitsread(strcat(neil_dir, s, plateEmulationName, s, 'deslanted', s, beamNum2legacyBeamNumStr(modelBeamNum), '_deslanted_fit', s, obsNum2LegacyObsNum(obsNum), '_deslant.fits'));
        
        flux_adu = realSpectrum;
        save(strcat(star_spectra_deslanted_dir, s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'), 'flux_adu'); 
        clear flux_adu
    end
    
    try
        load([plate_dir, s, type0, s, type0, '_flux_deslanted', s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'], 'flux_deslanted_adu')
        flux_adu = flux_deslanted_adu;
    catch
        try
            load([plate_dir, s, type0, s, type0, '_flux_deslanted', s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'], 'flux_adu')
        catch
            flux_adu = ones(size(synthSpectrum));
        end
    end
    
else
    
    %if 0 == exist([plate_dir, s, type0, s, type0, '_flux_deslanted', s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'], 'file')
    if 0 ~= exist(strcat(neil_dir, s, plateEmulationName, s, type0, s, 'BRdeslanted', s, beamNum2legacyBeamNumStr(modelBeamNum), '_deslanted_fit', s, obsNum2LegacyObsNum(obsNum), '_deslant.fits'), 'file')
        
        realSpectrum = fitsread(strcat(neil_dir, s, plateEmulationName, s, type0, s, 'BRdeslanted', s, beamNum2legacyBeamNumStr(modelBeamNum), '_deslanted_fit', s, obsNum2LegacyObsNum(obsNum), '_deslant.fits'));
        
        flux_adu = realSpectrum;
        save(strcat(star_spectra_deslanted_dir, s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'), 'flux_adu');
        clear flux_adu
    end
    
    try
        load([plate_dir, s, type0, s, type0, '_flux_deslanted', s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'], 'flux_deslanted_adu')
        flux_adu = flux_deslanted_adu;
    catch
        try
            load([plate_dir, s, type0, s, type0, '_flux_deslanted', s, type0, '_beam', modelBeamNumStr, '_obs', n2s(obsNum), '_flux_deslanted.mat'], 'flux_adu')
        catch
            flux_adu = ones(size(synthSpectrum));
        end
    end
end

realSpectrum = flux_adu; clear flux_adu;



if norm_q
    
    allFlux = realSpectrum(:);
    
    meanFlux = nanmean(allFlux);
    
    stdFlux = std(allFlux) * 3;
    
    goodInd = realSpectrum > (meanFlux - stdFlux) & realSpectrum < (meanFlux + stdFlux);
    
    realSpectrum(~goodInd) = meanFlux;
    
    synthSpectrum = synthSpectrum .* max(realSpectrum(:)) / max(synthSpectrum(:));
end

maxY = size(realSpectrum, 1) + size(synthSpectrum, 1);

range_1 = 1:1000;
range_2 = 1001:2000;
range_3 = 2001:3000;
range_4 = 3001:4001;

figure(91); clf(figure(91));

subplot(411); hold on
titlel(cat(2, type0, ' Spectrum Comparison: Real (Top) and Synthetic (Bottom)'))
imagesc(range_1, 1:maxY, [synthSpectrum(:, range_1) ; realSpectrum(:, range_1)]); colormap gray
axis([range_1(1) range_1(end) 0.5 (maxY + 0.5)])
hold off

subplot(412); hold on
imagesc(range_2, 1:maxY, [synthSpectrum(:, range_2) ; realSpectrum(:, range_2)]); colormap gray
axis([range_2(1) range_2(end) 0.5 (maxY + 0.5)])
hold off

subplot(413); hold on
imagesc(range_3, 1:maxY, [synthSpectrum(:, range_3) ; realSpectrum(:, range_3)]); colormap gray
axis([range_3(1) range_3(end) 0.5 (maxY + 0.5)])
hold off

subplot(414); hold on
imagesc(range_4, 1:maxY, [synthSpectrum(:, range_4) ; realSpectrum(:, range_4)]); colormap gray
axis([range_4(1) range_4(end) 0.5 (maxY + 0.5)])
hold off


if savePlot_q
    
%     plot_dir = strcat(pipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots');
%     try mkdir(strcat(plot_dir, s, 'synthVSrealSpectrum_img')); catch; end
%     print(gcf, '-r0', '-dpng', strcat(plot_dir, s, 'synthVSrealSpectrum_img', s, type0, '_beam', beamNumStr, '_obs', num2str(obsNum), '_synthVSrealSpectrum_img.png'))
end







