function synthFinalContinuumPlot_logspace(s, synthPipeline_dir, plateName, beamNumStr, obsNum, RV_BCV_mps, ...
    wl_lo_ang, flux_deslanted_adu, type0)


wl1 = 5320;
wl2 = 5335;

x1 = find(min(abs(wl1-wl_lo_ang)) == abs(wl1-wl_lo_ang));
x2 = find(min(abs(wl2-wl_lo_ang)) == abs(wl2-wl_lo_ang));




figure(3); clf(figure(3));

subplot(4,6,1:4); hold on
if strcmp(lower(type0), 'tio')
    titlel(cat(2, 'Orig ', type0, ' Synth Spectrum'))
else
    if abs(RV_BCV_mps(obsNum)) < 0.001
        titlel(cat(2, 'Orig ', type0, ' Synth Spectrum. RV = 0 m/s'))
    else
        titlel(cat(2, 'Orig ', type0, ' Synth Spectrum. RV = ', num2str(RV_BCV_mps(obsNum)), ' m/s'))
    end
end
imagesc(wl_lo_ang, 1:size(flux_deslanted_adu, 1), flux_deslanted_adu);
colormap(gray);
set(gca,'YDir','normal')
ylim([0.5 size(flux_deslanted_adu, 1) + 0.5])
xlim([wl_lo_ang(1), wl_lo_ang(end)])
set(gca,'YTickLabel',{' '})

subplot(4,6,5:6); hold on
titlel(cat(2, 'Zoom'))
imagesc(wl_lo_ang(x1:x2), 1:size(flux_deslanted_adu, 1), flux_deslanted_adu(:, x1:x2));
colormap(gray);
set(gca,'YDir','normal')
xlim([wl_lo_ang(x1) wl_lo_ang(x2)])
ylim([0.5 size(flux_deslanted_adu, 1) + 0.5])
set(gca,'YTickLabel',{' '})

subplot(4,6,[7:6:24, 8:6:24, 9:6:24, 10:6:24]); hold on
plot(wl_lo_ang, flux_deslanted_adu(10, :), '-k');
xlim([wl_lo_ang(1), wl_lo_ang(end)])
% ylim([0 1])
xlabel('Wavelength [Angstrom]')
ylabel('Flux [ADU]')
grid on

subplot(4,6,[11:6:24, 12:6:24]); hold on
plot(wl_lo_ang(x1:x2), flux_deslanted_adu(10, x1:x2), '.-k');
xlim([wl_lo_ang(x1) wl_lo_ang(x2)])
% ylim([0 1])
grid on
set(gca,'YTickLabel',{' '})



% plot_dir = strcat(synthPipeline_dir, s, 'plates', s, plateName, s, 'plots', s, 'synthSpectrumCreationPlots');
% try mkdir(strcat(plot_dir, s, type0, '_2D_spectra')); catch; end
% print(gcf, '-r0', '-dpng', strcat(plot_dir, s, type0, '_2D_spectra', s, type0, '_beam', beamNumStr, '_obs', num2str(obsNum), '_', type0, '_2D_spectrum.png'))



