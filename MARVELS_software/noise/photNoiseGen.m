function I_noise = photNoiseGen(maxNoise, arrayLike)

if maxNoise == 0
    
    I_noise = zeros(size(arrayLike));
    
else
    
    I_noise = poissrnd(maxNoise * 1e5 * 2, size(arrayLike, 1), size(arrayLike, 2));
    
    
    I_noise = I_noise - nanmean(I_noise(:));
    
    
    x = fsolve(@(x) rms(I_noise ./ abs(x)) - maxNoise, max(I_noise(:)));
    
    
    % I_noise2 = rms(I_noise(:) ./ abs(x));
    
    
    I_noise = I_noise ./ abs(x);
    
end


