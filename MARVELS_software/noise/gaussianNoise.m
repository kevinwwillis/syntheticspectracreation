function gaussianNoise
%% Add White Gaussian Noise to a specified data set; you can specify the variance and mean value
% A_wnoise = A + sqrt(variance)*randn(size(A)) + meanValue;

originalSignalVector = (0:1:100)'; % Add MARVELS data here. This can add 
% white Gaussian noise with specified variance and mean value to any data 
% set

variance = 1;

mean_Value = 0;
% Generate white Gaussian noise for a particular dataset
originalSignalVector_gaussianNoise = originalSignalVector + sqrt(variance)*randn(size(originalSignalVector)) + mean_Value;
%% EXPERIMENTAL: Compare original data set with added white Gaussian noise

% plot(originalSignalVector)
% hold on
% plot(originalSignalVector_gaussianNoise)
% hold off
% legend('Original Signal','Original Signal with Gaussian Noise')
end