
% Andy French. April 2013
% Kevin Willis. Aug 2018

function [hist, d] = nBodySim(masses, rings, clusters, plot_q, showObjNames_q)


%Initialise structure d into which which all gravity_sim data is stored.
%This is a global variable so all sub-functions of gravity_sim.m will be
%able to see and modify it.
global d

d.totSec = masses(1).totSec;
d.dt = masses(1).dt;
d.earthVplot = masses(1).earthVplot;
d.masses = masses; clear masses
d.rings = rings; clear rings
d.clusters = clusters; clear clusters

objNums = 1:length(d.masses);

%
AU = 149597870700;
G0 = 6.67408e-11;
M_sun = 1.9885e30;
Yr = 1;%31556952; % s / yr

% conversions

m_au = 149597870700;
km_au = m_au * 1e-13;
s_yr = 31556952;

%Dimensionless number which controls the dynamics, and results from the
%scaling of mass, distance and time parameters to make them dimensionless.
G = G0;%*Yr^2*M_sun/(AU^3);


% [masses,rings,clusters,G,Lmax,k,view_option,map_underlay,dt,Ng,Nr,RM] = gravity_sim_defaults();
% [d.masses,d.rings,d.clusters,d.G,d.Lmax,d.k,d.view_option,d.map_underlay,d.dt,d.Ng,d.Nr,d.RM] = load_data('binaryWithPlanet_v3');

%% Hard coded inputs %%

d.plotSpheres_q = 1;

d.tailMarkerSize = 5;

d.ringMarkerSize = 3;

%Strength of gravity
G_factor = 1;
d.G = G_factor * G;

%Set dimensions of space
d.Lmax = 50;

%Timestep
% d.dt = (0.50 * 60 * 60);
% d.dt = (1 * 60 * 60);


% How long to run sim (in sim time)
tFinal = ceil( d.totSec / d.dt );
% tFinal = ceil( 20 / d.dt );

%Gravitation exponent
d.Ng = 2;

%Repulsion exponent
d.Nr = 1;

%Repulsion magnitude
d.RM = 0;%0.01;

%Boundary coefficient of restitution (if [] then there is no boundary)
d.k = [];%0.4;

%Radius for number density
d.rho_r = 1;

%Density multiplier. Maximum colour scale for density map is set by DM*
%number of objects / area of space (Lmax^2 * 4)
d.DM = 10;

%Speed multiplier. Maximum colour scale for speed map is set by SM*
%maximum initial speed * number of objects / area of space (Lmax^2 * 4)
d.SM = 10;

%Number density map grid
d.N = 20;

%Axes positions depending on 2D or 3D view. (Normalized units)
d.view2_axes_position = [0.1, 0.08, 0.8, 0.8];
d.view3_axes_position = [0.25, 0.25, 0.5, 0.5];

%Marker size for planets
d.msize = 2.5;

%Marker size of star trail
d.trail_size = 2;

%Store inter-mass distances (only for masses array, not rings or clusters)
d.store_inter_mass_distances = 1;

%% Get Sim Inputs

% [d.masses,d.rings,d.clusters,d.G,d.Lmax,d.k,d.view_option,d.map_underlay,d.dt,d.Ng,d.Nr,d.RM] = gravity_sim_defaults();
% [d.masses,d.rings,d.clusters,d.G,d.Lmax,d.k,d.view_option,d.map_underlay,d.dt,d.Ng,d.Nr,d.RM] = load_data('binaryWithPlanet_v3');


%% Generate vectors of parameters for masses, rings and clusters %%

%Start simulation time
d.t = 0;
d.t_store = 0;

%Masses
[x_m,y_m,z_m,vx_m,vy_m,vz_m,R_m,M_m,d.masses] = make_masses( d.masses );

%Compute initial inter-mass distance.
d.inter_mass_distance = distance( [x_m ; y_m ; z_m], [x_m ; y_m ; z_m] );

%Rings of massless planets
[x_r,y_r,z_r,vx_r,vy_r,vz_r,R_r,M_r,d.rings] = make_rings( d.rings, d.G );

%Add globular clusters of massless planets
[x_c,y_c,z_c,vx_c,vy_c,vz_c,R_c,M_c,d.clusters] = make_clusters( d.clusters, d.G );

%Assemble masses, rings and clusters into single vectors
d.x = [x_m,x_r,x_c]; clear x_m x_r x_c
d.y = [y_m,y_r,y_c]; clear y_m y_r y_c
d.z = [z_m,z_r,z_c]; clear z_m z_r z_c
d.vx = [vx_m,vx_r,vx_c]; clear vx_m vx_r vx_c
d.vy = [vy_m,vy_r,vy_c]; clear vy_m vy_r vy_c
d.vz = [vz_m,vz_r,vz_c]; clear vz_m vz_r vz_c
d.R = [R_m,R_r,R_c]; clear R_m R_r R_c
d.M = [M_m,M_r,M_c]; clear M_m M_r M_c

%Calculate maximum initial speed
d.max_speed = max(sqrt(d.vx.^2 + d.vy.^2 + d.vz.^2));


%%

if plot_q
    
d.spt = 4;
d.colorWheel = [{'r', 'b', 'g', 'c', 'k'}];

if d.plotSpheres_q
    
    N = 10;
    thetavec = linspace(0,pi,N);
    phivec = linspace(0,2*pi,2*N);
    [th, ph] = meshgrid(thetavec,phivec);
    
    for objNum = objNums
        
        d.masses(objNum).R_buffedRadius = (d.masses(objNum).mass / M_sun)^(0.8) * 0.00465 * 10 * d.masses(1).init_orbit_s / 1; % Main-sequence star mass-radius relation [AU]   ( M_sol ^ 0.8 = R_sol )
        
        R = d.masses(objNum).R_buffedRadius;
        
        d.masses(objNum).sphereX = R .* sin(th) .* cos(ph);
        d.masses(objNum).sphereY = R .* sin(th) .* sin(ph);
        d.masses(objNum).sphereZ = R .* cos(th);
        
    end

% surf(x,y,z); 
    
end

%Plot initial positions of masses
d = plot_initial_positions(d);
end



itTime = 1;

% History array preallocation
[hist.t, hist.K, hist.U, hist.E] = deal(nan(1,tFinal));

for objNum = objNums
    
    [ ...
        hist.m(objNum).r, hist.m(objNum).x, hist.m(objNum).y, hist.m(objNum).z, ...
        hist.m(objNum).v, hist.m(objNum).vx, hist.m(objNum).vy, hist.m(objNum).vz, ... 
        hist.m(objNum).a, hist.m(objNum).ax, hist.m(objNum).ay, hist.m(objNum).az] = deal(nan(1,tFinal));
end


for objNum = objNums
    
    hist.m(objNum).x(itTime) = d.x(objNum);
    hist.m(objNum).y(itTime) = d.y(objNum);
    hist.m(objNum).z(itTime) = d.z(objNum);
    hist.m(objNum).vx(itTime) = d.vx(objNum);
    hist.m(objNum).vy(itTime) = d.vy(objNum);
    hist.m(objNum).vz(itTime) = d.vz(objNum);
%     hist.m(objNum).ax(itTime) = d.acc_x(objNum)^2;
%     hist.m(objNum).ay(itTime) = d.acc_y(objNum)^2;
%     hist.m(objNum).az(itTime) = d.acc_z(objNum)^2;
    
    hist.m(objNum).r(itTime) = sqrt(dot([d.x(objNum),d.y(objNum),d.z(objNum)], [d.x(objNum),d.y(objNum),d.z(objNum)]));
    hist.m(objNum).v(itTime) = sqrt(dot([d.vx(objNum),d.vy(objNum),d.vz(objNum)], [d.vx(objNum),d.vy(objNum),d.vz(objNum)]));
%     hist.m(objNum).a(itTime) = sqrt(dot([d.acc_x(objNum),d.acc_y(objNum),d.acc_z(objNum)]^2, [d.acc_x(objNum),d.acc_y(objNum),d.acc_z(objNum)]^2));
end

U = 0;
K = 0;

for objNum1 = objNums
    
    K = K + 0.5 * d.masses(objNum1).mass * (sqrt(dot([d.vx(objNum1),d.vy(objNum1),d.vz(objNum1)], [d.vx(objNum1),d.vy(objNum1),d.vz(objNum1)])))^2;
    
    for objNum2 = setdiff(objNums, objNum1)
        
        q0 = ([d.x(objNum1),d.y(objNum1),d.z(objNum1)] - [d.x(objNum2),d.y(objNum2),d.z(objNum2)]);
        
        U = U + G0 * d.masses(objNum1).mass * d.masses(objNum2).mass / (sqrt(dot(q0, q0)));
    end
end

hist.t(itTime) = d.t;
hist.K(itTime) = K;
hist.U(itTime) = -U;
hist.E(itTime) = K - U;



% Plot 2 init

if plot_q
p2_markerStyle = '-';

tr = 1;

qf(2,1)

subplot(d.spt, 1, 1); hold on
title('Distance from System''s Center of Mass')
grid on
ylabel('r_{CM} [AU]')
xlim([0 inf])
subplot(d.spt, 1, 1); hold on
for objNum = objNums
    pr_h(objNum) = plot(hist.t(tr), hist.m(objNum).r(tr) / m_au, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
end
legend({d.masses(:).name}, 'location', 'northwest', 'AutoUpdate','off')

subplot(d.spt, 1, 2); hold on
if d.earthVplot
title('Velocity Seen on Earth')    
else
title('Velocity')
end
grid on
ylabel('v [m/s]')
xlim([0 inf])
for objNum = objNums
    
    if d.earthVplot
        pv_h(objNum) = plot(hist.t(tr), hist.m(objNum).vx(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    else
        pv_h(objNum) = plot(hist.t(tr), hist.m(objNum).v(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    end
end

subplot(d.spt, 1, 3); hold on
title('Acceleration')
grid on
ylabel('a [m/s^2]')
xlim([0 inf])
for objNum = objNums
    %pa_h(objNum) = plot(hist.t(tr), hist.m(objNum).a(tr), p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    pa_h(objNum) = plot(hist.t(tr), 0, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
end

subplot(d.spt, 1, 4); hold on
title('Total Energy of System')
grid on
ylabel('E_{tot} [J]')
xlabel('time [Earth yr]')
xlim([0 inf])
pU_h = plot(hist.t(tr), hist.U(tr), [p2_markerStyle, 'b']);
pK_h = plot(hist.t(tr), hist.K(tr), [p2_markerStyle, 'r']);
pE_h = plot(hist.t(tr), hist.K(tr) + hist.U(tr), [p2_markerStyle, 'k']);




% Plot object names
if showObjNames_q & plot_q
    
    figure(1); hold on
    
    for objNum = objNums
        
        if d.plotSpheres_q
            R = d.masses(objNum).R_buffedRadius;
        else
            d.masses(objNum).R_buffedRadius = (d.masses(objNum).mass / M_sun)^(0.8) * 0.00465 * 10 * d.masses(1).init_orbit_s / 1; % Main-sequence star mass-radius relation [AU]   ( M_sol ^ 0.8 = R_sol )
            
            R = d.masses(objNum).R_buffedRadius;
        end
        
        objName_handle{objNum} = text(d.x(objNum) + R, d.y(objNum) + R, d.z(objNum) + R, d.masses(objNum).name);
    end
end
end


%% Run simulation %%

if plot_q
    lim0 = 20;
    % axis([-lim0 lim0 -lim0 lim0 -lim0 lim0])
    xlim([-lim0 lim0])
    ylim([-lim0 lim0])
    zlim([-lim0 lim0])
    axis square
    axis vis3d

    if isempty(d.rings)
        set(gca, 'XLimMode', 'auto')
        set(gca, 'YLimMode', 'auto')
        set(gca, 'ZLimMode', 'auto')
    else

    end
end

% opengl( 'save', 'hardware' )


for itTime = 2:tFinal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    tr = 1:itTime;
    
%     %Timer diagnostic to see if gravity_sim.m is slowing down with time
%     iter = iter+1;
%     if iter == 100
%         d.hundred_iter_times = [d.hundred_iter_times,toc(timer_zero)];
%         timer_zero = tic;
%         iter = 0;
%     end
    
    %Update positions, veocities and accelerations and times
    [ d.x, d.y, d.z, d.vx, d.vy, d.vz, d.acc_x, d.acc_y, d.acc_z ] =...
        gravity( d.x, d.y, d.z, d.vx, d.vy, d.vz, d.R, d.M, d.G, d.Ng, d.Nr, d.RM, d.dt, 2 );
    
    d.t = d.t + d.dt;
    
    d.x = d.x(2,:);
    d.y = d.y(2,:);
    d.z = d.z(2,:);
    
    d.vx = d.vx(2,:);
    d.vy = d.vy(2,:);
    d.vz = d.vz(2,:);
    
    d.acc_x = d.acc_x(2,:);
    d.acc_y = d.acc_y(2,:);
    d.acc_z = d.acc_z(2,:);
    
    
    for objNum = objNums
        
        hist.m(objNum).x(itTime) = d.x(objNum);
        hist.m(objNum).y(itTime) = d.y(objNum);
        hist.m(objNum).z(itTime) = d.z(objNum);
        
        hist.m(objNum).vx(itTime) = d.vx(objNum);
        hist.m(objNum).vy(itTime) = d.vy(objNum);
        hist.m(objNum).vz(itTime) = d.vz(objNum);
        
        hist.m(objNum).ax(itTime) = d.acc_x(objNum);
        hist.m(objNum).ay(itTime) = d.acc_y(objNum);
        hist.m(objNum).az(itTime) = d.acc_z(objNum);
        
        hist.m(objNum).r(itTime) = sqrt(dot([d.x(objNum), d.y(objNum), d.z(objNum)], [d.x(objNum), d.y(objNum), d.z(objNum)]));
        
        hist.m(objNum).v(itTime) = sqrt(dot([d.vx(objNum), d.vy(objNum), d.vz(objNum)], [d.vx(objNum), d.vy(objNum), d.vz(objNum)]));
        
        hist.m(objNum).a(itTime) = sqrt(dot([d.acc_x(objNum), d.acc_y(objNum), d.acc_z(objNum)], [d.acc_x(objNum), d.acc_y(objNum), d.acc_z(objNum)]));
    end
    
    if plot_q
    figure(1); hold on
    end
    
    % Plot object names
    if showObjNames_q & plot_q
        
        for objNum = objNums
            
            R = d.masses(objNum).R_buffedRadius;
            
           set(objName_handle{objNum}, 'Position', [d.x(objNum) + R, d.y(objNum) + R, d.z(objNum) + R] / m_au)
        end
    end
    
    %Update positions of masses
    
    %Masses
    if ~isempty(d.masses) & plot_q
        
        for objNum = objNums
            
            if isempty( d.masses(objNum).marker_RGB )
                [r,g,b] = x_to_color(sqrt(d.vx(objNum).^2 + d.vy(objNum).^2 + d.vz(objNum).^2 ),speed_colormap,0,d.max_speed);
            else
                r = d.masses(objNum).marker_RGB(1);
                g = d.masses(objNum).marker_RGB(2);
                b = d.masses(objNum).marker_RGB(3);
            end
            
            %Update position
            if d.plotSpheres_q
                set( d.p(objNum), 'Xdata', (d.masses(objNum).sphereX + d.x(objNum)) / m_au, 'Ydata', (d.masses(objNum).sphereY + d.y(objNum)) / m_au, 'Zdata', (d.masses(objNum).sphereZ + d.z(objNum)) / m_au);
            else
                set( d.p(objNum), 'Xdata', d.x(objNum) / m_au, 'Ydata', d.y(objNum) / m_au, 'Zdata', d.z(objNum) / m_au, 'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
            end
            
            %Add to trail
            if d.masses(objNum).plot_trail == 1
                
%                 plot3( d.x(objNum),d.y(objNum),d.z(objNum),'r.','markerfacecolor',[r,g,b],'markeredgecolor',...
%                     [r,g,b],'markersize',d.trail_size);
                
                set( d.tail(objNum), 'Xdata', hist.m(objNum).x(tr) / m_au, 'Ydata', hist.m(objNum).y(tr) / m_au, 'Zdata', hist.m(objNum).z(tr) / m_au, 'markerfacecolor', [r,g,b],'markeredgecolor', [r,g,b] );
            end
        end
    end
    
%     %Update inter-mass distance store
%     if d.store_inter_mass_distances==1
%         if ~isempty(d.masses)
%             %Add a new page to this array
%             nM = length(d.masses);
%             d.inter_mass_distance(:,:,itTime) = distance( [d.x(1:nM);d.y(1:nM);d.z(1:nM)],[d.x(1:nM);d.y(1:nM);d.z(1:nM)] );
%         end
%     end
    
    %Rings
    if ~isempty(d.rings) & plot_q
        n0 = length(d.masses);
        end_index = n0;
        for objNum=1:length(d.rings)
            start_index = end_index+1;
            end_index = start_index + d.rings(objNum).num_masses - 1;
            if isempty( d.rings(objNum).marker_RGB )
                [r,g,b] = x_to_color( mean( sqrt(d.vx(start_index:end_index).^2 +...
                    d.vy(start_index:end_index).^2 + d.vz(start_index:end_index).^2 )),speed_colormap,0,d.max_speed);
            else
                r = d.rings(objNum).marker_RGB(1);
                g = d.rings(objNum).marker_RGB(2);
                b = d.rings(objNum).marker_RGB(3);
            end
            set( d.p(n0+objNum), 'Xdata', d.x(start_index:end_index), 'Ydata', d.y(start_index:end_index), 'Zdata',...
                d.z(start_index:end_index),...
                'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
        end
    end
    
    %Clusters
    if ~isempty(d.clusters) & plot_q
        n0 = n0 + length(d.rings);
        for objNum=1:length(d.clusters)
            start_index = end_index+1;
            end_index = start_index + d.clusters(objNum).num_masses - 1;
            if isempty( d.clusters(objNum).marker_RGB )
                [r,g,b] = x_to_color( mean( sqrt(d.vx(start_index:end_index).^2 +...
                    d.vy(start_index:end_index).^2 + d.vz(start_index:end_index).^2 )),speed_colormap,0,d.max_speed);
            else
                r = d.clusters(objNum).marker_RGB(1);
                g = d.clusters(objNum).marker_RGB(2);
                b = d.clusters(objNum).marker_RGB(3);
            end
            set( d.p(n0+objNum), 'Xdata', d.x(start_index:end_index), 'Ydata', d.y(start_index:end_index), 'Zdata',...
                d.z(start_index:end_index),...
                'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
        end
    end
    
    %Flush pending graphics requests
    if plot_q
        drawnow
    end
    
    %Modify position of first mass
    d.m1_x = d.x(1);
    d.m1_y = d.y(1);
    
    
    
    % Potential Energy
    
    U = 0;
    K = 0;
    
    for objNum1 = objNums
        
%         r1 = sqrt(dot([d.x(objNum1),d.y(objNum1),d.z(objNum1)], [d.x(objNum1),d.y(objNum1),d.z(objNum1)]));
        
        K = K + 0.5 * d.masses(objNum1).mass * (sqrt(dot([d.vx(objNum1),d.vy(objNum1),d.vz(objNum1)], [d.vx(objNum1),d.vy(objNum1),d.vz(objNum1)])))^2;
        
        for objNum2 = setdiff(objNums, objNum1)
            
%             r2 = sqrt(dot([d.x(objNum2),d.y(objNum2),d.z(objNum2)], [d.x(objNum2),d.y(objNum2),d.z(objNum2)]));
            
%             r_CM = 
            
            q0 = ([d.x(objNum1),d.y(objNum1),d.z(objNum1)] - [d.x(objNum2),d.y(objNum2),d.z(objNum2)]);
            
            U = U + G0 * d.masses(objNum1).mass * d.masses(objNum2).mass / (sqrt(dot(q0, q0)));
            
            %U = U + G0 * (d.masses(objNum1).mass / r1 + d.masses(objNum2).mass / r2) + 0.5 * omega^2;
            
%             G * d.masses(objNum1).mass * d.masses(objNum2).mass / dot(r0, r0)
        end
        
        %U = U + (d.masses(objNum1).mass) * G0 * d.masses(objNum2).mass / (sqrt(dot(q0, q0)));
    end
    
    hist.t(itTime) = d.t;
    hist.K(itTime) = K;
    hist.U(itTime) = -U;
    hist.E(itTime) = K - U;
    
    
    
%     figure(2); hold on

    
    
    % R, V, A, E_tot
    if plot_q
    for objNum = objNums
        
%         subplot(d.spt, 1, 1); hold on
%         plot(d.t, sqrt(dot([d.x(objNum),d.y(objNum),d.z(objNum)], [d.x(objNum),d.y(objNum),d.z(objNum)])), '.', 'markeredgecolor', d.masses(objNum).marker_RGB);
        set( pr_h(objNum), 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.m(objNum).r(tr) / m_au)
        
%         subplot(d.spt, 1, 2); hold on
%         plot(d.t, sqrt(dot([d.vx(objNum),d.vy(objNum),d.vz(objNum)], [d.vx(objNum),d.vy(objNum),d.vz(objNum)])), '.', 'markeredgecolor', d.masses(objNum).marker_RGB);
        if d.earthVplot
            set( pv_h(objNum), 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.m(objNum).vx(tr) )
        else
            set( pv_h(objNum), 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.m(objNum).v(tr) )
        end
        
%         subplot(d.spt, 1, 3); hold on
%         plot(d.t, sqrt(dot([d.acc_x(objNum),d.acc_y(objNum),d.acc_z(objNum)], [d.acc_x(objNum),d.acc_y(objNum),d.acc_z(objNum)]))^2, '.', 'markeredgecolor', d.masses(objNum).marker_RGB);
        set( pa_h(objNum), 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.m(objNum).a(tr) )
        
%         subplot(d.spt, 1, 4); hold on
%         plot(d.t, -U, '.b')
%         plot(d.t, K, '.r')
%         plot(d.t, K - U, '.k')

    end
    
    set( pU_h, 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.U(tr) )
    set( pK_h, 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.K(tr) )
    set( pE_h, 'Xdata', hist.t(tr) / s_yr, 'Ydata', hist.E(tr) )
    end
    

    %% An acceleration plot to see derviative of velocities and calculated acceleration (previously used to find errors. no errors found.)
%     qf(3,1);
%     subplot(4,1,1); hold on
%     title('x acc');
%     grid on
%     for objNum = objNums
% %         plot(hist.t(tr) / s_yr, hist.m(objNum).ax(tr), '-', 'color', d.masses(objNum).marker_RGB);
% %         plot(hist.t(tr) / s_yr, ppval(fnder(spline(hist.t(tr) / s_yr, hist.m(objNum).vx(tr)), 1), hist.t(tr) / s_yr), ':k');
% 
% 
%         plot(hist.t(tr) / s_yr, ppval(fnder(spline(hist.t(tr), hist.m(objNum).vx(tr)), 1), hist.t(tr)), ':k', 'linewidth', 3);
%         plot(hist.t(tr) / s_yr, hist.m(objNum).ax(tr), '-', 'color', d.masses(objNum).marker_RGB);
%         
%     end
%     
%     subplot(4,1,2); hold on
%     title('y acc');
%     grid on
%     for objNum = objNums
%         plot(hist.t(tr) / s_yr, ppval(fnder(spline(hist.t(tr), hist.m(objNum).vy(tr)), 1), hist.t(tr)), ':k', 'linewidth', 3);
%         plot(hist.t(tr) / s_yr, hist.m(objNum).ay(tr), '-', 'color', d.masses(objNum).marker_RGB);
%     end
%     
%     subplot(4,1,3); hold on
%     title('z acc');
%     grid on
%     for objNum = objNums
%         plot(hist.t(tr) / s_yr, ppval(fnder(spline(hist.t(tr), hist.m(objNum).vz(tr)), 1), hist.t(tr)), ':k', 'linewidth', 3);
%         plot(hist.t(tr) / s_yr, hist.m(objNum).az(tr), '-', 'color', d.masses(objNum).marker_RGB);
%     end
%     
%     subplot(4,1,4); hold on
%     title('Mag acc');
%     grid on
%     for objNum = objNums
%         plot(hist.t(tr) / s_yr, ppval(fnder(spline(hist.t(tr), hist.m(objNum).v(tr)), 1), hist.t(tr)), ':k', 'linewidth', 3);
%         plot(hist.t(tr) / s_yr, hist.m(objNum).a(tr), '-', 'color', d.masses(objNum).marker_RGB);
%     end
    
    
    
%     qf(2,1)
%     
%     subplot(d.spt, 1, 1); hold on
%     title('Distance from System''s Center of Mass')
%     grid on
%     ylabel('r_{CM} [AU]')
%     xlim([0 inf])
%     subplot(d.spt, 1, 1); hold on
%     for objNum = objNums
%         plot(hist.t(tr), hist.m(objNum).r(tr), '.', 'markeredgecolor', d.masses(objNum).marker_RGB);
%     end
%     legend({d.masses(:).name}, 'location', 'northwest', 'AutoUpdate','off')
%     
%     subplot(d.spt, 1, 2); hold on
%     title('Velocity')
%     grid on
%     ylabel('v [m/s]')
%     xlim([0 inf])
%     for objNum = objNums
%         plot(hist.t(tr), hist.m(objNum).v(tr), '.', 'markeredgecolor', d.masses(objNum).marker_RGB);
%     end
%     
%     subplot(d.spt, 1, 3); hold on
%     title('Acceleration')
%     grid on
%     ylabel('a [km/s^2]')
%     xlim([0 inf])
%     for objNum = objNums
%         plot(hist.t(tr), hist.m(objNum).a(tr), '.', 'markeredgecolor', d.masses(objNum).marker_RGB);
%     end
%     
%     subplot(d.spt, 1, 4); hold on
%     title('Total Energy of System')
%     grid on
%     ylabel('E_{tot} [J]')
%     xlabel('time [Earth yr]')
%     xlim([0 inf])
%     plot(hist.t(tr), hist.U(tr), '.b')
%     plot(hist.t(tr), hist.K(tr), '.r')
%     plot(hist.t(tr), hist.K(tr) + hist.U(tr), '.k')
    
    
    if plot_q
    pause(0.1)
    end
end

%%

%% FUNCTIONS %%

%mass_rings
% Function which creates a vectors of x,y,z coordinates dscribing the
% initial position of rings of masses orbiting in a circular fashion about
% a centra mass mc. vx,vy and vz are the corresponding x,y,z velocities.
function [x0,y0,z0,vx0,vy0,vz0,num_masses] = mass_rings( xc,yc,zc,mc,arc_separation_AU,...
    num_rings, r0, ring_radius_diff_AU, d, alpha, G)

%Planets starting their orbits in concentric rings about star with coordinates
%(xc,yc,zc)
x0 = [];
y0 = [];
z0 = [];
vx0 = [];
vy0 = [];
vz0 = [];
for n=1:num_rings
    
    %Ring radius /AU
    r = r0 + ring_radius_diff_AU*(n-1);
    
    %Ring prbital speed /AU per Year
    v = sqrt( G * mc / r );
    
    %Number of planets per ring
    num_planets_per_ring = floor( 2*pi*r/arc_separation_AU );
    
    %Compute planet positions and velocities
    if num_planets_per_ring>=1
        theta = linspace(0,2*pi,num_planets_per_ring+1);
        for k=1:num_planets_per_ring
            x0 = [x0,xc+r*cos(theta(k))];
            vx0 = [vx0,-v*sin(theta(k))];
            y0 = [y0,yc+r*sin(theta(k))];
            vy0 = [vy0,v*cos(theta(k))];
            z0 = [z0,zc];
            vz0 = [vz0,0];
        end
    end
end

%Rotate to desired orientation
[x0,y0,z0] = point(x0,y0,z0,1,0,0,d(1),d(2),d(3));
[x0,y0,z0] = vrot(x0,y0,z0,d(1),d(2),d(3),d(1),d(2),d(3),alpha);
[vx0,vy0,vz0] = point(vx0,vy0,vz0,1,0,0,d(1),d(2),d(3));
[vx0,vy0,vz0] = vrot(vx0,vy0,vz0,d(1),d(2),d(3),d(1),d(2),d(3),alpha);

%Compute total number of masses
num_masses = length(x0);

%%

%gravity
% N-body gravity simulator. Uses the Verlet algorithm + Newtonian gravity +
% repulsion to determine future x,y,z coordinates (and velocities and accelerations)
% of N masses.
%
% [ t, x, y, z, vx, vy, vz, ax, ay, az ] =...
%    gravity( x0, y0, z0, vx0, vy0, vz0, R, M, g, dt, N )
%
% t               Vector of simulation times / Earth years
% x,y,z           x,y,z coordinates in AU (distance between the Earth and
%                 the Sun
% vx,vy,vz        Cartesian velocities in AU per Earth year
% ax,ay,az        Cartesian accelerations in AU per square Earth years.
%
% x0,y0,z0        Initial object positions in AU
% vx0,vy0,vz0     Initial ibject velocities in AU per Earth year
% R               Row vector of object raddi / Sun radii
% M               Row vector of object masses / Sun masses
% g               Strength of gravity / Gravitational force constant 6.67 * 10^-11
% dt              Time step, in  Earth years
% N               Number of time steps
function [ x, y, z, vx, vy, vz, ax, ay, az ] =...
    gravity( x0, y0, z0, vx0, vy0, vz0, R, M, G, Ng,Nr,RM, dt, N )

%Initialise output arrays
x = repmat( x0, [N,1] );
y = repmat( y0, [N,1] );
z = repmat( z0, [N,1] );
vx = repmat( vx0, [N,1] );
vy = repmat( vy0, [N,1] );
vz = repmat( vz0, [N,1] );

%Work out inital acceleration
[ax0,ay0,az0] = newton( x0,y0,z0,G,M,R,Ng,Nr,RM );
ax = repmat( ax0, [N,1] );
ay = repmat( ay0, [N,1] );
az = repmat( az0, [N,1] );

%Work out subsequent dynamics of all bodies via Verlet + Newtonian Gravity
for n=2:N
    
    %Update positions
    x(n,:) = x(n-1,:) + dt * vx(n-1,:) + 0.5*dt*dt*ax(n-1,:);
    y(n,:) = y(n-1,:) + dt * vy(n-1,:) + 0.5*dt*dt*ay(n-1,:);
    z(n,:) = z(n-1,:) + dt * vz(n-1,:) + 0.5*dt*dt*az(n-1,:);
    
    %Update acceleration
    [Ax,Ay,Az] = newton( x(n,:),y(n,:),z(n,:),G,M,R,Ng,Nr,RM );
    ax(n,:) = Ax(:);
    ay(n,:) = Ay(:);
    az(n,:) = Az(:);
    
    %Update velocity of objects using acceleration
    vx(n,:) = vx(n-1,:) + 0.5*dt*( ax(n,:) + ax(n-1,:) );
    vy(n,:) = vy(n-1,:) + 0.5*dt*( ay(n,:) + ay(n-1,:) );
    vz(n,:) = vz(n-1,:) + 0.5*dt*( az(n,:) + az(n-1,:) );
end

%%

%Compute acceleration of all objects via Newtonian-ish gravity (i.e.
% include a repulsion term to model close encounters)
% Note x,y,z are in units of AU, M are in Solar masses.
function [ax,ay,az] = newton( x,y,z,G,M,R,Ng,Nr,RM )

%Number of bodies
B = length(M);

%Work out bodies which don't have zero mass
nzm = find(M~=0);
zm = find(M==0);

%Initialise accelerations
ax = zeros(1,B);
ay = zeros(1,B);
az = zeros(1,B);

%

%Compute acceleration of non-zero masses

%Compute accelerations resulting from each mass and store these in a matrix aa.
d = distance( [x(nzm);y(nzm);z(nzm)],[x(nzm);y(nzm);z(nzm)] );
m = repmat( M(nzm).',[1,length(nzm)] );

%Compute repulsion constant
rc = RM * repmat( R(nzm).',[1,length(nzm)] );

%x component of acceleration
aa = m.*( repmat( x(nzm).',[1,length(nzm)] ) - repmat( x(nzm),[length(nzm),1] ))./d.^(Ng+1) -...
    rc.*m.*( repmat( x(nzm).',[1,length(nzm)] ) - repmat( x(nzm),[length(nzm),1] ))./d.^(Nr+1);
aa(1:length(nzm)+1:end) = 0;  %Don't let self same mass displacements contribute!
ax(nzm) = G*sum(aa,1);

%y component of acceleration
aa = m.*( repmat( y(nzm).',[1,length(nzm)] ) - repmat( y(nzm),[length(nzm),1] ))./d.^(Ng+1) -...
    rc.*m.*( repmat( y(nzm).',[1,length(nzm)] ) - repmat( y(nzm),[length(nzm),1] ))./d.^(Nr+1);
aa(1:length(nzm)+1:end) = 0; %Don't let self same mass displacements contribute!
ay(nzm) = G*sum(aa,1);

%z component of acceleration
aa = m.*( repmat( z(nzm).',[1,length(nzm)] ) - repmat( z(nzm),[length(nzm),1] ))./d.^(Ng+1) -...
    rc.*m.*( repmat( z(nzm).',[1,length(nzm)] ) - repmat( z(nzm),[length(nzm),1] ))./d.^(Nr+1);
aa(1:length(nzm)+1:end) = 0; %Don't let self same mass displacements contribute!
az(nzm) = G*sum(aa,1);

%

%Compute acceleration of zero masses

%Compute accelerations resulting from each mass and store these in a matrix aa.
d = distance( [x(nzm);y(nzm);z(nzm)],[x(zm);y(zm);z(zm)] );
m = repmat( M(nzm).',[1,length(zm)] );

%Compute repulsion constant
rc = repmat( R(nzm).',[1,length(zm)] );

%x component of acceleration
aa = m.*( repmat( x(nzm).',[1,length(zm)] ) - repmat( x(zm),[length(nzm),1] ))./d.^(Ng+1) -...
    rc.*m.*( repmat( x(nzm).',[1,length(zm)] ) - repmat( x(zm),[length(nzm),1] ))./d.^(Nr+1);
ax(zm) = G*sum(aa,1);

%y component of acceleration
aa = m.*( repmat( y(nzm).',[1,length(zm)] ) - repmat( y(zm),[length(nzm),1] ))./d.^(Ng+1) -...
    rc.*m.*( repmat( y(nzm).',[1,length(zm)] ) - repmat( y(zm),[length(nzm),1] ))./d.^(Nr+1);
ay(zm) = G*sum(aa,1);

%z component of acceleration
aa = m.*( repmat( z(nzm).',[1,length(zm)] ) - repmat( z(zm),[length(nzm),1] ))./d.^(Ng+1) -...
    rc.*m.*( repmat( z(nzm).',[1,length(zm)] ) - repmat( z(zm),[length(nzm),1] ))./d.^(Nr+1);
az(zm) = G*sum(aa,1);

%%

%Create mass density map. This creates an array of mass densities within a
%specified radius r about an N*N equispaced grid of points within the gravity
%simulation grid. This can be used to plot a smoothed surface which varies
%dynamically with the distribution of masses. By mass density we actually
%mean number density i.e. the number of objects within a particular radius.
function [xx,yy,rho] = mass_density_map( x,y,r,N,Lmax)

%Create grid
xx = linspace(-Lmax,Lmax,N);
yy = linspace(-Lmax,Lmax,N);
[xx,yy] = meshgrid(xx,yy);

%Turn grid points into row vectors to enable vectorized distance
%calculation
xx = reshape(xx,[1,N*N]);
yy = reshape(yy,[1,N*N]);

%Step through grid and find the number of masses within radius r of the
%grid point
d = distance( [xx;yy],[x;y] );

%Compute density map
rho = sum( d<r, 2 ).';

%Scale by size of area
rho = rho / (pi*r^2);

%Reshape to square arrays
xx = reshape(xx,[N,N]);
yy = reshape(yy,[N,N]);
rho = reshape(rho,[N,N]);

%%

%Create speed map. This creates an array of object speeds within a
%specified radius r about an N*N equispaced grid of points within the gravity
%simulation grid. This can be used to plot a smoothed surface which varies
%dynamically with the distribution of masses. By mass density we actually
%mean number density i.e. the number of objects within a particular radius.
function [xx,yy,speed] = speed_map( x,y,vx,vy,vz,r,N,Lmax)

%Create grid
xx = linspace(-Lmax,Lmax,N);
yy = linspace(-Lmax,Lmax,N);
[xx,yy] = meshgrid(xx,yy);

%Turn grid points into row vectors to enable vectorized distance
%calculation
xx = reshape(xx,[1,N*N]);
yy = reshape(yy,[1,N*N]);

%Form a vector of object speeds and then replicate by the number of grid
%points
s = sqrt( vx.^2 + vy.^2 + vz.^2 );
s = repmat( s, [N*N,1] );

%Step through grid and find the number of masses within radius r of the
%grid point
d = distance( [xx;yy],[x;y] );

%Compute average speed map
% speed = sum( s.*(d<r) ,2 ).' ./ sum( d<r, 2 ).';
speed = sum( s.*(d<r) ,2 ).';

%Scale by size of area
speed = speed / (pi*r^2);

%Set /0 rows (i.e. where an 'object bin' contains no masses) to be 0
speed(isnan(speed)) = 0;
speed(speed==Inf) = 0;

%Reshape to square arrays
xx = reshape(xx,[N,N]);
yy = reshape(yy,[N,N]);
speed = reshape(speed,[N,N]);

%%

%distance
% Function which computes Euclidean distance matrix.
% This fully vectorized (VERY FAST!) m-file computes the
% Euclidean distance between two vectors by:
%          ||A-B|| = sqrt ( ||A||^2 + ||B||^2 - 2*A.B )
%
% Syntax: E = distance(A,B)
%    A - (DxM) matrix
%    B - (DxN) matrix
%    E - (MxN) Euclidean distances between vectors in A and B
%
% Example :
%    A = rand(400,100); B = rand(400,200);
%    d = distance(A,B);

% Author   : Roland Bunschoten
%            University of Amsterdam
%            Intelligent Autonomous Systems (IAS) group
%            Kruislaan 403  1098 SJ Amsterdam
%            tel.(+31)20-5257524
%            bunschot@wins.uva.nl
% Last Rev : Oct 29 16:35:48 MET DST 1999
% Tested   : PC Matlab v5.2 and Solaris Matlab v5.3
% Thanx    : Nikos Vlassis
function d = distance(a,b)
if (nargin ~= 2)
    error('Not enough input arguments');
end
if (size(a,1) ~= size(b,1))
    error('A and B should be of same dimensionality');
end
aa=sum(a.*a,1); bb=sum(b.*b,1); ab=a'*b;
d = sqrt(abs(repmat(aa',[1 size(bb,2)]) + repmat(bb,[size(aa,2) 1]) - 2*ab));

%%

%interp_colormap
% Function which interpolates current colourmap to yield better graduated
% shading. N is number of possible colours.
function interp_colormap( N )

%Get current colourmap
map = colormap;

%Initialise new colormap
new_map = ones(N,3);

%Get size of current colormap and initalise red,green,blue vectors
dim = size(map);
R = ones(1,dim(1));
G = ones(1,dim(1));
B = ones(1,dim(1));
RR = ones(1,N);
GG = ones(1,N);
BB = ones(1,N);

%Populate these with current colormap
R(:) = map(:,1);
G(:) = map(:,2);
B(:) = map(:,3);

%Interpolate to yield new colour map
x = linspace( 1, dim(1), N );
RR = interp1( 1:dim(1), R, x );
GG = interp1( 1:dim(1), G, x );
BB = interp1( 1:dim(1), B, x );
new_map(:,1) = RR(:);
new_map(:,2) = GG(:);
new_map(:,3) = BB(:);

%Set colormap to be new map
colormap( new_map );

%%

%Make RGB color values from a scalar and a colormap
function [r,g,b] = x_to_color(x,cmap,xmin,xmax)
red = cmap(:,1).';
green = cmap(:,2).';
blue = cmap(:,3).';
cx = linspace( 0, 1, length(red) );
x = (x - xmin)/(xmax - xmin);
x(x>1) = 0.99;
x(x<0) = 0.01;
r = interp1( cx, red, x );
g = interp1( cx, green, x );
b = interp1( cx, blue, x );
r(r>1)=1; r(r<0)=0;
g(g>1)=1; g(g<0)=0;
b(b>1)=1; b(b<0)=0;

%%

%Convert a number x into a string of N characters
function s = leadingzero(x,N)
s = [strrep(blanks(N-1 - fix(log10(x))),' ','0'),...
    num2str(x,['%',num2str(N),'.0f'])];

%%

%globular_cluster
% Function which creates arrays of positions and velocities of masses that
% will form circular orbits aboyt a central mass, filling a spherical volume
% around the central masses.
function [x0,y0,z0,vx0,vy0,vz0,num_masses] = globular_cluster( xc,yc,zc,mc,shell_separation_AU,...
    num_shells, r0, num_masses_per_square_AU, G)

%Planets starting their orbits in spherical shells about star with coordinates
%(xc,yc,zc)
x0 = [];
y0 = [];
z0 = [];
vx0 = [];
vy0 = [];
vz0 = [];
for n=1:num_shells
    
    %Ring radius /AU
    r = r0 + shell_separation_AU*(n-1);
    
    %Ring prbital speed /AU per Year
    v = sqrt( G *mc/r );
    
    %Number of planets per shell
    s = floor( sqrt( num_masses_per_square_AU*4*pi*r^2 ));
    azi = linspace(0,2*pi,s+1);
    elev = linspace(-pi,pi,s+1);
    
    %Compute planet positions and velocities
    if s >=1
        for i=1:s
            for j=1:s
                %x
                x0 = [x0,xc+r*cos(elev(i))*sin(azi(j))];
                vx0 = [vx0,-v*sin(elev(i))*sin(azi(j))];
                
                %y
                y0 = [y0,yc+r*cos(elev(i))*cos(azi(j))];
                vy0 = [vy0,-v*sin(elev(i))*cos(azi(j))];
                
                %z
                z0 = [z0,zc + r*sin(elev(i))];
                vz0 = [vz0,v*cos(elev(i))];
            end
        end
    end
end

%Compute total number of masses
num_masses = length(x0);

%%

%Make vector of masses from input parameters
function [x,y,z,vx,vy,vz,R,M,masses] = make_masses( masses )
x = [];
y = [];
z = [];
vx = [];
vy = [];
vz = [];
R = [];
M = [];
if ~isempty(masses)
    for n=1:length(masses)
        x = [x,masses(n).x0];
        y = [y,masses(n).y0];
        z = [z,masses(n).z0];
        vx = [vx,masses(n).vx0];
        vy = [vy,masses(n).vy0];
        vz = [vz,masses(n).vz0];
        R = [R,masses(n).radii];
        M = [M,masses(n).mass];
    end
end

%%

%Make vector of massless rings from input parameters
function [x,y,z,vx,vy,vz,R,M,rings] = make_rings( rings, G )
x = [];
y = [];
z = [];
vx = [];
vy = [];
vz = [];
R = [];
M = [];
if ~isempty(rings)
    for n=1:length(rings)
        [xx,yy,zz,vxx,vyy,vzz,num_masses] = mass_rings( rings(n).xc,rings(n).yc,rings(n).zc,....
            rings(n).mass_at_centre, rings(n).arc_separation_AU,...
            rings(n).num_rings, rings(n).first_ring_radius_AU,...
            rings(n).ring_radius_diff_AU, rings(n).d, rings(n).alpha, G);
        x = [x,xx];
        y = [y,yy];
        z = [z,zz];
        vx = [vx,vxx + rings(n).vxc];
        vy = [vy,vyy + rings(n).vyc];
        vz = [vz,vzz + rings(n).vzc];
        R = [R,zeros(1,num_masses)];
        M = [M,zeros(1,num_masses)];
        rings(n).num_masses = num_masses;
    end
end

%%

%Make globular clusters from input parameters
function [x,y,z,vx,vy,vz,R,M,clusters] = make_clusters( clusters, G )
x = [];
y = [];
z = [];
vx = [];
vy = [];
vz = [];
R = [];
M = [];
if ~isempty(clusters)
    for n=1:length(clusters)
        [xx,yy,zz,vxx,vyy,vzz,num_masses] = globular_cluster( clusters(n).xc,clusters(n).yc,clusters(n).zc,...
            clusters(n).mass_at_centre,clusters(n).shell_separation_AU,...
            clusters(n).num_shells, clusters(n).first_shell_radius, clusters(n).num_masses_per_square_AU, G);
        x = [x,xx];
        y = [y,yy];
        z = [z,zz];
        vx = [vx,vxx + clusters(n).vxc];
        vy = [vy,vyy + clusters(n).vyc];
        vz = [vz,vzz + clusters(n).vzc];
        R = [R,zeros(1,num_masses)];
        M = [M,zeros(1,num_masses)];
        clusters(n).num_masses = num_masses;
    end
end

%%

%Plot initial positions of masses and density map, if selected
function d = plot_initial_positions(d)


qf(1,1);
hold on;
xlabel('x [AU]')
ylabel('y [AU]')
zlabel('z [AU]')
view(30,30)
grid on;

%Plot initial positions of masses

%Masses
if ~isempty(d.masses)
    for n=1:length(d.masses)
        if isempty( d.masses(n).marker_RGB )
            [r,g,b] = x_to_color(sqrt(d.vx(n).^2 + d.vy(n).^2 + d.vz(n).^2 ),speed_colormap,0,d.max_speed);
        else
            r = d.masses(n).marker_RGB(1);
            g = d.masses(n).marker_RGB(2);
            b = d.masses(n).marker_RGB(3);
        end
        
        if d.plotSpheres_q
            d.p(n) = surf(d.masses(n).sphereX + d.x(n), d.masses(n).sphereY + d.y(n), d.masses(n).sphereZ + d.z(n), 'EdgeColor', [0.2, 0.2, 0.2], 'FaceAlpha', 0.5, 'facecolor', [r,g,b]);
            
            d.tail(n) = plot3(d.x(n), d.y(n), d.z(n), '.', 'markerfacecolor', [r,g,b], 'markeredgecolor', [r,g,b], 'markersize', d.tailMarkerSize);
        else
            d.p(n) = plot3(d.x(n),d.y(n),d.z(n),'ro','markerfacecolor',[r,g,b],'markeredgecolor',...
                [r,g,b],'markersize',d.masses(n).marker_size);
            
            d.tail(n) = plot3(d.x(n), d.y(n), d.z(n), '.', 'markerfacecolor', [r,g,b], 'markeredgecolor', [r,g,b], 'markersize', d.tailMarkerSize);
        end
    end
end

%Rings
if ~isempty(d.rings)
    n0 = length(d.masses);
    end_index = n0;
    for n=1:length(d.rings)
        start_index = end_index+1;
        end_index = start_index + d.rings(n).num_masses - 1;
        if isempty( d.rings(n).marker_RGB )
            [r,g,b] = x_to_color( mean( sqrt(d.vx(start_index:end_index).^2 +...
                d.vy(start_index:end_index).^2 + d.vz(start_index:end_index).^2 )),speed_colormap,0,d.max_speed);
        else
            r = d.rings(n).marker_RGB(1);
            g = d.rings(n).marker_RGB(2);
            b = d.rings(n).marker_RGB(3);
        end
        d.p(n0+n) = plot3(d.x(start_index:end_index),d.y(start_index:end_index),d.z(start_index:end_index),'r.','markersize',d.ringMarkerSize,...
            'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
    end
end

%Clusters
if ~isempty(d.clusters)
    n0 = n0 + length(d.rings);
    for n=1:length(d.clusters)
        start_index = end_index+1;
        end_index = start_index + d.clusters(n).num_masses - 1;
        if isempty( d.clusters(n).marker_RGB )
            [r,g,b] = x_to_color( mean( sqrt(d.vx(start_index:end_index).^2 +...
                d.vy(start_index:end_index).^2 + d.vz(start_index:end_index).^2 )),speed_colormap,0,d.max_speed);
        else
            r = d.clusters(n).marker_RGB(1);
            g = d.clusters(n).marker_RGB(2);
            b = d.clusters(n).marker_RGB(3);
        end
        d.p(n0+n) = plot3(d.x(start_index:end_index),d.y(start_index:end_index),d.z(start_index:end_index),'r.','markersize',d.msize,...
            'markerfacecolor',[r,g,b],'markeredgecolor',[r,g,b]);
    end
end

%Set title
d.title_str = 'TITLE';
d.title = title(d.title_str);


%%

%Default settings
function [masses,rings,clusters,G,Lmax,k,view_option,map_underlay,dt,Ng,Nr,RM] = gravity_sim_defaults

%Astronomical parameters in SI units
% AU = 149.6e9;
% G = 6.67e-11;
% M_sun = 2e30;
% Yr = 365*24*3600;
% 
% %Dimensionless number which controls the dynamics, and results from the
% %scaling of mass, distance and time parameters to make them dimensionless.
% G = G*Yr^2*M_sun/(AU^3);
% 
% %Strength of gravity
% G_factor = 1;
% G = G_factor*G;

%Timestep
dt = 0.05;

%Gravitation exponent
Ng = 2;

%Repulsion exponent
Nr = 5;

%Repulsion magnitude
RM = 0.01;

%Set dimensions of space
Lmax = 8;

%Star masses in Solar masses
M1 = 1;
M2 = 1;

%Initial star separation
s = 6;

%Initial phase (polar angle) of two-mass elliptical orbit
theta0=0;

%Elliptical orbit eccentricity
ecc = 0;

%Orientation of semi-major axis of ellipse
d = [1,0,0];

%Rotation of ellipse clockwise about d /radians
alpha = 0;

%Centre of mass initial coordinates
h0 = [0,0,0];

%Cetre of mass velocity
hdot = [0,0,0];

%Boundary coefficient of restitution (if [] then there is
%no boundary)
k = [];

%Initial view option
view_option = 3;

%For 2D views, choose density or speed map underlay ('density', 'speed',
%'none')
map_underlay = 'none';

%Binary star circular orbit conditions
[ux,vx1,vx2,...
    uy,vy1,vy2,...
    uz,vz1,vz2,...
    xc,x1,x2,...
    yc,y1,y2,...
    zc,z1,z2,...
    theta,theta_dot,t,j,J,E,P] =...
    two_body_elliptical_orbit(  G, M1, M2, s, ecc, theta0, h0, hdot, d, alpha, 1 );

%Star (the position of this one can be modified)
masses(1).name = 'Sun #1';
masses(1).mass = M1;
masses(1).radii = 0.01;
masses(1).x0 = x1;
masses(1).y0 = y1;
masses(1).z0 = z1;
masses(1).vx0 = vx1;
masses(1).vy0 = vy1;
masses(1).vz0 = vz1;
masses(1).marker_RGB = [0,1,0];
masses(1).marker_size = 5;
masses(1).plot_trail = 1;
masses(1).init_orbit_M = M2;
masses(1).init_orbit_G = G;
masses(1).init_orbit_P = P;
masses(1).init_orbit_ecc = ecc;
masses(1).init_orbit_s = s;
masses(1).init_orbit_h0 = h0;
masses(1).init_orbit_hdot = hdot;
masses(1).init_orbit_d = d;
masses(1).init_orbit_alpha = alpha;
masses(1).init_orbit_theta0 = theta0;
masses(1).init_orbit_thetadot0 = theta_dot;
masses(1).init_orbit_J = J;
masses(1).init_orbit_j = j;
masses(1).init_orbit_E = E;

%Star (the position of this one can be modified)
masses(2).name = 'Sun #2';
masses(2).mass = M2;
masses(2).radii = 0.01;
masses(2).x0 = x2;
masses(2).y0 = y2;
masses(2).z0 = z2;
masses(2).vx0 = vx2;
masses(2).vy0 = vy2;
masses(2).vz0 = vz2;
masses(2).marker_RGB = [1,0,0];
masses(2).marker_size = 5;
masses(2).plot_trail = 1;
masses(2).init_orbit_M = M1;
masses(2).init_orbit_G = G;
masses(2).init_orbit_P = P;
masses(2).init_orbit_ecc = ecc;
masses(2).init_orbit_s = s;
masses(2).init_orbit_h0 = h0;
masses(2).init_orbit_hdot = hdot;
masses(2).init_orbit_d = d;
masses(2).init_orbit_alpha = alpha;
masses(2).init_orbit_theta0 = theta0;
masses(2).init_orbit_thetadot0 = theta_dot;
masses(2).init_orbit_J = J;
masses(2).init_orbit_j = j;
masses(2).init_orbit_E = E;

%Rings (1)
rings(1).xc = x1;
rings(1).yc = y1;
rings(1).zc = z1;
rings(1).vxc = vx1;
rings(1).vyc = vy1;
rings(1).vzc = vz1;
rings(1).num_rings = 30;
rings(1).arc_separation_AU = 1*pi/30;
rings(1).first_ring_radius_AU = 2;
rings(1).ring_radius_diff_AU = 0.1;
rings(1).d = [1,0,0];
rings(1).alpha = 0;
rings(1).mass_at_centre = M1;
rings(1).marker_RGB = [0,0,0];
rings(1).marker_size = 1;

%Rings (2)
rings(2).xc = x2;
rings(2).yc = y2;
rings(2).zc = z2;
rings(2).vxc = vx2;
rings(2).vyc = vy2;
rings(2).vzc = vz2;
rings(2).num_rings = 30;
rings(2).arc_separation_AU = 1*pi/30;
rings(2).first_ring_radius_AU = 2;
rings(2).ring_radius_diff_AU = 0.1;
rings(2).d = [1,0,0];
rings(2).alpha = 0;
rings(2).mass_at_centre = M2;
rings(2).marker_RGB = [1,0,1];
rings(2).marker_size = 1;

%Clusters
clusters = [];

%%

%Get settings from file
function [masses,rings,clusters,G,Lmax,k,view_option,map_underlay,dt,Ng,Nr,RM] = load_data( data_name )

%Note the desired outputs will be defined in the data file, which is a
%MATLAB script. All other parameters will be ignored since the file is run
%within this function.
run(data_name);

%End of code