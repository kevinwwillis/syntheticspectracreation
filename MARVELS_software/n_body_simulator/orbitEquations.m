
%% Mean Anomaly

M = E - e * sin(E);

M = M_0 + 2 * pi / P * (t - t_p);


%% True Anomaly

if dot(r, v) < 0
    nu = 2 * pi - acos( dot(e_vec, r) / ( vecnorm(e_vec) * vecnorm(r) ) );
else
    nu = acos( dot(e_vec, r) / ( vecnorm(e_vec) * vecnorm(r) ) );
end

nu = acos( ( cos(E) - e ) / ( 1 - e * cos(E) ) );

nu = 2 * atan2( sqrt( 1 - e ) * cos(E / 2), sqrt( 1 + e ) * sin(E / 2) );


%% Eccentric Anomaly

E - e * sin(E) = M;

E - e * sin(E) = M_0 + 2 * pi * (t + t_p) / P; % Cannot be solved directly. Must be solved by minimization iterations. M_0 is zero for most cases.


%% Eccentricity Vector

e_vec = cross(v, cross(r, v) ) / (G * sum(m(:))) - r / vecnorm(r);


%% Orbital Period

P = 2 * pi * sqrt( a^3 / ( G * sum(m(:)) ) );

P = (m2^3 * 2 * pi * G) / ( ( m1 + m2 )^2 * (1 - e^2)^(3/2) * K1^3 );


%% RV semi-amplitude

K = ( 2 * pi * G / P )^(1 / 3) * m_p * sin(i_inc) / ( m_s + m_p )^(2 / 3) * 1 / sqrt(1 - e^2);

K = 2 * pi / P * a * sin(i_inc) / sqrt(1 - e^2);


%% RV observed

v_s_obs = v_s * sin(i_inc);


%% RV planet

v_p = K * ( cos(v_s + w) + e * cos(w) ) + v_sys; % v_sys is unnacounted movement in the systems COM.


rvlin 
