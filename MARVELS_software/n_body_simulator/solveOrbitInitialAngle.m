function initAngle = solveOrbitInitialAngle(obsDates, t_p, P, ecc)



% Mean anomaly
Mp = 2 * pi * (obsDates(1) - t_p) / P;

% Eccentric anomaly
Ep = mod(newton( mod(Mp, 2 * pi), ecc), 2 * pi);

initAngle = 2 * atan( sqrt( (1 + ecc) / (1 - ecc) ) * tan( Ep / 2 ) );


% Phase Cycle Correction
while initAngle < 0
    
    initAngle = initAngle + 2*pi;
end