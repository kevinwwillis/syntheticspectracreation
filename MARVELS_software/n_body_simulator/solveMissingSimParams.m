function sim = solveMissingSimParams(sim, G)



%% Ecc

sim.object(1).ecc = sim.sys(1).ecc;
sim.object(2).ecc = sim.sys(1).ecc;
sim.object(3).ecc = sim.sys(2).ecc;


%% Separation

if isempty(sim.sys(1).a_m)
    
    if ~isempty(sim.object(1).an_m)
        
        sim.sys(1).a_m = sim.object(1).an_m + sim.object(2).an_m;
        sim.sys(2).a_m = sim.object(3).an_m;
    else
        
        if isempty(sim.object(1).an_m)
            
            if isempty(sim.object(1).P_s)
                
                if isempty(sim.object(1).K_mps) & isempty(sim.object(1).ecc)
                    
                    error('Variables "P", "K", "ecc", and "a" are empty. At least one of the following variables needs to be supplied: P, a, a_n, K, ecc.')
                    
                elseif isempty(sim.object(1).K_mps) & ~isempty(sim.object(1).ecc)
                    
                else
                    
                    sim.object(1).P_s = (sim.object(2).m_kg^3 * 2 * pi * G) / ( (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg)^2 * (1 - sim.object(1).ecc^2 )^(3/2) * sim.object(1).K_mps^3 );
                    sim.object(2).P_s = sim.object(1).P_s;
                    sim.object(3).P_s = ((sim.object(1).m_kg + sim.object(2).m_kg)^3 * 2 * pi * G) / ( (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg)^2 * (1 - sim.object(3).ecc^2 )^(3/2) * sim.object(3).K_mps^3 );
                    
                    sim.object(1).an_m = ( (sim.object(1).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
                    sim.object(2).an_m = ( (sim.object(2).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
                    sim.object(3).an_m = ( (sim.object(3).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
                end
                
            else
                
                sim.object(1).an_m = ( (sim.object(1).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
                sim.object(2).an_m = ( (sim.object(2).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
                sim.object(3).an_m = ( (sim.object(3).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
            end
        end
        sim.sys(1).a_m = sim.object(1).an_m + sim.object(2).an_m;
        sim.sys(2).a_m = sim.object(3).an_m;
    end
    
end



%% Period


if isempty(sim.sys(1).P_s)
    
    if isfield(sim.object(1), 'P_s')
       
        if ~isempty(sim.object(1).P_s)
            sim.sys(1).P_s = sim.object(1).P_s;
            sim.sys(2).P_s = sim.object(3).P_s;
            fail = 0;
        else
            fail = 1;
        end
    else
        fail = 1;
    end
    if fail == 1
        
        if ~isempty(sim.object(1).K_mps)
            
            sim.object(1).P_s = (sim.object(2).m_kg^3 * 2 * pi * G) / ( (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg)^2 * (1 - sim.object(1).ecc^2 )^(3/2) * sim.object(1).K_mps^3 );
            sim.object(2).P_s = sim.object(1).P_s;
            sim.object(3).P_s = ((sim.object(1).m_kg + sim.object(2).m_kg)^3 * 2 * pi * G) / ( (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg)^2 * (1 - sim.object(3).ecc^2 )^(3/2) * sim.object(3).K_mps^3 );
            
            sim.object(1).an_m = ( (sim.object(1).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
            sim.object(2).an_m = ( (sim.object(2).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
            sim.object(3).an_m = ( (sim.object(3).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
            
        elseif ~isempty(sim.sys(1).a_m)
            
            sim.object(1).P_s = 2 * pi * sqrt( (sim.sys(1).a_m)^3 / ( G * (sim.object(1).m_kg + sim.object(2).m_kg) ) );
            sim.object(2).P_s = sim.object(1).P_s;
            sim.object(3).P_s = 2 * pi * sqrt( sim.sys(2).a_m^3 / ( G * sum(sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) ) );
            
        else
            
            error('Variables "K", "ecc", and "a" are empty. At least one of the following variables needs to be supplied: a, a_n, K, ecc.')
        end
        
        sim.sys(1).P_s = sim.object(1).P_s;
        sim.sys(2).P_s = sim.object(3).P_s;
    end
    
else
    
    sim.object(1).P_s = sim.sys(1).P_s;
    sim.object(2).P_s = sim.sys(1).P_s;
    sim.object(3).P_s = sim.sys(2).P_s;
end



if ~isfield(sim.object(1), 'P_s')
    
    
    if ~isempty(sim.object(1).K_mps)
        
        sim.object(1).P_s = (sim.object(2).m_kg^3 * 2 * pi * G) / ( (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg)^2 * (1 - sim.object(1).ecc^2 )^(3/2) * sim.object(1).K_mps^3 );
        sim.object(2).P_s = sim.object(1).P_s;
        sim.object(3).P_s = ((sim.object(1).m_kg + sim.object(2).m_kg)^3 * 2 * pi * G) / ( (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg)^2 * (1 - sim.object(3).ecc^2 )^(3/2) * sim.object(3).K_mps^3 );
        
        sim.object(1).an_m = ( (sim.object(1).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
        sim.object(2).an_m = ( (sim.object(2).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
        sim.object(3).an_m = ( (sim.object(3).P_s)^2 * G * (sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) / (4 * pi^2) )^(1 / 3);
        
    elseif ~isempty(sim.sys(1).a_m)
        
        sim.object(1).P_s = 2 * pi * sqrt( (sim.sys(1).a_m)^3 / ( G * (sim.object(1).m_kg + sim.object(2).m_kg) ) );
        sim.object(2).P_s = sim.object(1).P_s;
        sim.object(3).P_s = 2 * pi * sqrt( sim.sys(2).a_m^3 / ( G * sum(sim.object(1).m_kg + sim.object(2).m_kg + sim.object(3).m_kg) ) );
        
    else
        
        error('Variables "K", "ecc", and "a" are empty. At least one of the following variables needs to be supplied: a, a_n, K, ecc.')
    end
end



%% Init Orbit Angle

if isempty(sim.sys(1).theta_rad) & ~isempty(sim.sys(1).tp_s)
    
    sim.sys(1).theta_rad = solveOrbitInitialAngle(sim.observationDates_s, sim.sys(1).tp_s, sim.object(1).P_s, sim.object(1).ecc);
    sim.sys(2).theta_rad = solveOrbitInitialAngle(sim.observationDates_s, sim.sys(2).tp_s, sim.object(3).P_s, sim.object(3).ecc);
    
elseif ~isempty(sim.sys(1).theta_rad) & isempty(sim.sys(1).tp_s)

    sim.sys(1).tp_s = fsolve(@(tp) sim.sys(1).theta_rad - solveOrbitInitialAngle(sim.observationDates_s, tp, sim.object(1).P_s, sim.object(1).ecc), 0);
    sim.sys(2).tp_s = fsolve(@(tp) sim.sys(3).theta_rad - solveOrbitInitialAngle(sim.observationDates_s, tp, sim.object(3).P_s, sim.object(3).ecc), 0);

else
    
    error('Variables "theta" and "tp" are empty. At least one of the following variables needs to be supplied: theta, tp.')
end



%% omega_xyz - Orientation of semi-major axis of ellipse [vector components]

sim.sys(1).omega_xyz(1, :) = -[cos(sim.sys(1).omeaga_rad + 0.25 * 2 * pi), sin(sim.sys(1).omeaga_rad + 0.25 * 2 * pi), 0]; % Weird math explination: rotate orientation by 90 deg
sim.sys(2).omega_xyz(1, :) = -[cos(sim.sys(2).omeaga_rad + 0.25 * 2 * pi), sin(sim.sys(2).omeaga_rad + 0.25 * 2 * pi), 0];


