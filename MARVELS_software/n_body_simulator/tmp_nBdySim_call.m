clearvars

% Notes:
% 1) Energy var (E) needs to be fixed. It is not used in the simulation,
% but may be used later. Can use to measure/correct energy drift
% 
% 

%% Constants

AU = 149.6e9;
G = 6.67e-11;
M_sun = 2e30;
Yr = 365*24*3600;

%Dimensionless number which controls the dynamics, and results from the
%scaling of mass, distance and time parameters to make them dimensionless.
G = G*Yr^2*M_sun/(AU^3);


%% Physical System Params

% Object names
name{1} = 'Star 1';
name{2} = 'Star 2';
name{3} = 'Planet 1';
name{4} = 'Planet 2';

% Object type
objType{1} = 's';
objType{2} = 's';
objType{3} = 'p';
objType{4} = 'p';

% Object masses [solar masses] (m)
m(1) = 1.5;
m(2) = 3;
m(3) = 0.000003 * 1000;
m(4) = 0.000009 * 1000;

% Object orbit configuration (which objects are gravitationally bound to each other?)
gBoundPairIndex{1} = [1, 2];
gBoundPairIndex{2} = [3];
gBoundPairIndex{3} = [4];

% Initial star separation [AU] (a)
gBoundPair_sep_AU(1) = 10;
gBoundPair_sep_AU(2) = 20;
gBoundPair_sep_AU(3) = 20;

% Initial phase (polar angle) of two-mass elliptical orbit
theta0(1) = 0;
theta0(2) = 0;
theta0(3) = 0;

% Orbit eccentricity [] (e)
ecc(1) = 0.4;
ecc(2) = 0.02;
ecc(3) = 0.0;

% Orientation of semi-major axis of ellipse
smo(1, :) = [1,0,0]; % <-------------- Needs to be converted to an angle input
smo(2, :) = [0,0,1];
smo(3, :) = [0,1,1];

% Rotation of ellipse clockwise about d [radians] (w {omega})
alpha(1) = 0;
alpha(2) = 0;
alpha(3) = 0;

% Center of mass initial coordinates [AU]
h0 = [0,0,0];

% Center of mass velocity [AU/s]
hdot0 = [0,0,0];


%% Plot Params

plot_q = 1;

showPathTrails_q = 1;

showObjNames_q = 1;

obj_RGB{1} = [1, 0, 0];
obj_RGB{2} = [0, 1, 0];
obj_RGB{3} = [0, 0, 1];
obj_RGB{4} = [0.5, 0, 0.5];

% (NOT WORKING) Add cool looking (massless) particle rings around a certain object
addMasslessParticleRings_q(1) = 0;
addMasslessParticleRings_q(2) = 0;
addMasslessParticleRings_q(3) = 0;
addMasslessParticleRings_q(4) = 0;



%%

sys_ind = 1:length(gBoundPair_sep_AU);

for sysNum = sys_ind
    
    obj_ind = gBoundPairIndex{sysNum};
    
    %     if sysNum ~= 1
    %
    %         foundMatch_ind = false((sysNum - 1), 2);
    %
    %         obj_ind2 = 0;
    %
    %         % Is either of these objects already accounted for in this loop?
    %         for objNum = obj_ind
    %
    %             obj_ind2 = obj_ind2 + 1;
    %
    %             for sysNum2 = sys_ind(1:(sysNum - 1))
    %
    %                 foundMatch_ind(sysNum2, obj_ind2) = any( gBoundPairIndex{sysNum2} == objNum );
    %             end
    %         end
    %     else
    %
    %     end
    
    
    if length(obj_ind) > 2 | length(obj_ind) <= 0
        
        error(cat(2, 'ERROR: The number of objects in "gBoundPairIndex" for system #', n2s(sysNum), ' is ', n2s(length(gBoundPairIndex{sysNum})), ', but only 1 or 2 objects allowed per bound system.'))
        
        
    elseif length(obj_ind) == 2 % Binary star systems and star-planet systems
        
        
        if strcmp(objType{obj_ind(1)}, 's') & strcmp(objType{obj_ind(2)}, 's')
            
            hdot = hdot0;
            
            h = h0;
            
            obj_ind_save = obj_ind;
            
            % Determine initial conditions of this system
            [ ...
                ux, vx(obj_ind(1)), vx(obj_ind(2)),...
                uy, vy(obj_ind(1)), vy(obj_ind(2)),...
                uz, vz(obj_ind(1)), vz(obj_ind(2)),...
                xc, x(obj_ind(1)), x(obj_ind(2)),...
                yc, y(obj_ind(1)), y(obj_ind(2)),...
                zc, z(obj_ind(1)), z(obj_ind(2)),...
                theta, theta_dot, t, j, J, E, P ...
                ] = two_body_elliptical_orbit(  G, m(obj_ind(1)), m(obj_ind(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
            
        else
            
            hdot = [vx(obj_ind(1)), vy(obj_ind(1)), vz(obj_ind(1))];
            
            h = [x(obj_ind(1)), y(obj_ind(1)), z(obj_ind(1))];
            
            obj_ind_save = obj_ind(2);
            
            % Determine initial conditions of this system
            [ ...
                ux, ~, vx(obj_ind(2)),...
                uy, ~, vy(obj_ind(2)),...
                uz, ~, vz(obj_ind(2)),...
                xc, ~, x(obj_ind(2)),...
                yc, ~, y(obj_ind(2)),...
                zc, ~, z(obj_ind(2)),...
                theta, theta_dot, t, j, J, E, P ...
                ] = two_body_elliptical_orbit(  G, m(obj_ind(1)), m(obj_ind(2)), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
            
            
        end
        
        

        
        
        % Place params into final structure
        for objNum = obj_ind_save
            
            masses(objNum).name = name{objNum};
            masses(objNum).mass = m(objNum);
            masses(objNum).radii = 1e-10;
            masses(objNum).x0 = x(objNum);
            masses(objNum).y0 = y(objNum);
            masses(objNum).z0 = z(objNum);
            masses(objNum).vx0 = vx(objNum);
            masses(objNum).vy0 = vy(objNum);
            masses(objNum).vz0 = vz(objNum);
            masses(objNum).marker_RGB = obj_RGB{objNum};
            masses(objNum).marker_size = 5;
            masses(objNum).plot_trail = showPathTrails_q;
            masses(objNum).init_orbit_M = m(objNum);
            masses(objNum).init_orbit_P = P;
            masses(objNum).init_orbit_G = G;
            masses(objNum).init_orbit_ecc = ecc(sysNum);
            masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
            masses(objNum).init_orbit_h0 = h0;
            masses(objNum).init_orbit_hdot = hdot;
            masses(objNum).init_orbit_d = smo(sysNum, :);
            masses(objNum).init_orbit_alpha = alpha(sysNum);
            masses(objNum).init_orbit_theta0 = theta0(sysNum);
            masses(objNum).init_orbit_thetadot0 = theta_dot;
            masses(objNum).init_orbit_J = J;
            masses(objNum).init_orbit_j = j;
            masses(objNum).init_orbit_E = E;
        end
        
        
        
        
    elseif length(obj_ind) == 1 & strcmp(objType(obj_ind), 'p') % Circumbinary Planet
          
        
        objNum = obj_ind;
            
        hdot = hdot0;
        
        h = h0;
        
        m_tot = m(1) + m(2);
        
        % Determine initial conditions of this system
        [ ...
            ux, ~, vx(objNum),...
            uy, ~, vy(objNum),...
            uz, ~, vz(objNum),...
            xc, ~, x(objNum),...
            yc, ~, y(objNum),...
            zc, ~, z(objNum),...
            theta, theta_dot, t, j, J, E, P ...
            ] = two_body_elliptical_orbit(  G, m_tot, m(objNum), gBoundPair_sep_AU(sysNum), ecc(sysNum), theta0(sysNum), h, hdot, smo(sysNum, :), alpha(sysNum), 1 );
        
        
        masses(objNum).name = name{objNum};
        masses(objNum).mass = m(objNum);
        masses(objNum).radii = 1e-10;
        masses(objNum).x0 = x(objNum);
        masses(objNum).y0 = y(objNum);
        masses(objNum).z0 = z(objNum);
        masses(objNum).vx0 = vx(objNum);
        masses(objNum).vy0 = vy(objNum);
        masses(objNum).vz0 = vz(objNum);
        masses(objNum).marker_RGB = obj_RGB{objNum};
        masses(objNum).marker_size = 5;
        masses(objNum).plot_trail = showPathTrails_q;
        masses(objNum).init_orbit_M = m(objNum);
        masses(objNum).init_orbit_P = P;
        masses(objNum).init_orbit_G = G;
        masses(objNum).init_orbit_ecc = ecc(sysNum);
        masses(objNum).init_orbit_s = gBoundPair_sep_AU(sysNum);
        masses(objNum).init_orbit_h0 = h0;
        masses(objNum).init_orbit_hdot = hdot;
        masses(objNum).init_orbit_d = smo(sysNum, :);
        masses(objNum).init_orbit_alpha = alpha(sysNum);
        masses(objNum).init_orbit_theta0 = theta0(sysNum);
        masses(objNum).init_orbit_thetadot0 = theta_dot;
        masses(objNum).init_orbit_J = J;
        masses(objNum).init_orbit_j = j;
        masses(objNum).init_orbit_E = E;
        
        
        
    elseif length(obj_ind) == 1 & strcmp(objType(obj_ind), 's') % Circumbinary Star
        
        error('ERROR: Simulating a circumbinary star is not currently available. Please remove that star.')
    end
    
    
    
end


rings = [];

for objNum = 1:length(objType)
    
    if addMasslessParticleRings_q(objNum)
        
%         if strcmp(objType{objNum}, 'p')
            rings(objNum).xc = x(objNum);
            rings(objNum).yc = y(objNum);
            rings(objNum).zc = z(objNum);
%         else
%             rings(objNum).xc = x(objNum);
%             rings(objNum).yc = y(objNum);
%             rings(objNum).zc = z(objNum);
%         end
        rings(objNum).vxc = vx(objNum);
        rings(objNum).vyc = vy(objNum);
        rings(objNum).vzc = vz(objNum);
        rings(objNum).num_rings = 40;
        rings(objNum).arc_separation_AU = 1*pi/30;
        rings(objNum).first_ring_radius_AU = 0.10;
        rings(objNum).ring_radius_diff_AU = 0.5;
        rings(objNum).d = masses(objNum).init_orbit_d;
        rings(objNum).alpha = pi/2;
        rings(objNum).mass_at_centre = m(objNum);
        rings(objNum).marker_RGB = obj_RGB{objNum} * 0.8;
        
    end
end


clusters = [];









nBodySim(masses, rings, clusters, plot_q, showObjNames_q)