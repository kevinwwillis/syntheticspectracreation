

% Plot simulated and recovered orbits

load(['E:\Research\syntheticSpectraCreation\binary_orbitParams.mat'], 'hist')

load(['E:\Research\syntheticSpectraCreation\binary_orbitParams_RECOVERY_v1.mat'], 'hist_r')


p2_markerStyle = '-';

tr = 1:length(hist.t);

objNums = 1:3;

qf(2,1)


title('Velocity')
grid on
ylabel('v [m/s]')
xlim([0 inf])



objNum = 1;
    

subplot(2,1,1); hold on
% plot(hist.t(tr), hist.m(objNum).vx(tr), '-k');
% plot(hist_r.t(tr), hist_r.m(objNum).vx(tr), '-r');




% plot(hist.t(tr), hist.m(objNum).vx(tr) - nanmean(hist.m(objNum).vx(tr)), '--k');
% plot(hist_r.t(tr), hist_r.m(objNum).vx(tr) - nanmean(hist_r.m(objNum).vx(tr)), '--r');




plot(hist.t(tr), hist.m(objNum).vx(tr) - nanmean([max(hist.m(objNum).vx(tr)) , min(hist.m(objNum).vx(tr))]), '-k');
plot(hist_r.t(tr), hist_r.m(objNum).vx(tr) - nanmean([max(hist_r.m(objNum).vx(tr)) , min(hist_r.m(objNum).vx(tr))]), '-r');


subplot(2,1,2); hold on

plot(hist.t(tr), (hist.m(objNum).vx(tr) - nanmean([max(hist.m(objNum).vx(tr)) , min(hist.m(objNum).vx(tr))])) - ...
    (hist_r.m(objNum).vx(tr) - nanmean([max(hist_r.m(objNum).vx(tr)) , min(hist_r.m(objNum).vx(tr))])), '-k');














































