% function simulateBinary_template_v1

% This script uses solved orbital parameters from REAL data to simulate
% the system.

clearvars


%% Constants

m_AU = 149597870700;
G = 6.67408e-11;
m_sun_kg = 1.9885e30;
m_jup_kg = 1.89813e27;
s_yr = 365*24*3600;
s_day = 24 * 60 * 60;


%% Save names

pipeline_dir = pwd;

plateName = 'FIELD1572_testSet1';

beamNum = '87';


%% Physical System Params

% Time step of each simulation iteration. (Testing can be done with 5 hr time steps, but final results should be done using 0.5 hr time steps)
sim.simTimeStep_s = seconds(hours(5));
% sim.simTimeStep_s = seconds(hours(1));
% sim.simTimeStep_s = seconds(hours(0.5));

% Observation Dates
% sim.observationDates_s = [1 3 6 10 14 14.2 16 30 35] * s_day;
% sim.observationDates_s = sort(randi([0 670], 1, 30)) * s_day;
sim.observationDates_s = linspace(0, 680, 30) * s_day;

% Object names
sim.object(1).name = 'Star 1';
sim.object(2).name = 'Star 2';
sim.object(3).name = 'Planet 1';

sim.sys(1).name = '(Star 1) <-> (Star 2)';
sim.sys(2).name = '(Star 1 + Star 2) <-> (Planet 1)';

% Object type
sim.object(1).objType = 's';
sim.object(2).objType = 's';
sim.object(3).objType = 'p';

% m - object masses [kg]
sim.object(1).m_kg = [1 * m_sun_kg];
sim.object(2).m_kg = [0.26 * m_sun_kg];
sim.object(3).m_kg = [0.0017 * m_sun_kg];

% P - orbital period [s/rad]
sim.sys(1).P_s = [42 * s_day];
sim.sys(2).P_s = [394.7 * s_day];

% a_n - COM separation at t = t_p [m]
sim.object(1).an_m = [];%[0.04 * m_AU];
sim.object(2).an_m = [];%[0.01 * m_AU];
sim.object(3).an_m = [];%[1.15 * m_AU];

% K - RV semi-amplitude at t = t_p [m/s]
sim.object(1).K_mps = [];
sim.object(2).K_mps = [];
sim.object(3).K_mps = [];

% a - object separation at t = t_p [m]
sim.sys(1).a_m = [0.053 * m_AU];
sim.sys(2).a_m = [1.14 * m_AU];

% theta - initial orbit angle [radians]
sim.sys(1).theta_rad = [];
sim.sys(2).theta_rad = []; % initAngle_p;

% t_p - time of periastron [s]
sim.sys(1).tp_s = [26.3 * s_day];
sim.sys(2).tp_s = [364.6 * s_day];

% e - orbit eccentricity
sim.sys(1).ecc = [0.147];
sim.sys(2).ecc = [0.15];

% omega - orientation of semi-major axis of ellipse [radians]
sim.sys(1).omeaga_rad = [0.79];
sim.sys(2).omeaga_rad = [2.9];

% ? - Rotation of ellipse clockwise about d [radians]
sim.sys(1).alpha_rad = [0];
sim.sys(2).alpha_rad = [0];

% COM - Center of mass initial coordinates [m]
sim.sys(2).COM = [0,0,0];

% COMV - Center of mass velocity [m/s]
sim.sys(2).COM_v_mps = [0,0,0];



%% Plot Params

plot_q = 0;

showPathTrails_q = 1;

showObjNames_q = 1;

obj_RGB{1} = [1, 0, 0];
obj_RGB{2} = [0, 1, 0];
obj_RGB{3} = [0, 0, 1];

% (NOT WORKING) Add cool looking (massless) particle rings around a certain object
addMasslessParticleRings_q(1) = 0;
addMasslessParticleRings_q(2) = 0;
addMasslessParticleRings_q(3) = 0;

% Show velocity on plot 2, as seen from Earth
masses(1).earthVplot = 1;

masses(1).dt = sim.simTimeStep_s;


%% Solve for missing system params

sim = solveMissingSimParams(sim, G);


%% Total simulation time needed [s]

masses(1).totSec = sim.observationDates_s(end);


%% Solve for the system's initial state

% Planet and sCOM

sysNum = 2;

objNum = 3;

% Determine initial conditions of this system
[ ...
    ux, vx_sCOM, vx(objNum),...
    uy, vy_sCOM, vy(objNum),...
    uz, vz_sCOM, vz(objNum),...
    xc, x_sCOM, x(objNum),...
    yc, y_sCOM, y(objNum),...
    zc, z_sCOM, z(objNum),...
    theta, theta_dot, t, j, J, E, Pp ...
    ] = two_body_elliptical_orbit(  ...
    G, sim.object(1).m_kg + sim.object(2).m_kg, sim.object(3).m_kg, ...
    sim.sys(sysNum).a_m, sim.sys(sysNum).ecc, sim.sys(sysNum).theta_rad, ...
    sim.sys(sysNum).COM, sim.sys(sysNum).COM_v_mps, sim.sys(sysNum).omega_xyz, sim.sys(sysNum).alpha_rad, 1 );


masses(objNum).name = sim.object(objNum).name;
masses(objNum).mass = sim.object(objNum).m_kg;
masses(objNum).radii = 1e-10;
masses(objNum).x0 = x(objNum);
masses(objNum).y0 = y(objNum);
masses(objNum).z0 = z(objNum);
masses(objNum).vx0 = vx(objNum);
masses(objNum).vy0 = vy(objNum);
masses(objNum).vz0 = vz(objNum);
masses(objNum).marker_RGB = obj_RGB{objNum};
masses(objNum).marker_size = 5;
masses(objNum).plot_trail = showPathTrails_q;
masses(objNum).init_orbit_M = sim.object(objNum).m_kg;
masses(objNum).init_orbit_P = Pp;
masses(objNum).init_orbit_G = G;
masses(objNum).init_orbit_ecc = sim.sys(sysNum).ecc;
masses(objNum).init_orbit_s = sim.sys(sysNum).a_m;
masses(objNum).init_orbit_h0 = sim.sys(sysNum).COM;
masses(objNum).init_orbit_hdot = sim.sys(sysNum).COM_v_mps;
masses(objNum).init_orbit_d = sim.sys(sysNum).omega_xyz;
masses(objNum).init_orbit_alpha = sim.sys(sysNum).alpha_rad;
masses(objNum).init_orbit_theta0 = sim.sys(sysNum).theta_rad;
masses(objNum).init_orbit_thetadot0 = theta_dot;
masses(objNum).init_orbit_J = J;
masses(objNum).init_orbit_j = j;
masses(objNum).init_orbit_E = E;




% s1 and s2

sysNum = 1;

% Set the COM velocity to the one calculated above
sim.sys(sysNum).COM_v_mps = [vx_sCOM, vy_sCOM, vz_sCOM];

% Set the COM position to the one calculated above
sim.sys(sysNum).COM = [x_sCOM, y_sCOM, z_sCOM];

obj_ind = [1, 2];

% The calculation script
[ ...
    ux, vx(obj_ind(1)), vx(obj_ind(2)),...
    uy, vy(obj_ind(1)), vy(obj_ind(2)),...
    uz, vz(obj_ind(1)), vz(obj_ind(2)),...
    xc, x(obj_ind(1)), x(obj_ind(2)),...
    yc, y(obj_ind(1)), y(obj_ind(2)),...
    zc, z(obj_ind(1)), z(obj_ind(2)),...
    theta, theta_dot, t, j, J, E, Ps ...
    ] = two_body_elliptical_orbit(  G, sim.object(1).m_kg, sim.object(2).m_kg, ...
    sim.sys(sysNum).a_m, sim.sys(sysNum).ecc, sim.sys(sysNum).theta_rad, ...
        sim.sys(sysNum).COM, sim.sys(sysNum).COM_v_mps, sim.sys(sysNum).omega_xyz, sim.sys(sysNum).alpha_rad, 1 );




% Place object params into final structure
for objNum = obj_ind
    
    masses(objNum).name = sim.object(objNum).name;
    masses(objNum).mass = sim.object(objNum).m_kg;
    masses(objNum).radii = 1e-10;
    masses(objNum).x0 = x(objNum);
    masses(objNum).y0 = y(objNum);
    masses(objNum).z0 = z(objNum);
    masses(objNum).vx0 = vx(objNum);
    masses(objNum).vy0 = vy(objNum);
    masses(objNum).vz0 = vz(objNum);
    masses(objNum).marker_RGB = obj_RGB{objNum};
    masses(objNum).marker_size = 5;
    masses(objNum).plot_trail = showPathTrails_q;
    masses(objNum).init_orbit_M = sim.object(objNum).m_kg;
    masses(objNum).init_orbit_P = Ps;
    masses(objNum).init_orbit_G = G;
    masses(objNum).init_orbit_ecc = sim.sys(sysNum).ecc;
    masses(objNum).init_orbit_s = sim.sys(sysNum).a_m;
    masses(objNum).init_orbit_h0 = sim.sys(sysNum).COM;
    masses(objNum).init_orbit_hdot = sim.sys(sysNum).COM_v_mps;
    masses(objNum).init_orbit_d = sim.sys(sysNum).omega_xyz;
    masses(objNum).init_orbit_alpha = sim.sys(sysNum).alpha_rad;
    masses(objNum).init_orbit_theta0 = sim.sys(sysNum).theta_rad;
    masses(objNum).init_orbit_thetadot0 = theta_dot;
    masses(objNum).init_orbit_J = J;
    masses(objNum).init_orbit_j = j;
    masses(objNum).init_orbit_E = E;
end


% If massless rings requested, create them.
rings = [];

for objNum = 1:length(sim.object)
    
    if addMasslessParticleRings_q(objNum)
        
        %         if strcmp(objType{objNum}, 'p')
        rings(objNum).xc = x(objNum);
        rings(objNum).yc = y(objNum);
        rings(objNum).zc = z(objNum);
        %         else
        %             rings(objNum).xc = x(objNum);
        %             rings(objNum).yc = y(objNum);
        %             rings(objNum).zc = z(objNum);
        %         end
        rings(objNum).vxc = vx(objNum);
        rings(objNum).vyc = vy(objNum);
        rings(objNum).vzc = vz(objNum);
        rings(objNum).num_rings = 40;
        rings(objNum).arc_separation_AU = 1*pi/30;
        rings(objNum).first_ring_radius_AU = 0.10;
        rings(objNum).ring_radius_diff_AU = 0.5;
        rings(objNum).d = masses(objNum).init_orbit_d;
        rings(objNum).alpha = pi/2;
        rings(objNum).mass_at_centre = m(objNum);
        rings(objNum).marker_RGB = obj_RGB{objNum} * 0.8;
        
    end
end


% Cluster objects not supported. Left empty.
clusters = [];




%% Run the simulation

[hist, d] = nBodySim(masses, rings, clusters, plot_q, showObjNames_q);

1;



%%

tr = 1:length(hist.t);

% t = orbitParams.s1.t.known;
% 
% t = t - t(1);
% 
% th = orbitParams.s1.t.hi.known;
% 
% th = th - th(1);


qf(88,1);

% subplot(10,1,1:8); hold on
plot(days(seconds(hist.t(tr))), -hist.m(1).vx(tr) / 1e3, '-k', 'linewidth', 3)
% plot(th, (orbitParams.s1.RV.hi.fit + orbitParams.s1.RV_off.final) / 1e3, '--r', 'linewidth', 2)
% plot(t, (orbitParams.s1.RV.known + orbitParams.s1.RV_off.final) / 1e3, 'or', 'markerfacecolor', 'r')
title('Primary Star Velocity Seen on Earth')    
grid on
ylabel('v [km/s]')
% xlabel('t [days]')
xlim([0 inf])
legend({'Simulation Orbit', 'Real Orbit Fit', 'Real Observed RV'})
set(gca, 'XTickLabel', []);

% subplot(10,1,9:10); hold on
% err = spline(days(seconds(hist.t(tr))), -hist.m(1).vx(tr), th) - orbitParams.s1.RV.hi.fit + orbitParams.s1.RV_off.final;
% err = err - nanmean(err);
% plot(th, err, '-k', 'linewidth', 2)
% title(cat(2, 'Residual [(sim orbit [3 body]) - (real orbit fit [2 body])]  |  RMS = ', n2s(rms(err), 4), ' m/s'))    
% grid on
% ylabel('\Delta v [m/s]')
% xlabel('t [days]')
% xlim([0 inf])
% % sslegend({'Simulation Orbit', 'Real Orbit Fit', 'Real Observed RV'})



% qf(99,1);
% 
% % subplot(10,1,1:8); hold on
% rvNorm = hist.m(3).vx(tr) / max(hist.m(3).vx(tr)) * max(orbitParams.p1.RV.hi.fit + orbitParams.p1.RV_off.final);
% plot(days(seconds(hist.t(tr))), rvNorm, '-k', 'linewidth', 3)
% % plot(th, orbitParams.p1.RV.hi.fit + orbitParams.p1.RV_off.final, '--b', 'linewidth', 2)
% % plot(t, orbitParams.p1.RV.known + orbitParams.p1.RV_off.final, 'ob', 'markerfacecolor', 'b')
% title('Primary Star (Planetary Induced) Velocity Seen on Earth')    
% grid on
% ylabel('v [m/s]')
% set(gca, 'XTickLabel', []);
% xlim([0 inf])
% sslegend({'Simulation Planet Orbit (NORMALIZED)', 'Real Orbit Planet Fit', 'Real Observed Planet RV Residual'})
% 
% subplot(10,1,9:10); hold on
% err = spline(days(seconds(hist.t(tr))), rvNorm, th) - orbitParams.p1.RV.hi.fit + orbitParams.p1.RV_off.final;
% err = err - nanmean(err);
% plot(th, err, '-k', 'linewidth', 2)
% title(cat(2, 'Residual [(sim orbit [3 body]) - (real orbit fit [2 body])]  |  RMS = ', n2s(rms(err), 4), ' m/s'))       
% grid on
% ylabel('\Delta v [m/s]')
% xlabel('t [days]')
% xlim([0 inf])
% sslegend({'Simulation Planet Orbit (NORMALIZED)', 'Real Orbit Planet Fit', 'Real Observed Planet RV Residual'})

1;


%%


p2_markerStyle = '-';

tr = 1:length(hist.t);

objNums = 1:3;

qf(2,1)

d.spt = 2;

t = days(seconds(hist.t(tr)));

subplot(d.spt, 1, 1); hold on
title('Distance from System''s Center of Mass')
grid on
ylabel('r_{CM} [AU]')
xlim([0 inf])
subplot(d.spt, 1, 1); hold on
for objNum = objNums
    pr_h(objNum) = plot(t, hist.m(objNum).r(tr) / m_AU, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
end
legend({d.masses(:).name}, 'location', 'northwest', 'AutoUpdate','off')

subplot(d.spt, 1, 2); hold on
if d.earthVplot
title('Velocity Seen on Earth')    
else
title('Velocity')
end
grid on
ylabel('v [km/s]')
xlabel('time [Earth days]')
xlim([0 inf])
for objNum = objNums
    
    if d.earthVplot
        pv_h(objNum) = plot(t, -hist.m(objNum).vx(tr) / 1e3, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    else
        pv_h(objNum) = plot(t, -hist.m(objNum).v(tr) / 1e3, p2_markerStyle, 'markeredgecolor', d.masses(objNum).marker_RGB, 'color', d.masses(objNum).marker_RGB);
    end
end



%% Save sim data

save(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'simulation', 'MISC', 'NA', 3, 1, 1), 'hist', 'd', 'sim')


