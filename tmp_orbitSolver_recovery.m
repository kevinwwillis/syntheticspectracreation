function tmp_orbitSolver_recovery(pipeline_dir, template_plateName, plateName, beamNum, nameAppend)

% clearvars


%%

s = dslash;

load(dirFinder(pipeline_dir, template_plateName, beamNum, [], 'orbitParams', 'mat', [], 'solved_two2bdyMeth', 'MISC', 'NA', 3, 1, 1), 'orbitParams')

orbitParams_a = orbitParams; clear orbitParams

load(dirFinder(pipeline_dir, plateName, beamNum, [], 'RV', 'mat', [], '2D_relative', 'rv', ['mps', nameAppend], 3, 1, 1),'rv_mps')

% load(strcat(pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, plateName, s, plateName, '_synthParams.mat'), 'synthParams');

load(dirFinder(pipeline_dir, plateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0), 'MJDobs')

%%

s = dslash;

orbitParams_b.plateName = plateName;

% templatePlateName = 'FIELD1572';
% 
% templateBeamNum = 87;
% 
% nameAppend = '_XX_CR';

orbitParams_b.beamNum = 101;

expectDataTohaveFullPeriod_q = 0;

objectStructName = 's1';

%%

orbitParams_b.(objectStructName).P.known = nan;
orbitParams_b.(objectStructName).K.known = nan;
orbitParams_b.(objectStructName).ecc.known = nan;
% orbitParams.(objectStructName).ecc.known = 0.68;
orbitParams_b.(objectStructName).w.known = nan;

orbitParams_b.(objectStructName).solverOptions.parP_q = 0;
orbitParams_b.(objectStructName).solverOptions.MaxFevals = 5000;
orbitParams_b.(objectStructName).solverOptions.MaxIter = 5000;
orbitParams_b.(objectStructName).solverOptions.TolFun = 1e-20;
orbitParams_b.(objectStructName).solverOptions.TolX = 1e-20;
orbitParams_b.(objectStructName).solverOptions.numRandPoints = 50;
orbitParams_b.(objectStructName).solverOptions.globalSolver_q = 1;

graph_q = 0;
optimizationMatlabGraph_q = 0;


orbitParams_b.p1.P.known = nan;
orbitParams_b.p1.P.known = 394.66;
orbitParams_b.p1.K.known = nan;
orbitParams_b.p1.ecc.known = nan;
orbitParams_b.p1.ecc.known = 0.15;
orbitParams_b.p1.w.known = nan;

orbitParams_b.p1.solverOptions.parP_q = 0;
orbitParams_b.p1.solverOptions.MaxFevals = 5000;
orbitParams_b.p1.solverOptions.MaxIter = 5000;
orbitParams_b.p1.solverOptions.TolFun = 1e-20;
orbitParams_b.p1.solverOptions.TolX = 1e-20;
orbitParams_b.p1.solverOptions.numRandPoints = 20;
orbitParams_b.p1.solverOptions.globalSolver_q = 1;


%%

% if strcmp(s, '/')
%     neils_dir = '/astro/data/marvels/nthomas819Working/pipeline/May15';
% else
%     neils_dir = 'R:\astro\data\marvels\nthomas819Working\pipeline\May15';
% end

% load(dirFinder(pipeline_dir, orbitParams_b.plateName, orbitParams_b.beamNum, [], 'RV', 'mat', [], '2D_relative', 'rv', ['mps', nameAppend], 3, 1, 1),'rv_mps')


% if 0 == exist(dirFinder(pipeline_dir, templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0), 'file')
%     
%     load(strcat(neils_dir, s, templatePlateName, s, 'results', s, 'MJDobs.mat'), 'MJDobs')
% 
%     save(dirFinder(pipeline_dir, templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 1),'MJDobs')
% else
%     load(dirFinder(pipeline_dir, templatePlateName, [], [], 'dates', 'mat', [], [], 'obsDate', 'JD', 3, 1, 0),'MJDobs')
% end

% load(strcat(pipeline_dir, s, 'MARVELS_data', s, 'synthPlateInfo', s, plateName, s, plateName, '_synthParams.mat'), 'synthParams');


%%

orbitParams_b.(objectStructName).t.known = MJDobs';


orbitParams_b.(objectStructName).t.hi.known = MJDobs(1) :0.01: MJDobs(end);


%%

% orbitParams.(objectStructName).RV.known = double(V2(orbitParams.beamNum, :));

% orbitParams.(objectStructName).RV.known = mean(double(V2(orbitParams.beamNum + (0:1), :)), 1);

orbitParams_b.(objectStructName).RV.known = rv_mps;

%%

    
    
% Time of pasterion    

orbitParams_b.(objectStructName).tp.sv = 1;
orbitParams_b.(objectStructName).tp.lb = 0;
orbitParams_b.(objectStructName).tp.ub = inf;

% RV max

orbitParams_b.(objectStructName).K.sv = max(abs(orbitParams_b.(objectStructName).RV.known));
orbitParams_b.(objectStructName).K.lb = 0.01;
orbitParams_b.(objectStructName).K.ub = 100000;

% Argument of pasterion

orbitParams_b.(objectStructName).w.sv = 0.001;
orbitParams_b.(objectStructName).w.lb = 0;
orbitParams_b.(objectStructName).w.ub = 10000*2*pi;

% Eccentricity

orbitParams_b.(objectStructName).ecc.sv = 0.001;
orbitParams_b.(objectStructName).ecc.lb = 0;
orbitParams_b.(objectStructName).ecc.ub = 1;
% orbitParams.(objectStructName).ecc.lb = 0;
% orbitParams.(objectStructName).ecc.ub = 0.2;


% RV Offset

orbitParams_b.(objectStructName).RV_off.sv = 0;
orbitParams_b.(objectStructName).RV_off.lb = min(orbitParams_b.(objectStructName).RV.known);
orbitParams_b.(objectStructName).RV_off.ub = max(orbitParams_b.(objectStructName).RV.known);



% Period

orbitParams_b.(objectStructName).P.sv = 42;

% orbitParams.(objectStructName).P.lb = 0.01;
% orbitParams.(objectStructName).P.ub = 2000;

orbitParams_b.(objectStructName).P.lb = 41;

if expectDataTohaveFullPeriod_q
    orbitParams_b.(objectStructName).P.ub = ( MJDobs(end) - MJDobs(1) ) / 2;
else
    orbitParams_b.(objectStructName).P.ub = 43;
end



%% Get period of the primary star orbit


P_expected = orbitParams_a.s1.P.known;

Vobs=orbitParams_b.s1.RV.known;
Mjd=MJDobs';
ind=find(isnan(Vobs));
Vobs(ind)=[];
Mjd(ind)=[];

%Analyze the full signal
[f,powf,fapf]=lomb(Mjd',Vobs',12,40);
perf=1./f;

% bi = find(powf==max(powf(perf > (P_expected - 50) & perf < (P_expected + 50))) );
% 
% P=perf(bi);
% 
% FAP=fapf(bi);
% POW=powf(bi)';
% P=P(1);
% FAP=FAP(1);
% POW=POW(1);



perfh = min(perf) :0.01: max(perf);

ppSpline = spline(perf,powf);

[maxima, ~] = splineMaximaMinima(ppSpline);

bi = indClosestVal(maxima, P_expected);

powfh = ppval(ppSpline, perfh);

% [POW, ~] = max(powfh);

% bi = find(powfh==max(powfh(perfh > (P_expected - 100) & perfh < (P_expected + 100))) );

P = maxima(bi);

POW = ppval(ppSpline, maxima(bi));


qf(1231, 1);
plot(perfh,powfh, '-k')
plot(P,POW, 'or')
text(P + 5,POW, cat(2, n2s(P), ' days'))
% plot(powf,FAP * 100, '-.k')
% plot(powf,fapf * 0.01 * 100, '--k')
% plot(powf,fapf * 0.1 * 100, ':k')
xlim([0, 100])
grid on
title('Periodigram of Primary Star RVs')
ylabel('Power')
xlabel('Period [days]')
ylim([0 inf])

orbitParams_b.(objectStructName).P.known = P;

%%



for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams_b.(objectStructName).(varName{1}), 'known')
        
        if isfinite(orbitParams_b.(objectStructName).(varName{1}).known)
            
            orbitParams_b.(objectStructName).(varName{1}).sv = orbitParams_b.(objectStructName).(varName{1}).known;
        end
    end
end



orbitParams_b.(objectStructName) = solveOrbitParams(orbitParams_b.(objectStructName), graph_q, optimizationMatlabGraph_q);



for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams_b.(objectStructName).(varName{1}), 'known')
        
        if ~isfinite(orbitParams_b.(objectStructName).(varName{1}).known)
            
            orbitParams_b.(objectStructName).(varName{1}).final = orbitParams_b.(objectStructName).(varName{1}).solved;
            
        else
            
            orbitParams_b.(objectStructName).(varName{1}).final = orbitParams_b.(objectStructName).(varName{1}).known;
        end
    
    else
        
        orbitParams_b.(objectStructName).(varName{1}).final = orbitParams_b.(objectStructName).(varName{1}).solved;
        
    end
end



%     tp, K, w,  e,  off

qf(5,1); 
subplot(2,1,1); 
plot(orbitParams_b.(objectStructName).t.hi.known, orbitParams_b.(objectStructName).RV.hi.fit, '-k', orbitParams_b.(objectStructName).t.known, orbitParams_b.(objectStructName).RV.fit, 'og', orbitParams_b.(objectStructName).t.known, orbitParams_b.(objectStructName).RV.known, '+k')
grid on

subplot(2,1,2);
plot(orbitParams_b.(objectStructName).t.known, orbitParams_b.(objectStructName).RV.known - orbitParams_b.(objectStructName).RV.fit, 'ok')
grid on


gi = isfinite(orbitParams_b.(objectStructName).RV.known);

orbitParams_b.(objectStructName).fitStats.Rsquared = Rsqr_imgResid(orbitParams_b.(objectStructName).RV.known(gi), orbitParams_b.(objectStructName).RV.fit(gi));

% X2 = chiSqr(orbitParams.(objectStructName).RV.known(gi), orbitParams.(objectStructName).RV.fit(gi), 5);

% sum( abs( abs(orbitParams.(objectStructName).RV.fit(gi)) - abs(orbitParams.(objectStructName).RV.known(gi)) ) / sum(abs(orbitParams.(objectStructName).RV.fit(gi))) )

[~, ~, c] = chi2gof( abs(orbitParams_b.(objectStructName).RV.fit(gi) - orbitParams_b.(objectStructName).RV.known(gi)) );

orbitParams_b.(objectStructName).fitStats.reducedChiSquared = c.chi2stat / 5;

orbitParams_b.(objectStructName).fitStats.errorRMS = nanrms(orbitParams_b.(objectStructName).RV.fit(gi) - orbitParams_b.(objectStructName).RV.known(gi));

disp(' ')
disp(cat(2, 'tp = ', n2s(orbitParams_b.(objectStructName).tp.final)))
disp(' ')
disp(cat(2, 'K = ', n2s(orbitParams_b.(objectStructName).K.final)))
disp(' ')
disp(cat(2, 'w = ', n2s(orbitParams_b.(objectStructName).w.final)))
disp(' ')
disp(cat(2, 'ecc = ', n2s(orbitParams_b.(objectStructName).ecc.final)))
disp(' ')
disp(cat(2, 'off = ', n2s(orbitParams_b.(objectStructName).RV_off.final)))
disp(' ')
disp(cat(2, 'P = ', n2s(orbitParams_b.(objectStructName).P.final)))
disp(' ')
disp(cat(2, 'RMS = ', n2s(orbitParams_b.(objectStructName).fitStats.errorRMS)))
disp(' ')
disp(cat(2, 'Rsquared = ', n2s(orbitParams_b.(objectStructName).fitStats.Rsquared)))
disp(' ')
disp(cat(2, 'Reduced Chi Squared = ', n2s(orbitParams_b.(objectStructName).fitStats.reducedChiSquared)))
disp(' ')








%% Planet 1

objectStructName = 'p1';

orbitParams_b.(objectStructName).RV.known = orbitParams_b.s1.RV.known - orbitParams_b.s1.RV.fit;

orbitParams_b.(objectStructName).t = orbitParams_b.s1.t;


%% Get period of the primary star orbit (planet only)

P_expected = orbitParams_a.p1.P.known;

Vobs=orbitParams_b.p1.RV.known;
Mjd=MJDobs';
ind=find(isnan(Vobs));
Vobs(ind)=[];
Mjd(ind)=[];

%Analyze the full signal
[f,powf,fapf]=lomb(Mjd',Vobs',12,40);

perf=1./f;

perfh = min(perf) :0.01: max(perf);

ppSpline = spline(perf,powf);

[maxima, ~] = splineMaximaMinima(ppSpline);

bi = indClosestVal(maxima, P_expected);

powfh = ppval(ppSpline, perfh);

% [POW, ~] = max(powfh);

% bi = find(powfh==max(powfh(perfh > (P_expected - 100) & perfh < (P_expected + 100))) );

P = maxima(bi);

POW = ppval(ppSpline, maxima(bi));

qf(1231444, 1);
% plot(perf,powf, '-k')
plot(perfh,powfh, '-k')
plot(P,POW, 'or')
text(P + 5,POW, cat(2, n2s(P, 4), ' days'))
% plot(powf,FAP * 100, '-.k')
% plot(powf,fapf * 0.01 * 100, '--k')
% plot(powf,fapf * 0.1 * 100, ':k')
xlim([0, 1000])
grid on
title('Periodigram of Planet RVs')
ylabel('Power')
xlabel('Period [days]')
ylim([0 inf])




orbitParams_b.p1.P.known = P;

%%
    
% Time of pasterion    

orbitParams_b.(objectStructName).tp.sv = 1;
orbitParams_b.(objectStructName).tp.lb = 0;
orbitParams_b.(objectStructName).tp.ub = inf;

% RV max

orbitParams_b.(objectStructName).K.sv = max(abs(orbitParams_b.(objectStructName).RV.known));
orbitParams_b.(objectStructName).K.lb = 0.01;
orbitParams_b.(objectStructName).K.ub = max(abs(orbitParams_b.(objectStructName).RV.known)) + 3 * nanstd(abs(orbitParams_b.(objectStructName).RV.known));

% Argument of pasterion

orbitParams_b.(objectStructName).w.sv = 0.001;
orbitParams_b.(objectStructName).w.lb = 0;
orbitParams_b.(objectStructName).w.ub = 2*pi * 1000;

% Eccentricity

orbitParams_b.(objectStructName).ecc.sv = 0.0;
orbitParams_b.(objectStructName).ecc.lb = 0;
orbitParams_b.(objectStructName).ecc.ub = 1;
% orbitParams.(objectStructName).ecc.lb = 0;
% orbitParams.(objectStructName).ecc.ub = 0.2;


% RV Offset

orbitParams_b.(objectStructName).RV_off.sv = 0;
orbitParams_b.(objectStructName).RV_off.lb = min(orbitParams_b.(objectStructName).RV.known);
orbitParams_b.(objectStructName).RV_off.ub = max(orbitParams_b.(objectStructName).RV.known);



% Period

orbitParams_b.(objectStructName).P.sv = 394.66;

% orbitParams.(objectStructName).P.lb = 0.01;
% orbitParams.(objectStructName).P.ub = 2000;

orbitParams_b.(objectStructName).P.lb = 0.01;

if expectDataTohaveFullPeriod_q
    orbitParams_b.(objectStructName).P.ub = ( orbitParams_b.(objectStructName).t.known(end) - orbitParams_b.(objectStructName).t.known(1) ) / 2;
else
    orbitParams_b.(objectStructName).P.ub = 10000;
end





for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams_b.(objectStructName).(varName{1}), 'known')
        
        if isfinite(orbitParams_b.(objectStructName).(varName{1}).known)
            
            orbitParams_b.(objectStructName).(varName{1}).sv = orbitParams_b.(objectStructName).(varName{1}).known;
        end
    end
end


orbitParams_b.(objectStructName) = solveOrbitParams(orbitParams_b.(objectStructName), graph_q, optimizationMatlabGraph_q);



for varName = [{'tp'}, {'K'}, {'w'}, {'ecc'}, {'RV_off'}, {'P'}]
    
    if isfield(orbitParams_b.(objectStructName).(varName{1}), 'known')
        
        if ~isfinite(orbitParams_b.(objectStructName).(varName{1}).known)
            
            orbitParams_b.(objectStructName).(varName{1}).final = orbitParams_b.(objectStructName).(varName{1}).solved;
            
        else
            
            orbitParams_b.(objectStructName).(varName{1}).final = orbitParams_b.(objectStructName).(varName{1}).known;
        end
    
    else
        
        orbitParams_b.(objectStructName).(varName{1}).final = orbitParams_b.(objectStructName).(varName{1}).solved;
        
    end
end



%     tp, K, w,  e,  off

qf(3,1); 
subplot(4,1,1); hold on
title('Simulation Recovery: Binary Orbit Fit')
xlabel('JD')
ylabel('RV [m/s]')
plot(orbitParams_b.s1.t.hi.known, orbitParams_b.s1.RV.hi.fit, '-k', orbitParams_b.s1.t.known, orbitParams_b.s1.RV.fit, 'og', orbitParams_b.s1.t.known, orbitParams_b.s1.RV.known, '+k')
grid on
legend({'Binary Orbit', 'Binary RV Fit', 'Binary Actual RV'},'location', 'best')

subplot(4,1,2); hold on
title('Binary Orbit Fit Error')
xlabel('JD')
ylabel('RV Error [m/s]')
plot(orbitParams_b.(objectStructName).t.known, orbitParams_b.s1.RV.known - orbitParams_b.s1.RV.fit, 'ok')
grid on

subplot(4,1,3); hold on
title('Simulation Recovery: Planet Orbit Fit')
xlabel('JD')
ylabel('RV [m/s]')
plot(orbitParams_b.(objectStructName).t.hi.known, orbitParams_b.(objectStructName).RV.hi.fit, '-k', orbitParams_b.(objectStructName).t.known, orbitParams_b.(objectStructName).RV.fit, 'og', orbitParams_b.p1.t.known, orbitParams_b.p1.RV.known, '+k')
grid on
legend({'Planet Orbit', 'Planet RV Fit', 'Planet RV'},'location', 'best')

subplot(4,1,4); hold on
title('Planet Orbit Fit Error')
xlabel('JD')
ylabel('RV Error [m/s]')
plot(orbitParams_b.(objectStructName).t.known, orbitParams_b.(objectStructName).RV.known - orbitParams_b.(objectStructName).RV.fit, 'ok')
grid on




gi = isfinite(orbitParams_b.s1.RV.known) & isfinite(orbitParams_b.p1.RV.known);

aa = orbitParams_b.s1.RV.known(gi) - orbitParams_b.s1.RV.fit(gi);
aa = aa - nanmean(aa);

bb = orbitParams_b.p1.RV.known(gi) - orbitParams_b.p1.RV.fit(gi);
bb = bb - nanmean(bb);

R2 = Rsqr_imgResid(aa, bb);

qf(4,1)
% subplot(2,1,1); hold on
title(cat(2, 'Planet Fit Significance | Binary vs Planet Fit Error | Significance = ', n2s(R2), ' (0 ~= very significant, 1 ~= no significance)'))
xlabel('JD')
ylabel('RV Error [m/s]')
plot(orbitParams_b.(objectStructName).t.known, orbitParams_b.s1.RV.known - orbitParams_b.s1.RV.fit, 'ob')
plot(orbitParams_b.(objectStructName).t.known, orbitParams_b.(objectStructName).RV.known - orbitParams_b.(objectStructName).RV.fit, '^r')
legend({'Binary RV Fit Error', 'Planet RV Fit Error'},'location', 'best')
grid on

% subplot(2,1,2); hold on
% title('Binary / Planet Fit Error Residual')
% xlabel('JD')
% ylabel('RV [m/s]')
% plot(orbitParams.s1.t.known, (orbitParams.s1.RV.known - orbitParams.s1.RV.fit) - (orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit), '+k')
% plot(orbitParams.(objectStructName).t.hi.known, orbitParams.(objectStructName).RV.hi.fit, '-k')
% grid on





gi = isfinite(orbitParams_b.(objectStructName).RV.known);

orbitParams_b.(objectStructName).fitStats.Rsquared = Rsqr_imgResid(orbitParams_b.(objectStructName).RV.known(gi), orbitParams_b.(objectStructName).RV.fit(gi));

% X2 = chiSqr(orbitParams.(objectStructName).RV.known(gi), orbitParams.(objectStructName).RV.fit(gi), 5);

% sum( abs( abs(orbitParams.(objectStructName).RV.fit(gi)) - abs(orbitParams.(objectStructName).RV.known(gi)) ) / sum(abs(orbitParams.(objectStructName).RV.fit(gi))) )

[~, ~, c] = chi2gof( abs(orbitParams_b.(objectStructName).RV.fit(gi) - orbitParams_b.(objectStructName).RV.known(gi)) );

orbitParams_b.(objectStructName).fitStats.reducedChiSquared = c.chi2stat / 5;

orbitParams_b.(objectStructName).fitStats.errorRMS = nanrms(orbitParams_b.(objectStructName).RV.fit(gi) - orbitParams_b.(objectStructName).RV.known(gi));

disp(' ')
disp(cat(2, 'tp = ', n2s(orbitParams_b.(objectStructName).tp.final)))
disp(' ')
disp(cat(2, 'K = ', n2s(orbitParams_b.(objectStructName).K.final)))
disp(' ')
disp(cat(2, 'w = ', n2s(orbitParams_b.(objectStructName).w.final)))
disp(' ')
disp(cat(2, 'ecc = ', n2s(orbitParams_b.(objectStructName).ecc.final)))
disp(' ')
disp(cat(2, 'off = ', n2s(orbitParams_b.(objectStructName).RV_off.final)))
disp(' ')
disp(cat(2, 'P = ', n2s(orbitParams_b.(objectStructName).P.final)))
disp(' ')
disp(cat(2, 'RMS = ', n2s(orbitParams_b.(objectStructName).fitStats.errorRMS)))
disp(' ')
disp(cat(2, 'Rsquared = ', n2s(orbitParams_b.(objectStructName).fitStats.Rsquared)))
disp(' ')
disp(cat(2, 'Reduced Chi Squared = ', n2s(orbitParams_b.(objectStructName).fitStats.reducedChiSquared)))
disp(' ')






% rv_p_mps = orbitParams.(objectStructName).RV.known - orbitParams.(objectStructName).RV.fit;
% 
% save(['E:\Research', s, 'rv_p_mps.mat'], 'rv_p_mps')


% save(['E:\Research', s, 'binary_orbitParams.mat'], 'orbitParams')







% Notes:
% 1) Energy var (E) needs to be fixed. It is not used in the simulation,
% but may be used later. Can use to measure/correct energy drift
% 
% 

%% Constants

AU = 149597870700;
G0 = 6.67408e-11;
M_sun = 1.9885e30;
M_jup = 1.89813e27;
Yr = 365*24*3600;
s_day = 24 * 60 * 60;

%Dimensionless number which controls the dynamics, and results from the
%scaling of mass, distance and time parameters to make them dimensionless.
G = G0*Yr^2*M_sun/(AU^3);

%% Solve other params

% Stars
P1_s = orbitParams_b.s1.P.final * s_day;    
K1_mps = orbitParams_b.s1.K.final;
ecc1 = orbitParams_b.s1.ecc.final;
[m1_kg, orbitParams_b.s1.m.final] = deal(1 * M_sun);

m2_kg = solveMass_type1(m1_kg, P1_s, K1_mps, ecc1);

% m2_kg = 0 : 1e25: M_sun;
% 
% qf(90,1); plot(m2_kg, abs(m2_kg.^3 ./ (m1_kg + m2_kg).^2 - P1_s * K1_mps^3 / (2 * pi * G0) * ( 1 - ecc1^2 )^(3/2)), '.-k')





% m2_kg / M_sun

orbitParams_b.s2.m.final = m2_kg;

a_m = ( P1_s^2 * G0 * (m1_kg + m2_kg) / (4 * pi^2) )^(1 / 3);

a_AU = a_m / AU;

a1_m = m2_kg / (m1_kg + m2_kg) * a_m;

a1_AU = a1_m / AU;

orbitParams_b.s1.a1.final = a1_m;

a2_m = m1_kg / (m1_kg + m2_kg) * a_m;

a2_AU = a2_m / AU;

orbitParams_b.s2.a2.final = a2_m;

% Planet
orbitParams_b.p1.m.final = solveMass_type1(m1_kg + m2_kg, orbitParams_b.p1.P.final * s_day, orbitParams_b.p1.K.final, orbitParams_b.p1.ecc.final);

% orbitParams.p1.m.final / M_sun

ap_m = ( (orbitParams_b.p1.P.final * s_day)^2 * G0 * (m1_kg + m2_kg + orbitParams_b.p1.m.final) / (4 * pi^2) )^(1 / 3);

ap_AU = ap_m / AU;

ap1_m = (m1_kg + m2_kg) / (m1_kg + m2_kg + orbitParams_b.p1.m.final) * ap_m;

ap1_AU = ap1_m / AU;

orbitParams_b.p1.ap1.final = ap1_m;



orbitParams = orbitParams_b;

% save(['E:\Research\syntheticSpectraCreation', s, 'binary_orbitParams_RECOVERY_v1.mat'], 'orbitParams')

save(dirFinder(pipeline_dir, plateName, beamNum, [], 'orbitParams', 'mat', [], 'solved_two2bdyMeth_recovery', 'MISC', 'NA', 3, 1, 1), 'orbitParams')





