% function runFullBinaryProcess(pipeline_dir, plateName, beamNum, nameAppend)


plateName = 'synth_simBinary_v3_linDate35';

template_plateName = 'FIELD1572';

template_beamNum = 87;

pipeline_dir = pwd;




%% get RVs from the real data

% RUNALL

%% get orbit params

% tmp_orbitSolver(pipeline_dir, template_plateName, template_beamNum, nameAppend)

%% simulate the system

tmp_nBdySim_call_v4(pipeline_dir, template_plateName, template_beamNum)


%% Create synthetic spectra

a1_synthSpectra_caller

%% get RVs from the simulated data

% RUNALL

%% recover the params of the simulated system

tmp_orbitSolver_recovery(pipeline_dir, template_plateName, plateName, beamNum, nameAppend)


%% simulate recoverd system

tmp_nBdySim_call_v4_recovery(pipeline_dir, template_plateName, plateName, beamNum, nameAppend)





%% Plots

%
plotRealAndSimPlanetRVrecovery(pipeline_dir, plateName, beamNum, nameAppend)

%
plotSavedSimData_orbits(pipeline_dir, template_plateName, template_beamNum, nameAppend, 0)
plotSavedSimData_orbits(pipeline_dir, plateName, beamNum, nameAppend, 1)

%
plotSimAndSimPlanetRVrecovery(pipeline_dir, plateName, beamNum, nameAppend)


